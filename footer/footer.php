<!-- Section Footer Classic-->
<footer id="colophon" class="section bg-default site-footer footer-layout1">
  <div class="bg-decoration">
     <div class="bg-decoration-item"></div>
     <div class="bg-decoration-item"></div>
     <div class="bg-decoration-item"></div>
     <div class="bg-decoration-item"></div>
  </div>
  <div class="top-footer bg-image bg-overlay">
    <div class="container">
      <div class="row">
        <div class="ct-footer-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <section id="text-2" class="widget widget_text wow fadeInUpSmall">
            <h4 class="footer-widget-title">About Iceel</h4>
            <div class="textwidget">
              <p>It uses a dictionary of over 200 Latin words combined with a handful of model sentence structures generate which looks reasonable generated is therefore allow.</p>
              <p>It uses a dictionary of over 200 Latin words
                <br>generated is therefore allow.</p>
            </div>
          </section>
        </div>
        <div class="ct-footer-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <section id="nav_menu-2" class="widget widget_nav_menu wow fadeInUpSmall">
            <h4 class="footer-widget-title">Quick Links</h4>
            <div class="menu-menu-company-container">
              <ul id="menu-menu-company" class="menu">
                <li id="menu-item-25" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25"><a href="#" class="no-one-page">About Iceel</a>
                </li>
                <li id="menu-item-26" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-26"><a href="#" class="no-one-page">Our Services</a>
                </li>
                <li id="menu-item-28" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28"><a href="#" class="no-one-page">Portfolio</a>
                </li>
                <li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27"><a href="#" class="no-one-page">Terms of service</a>
                </li>
                <li id="menu-item-29" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-29"><a href="#" class="no-one-page">Privacy policy</a>
                </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="ct-footer-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <section id="nav_menu-3" class="widget widget_nav_menu wow fadeInUpSmall">
            <h4 class="footer-widget-title">What we Do?</h4>
            <div class="menu-menu-product-container">
              <ul id="menu-menu-product" class="menu">
                <li id="menu-item-30" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-30"><a href="#" class="no-one-page">Web Designing & Development</a>
                </li>
                <li id="menu-item-31" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-31"><a href="#" class="no-one-page">Digital Marketing Solutions</a>
                </li>
                <li id="menu-item-32" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-32"><a href="#" class="no-one-page">E-Commerce Services</a>
                </li>
                <li id="menu-item-33" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-33"><a href="#" class="no-one-page">App Development</a>
                </li>
                <li id="menu-item-34" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34"><a href="#" class="no-one-page">Software Development</a>
                </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="ct-footer-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <section id="nav_menu-4" class="widget widget_nav_menu wow fadeInUpSmall">
            <h4 class="footer-widget-title">Contact Us</h4>
            <div class="menu-menu-help-center-container">
              <ul id="menu-menu-help-center" class="menu">
                <li id="menu-item-35" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35"><a href="#" class="no-one-page">Help centre</a>
                </li>
                <li id="menu-item-36" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-36"><a href="#" class="no-one-page">Email Us</a>
                </li>
                <li id="menu-item-37" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-37"><a href="#" class="no-one-page">Customers</a>
                </li>
                <li id="menu-item-38" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-38"><a href="#" class="no-one-page">Message Us</a>
                </li>
                <li id="menu-item-39" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-39"><a href="#" class="no-one-page">Blog</a>
                </li>
              </ul>
            </div>
          </section>
        </div>
      </div>
      <div class="row worldwideText">
        <div class="ct-footer-item col-md-12 wow fadeInUpSmall" style="text-align: center;">
          <strong>Worldwide Clients:</strong> United States(USA), Canada, United Kingdom(UK), Australia, United Arab Emirates, Denmark, South Africa, New Zealand, Poland, Russia, Switzerland, Jordan, Kuwait, Belgium, Spain, Sweden, Italy, Singapor, Germany, France, Mexico, Turkey, Portugal, Netherlands, Bulgaria, Ecuador.
        </div>
      </div>
    </div>
  </div>
  <div class="bottom-footer">
    <div class="container">
      <div class="row" style="align-items: center;">
        <div class="bottom-copyright">2019 © All rights reserved by <a target="_blank" href="http://www.iceel.net/">iceel.net</a>
        </div>
        <div class="bottom-social"> 
          <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a>
          <a href="#" target="_blank"><i class="fab fa-twitter"></i></a>
          <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
          <a href="#" target="_blank"><i class="fab fa-pinterest-p"></i></a>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>
<!-- <div class="snackbars" id="form-output-global"></div> -->
<script src="js/site.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/scripts.js"></script>
<script type="text/javascript">
</script>
</body>
</html>