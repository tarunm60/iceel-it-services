<div class="section section-md bg-default text-center" id="our-services">
    <div class="container">
       <h1 class="font-size-medium">Web Designing, Development and Digital Marketing Services</h1>
       <div class="row row-2 row-narrow-xxs justify-content-center">
          <div class="row">
            <div class="col-lg-6 wow fadeInLeft">
              <div class="services primary ">
                <div class="services-info">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left mt-4 wow fadeInUpSmall">Web Designing & Development</h3>
                  <div class="clearfix"></div>
                  <p class="mt-3 mb-0">We offer webdesigning & development in Core PHP, PHP frameworks, CMS, html, Javascript, Bootstrap.</p>
                </div>
                <a href="service.html" class="button-info">Read More <span class="float-right"><i class="mdi mdi-dots-horizontal"></i></span></a>
              </div>
            </div>
            <div class="col-lg-6 wow fadeInRight">
              <div class="services primary ">
                <div class="services-info">
                  <div class="service-shap primary"><i class="fas fa-bullhorn"></i></div>
                  <h3 class="float-left mt-4">Digital Marketing Solutions</h3>
                  <div class="clearfix"></div>
                  <p class="mt-3 mb-0">We offer SEO, ASO, SEM / PPC, SMO / SMM, E-mail, SMS, WhatsApp Marketing etc services.</p>
                </div>
                <a href="service.html" class="button-info">Read More <span class="float-right"><i class="mdi mdi-dots-horizontal"></i></span></a>
              </div>
            </div>
            <div class="col-lg-6 wow fadeInLeft">
              <div class="services primary mb-lg-0 ">
                <div class="services-info">
                  <div class="service-shap primary"><i class="fas fa-cart-plus"></i></div>
                  <h3 class="float-left mt-4">E-Commerce Services</h3>
                  <div class="clearfix"></div>
                  <p class="mt-3 mb-0">We offer e-commerce development services along with promoting your products on e-commerces platforms like amazon, ebay, walmart etc</p>
                </div>
                <a href="service.html" class="button-info">Read More <span class="float-right"><i class="mdi mdi-dots-horizontal"></i></span></a>
              </div>
            </div>
            <div class="col-lg-6 wow fadeInRight">
              <div class="services primary mb-lg-0">
                <div class="services-info">
                  <div class="service-shap primary"><i class="fas fa-mobile-alt"></i></div>
                  <h3 class="float-left mt-4">App Development</h3>
                  <div class="clearfix"></div>
                  <p class="mt-3 mb-0">We have team to develop mobile app for OS like Android, IOS, Windows, React native.</p>
                </div>
                <a href="service.html" class="button-info">Read More <span class="float-right"><i class="mdi mdi-dots-horizontal"></i></span></a>
              </div>
            </div>
          </div>
       </div>
    </div>
 </div>