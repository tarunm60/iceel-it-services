<section class="iq-choose-info section-md position-relative">
 <div class="container">
  <div class="row flex-row-reverse align-items-center h-100">
     <div class="col-lg-6 align-self-center wow fadeInRight">
        <h2 class="iq-fw-8 mb-4">About Iceel IT Serices</h2>
        <h3>(Web Designing, Development, Digital Marketing Solutions)</h3>
        <p>We are one of the oldest IT Services & Solution Company in India working in IT, Web Development & Designing, Digital Marketing domain prior to 2006. We have well experienced team. Please read our services section to view our all services.</p>
        <div class="mt-3 d-inline-block">
           <a class="slide-button button button-primary button-ujarak float-left" href="about-us.html">
              <div class="first">Explore More </div>
              <div class="second">Explore More </div>
           </a>
        </div>
     </div>
     <div class="col-lg-6 iq-rmt-40">
        <img src="images/icee-it-about-us.png" class="img-fluid wow fadeInLeft" alt="image">
     </div>
  </div>
 </div>
</section>