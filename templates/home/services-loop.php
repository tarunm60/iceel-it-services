<?php
	foreach ($sliderArray as $singleItem) {
?>
	<div class="owl-item">
		<div class="item">
	      <div class="main-service">
	        <div class="service-img">
	          <img alt="<?php echo $singleItem['alt'];?>" src="<?php echo $singleItem['imgeUrl'];?>" class="img-fluid">
	        </div>
	        <div class="service-detail">
	          <div class="text-primary text-bold iq-fw-8" href="blog-details-left-sidebar.html">
	      		<?php
	          		echo $singleItem['cat'];
	          	?>
	          </div>
	          <a href="<?php echo $singleItem['url'];?>">
	          	<h5 class="mt-1 iq-fw-8">
		          	<?php
		          		echo $singleItem['title'];
		          	?>
	          	</h5>
	          </a>
	          <p class="mb-0">
	          	<?php
	          		echo $singleItem['content'];
	          	?>
	          </p>
	          <div class="service-info">
	            <a class="service-info-readmore" href="<?php echo $singleItem['url'];?>"><span class="iq-fw-8 iq-font-14">Read More</span> <i class="fas fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	      </div>
		</div>
	</div>
<?php
	}
?>