<div class="section section-md bg-default text-center">
    <div class="bg-decoration">
       <div class="bg-decoration-item"></div>
       <div class="bg-decoration-item"></div>
       <div class="bg-decoration-item"></div>
       <div class="bg-decoration-item"></div>
    </div>
    <div class="container">
       <h6 class="wow fadeInUpSmall" data-wow-delay=".1s"><span class="head-text">our blog</span></h6>
       <h2 class="wow fadeInUpSmall" data-wow-delay=".1s">News & Articles</h2>
       <div class="row row-30 justify-content-center">
          <div class="col-sm-8 col-md-4 wow fadeInUpSmall" data-wow-delay=".15s">
             <!-- Post-->
             <a class="news" href="single-blog.html">
                <span class="news-img"><img src="images/news-01-370x397.jpg" alt="" width="370" height="397"></span><span class="news-date"><span>10</span><span>Jan</span></span>
                <div class="news-content">
                   <ul class="news-meta">
                      <li><span>By admin</span></li>
                      <li><span>0 Comments</span></li>
                   </ul>
                   <span class="news-title heading-3"><span>Baby Boomer and Empty Nesters on the Move</span></span>
                </div>
             </a>
          </div>
          <div class="col-sm-8 col-md-4 wow fadeInUpSmall" data-wow-delay=".3s">
             <!-- Post-->
             <a class="news" href="single-blog.html">
                <span class="news-img"><img src="images/news-02-370x397.jpg" alt="" width="370" height="397"></span><span class="news-date"><span>09</span><span>Jan</span></span>
                <div class="news-content">
                   <ul class="news-meta">
                      <li><span>By admin</span></li>
                      <li><span>2 Comments</span></li>
                   </ul>
                   <span class="news-title heading-3"><span>Our Home Entertainment has Evolved Significantly</span></span>
                </div>
             </a>
          </div>
          <div class="col-sm-8 col-md-4 wow fadeInUpSmall" data-wow-delay=".45s">
             <!-- Post-->
             <a class="news" href="single-blog.html">
                <span class="news-img"><img src="images/news-03-370x397.jpg" alt="" width="370" height="397"></span><span class="news-date"><span>02</span><span>Jan</span></span>
                <div class="news-content">
                   <ul class="news-meta">
                      <li><span>By admin</span></li>
                      <li><span>3 Comments</span></li>
                   </ul>
                   <span class="news-title heading-3"><span>Making a Choice Between Moving Companies</span></span>
                </div>
             </a>
          </div>
       </div>
    </div>
 </div>