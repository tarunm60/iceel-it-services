<?php 

	$sliderArray = array(
		
		array(

			'title' => 'WordPress Development',
			'cat' => 'PHP Development',
			'alt' => '',
			'url' => 'ordpress-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/wordpress-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'SEO Services',
			'cat' => 'Digital Marketing Services',
			'alt' => '',
			'url' => 'seo-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/php-web-development-services-solution-company.jpg',
		),
		array(

			'title' => 'React.JS Development',
			'cat' => 'JaveScript Development',
			'alt' => '',
			'url' => 'react-js-development-services-solutions-company
.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/react-js-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'Ios App Development',
			'cat' => 'App Development',
			'alt' => '',
			'url' => 'ios-app-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/ios-app-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'Laravel Development',
			'cat' => 'PHP Development',
			'alt' => '',
			'url' => 'laravel-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/laravel-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'SEM - PPC Services',
			'cat' => 'Digital Marketing Services',
			'alt' => '',
			'url' => 'sem-ppc-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'Core PHP Development',
			'cat' => 'PHP Development',
			'alt' => '',
			'url' => 'php-web-development-services-solution-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/php-web-development-services-solution-company.jpg',
		),
		array(

			'title' => 'Vue.js Development',
			'cat' => 'JaveScript Development',
			'alt' => '',
			'url' => 'vue-js-development-services-solutions-company',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'Android App development',
			'cat' => 'App Development',
			'alt' => '',
			'url' => 'android-app-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/android-app-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'CodeIgniter Development',
			'cat' => 'PHP Development',
			'alt' => '',
			'url' => 'codeigniter-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/codeigniter-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'SMM - SMO Services',
			'cat' => 'Digital Marketing Services',
			'alt' => '',
			'url' => 'smm-smo-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'Angular.js Development',
			'cat' => 'JaveScript Development',
			'alt' => '',
			'url' => 'angular-js-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'Web Designing Services',
			'cat' => 'Other It Services',
			'alt' => '',
			'url' => 'web-designing-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'Shopify Development',
			'cat' => 'E-Commerce Development',
			'alt' => '',
			'url' => 'shopify-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/shopify-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'Local Advertising Services',
			'cat' => 'Digital Marketing Services',
			'alt' => '',
			'url' => 'local-advertise-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'JaveScript Development',
			'cat' => 'JS Development',
			'alt' => '',
			'url' => 'javescript-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'WooCommerce Development',
			'cat' => 'E-Commerce Development',
			'alt' => '',
			'url' => 'woocomerce-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/woocomerce-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'Magento Development',
			'cat' => 'E-Commerce Development',
			'alt' => '',
			'url' => 'magento-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/magento-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'App Store Optimization',
			'cat' => 'Digital Marketing Services',
			'alt' => '',
			'url' => 'app-store-optimization-aso-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(

			'title' => 'Node.Js Development',
			'cat' => 'JaveScript Development',
			'alt' => '',
			'url' => 'node-js-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/node-js-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'Prestashop Development',
			'cat' => 'E-Commerce Development',
			'alt' => '',
			'url' => 'prestashop-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/prestashop-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'Electron.js Development',
			'cat' => 'JaveScript Development',
			'alt' => '',
			'url' => 'electron-js-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Database Management',
			'cat' => 'Other It Services',
			'alt' => '',
			'url' => 'database-management-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/database-management-services-solutions-company.jpg',
		),
		array(
			'title' => 'E-Commerce Development',
			'cat' => 'E-Commerce Services',
			'alt' => '',
			'url' => 'ecommerce-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/ecommerce-development-services-solutions-company.jpg',
		),
		array(

			'title' => 'Server & Network Manage',
			'cat' => 'Other It Services',
			'alt' => '',
			'url' => 'server-network-management-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services/server-network-management-services-solutions-company.jpg',
		)
	);
?>
<div id="services-crousel-wrapper" class="section section-md bg-gray-100 bg-decoration-show text-center">
	<div class="bg-decoration">
		<div class="bg-decoration-item"></div>
		<div class="bg-decoration-item"></div>
		<div class="bg-decoration-item"></div>
		<div class="bg-decoration-item"></div>
	</div>
    <div id="services-crousel" class="container-wide">
       <h6 class="wow fadeInUpSmall mb-2" data-wow-delay=".1s"><span class="head-text">At Your Service</span></h6>
       <h2 class="font-size-medium wow fadeInUpSmall">What We Offer</h2>
       <div class="row">
       		<div class="col-lg-12 text-left">
       			<div class="owl-carousel owl-loaded owl-drag" data-loop="true" data-nav="true" data-autoplay-timeout="3000" data-items="4" data-items-laptop="4" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1">
	              	<div class="owl-stage-outer">
	              		<div class="owl-stage">
	              			<?php require 'services-loop.php'; ?>
	                	</div>
	            	</div>
	          	</div>
   			</div>
     	</div>
  </div>
</div>