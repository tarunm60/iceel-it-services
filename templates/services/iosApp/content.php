<section class="about-iceel ptb-100">
	<div class="container">
	  <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInLeft">
          <h2 class="font-size-normal">Why choose Magento?</h2>
          <p>Magento is the best choice for online retailers, stores, e-commerce sites due to its exceptional versatility and advanced features, as it allows you to customize the layout and thus, extend your business to create fentastic shopping experience for your customers.</p>
          <p>The Magento Community Edition (CE) is completely free. You can easily install or use any version of Magento CE you want. If you are a Magento expert, you can accelerate the performance of your Magento online store and also promote your ecommerce website with new features and functionalities by developing or installing the Magento modules.</p>
          <p>You can find a huge library of modules at the Magento Connect/Marketplace. These modules will help up your game as a Magento website developer while harnessing your progress at the same time.</p>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInRight">
          <img src="<?php echo $siteUrl;?>/images/wordpress-development-services-solutions-company.png" alt="image">
        </div>
      </div>
	  </div>
	</div>
</section>

<div class="section section-md bg-default text-center singeItService pt-0" id="our-services">
  <div class="container">
     <h2 class="font-size-normal pb-4">Ios App Development Service We Provide</h2>
     <div class="row row-2 row-narrow-xxs justify-content-center">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInLeft">
            <div class="services primary ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Store Design & Development</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">If you want to make a new development, Our Magento experts team provides highly customizable E-Commerce Stores with creative designs and development.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp">
            <div class="services primary ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Magento Customization</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">In your existing site of magento, if you want to make any changes and want to do customization as per your needs, our team will be able to offer you services.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInRight">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Magento Theme Design & Integration</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Do you want to develop magento themes to customize your magento website, store or e-commerce site? We are at you services to do the customization as per your needs.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInLeft">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Magento Migration Services</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Our experts Magento developers provides migration service for existing e-commerce stores to Magento without any bugs and data loss as per client needs.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Magento Extension Development</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">We develop Magento plugins extension in basic built-in functionality. This allows the Magento users to directly download, install, and upload them to their server.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInRight">
            <div class="services primary mb-lg-0">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Magento Upgrade</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">In Magento, there is a requirement of regular updates, but updating this application is tricky, and we provide an exact solution to it.</p>
              </div>
            </div>
          </div>
        </div>
     </div>
  </div>
</div>