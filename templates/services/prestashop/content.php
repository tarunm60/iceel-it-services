<section class="about-iceel ptb-100">
	<div class="container">
	  <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInLeft">
          <h2 class="font-size-normal">Why choose Prestashop?</h2>
          <p>WordPress is the most elegant open source CMS on the market for a website. ICEEL widely uses WordPress Development Services for the development of interactive websites and web applications. Hence, the WordPress Development services provides a platform where a non-technical person manages the entire website from the back end.</p>
          <p>Wordpress has been undoubtedly the most preferred development platforms for the development of highly responsive and scalable websites offering result oriented features, easy integration, ease of set-up, installation, use and maintenance.</p>
          <p>Whether you are looking to modify an old website or want to build a new website from the scratch, our years old experienced developer team has all the coding knowledge to infuse the customization into it, as per your needs.</p>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInRight">
          <img src="<?php echo $siteUrl;?>/images/wordpress-development-services-solutions-company.png" alt="image">
        </div>
      </div>
	  </div>
	</div>
</section>

<div class="section section-md bg-default text-center singeItService pt-0" id="our-services">
  <div class="container">
     <h2 class="font-size-normal pb-4">Prestashop Development Service We Provide</h2>
     <div class="row row-2 row-narrow-xxs justify-content-center">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInLeft">
            <div class="services primary ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">CMS Development</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Do you want services for tested and proven technologies to develop highly interactive websites that would run effectively on the Wordpress CMS? You are at right place.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp">
            <div class="services primary ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Wordpress Theme development</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">We provide wordpress theme development from scratch and modification of existing theme. No matter what adjustments and customization you are looking for.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInRight">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Wordpress Plugin Development</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Do you want customize plugin development for your wordpres website? We can customize bespoke Wordpress plugins to make your ideas come true.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInLeft">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">PSD to Wordpress Conversion</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">We can design for you first of all PSD (Photoshop design) and then convert it into WordPress, so that your dreams come to reality.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Wordpress Plugin Integration</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Depending on your business needs, we can assist you to select the best plugins to enhance functionality of your website without compromising site speed.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInRight">
            <div class="services primary mb-lg-0">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Maintenance And Support</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">By our regular maintenance and support, we will keep upgrading your website to the latest version, and also will secure it by eliminating the malware and virus attacks.</p>
              </div>
            </div>
          </div>
        </div>
     </div>
  </div>
</div>