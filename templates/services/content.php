<section class="about-iceel ptb-100">
	<div class="container">
	  <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInLeft">
          <h1 class="font-size-medium">What we Provide?</h1>
          <p>We are <strong>IT (Information and Technology) Company</strong> prior to 2005 providing end-to-end solutions in Web Application Development, Software Development, E-commerce Website Development, Web Designing, Mobile Application Development, Digital Marketing Services, Search Engine Optimization, Social Media Marketing, App Store Optimization and also like IT consultancy, IT support and managed it services to small, medium or large organizations.</p>
          <p>We hope to empower your business with the bucket of these comprehensive, focused & flexible managed IT services to boost your organization by scoping the requirements to the final delivery, maintenance and continuous upgrade.</p>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInRight">
          <img src="<?php echo $siteUrl;?>/images/iceel-what-we-provide-light.png" alt="image">
        </div>
      </div>
	  </div>
	</div>
</section>

<section class="section section-md bg-gray-100 bg-decoration-show iceel-mission">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-12 wow fadeInLeft">
        <div class="about-image img-fluid wow fadeInLeft">
          <img src="<?php echo $siteUrl;?>/images/icee-it-our-mission.png" alt="image">
        </div>
      </div>
      <div class="col-lg-6 col-md-12 wow fadeInRight">
        <h2 class="font-size-medium">How we deliver?</h2>
        <p>We are into IT services since 2005 and so we know based on our experiences what you need to get the best and economically viable result for you goal achievement suggesting you the best suited technology for your target IT project may it be web designing  and web development, digital marketing, software, app development or any other.</p>
        <p>Understanding of what you need is our strength and then to translate your needs into clear results is our magical expertise. Hope to experience an enchanting magical results with ICEEL.</p>
      </div>
    </div>
  </div>
</section>