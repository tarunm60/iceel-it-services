<?php

	$phpSericesArray = array(
		array(
			'title' => 'PHP Web Development',
			'cat' => 'PHP Development',
			'url' => 'php-web-development-service-solution-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'WordPress Development',
			'cat' => 'PHP Development',
			'url' => 'wordpress-development-services-solutions-company.php',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Laravel Development',
			'cat' => 'PHP Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'CodeIgniter Development',
			'cat' => 'PHP Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
	);

	$ecommerceSericesArray = array(
		array(
			'title' => 'Shopify Development',
			'cat' => 'E-Commerce Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'WooCommerce Development',
			'cat' => 'E-Commerce Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Magento Development',
			'cat' => 'E-Commerce Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Prestashop Development',
			'cat' => 'E-Commerce Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
	);

	$jsServicesArray = array(
		array(
			'title' => 'JS Development',
			'cat' => 'JaveScript Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'React.JS Development',
			'cat' => 'JaveScript Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Vue.js Development',
			'cat' => 'JaveScript Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Angular.js Development',
			'cat' => 'JaveScript Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Electron.js Development',
			'cat' => 'JaveScript Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Node.Js Development',
			'cat' => 'JaveScript Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
	);

	$digitalServicesArray = array( 
		array(
			'title' => 'SEO Services',
			'cat' => 'Digital Marketing Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Local Advertising Services',
			'cat' => 'Digital Marketing Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'App Store Optimization',
			'cat' => 'Digital Marketing Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'SEM - PPC Services',
			'cat' => 'Digital Marketing Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'SMM - SMO Services',
			'cat' => 'Digital Marketing Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
	);

	$appServicesArray = array(
		array(
			'title' => 'Ios App Development',
			'cat' => 'App Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Android App development',
			'cat' => 'App Development',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
	);
	
	$servicesArray = array(
		array(
			'title' => 'Web Designing Services',
			'cat' => 'Other It Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Database Management',
			'cat' => 'Other It Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
		array(
			'title' => 'Server & Network Manage',
			'cat' => 'Other It Services',
			'url' => '',
			'content' => 'Progravida nibh vel velit auctor alinean sollicitudin.',
			'imgeUrl' => $siteUrl.'/images/services1.jpg',
		),
	);

	function croselLoopShotcode($singleItem=array())
	{
	?>
		<div class="owl-item">
			<div class="item">
	      <div class="main-service">
	        <div class="service-img">
	          <img src="<?php	echo $singleItem['imgeUrl'];?>" class="img-fluid" alt="image">
	        </div>
	        <div class="service-detail">
	          <div class="text-primary text-bold iq-fw-8" href="blog-details-left-sidebar.html">
	        	<?php
	          	echo $singleItem['cat'];
	          ?>
	          </div>
	          <a href="<?php echo $singleItem['url']; ?>">
	          	<h5 class="mt-1 iq-fw-8">
	          	<?php
	          		echo $singleItem['title'];
	          	?>
	          	</h5>
	          </a>
	          <p class="mb-0">
	          	<?php
	          		echo $singleItem['content'];
	          	?>
	          </p>
	          <div class="service-info">
	            <a class="service-info-readmore" href="<?php echo $singleItem['url']; ?>"><span class="iq-fw-8 iq-font-14">Read More</span> <i class="fas fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	      </div>
	  	</div>
		</div>
	<?php
	}
?>

<section id="allServices" class="section section-md bg-default">
	<div class="container">
    <div class="row align-items-center">
      <div class="col-lg-12 col-md-12">
        <h2 class="font-size-medium text-center mb-0">IT SERVICES, SUPPORT & SOLUTION</h2>
        <div class="text-center subHeadingServices mb-4">(Web Designing, Development, Digital Marketing)</div>
        <div class="text-center">
        	<p>Looking for an IT service and support provider companies in India? ICEEL is a leading IT outsourcing company for IT consulting and managed IT services for your IT project. ICEEL believes in helping corporate to adopt new generation and advance level technologies leaving behind old age marketing and other technologies and thus overcoming challenges growing due to digital transformations.</p>
        	<p>IT support and service means not creating or maintaining website, application or software during any technical failures or lead generation but it is quite beyond that stretching to a new and modernized approach to apply cutting-edge technologies enabling you to differentiate your organization from your competitors.</p>
        	<p>We in ICEEL encircle all IT services like planning strategies, design and development, testing, deployment etc employing them throughout the entire process for scalability, problem-solving, best output in terms of quality and usability.</p>
        </div>
      </div>
    </div>
    <div class="row mt-5">
    	<div class="col-lg-12 col-md-12">
        <h3 class="heading font-size-normal text-primary">PHP Development Services:</h3>
      </div>
    </div>
		<div class="row mt-0">
			<div class="col-lg-12 text-left">
   			<div class="owl-carousel owl-loaded owl-drag" data-loop="true" data-nav="true" data-autoplay="true" data-autoplay-timeout="3000" data-items="3" data-items-laptop="3" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1">
        	<div class="owl-stage-outer">
        		<div class="owl-stage">
							<?php
								foreach ($phpSericesArray as $singleItem) {
									croselLoopShotcode($singleItem);
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

 		<div class="row mt-70">
    	<div class="col-lg-12 col-md-12">
        <h3 class="heading font-size-normal text-primary">E-Commerce Development  Services:</h3>
      </div>
    </div>
		<div class="row mt-0">
			<div class="col-lg-12 text-left">
   			<div class="owl-carousel owl-loaded owl-drag" data-loop="true" data-nav="true" data-autoplay="true" data-autoplay-timeout="3000" data-items="3" data-items-laptop="3" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1">
        	<div class="owl-stage-outer">
        		<div class="owl-stage">
							<?php
								foreach ($ecommerceSericesArray as $singleItem) {
									croselLoopShotcode($singleItem);
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row mt-70">
    	<div class="col-lg-12 col-md-12">
        <h3 class="heading font-size-normal text-primary">Digital Marketing Services:</h3>
      </div>
    </div>
		<div class="row mt-0">
			<div class="col-lg-12 text-left">
   			<div class="owl-carousel owl-loaded owl-drag" data-loop="true" data-nav="true" data-autoplay="true" data-autoplay-timeout="3000" data-items="3" data-items-laptop="3" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1">
        	<div class="owl-stage-outer">
        		<div class="owl-stage">
							<?php
								foreach ($digitalServicesArray as $singleItem) {
									croselLoopShotcode($singleItem);
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row mt-70">
    	<div class="col-lg-12 col-md-12">
        <h3 class="heading font-size-normal text-primary">JaveScript Development Services:</h3>
      </div>
    </div>
		<div class="row mt-0">
			<div class="col-lg-12 text-left">
   			<div class="owl-carousel owl-loaded owl-drag" data-loop="true" data-nav="true" data-autoplay="true" data-autoplay-timeout="3000" data-items="3" data-items-laptop="3" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1">
        	<div class="owl-stage-outer">
        		<div class="owl-stage">
							<?php
								foreach ($jsServicesArray as $singleItem) {
									croselLoopShotcode($singleItem);
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row mt-70">
    	<div class="col-lg-12 col-md-12">
        <h3 class="heading font-size-normal text-primary">App Development:</h3>
      </div>
    </div>
		<div class="row mt-0">
			<div class="col-lg-12 text-left">
   			<div class="owl-carousel owl-loaded owl-drag" data-nav="true" data-autoplay="true" data-autoplay-timeout="3000" data-items="3" data-items-laptop="3" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1">
        	<div class="owl-stage-outer">
        		<div class="owl-stage">
							<?php
								foreach ($appServicesArray as $singleItem) {
									croselLoopShotcode($singleItem);
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row mt-70">
    	<div class="col-lg-12 col-md-12">
        <h3 class="heading font-size-normal text-primary">Other It Services:</h3>
      </div>
    </div>
		<div class="row mt-0">
			<div class="col-lg-12 text-left">
   			<div class="owl-carousel owl-loaded owl-drag" data-nav="true" data-autoplay="true" data-autoplay-timeout="3000" data-items="3" data-items-laptop="3" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1">
        	<div class="owl-stage-outer">
        		<div class="owl-stage">
							<?php
								foreach ($servicesArray as $singleItem) {
									croselLoopShotcode($singleItem);
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>