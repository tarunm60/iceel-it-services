<section class="about-iceel ptb-100">
	<div class="container">
	  <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInLeft">
          <h2 class="font-size-normal">Why choose PHP?</h2>
          <p>PHP is a scripting language that enables people to create dynamic web pages and applications. It is a server-side language that is one of the most popular used by developers today.</p>
          <p>PHP has good compatibility with other technologies, PHP is open-source so cheap, PHP is dynamic and interactive nature, PHP has universal browser support, Good database integration i.e. Integrated Database Support of php, PHP is very easy to plug into a wide array of services, APIs, libraries, components etc.</p>
          <p>PHP code may be embedded into HTML code, or it can use in combination with various Web template systems and web frameworks. There are couple of frameworks and CMSs are built with PHP.</p>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInRight">
          <img src="<?php echo $siteUrl;?>/images/wordpress-development-services-solutions-company.png" alt="image">
        </div>
      </div>
	  </div>
	</div>
</section>

<div class="section section-md bg-default text-center singeItService pt-0" id="our-services">
  <div class="container">
     <h2 class="font-size-normal pb-4">PHP Web Development Service We Provide</h2>
     <div class="row row-2 row-narrow-xxs justify-content-center">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInLeft">
            <div class="services primary ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Core PHP Programming</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Our PHP expert team provide best web solution for any buesiness areas like Informative, E-commerce, Blog, Startup Solution, Enterprise Solutions.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp">
            <div class="services primary ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Web or Application Development</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">In your existing site of magento, if you want to make any changes and want to do customization as per your needs, our team will be able to offer you services.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInRight">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">E-Commerce Solutions & Services</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Do you want to develop magento themes to customize your magento website, store or e-commerce site? We are at you services to do the customization as per your needs.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInLeft">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Customized Services /Solutions</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">Our experts Magento developers provides migration service for existing e-commerce stores to Magento without any bugs and data loss as per client needs.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInUp">
            <div class="services primary mb-lg-0 ">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">Enterprise Resource Planning Solutions</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">We develop Magento plugins extension in basic built-in functionality. This allows the Magento users to directly download, install, and upload them to their server.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 wow fadeInRight">
            <div class="services primary mb-lg-0">
              <div class="services-info">
                <div class="service-info-header">
                  <div class="service-shap primary"><i class="fas fa-laptop"></i></div>
                  <h3 class="float-left wow fadeInUpSmall">PHP CMS  Solutions</h3>
                </div>
                <div class="clearfix"></div>
                <p class="mt-3 mb-0">In Magento, there is a requirement of regular updates, but updating this application is tricky, and we provide an exact solution to it.</p>
              </div>
            </div>
          </div>
        </div>
     </div>
  </div>
</div>