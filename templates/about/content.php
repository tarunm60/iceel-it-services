<section class="about-iceel ptb-100">
	<div class="container">
	  <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInLeft">
          <h2 class="font-size-medium">About Us</h2>
          <div class="innerContent">
            <p>We as an organization are a team of exports in web designing, web developers, SEO experts, Social Media Marketers providing you all IT / information technology services and solutions prior to 2005 with our head office at Ahmedabad, Gujarat, India.</p>
            <p><strong>Our focus cliental areas:</strong></p>
            <p>We are a leading IT company in India who can cater to various industries such as tours & travels, car rental, E-commerce, online Education Training, property and Real-estate. May you be an SMEs or corporate, we are at your services providing high-quality & cost-effective solution.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInRight">
          <img src="<?php echo $siteUrl;?>/images/icee-it-about-us.png" alt="image">
        </div>
      </div>
	  </div>
	</div>
</section>

<section class="section section-md bg-gray-100 bg-decoration-show iceel-mission">
	<div class="container">
	  <div class="row align-items-center">
	  	<div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInLeft">
          <img src="<?php echo $siteUrl;?>/images/what-we-do.png" alt="image">
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInRight">
          <h2 class="font-size-medium">What we do?</h2>
          <div class="innerContent">
            <p>We do a magic; magic with your business. We first of all prepare your online presence attires as per google and w3c standards and then increase your visibility through different search engines or through social media marketing.</p>
            <p>Skyrocketing your success is possible with us if your organization can patch up and pace up with us for a while even.</p>
            <p>Our core service areas are Digital Marketing Services, Web Designing & Development Services and solutions, Application / App Development Services, E-commerce development, Plug-in Development, ASO (App Store Optimization), SMM / SMO (Social Media Marketing / Social Media Optimization), SEO (Search Engine Optimization) etc.</p>
          </div>
        </div>
      </div>
	  </div>
	</div>
</section>

<section class="section section-md bg-default iceel-whoweare">
	<div class="container">
	  <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInLeft">
          <h2 class="font-size-medium">Our Mission</h2>
          <div class="innerContent">
            <p>Our mission is to endeavour tirelessly to maintain a smile on our customer’s faces as a resultant of goal achievement in their organization.</p>
            <p><strong>Our mission is to achieve your goal:</strong></p>
            <ul class="commonBullet">
              <li>Timely</li>
              <li>Economically</li>
              <li>Precisely</li>
            </ul>
            <p>In a technologically advanced way.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInRight">
          <img src="<?php echo $siteUrl;?>/images/icee-it-our-mission-2.png" alt="image">
        </div>
      </div>
	  </div>
	</div>
</section>

<section class="section section-md bg-gray-100 bg-decoration-show iceel-mission">
	<div class="container">
	  <div class="row align-items-center">
	  	<div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInLeft">
          <img src="<?php echo $siteUrl;?>/images/iceel-about-vision.png" alt="image">
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInRight">
          <h2 class="font-size-medium">Our Vision</h2>
          <div class="innerContent">
            <p>Universal access of IT services and digital solutions to small scale industries to fortune 500.</p>
            <p>Enabling full market potential available to all level industries to bring for them sunshine of growth and development increasing their productivity.</p>
            <p>Availing complete potential market capture for organizations of all level providing them smart IT technology by full capacity utilization of all of their resources available at their disposal.</p>
          </div>
        </div>
      </div>
	  </div>
	</div>
</section>

<section class="section section-md bg-default iceel-whoweare">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-content wow fadeInLeft">
          <h2 class="font-size-medium">Why iCEEL?</h2>
          <h4>Why Choose ICEEL as a true partner Company for your all IT Services and Solutions?</h4>
          <ul class="commonBullet">
            <li>Timely Delivery of Project.</li>
            <li>Prompt Support.</li>
            <li>Flexibility in support till project continues and after project completion.</li>
            <li>Value base ethical business practice and no hidden costs.</li>
            <li>Consistently Reliable</li>
          </ul>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-image img-fluid wow fadeInRight">
          <img src="<?php echo $siteUrl;?>/images/about-us-3.png" alt="image">
        </div>
      </div>
    </div>
  </div>
</section>