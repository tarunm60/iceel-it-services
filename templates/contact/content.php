<div class="section section-md bg-default">
	<div class="container">
		<div class="row row-30 justify-content-center">
			<div class="col-sm-10 col-md-6 col-lg-5 col-xl-4">
				<div class="contact-info">
					<h6><span class="head-text">Contact us</span></h6>
					<h2>We are Here for You.</h2>
					<p>Tincidunt elit magnis nulla facilisis sagittis maece nass apien nunced ultrices.</p>
					<ul class="list-lg" style="max-width: 250px;">
						<li>
							<div class="unit align-items-center">
								<div class="unit-left">
									<div class="svg-icon-wrapper svg-icon-wrapper-rounded svg-icon-wrapper-gray-100">
										<svg class="icon icon-secondary" role="img">
											<use xlink:href="images/svg/sprite.svg#message"></use>
										</svg>
									</div>
								</div>
								<div class="unit-body">
									<h5 class="font-weight-sbold"><a href="mailto:#">info@moling.com</a></h5>
									<p class="small small-md">Email address</p>
								</div>
							</div>
						</li>
						<li>
							<div class="unit align-items-center">
								<div class="unit-left">
									<div class="svg-icon-wrapper svg-icon-wrapper-rounded svg-icon-wrapper-gray-100">
										<svg class="icon icon-secondary" role="img">
											<use xlink:href="images/svg/sprite.svg#phone-call"></use>
										</svg>
									</div>
								</div>
								<div class="unit-body">
									<h5 class="font-weight-sbold"><a href="mailto:#">+ 777 444 0000</a></h5>
									<p class="small small-md">Phone line</p>
								</div>
							</div>
						</li>
						<li>
							<div class="unit align-items-center">
								<div class="unit-left">
									<div class="svg-icon-wrapper svg-icon-wrapper-rounded svg-icon-wrapper-gray-100">
										<svg class="icon icon-secondary" role="img">
											<use xlink:href="images/svg/sprite.svg#warehouse"></use>
										</svg>
									</div>
								</div>
								<div class="unit-body">
									<h5 class="font-weight-sbold"><a href="#">88 road broklyn st</a></h5>
									<p class="small small-md">Our address</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-10 col-md-6 col-lg-7 col-xl-8">
				<!--RD Mailform-->
				<h3 class="contactFormHeading">Let's Talk About Your Needs!</h3>
				<form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php" novalidate="novalidate">
					<div class="row row-30">
						<div class="col-lg-6">
							<div class="form-wrap">
								<label class="form-label rd-input-label focus not-empty" for="contact-name">full name</label>
								<input placeholder="full name" class="form-input form-control-has-validation form-control-last-child" id="contact-name" type="text" name="name" data-constraints="@Required"><span class="form-validation"></span>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-wrap">
								<label class="form-label rd-input-label focus not-empty" for="contact-email">email address</label>
								<input placeholder="email address" class="form-input form-control-has-validation form-control-last-child" id="contact-email" type="email" name="email" data-constraints="@Required @Email"><span class="form-validation"></span>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-wrap">
								<label class="form-label rd-input-label focus not-empty" for="contact-phone">phone number</label>
								<input placeholder="phone number" class="form-input form-control-has-validation form-control-last-child" id="contact-phone" type="text" name="phone" data-constraints="@Required @PhoneNumber"><span class="form-validation"></span>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-wrap has-error">
								<label class="form-label rd-input-label" for="contact-subject">subject</label>
								<input placeholder="subject" class="form-input form-control-has-validation form-control-last-child" id="contact-subject" type="text" name="subject" data-constraints="@Required"><span class="form-validation">The text field is required.</span>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-wrap has-error">
								<label class="form-label rd-input-label" for="contact-message">write message</label>
								<textarea placeholder="write message" class="form-input form-control-has-validation form-control-last-child" id="contact-message" name="message" data-constraints="@Required"></textarea><span class="form-validation">The text field is required.</span>
							</div>
						</div>
						<div class="col-md-12">
							<button class="button button-primary button-ujarak" type="submit">Send message</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>