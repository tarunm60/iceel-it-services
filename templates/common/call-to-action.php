<div class="section section-xsmall bg-primary text-center">
    <div class="container">
       <div class="row row-15 align-items-center">
          <div class="col-md-7 col-lg-8 text-md-left wow fadeInLeft" data-wow-delay=".3s">
             <h3><span class="head-big">Why Wait? <span class="text-secondary">Start It</span> Right Now!</span></h3>
          </div>
          <div class="col-md-5 col-lg-4 text-md-right wow fadeInRight" data-wow-delay=".3s"><a class="button button-primary button-ujarak" href="about.html">Contact Now</a></div>
       </div>
    </div>
 </div>