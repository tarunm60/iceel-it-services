<div class="section swiper-slider-wrapper">
  <div class="bg-decoration">
     <div class="bg-decoration-item"></div>
     <div class="bg-decoration-item"></div>
     <div class="bg-decoration-item"></div>
     <div class="bg-decoration-item"></div>
     <div class="bg-decoration-item"></div>
  </div>
  <div class="swiper-container swiper-slider main-slider context-dark text-center" data-loop="true" data-autoplay="true" data-autoplay="7500" data-custom-effect="parallax">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
			   <figure class="slide-bgimg" style="background-image:url('<?php echo $baseUrl;?>images/iceel-it-services-ecomerce-development-solutions.jpg')"><img class="entity-img" src="<?php echo $baseUrl;?>images/iceel-it-services-ecomerce-development-solutions.jpg" alt="Online Presence & E-commerce" width="1894" height="777"></figure>
			   <div class="container">
			      <div class="swiper-slide-content section-lg">
			         <h6 data-caption-animate="fadeInUpSmall" data-caption-delay="200" data-caption-duration="800"><span class="head-text">You are at a right place for your</span></h6>
			         <div class="sliderHeading" data-caption-animate="fadeInUpSmall" data-caption-delay="300" data-caption-duration="800"><span>Online Presence & E-commerce</span></div>
			         <a class="button button-primary button-ujarak" data-custom-scroll-to="our-services" href="#our-services" data-caption-animate="fadeInUpSmall" data-caption-delay="600" data-caption-duration="800">Get Started</a>
			      </div>
			   </div>
			</div>
			<div class="swiper-slide">
			   <figure class="slide-bgimg" style="background-image:url('<?php echo $baseUrl;?>images/iceel-it-services-websiteoptimization.jpg')"><img class="entity-img" src="<?php echo $baseUrl;?>images/iceel-it-services-websiteoptimization.jpg" alt="We will Get You on Track" width="1894" height="777"></figure>
			   <div class="container">
			      <div class="swiper-slide-content section-lg">
			         <h6 data-caption-animate="fadeInUpSmall" data-caption-delay="200" data-caption-duration="800"><span class="head-text">Struggling to attract & Convert Customers Online?</span></h6>
			         <div class="sliderHeading" data-caption-animate="fadeInUpSmall" data-caption-delay="300" data-caption-duration="800"><span>We'll Get You on Track.</span></div>
			         <a class="button button-primary button-ujarak" data-custom-scroll-to="our-services" href="#our-services" data-caption-animate="fadeInUpSmall" data-caption-delay="600" data-caption-duration="800">Get Started</a>
			      </div>
			   </div>
			</div>
			<div class="swiper-slide">
			   <figure class="slide-bgimg" style="background-image:url('<?php echo $baseUrl;?>images/iceel-it-services-web-designing-services-solutions.jpg')"><img class="entity-img" src="<?php echo $baseUrl;?>images/iceel-it-services-web-designing-services-solutions.jpg" alt="iceel-it-services-web-designing-services-solutions" width="1894" height="777"></figure>
			   <div class="container">
			      <div class="swiper-slide-content section-lg">
			         <h6 data-caption-animate="fadeInUpSmall" data-caption-delay="200" data-caption-duration="800"><span class="head-text">IT Solutions You've Been Looking For</span></h6>
			         <div class="sliderHeading" data-caption-animate="fadeInUpSmall" data-caption-delay="300" data-caption-duration="800"><span>The time to Get Started is Now!</span></div>
			         <a class="button button-primary button-ujarak" data-custom-scroll-to="our-services" href="#our-services" data-caption-animate="fadeInUpSmall" data-caption-delay="600" data-caption-duration="800">Get Started</a>
			      </div>
			   </div>
			</div>
			<div class="swiper-slide">
			   <figure class="slide-bgimg" style="background-image:url('<?php echo $baseUrl;?>images/digital-marketing-services-solutions-company.jpg')"><img class="entity-img" src="<?php echo $baseUrl;?>images/digital-marketing-services-solutions-company.jpg" alt="digital-marketing-services-solutions-company" width="1894" height="777"></figure>
			   <div class="container">
			      <div class="swiper-slide-content section-lg">
			         <h6 data-caption-animate="fadeInUpSmall" data-caption-delay="200" data-caption-duration="800"><span class="head-text">Reach Your Website's Full Potential with our expertise!</span></h6>
			         <div class="sliderHeading" data-caption-animate="fadeInUpSmall" data-caption-delay="300" data-caption-duration="800"><span>Digital Marketing Services</span></div>
			         <a class="button button-primary button-ujarak" data-custom-scroll-to="our-services" href="#our-services" data-caption-animate="fadeInUpSmall" data-caption-delay="600" data-caption-duration="800">Get Started</a>
			      </div>
			   </div>
			</div>
			<div class="swiper-slide">
		   <figure class="slide-bgimg" style="background-image:url('<?php echo $baseUrl;?>images/app-development-services-solutions-company.jpg')"><img class="entity-img" src="<?php echo $baseUrl;?>images/app-development-services-solutions-company.jpg" alt="app-development-services-solutions-company" width="1894" height="777"></figure>
				<div class="container">
			    <div class="swiper-slide-content section-lg">
				     <h6 data-caption-animate="fadeInUpSmall" data-caption-delay="200" data-caption-duration="800"><span class="head-text">The Magic wand for the success of any business!</span></h6>
				     <div class="sliderHeading" data-caption-animate="fadeInUpSmall" data-caption-delay="300" data-caption-duration="800"><span>Stuning Websites & Mobile Apps</span></div>
				     <a class="button button-primary button-ujarak" data-custom-scroll-to="our-services" href="#our-services" data-caption-animate="fadeInUpSmall" data-caption-delay="600" data-caption-duration="800">Get Started</a>
			    </div>
			 </div>
			</div>
		</div>
 		<div class="swiper-pagination"></div>
 		<div class="swiper-button-prev fa-angle-left fas"></div>
 		<div class="swiper-button-next fa-angle-right fas"></div>
	</div>
</div>