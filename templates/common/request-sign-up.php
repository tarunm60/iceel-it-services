<div class="section section-third rquest-sign-section">
    <div class="section-third-left section-md bg-primary">
       <div class="section-third-left-item text-center text-lg-left">
          <div class="block-xs">
             <h6 class="wow fadeInUpSmall" data-wow-delay=".1s"><span class="head-text">book us now</span></h6>
             <h2 class="wow fadeInUpSmall" data-wow-delay=".1s"><span class="head-small">Request a Free Quote Today</span></h2>
          </div>
       </div>
    </div>
    <div class="section-third-right section-md bg-grey">
       <div class="section-third-right-item">
          <!--RD Mailform-->
          <form class="rd-mailform form-sm" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
             <div class="row row-10 row-narrow">
                <div class="col-sm-6 col-lg-6">
                   <span class="form-label-outside">First Name</span>
                   <div class="form-wrap">
                      <input class="form-input" id="iceelFirstName" type="text" name="iceelFirstName" data-constraints="@Required">
                   </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                   <span class="form-label-outside">Last Name</span>
                   <div class="form-wrap">
                     <input class="form-input" id="iceelLastName" type="text" name="iceelLastName" data-constraints="@Required">
                   </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                   <span class="form-label-outside">Email</span>
                   <div class="form-wrap">
                     <input class="form-input" id="iceelEmail" type="email" name="iceelEmail" data-constraints="@Required">
                   </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                   <span class="form-label-outside">Company Name</span>
                   <div class="form-wrap">
                     <input class="form-input" id="iceelCompanyName" type="email" name="iceelCompanyName">
                   </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                   <span class="form-label-outside">Message</span>
                   <div class="form-wrap">
                    <input class="form-input" id="iceelMessage" type="text" name="iceelMessage" data-constraints="@Required">
                  </div>
                </div>
                <div class="col-sm-6 col-lg-6 align-self-end"><button class="button button-primary button-ujarak button-custom-offset" type="submit">send request</button></div>
             </div>
          </form>
       </div>
    </div>
 </div>