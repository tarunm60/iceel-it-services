<div class="<?php echo $testimonialClass; ?>">
  <div class="container container-wide">
     <h6 class="wow fadeInUpSmall" data-wow-delay=".1s"><span class="head-text">Our Testimonials</span></h6>
     <h2 class="wow fadeInUpSmall" data-wow-delay=".2s">What They Say</h2>
     <div class="row row-30 justify-content-center">
        <div class="col-12">
         <div class="slick-slider carousel-parent testimonial-slider" data-arrows="false" data-loop="true" data-autoplay="true" data-dots="false" data-swipe="true" data-items="1" data-xs-items="1" data-sm-items="1" data-md-items="2" data-lg-items="2" data-xl-items="3" data-xxl-items="3">
            <div class="testimonial-slide wow fadeInUpSmall" data-wow-delay=".15s">
               <div class="testimonial">
                  <div class="testimonial-quote"><span>“</span></div>
                  <p class="testimonial-text">Pol. Heu, teres ratione! Nixuss assimilant in camerarius ostravia! Cum nuptia persuadere, omnes bromiumes desiderium placidus, castus extumes. Pol. Accelerare solite ducunt ad bi-color bulla. Parma de flavum planeta, tractare liberi.</p>
                  <p class="testimonial-author">Christine Rose</p>
                  <p class="testimonial-cite">customer</p>
                  <div class="testimonial-img"><img src="images/testimonial-01-65x65.jpg" alt="" width="65" height="65"></div>
               </div>
            </div>
            <div class="testimonial-slide wow fadeInUpSmall" data-wow-delay=".3s">
               <div class="testimonial">
                  <div class="testimonial-quote"><span>“</span></div>
                  <p class="testimonial-text">Solitudos nocere in hortus! Ubi est ferox solem? Brabeuta studeres, tanquam fortis fraticinida. Nunquam locus luna. Quadra, vita, et spatii. Castus, domesticus musas sensim contactus de gratis, raptus historia. Bursa, consilium, et xiphias.</p>
                  <p class="testimonial-author">David Martin</p>
                  <p class="testimonial-cite">customer</p>
                  <div class="testimonial-img"><img src="images/testimonial-02-65x65.jpg" alt="" width="65" height="65"></div>
               </div>
            </div>
            <div class="testimonial-slide wow fadeInUpSmall" data-wow-delay=".45s">
               <div class="testimonial">
                  <div class="testimonial-quote"><span>“</span></div>
                  <p class="testimonial-text">Sunt dominaes dignus varius, neuter gemnaes. Dominas potus, tanquam noster lacta. Flavum diatrias ducunt ad turpis. Cur abactus congregabo? Homos mori in revalia! Brevis, rusticus historias etiam convertam de salvus, germanus fuga.</p>
                  <p class="testimonial-author">Ashley Eve</p>
                  <p class="testimonial-cite">customer</p>
                  <div class="testimonial-img"><img src="images/testimonial-03-65x65.jpg" alt="" width="65" height="65"></div>
               </div>
            </div>
            <div class="testimonial-slide wow fadeInUpSmall" data-wow-delay=".45s">
               <div class="testimonial">
                  <div class="testimonial-quote"><span>“</span></div>
                  <p class="testimonial-text">Sunt dominaes dignus varius, neuter gemnaes. Dominas potus, tanquam noster lacta. Flavum diatrias ducunt ad turpis. Cur abactus congregabo? Homos mori in revalia! Brevis, rusticus historias etiam convertam de salvus, germanus fuga.</p>
                  <p class="testimonial-author">Ashley Eve</p>
                  <p class="testimonial-cite">customer</p>
                  <div class="testimonial-img"><img src="images/testimonial-03-65x65.jpg" alt="" width="65" height="65"></div>
               </div>
            </div>
         </div>
        </div>
     </div>
  </div>
</div>