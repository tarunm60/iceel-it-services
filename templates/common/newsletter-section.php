<div id="news-letter-section" class="section section-lg bg-default text-center">
    <div class="bg-decoration">
       <div class="bg-decoration-item"></div>
       <div class="bg-decoration-item"></div>
       <div class="bg-decoration-item"></div>
       <div class="bg-decoration-item"></div>
    </div>
    <div class="container-wide">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12">
          <h2 class="wow fadeInUpSmall" data-wow-delay=".3s">Join For New Updates</h2>
         <div class="row row-lg justify-content-center">
            <div class="col-lg-10 col-xl-8">
               <form class="rd-form rd-mailform rd-form-inline" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                  <div class="form-wrap"><input class="form-input" id="subscribe-form-email" type="email" name="email" data-constraints="@Email @Required"> <label class="form-label" for="subscribe-form-email">email address</label></div>
                  <div class="form-button"><button class="button button-primary button-ujarak" type="submit">Subscribe now</button></div>
               </form>
            </div>
         </div>
        </div>
      </div>
       
    </div>
 </div>