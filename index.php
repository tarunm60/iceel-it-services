<?php

$page = array(

	'name' => 'it-services',
	'title' => 'Iceel It Services',
	'keywords' => 'dsdsdsd',
	'description' => 'dsdsd',
	'allowIndex' => false,
);

$testimonialClass = "section section-md bg-gray-100 text-center"; 

require 'config/config.php';

require 'header/head.php';

require 'templates/common/slider.php'; 

require 'templates/home/about-section.php';

require 'templates/home/services-crousel.php';

require 'templates/home/our-services.php';

//<!-- Section Book Us Now-->
require 'templates/common/request-sign-up.php';

//<!-- Section Our Blog-->
require 'templates/home/blog-section.php';

//<!-- Testimonials section-->
require 'templates/common/testimonials.php';

//<!-- Newsletter section-->
require 'templates/common/newsletter-section.php';

//<!-- call-to-action section-->
require 'templates/common/call-to-action.php';

require 'footer/footer.php';

?>