function pageTransition(i) {
    (i = i || {}).target = i.target || null, i.delay = i.delay || 500, i.duration = i.duration || 1e3, i.classIn = i.classIn || null, i.classOut = i.classOut || null, i.classActive = i.classActive || null, i.onReady = i.onReady || null, i.onTransitionStart = i.onTransitionStart || null, i.onTransitionEnd = i.onTransitionEnd || null, i.conditions = i.conditions || function(e, t) {
        return !/(\#|callto:|tel:|mailto:|:\/\/)/.test(t)
    }, i.target && (setTimeout(function() {
        i.onReady && i.onReady(i), i.classIn && i.target.classList.add(i.classIn), i.classActive && i.target.classList.add(i.classActive), i.duration && (i.target.style.animationDuration = i.duration + "ms"), i.target.addEventListener("animationstart", function() {
            setTimeout(function() {
                i.classIn && i.target.classList.remove(i.classIn), i.onTransitionEnd && i.onTransitionEnd(i)
            }, i.duration)
        })
    }, i.delay), $("a").click(function(e) {
        var t = e.currentTarget.getAttribute("href");
        if (i.conditions(e, t)) {
            var n = this.href;
            e.preventDefault(), i.onTransitionStart && i.onTransitionStart(i), i.classIn && i.target.classList.remove(i.classIn), i.classOut && i.target.classList.add(i.classOut), setTimeout(function() {
                window.location = n, /firefox/i.test(navigator.userAgent) && setTimeout(function() {
                    i.onReady && i.onReady(i), i.classOut && i.target.classList.remove(i.classOut)
                }, 1e3), /safari/i.test(navigator.userAgent) && !/chrome/i.test(navigator.userAgent) && (i.onReady && i.onReady(i), i.classOut && i.target.classList.remove(i.classOut))
            }, i.duration)
        }
    }))
}

function Util() {}

function aCounter(e) {
    function t(e) {
        if (!e || !e.node) throw Error("Missing required aCounter parameters (node).");
        if (this.params = Util.merge([this.defaults, e]), !(this.params.node.counter = this).params.to) try {
            this.params.to = parseInt(this.params.node.textContent, 10)
        } catch (e) {
            throw Error("Unable to get aCounter value")
        }
        return this.run = this.run.bind(this), this
    }
    return t.prototype.internal = {
        intervalId: null,
        value: 0,
        loops: 0,
        increment: 0,
        loop: 0
    }, t.prototype.defaults = {
        node: null,
        from: 0,
        to: null,
        duration: 3e3,
        refresh: 30,
        formatter: null,
        onStart: null,
        onUpdate: null,
        onComplete: null
    }, t.prototype.run = function() {
        clearInterval(this.internal.intervalId), this.internal.value = this.params.from, this.internal.loops = Math.ceil(this.params.duration / this.params.refresh), this.internal.increment = (this.params.to - this.params.from) / this.internal.loops, this.internal.loop = 0, this.params.onStart instanceof Function && this.params.onStart.call(this, ~~this.internal.value), this.internal.intervalId = setInterval(this.update.bind(this), this.params.refresh)
    }, t.prototype.update = function() {
        this.internal.value += this.internal.increment, this.internal.loop++, this.params.onUpdate instanceof Function && this.params.onUpdate.call(this, ~~this.internal.value), this.internal.loop >= this.internal.loops && (clearInterval(this.internal.intervalId), this.internal.value = this.params.to, this.params.onComplete instanceof Function && this.params.onComplete.call(this, ~~this.internal.value)), this.render()
    }, t.prototype.render = function() {
        this.params.formatter instanceof Function ? this.params.node.innerHTML = this.params.formatter.call(this, ~~this.internal.value) : this.params.node.innerHTML = ~~this.internal.value
    }, new t(e)
}
if (function(e, t) {
        "use strict";
        "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
            if (!e.document) throw new Error("jQuery requires a window with a document");
            return t(e)
        } : t(e)
    }("undefined" != typeof window ? window : this, function(C, e) {
        "use strict";

        function m(e, t) {
            var n = (t = t || G).createElement("script");
            n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
        }

        function a(e) {
            var t = !!e && "length" in e && e.length,
                n = oe.type(e);
            return "function" !== n && !oe.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
        }

        function c(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        }

        function t(e, n, i) {
            return oe.isFunction(n) ? oe.grep(e, function(e, t) {
                return !!n.call(e, t, e) !== i
            }) : n.nodeType ? oe.grep(e, function(e) {
                return e === n !== i
            }) : "string" != typeof n ? oe.grep(e, function(e) {
                return -1 < J.call(n, e) !== i
            }) : me.test(n) ? oe.filter(n, e, i) : (n = oe.filter(n, e), oe.grep(e, function(e) {
                return -1 < J.call(n, e) !== i && 1 === e.nodeType
            }))
        }

        function n(e, t) {
            for (;
                (e = e[t]) && 1 !== e.nodeType;);
            return e
        }

        function d(e) {
            return e
        }

        function u(e) {
            throw e
        }

        function l(e, t, n, i) {
            var s;
            try {
                e && oe.isFunction(s = e.promise) ? s.call(e).done(t).fail(n) : e && oe.isFunction(s = e.then) ? s.call(e, t, n) : t.apply(void 0, [e].slice(i))
            } catch (e) {
                n.apply(void 0, [e])
            }
        }

        function i() {
            G.removeEventListener("DOMContentLoaded", i), C.removeEventListener("load", i), oe.ready()
        }

        function s() {
            this.expando = oe.expando + s.uid++
        }

        function h(e, t, n) {
            var i;
            if (void 0 === n && 1 === e.nodeType)
                if (i = "data-" + t.replace(De, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(i))) {
                    try {
                        n = function(e) {
                            return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : ke.test(e) ? JSON.parse(e) : e)
                        }(n)
                    } catch (e) {}
                    _e.set(e, t, n)
                } else n = void 0;
            return n
        }

        function p(e, t, n, i) {
            var s, r = 1,
                o = 20,
                a = i ? function() {
                    return i.cur()
                } : function() {
                    return oe.css(e, t, "")
                },
                l = a(),
                c = n && n[3] || (oe.cssNumber[t] ? "" : "px"),
                d = (oe.cssNumber[t] || "px" !== c && +l) && Oe.exec(oe.css(e, t));
            if (d && d[3] !== c)
                for (c = c || d[3], n = n || [], d = +l || 1; d /= r = r || ".5", oe.style(e, t, d + c), r !== (r = a() / l) && 1 !== r && --o;);
            return n && (d = +d || +l || 0, s = n[1] ? d + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = d, i.end = s)), s
        }

        function v(e, t) {
            for (var n, i, s = [], r = 0, o = e.length; r < o; r++)(i = e[r]).style && (n = i.style.display, t ? ("none" === n && (s[r] = Ee.get(i, "display") || null, s[r] || (i.style.display = "")), "" === i.style.display && $e(i) && (s[r] = (u = c = l = void 0, c = (a = i).ownerDocument, d = a.nodeName, (u = Pe[d]) || (l = c.body.appendChild(c.createElement(d)), u = oe.css(l, "display"), l.parentNode.removeChild(l), "none" === u && (u = "block"), Pe[d] = u)))) : "none" !== n && (s[r] = "none", Ee.set(i, "display", n)));
            var a, l, c, d, u;
            for (r = 0; r < o; r++) null != s[r] && (e[r].style.display = s[r]);
            return e
        }

        function g(e, t) {
            var n;
            return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && c(e, t) ? oe.merge([e], n) : n
        }

        function y(e, t) {
            for (var n = 0, i = e.length; n < i; n++) Ee.set(e[n], "globalEval", !t || Ee.get(t[n], "globalEval"))
        }

        function b(e, t, n, i, s) {
            for (var r, o, a, l, c, d, u = t.createDocumentFragment(), h = [], p = 0, f = e.length; p < f; p++)
                if ((r = e[p]) || 0 === r)
                    if ("object" === oe.type(r)) oe.merge(h, r.nodeType ? [r] : r);
                    else if (Fe.test(r)) {
                for (o = o || u.appendChild(t.createElement("div")), a = (Ne.exec(r) || ["", ""])[1].toLowerCase(), l = He[a] || He._default, o.innerHTML = l[1] + oe.htmlPrefilter(r) + l[2], d = l[0]; d--;) o = o.lastChild;
                oe.merge(h, o.childNodes), (o = u.firstChild).textContent = ""
            } else h.push(t.createTextNode(r));
            for (u.textContent = "", p = 0; r = h[p++];)
                if (i && -1 < oe.inArray(r, i)) s && s.push(r);
                else if (c = oe.contains(r.ownerDocument, r), o = g(u.appendChild(r), "script"), c && y(o), n)
                for (d = 0; r = o[d++];) je.test(r.type || "") && n.push(r);
            return u
        }

        function r() {
            return !0
        }

        function f() {
            return !1
        }

        function o() {
            try {
                return G.activeElement
            } catch (e) {}
        }

        function w(e, t, n, i, s, r) {
            var o, a;
            if ("object" == typeof t) {
                for (a in "string" != typeof n && (i = i || n, n = void 0), t) w(e, a, n, i, t[a], r);
                return e
            }
            if (null == i && null == s ? (s = n, i = n = void 0) : null == s && ("string" == typeof n ? (s = i, i = void 0) : (s = i, i = n, n = void 0)), !1 === s) s = f;
            else if (!s) return e;
            return 1 === r && (o = s, (s = function(e) {
                return oe().off(e), o.apply(this, arguments)
            }).guid = o.guid || (o.guid = oe.guid++)), e.each(function() {
                oe.event.add(this, t, s, i, n)
            })
        }

        function x(e, t) {
            return c(e, "table") && c(11 !== t.nodeType ? t : t.firstChild, "tr") && oe(">tbody", e)[0] || e
        }

        function T(e) {
            return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
        }

        function S(e) {
            var t = Xe.exec(e.type);
            return t ? e.type = t[1] : e.removeAttribute("type"), e
        }

        function E(e, t) {
            var n, i, s, r, o, a, l, c;
            if (1 === t.nodeType) {
                if (Ee.hasData(e) && (r = Ee.access(e), o = Ee.set(t, r), c = r.events))
                    for (s in delete o.handle, o.events = {}, c)
                        for (n = 0, i = c[s].length; n < i; n++) oe.event.add(t, s, c[s][n]);
                _e.hasData(e) && (a = _e.access(e), l = oe.extend({}, a), _e.set(t, l))
            }
        }

        function _(n, i, s, r) {
            i = K.apply([], i);
            var e, t, o, a, l, c, d = 0,
                u = n.length,
                h = u - 1,
                p = i[0],
                f = oe.isFunction(p);
            if (f || 1 < u && "string" == typeof p && !re.checkClone && Ge.test(p)) return n.each(function(e) {
                var t = n.eq(e);
                f && (i[0] = p.call(this, e, t.html())), _(t, i, s, r)
            });
            if (u && (t = (e = b(i, n[0].ownerDocument, !1, n, r)).firstChild, 1 === e.childNodes.length && (e = t), t || r)) {
                for (a = (o = oe.map(g(e, "script"), T)).length; d < u; d++) l = e, d !== h && (l = oe.clone(l, !0, !0), a && oe.merge(o, g(l, "script"))), s.call(n[d], l, d);
                if (a)
                    for (c = o[o.length - 1].ownerDocument, oe.map(o, S), d = 0; d < a; d++) l = o[d], je.test(l.type || "") && !Ee.access(l, "globalEval") && oe.contains(c, l) && (l.src ? oe._evalUrl && oe._evalUrl(l.src) : m(l.textContent.replace(Qe, ""), c))
            }
            return n
        }

        function k(e, t, n) {
            for (var i, s = t ? oe.filter(t, e) : e, r = 0; null != (i = s[r]); r++) n || 1 !== i.nodeType || oe.cleanData(g(i)), i.parentNode && (n && oe.contains(i.ownerDocument, i) && y(g(i, "script")), i.parentNode.removeChild(i));
            return e
        }

        function D(e, t, n) {
            var i, s, r, o, a = e.style;
            return (n = n || rt(e)) && ("" !== (o = n.getPropertyValue(t) || n[t]) || oe.contains(e.ownerDocument, e) || (o = oe.style(e, t)), !re.pixelMarginRight() && st.test(o) && it.test(t) && (i = a.width, s = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = o, o = n.width, a.width = i, a.minWidth = s, a.maxWidth = r)), void 0 !== o ? o + "" : o
        }

        function $(e, t) {
            return {
                get: function() {
                    return e() ? void delete this.get : (this.get = t).apply(this, arguments)
                }
            }
        }

        function M(e) {
            var t = oe.cssProps[e];
            return t = t || (oe.cssProps[e] = function(e) {
                if (e in ht) return e;
                for (var t = e[0].toUpperCase() + e.slice(1), n = ut.length; n--;)
                    if ((e = ut[n] + t) in ht) return e
            }(e) || e)
        }

        function A(e, t, n) {
            var i = Oe.exec(t);
            return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
        }

        function O(e, t, n, i, s) {
            var r, o = 0;
            for (r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0; r < 4; r += 2) "margin" === n && (o += oe.css(e, n + Ie[r], !0, s)), i ? ("content" === n && (o -= oe.css(e, "padding" + Ie[r], !0, s)), "margin" !== n && (o -= oe.css(e, "border" + Ie[r] + "Width", !0, s))) : (o += oe.css(e, "padding" + Ie[r], !0, s), "padding" !== n && (o += oe.css(e, "border" + Ie[r] + "Width", !0, s)));
            return o
        }

        function I(e, t, n) {
            var i, s = rt(e),
                r = D(e, t, s),
                o = "border-box" === oe.css(e, "boxSizing", !1, s);
            return st.test(r) ? r : (i = o && (re.boxSizingReliable() || r === e.style[t]), "auto" === r && (r = e["offset" + t[0].toUpperCase() + t.slice(1)]), (r = parseFloat(r) || 0) + O(e, t, n || (o ? "border" : "content"), i, s) + "px")
        }

        function P(e, t, n, i, s) {
            return new P.prototype.init(e, t, n, i, s)
        }

        function L() {
            ft && (!1 === G.hidden && C.requestAnimationFrame ? C.requestAnimationFrame(L) : C.setTimeout(L, oe.fx.interval), oe.fx.tick())
        }

        function N() {
            return C.setTimeout(function() {
                pt = void 0
            }), pt = oe.now()
        }

        function j(e, t) {
            var n, i = 0,
                s = {
                    height: e
                };
            for (t = t ? 1 : 0; i < 4; i += 2 - t) s["margin" + (n = Ie[i])] = s["padding" + n] = e;
            return t && (s.opacity = s.width = e), s
        }

        function H(e, t, n) {
            for (var i, s = (z.tweeners[t] || []).concat(z.tweeners["*"]), r = 0, o = s.length; r < o; r++)
                if (i = s[r].call(n, t, e)) return i
        }

        function z(r, e, t) {
            var n, o, i = 0,
                s = z.prefilters.length,
                a = oe.Deferred().always(function() {
                    delete l.elem
                }),
                l = function() {
                    if (o) return !1;
                    for (var e = pt || N(), t = Math.max(0, c.startTime + c.duration - e), n = 1 - (t / c.duration || 0), i = 0, s = c.tweens.length; i < s; i++) c.tweens[i].run(n);
                    return a.notifyWith(r, [c, n, t]), n < 1 && s ? t : (s || a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c]), !1)
                },
                c = a.promise({
                    elem: r,
                    props: oe.extend({}, e),
                    opts: oe.extend(!0, {
                        specialEasing: {},
                        easing: oe.easing._default
                    }, t),
                    originalProperties: e,
                    originalOptions: t,
                    startTime: pt || N(),
                    duration: t.duration,
                    tweens: [],
                    createTween: function(e, t) {
                        var n = oe.Tween(r, c.opts, e, t, c.opts.specialEasing[e] || c.opts.easing);
                        return c.tweens.push(n), n
                    },
                    stop: function(e) {
                        var t = 0,
                            n = e ? c.tweens.length : 0;
                        if (o) return this;
                        for (o = !0; t < n; t++) c.tweens[t].run(1);
                        return e ? (a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c, e])) : a.rejectWith(r, [c, e]), this
                    }
                }),
                d = c.props;
            for (function(e, t) {
                    var n, i, s, r, o;
                    for (n in e)
                        if (s = t[i = oe.camelCase(n)], r = e[n], Array.isArray(r) && (s = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), (o = oe.cssHooks[i]) && "expand" in o)
                            for (n in r = o.expand(r), delete e[i], r) n in e || (e[n] = r[n], t[n] = s);
                        else t[i] = s
                }(d, c.opts.specialEasing); i < s; i++)
                if (n = z.prefilters[i].call(c, r, d, c.opts)) return oe.isFunction(n.stop) && (oe._queueHooks(c.elem, c.opts.queue).stop = oe.proxy(n.stop, n)), n;
            return oe.map(d, H, c), oe.isFunction(c.opts.start) && c.opts.start.call(r, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), oe.fx.timer(oe.extend(l, {
                elem: r,
                anim: c,
                queue: c.opts.queue
            })), c
        }

        function q(e) {
            return (e.match(we) || []).join(" ")
        }

        function F(e) {
            return e.getAttribute && e.getAttribute("class") || ""
        }

        function R(n, e, i, s) {
            var t;
            if (Array.isArray(e)) oe.each(e, function(e, t) {
                i || Dt.test(n) ? s(n, t) : R(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, i, s)
            });
            else if (i || "object" !== oe.type(e)) s(n, e);
            else
                for (t in e) R(n + "[" + t + "]", e[t], i, s)
        }

        function Y(r) {
            return function(e, t) {
                "string" != typeof e && (t = e, e = "*");
                var n, i = 0,
                    s = e.toLowerCase().match(we) || [];
                if (oe.isFunction(t))
                    for (; n = s[i++];) "+" === n[0] ? (n = n.slice(1) || "*", (r[n] = r[n] || []).unshift(t)) : (r[n] = r[n] || []).push(t)
            }
        }

        function W(t, s, r, o) {
            function a(e) {
                var i;
                return l[e] = !0, oe.each(t[e] || [], function(e, t) {
                    var n = t(s, r, o);
                    return "string" != typeof n || c || l[n] ? c ? !(i = n) : void 0 : (s.dataTypes.unshift(n), a(n), !1)
                }), i
            }
            var l = {},
                c = t === zt;
            return a(s.dataTypes[0]) || !l["*"] && a("*")
        }

        function B(e, t) {
            var n, i, s = oe.ajaxSettings.flatOptions || {};
            for (n in t) void 0 !== t[n] && ((s[n] ? e : i = i || {})[n] = t[n]);
            return i && oe.extend(!0, e, i), e
        }

        function U(e, t) {
            return t.toUpperCase()
        }
        var V = [],
            G = C.document,
            X = Object.getPrototypeOf,
            Q = V.slice,
            K = V.concat,
            Z = V.push,
            J = V.indexOf,
            ee = {},
            te = ee.toString,
            ne = ee.hasOwnProperty,
            ie = ne.toString,
            se = ie.call(Object),
            re = {},
            oe = function(e, t) {
                return new oe.fn.init(e, t)
            },
            ae = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            le = /^-ms-/,
            ce = /-([a-z])/g;
        oe.fn = oe.prototype = {
            jquery: "3.2.1",
            constructor: oe,
            length: 0,
            toArray: function() {
                return Q.call(this)
            },
            get: function(e) {
                return null == e ? Q.call(this) : e < 0 ? this[e + this.length] : this[e]
            },
            pushStack: function(e) {
                var t = oe.merge(this.constructor(), e);
                return t.prevObject = this, t
            },
            each: function(e) {
                return oe.each(this, e)
            },
            map: function(n) {
                return this.pushStack(oe.map(this, function(e, t) {
                    return n.call(e, t, e)
                }))
            },
            slice: function() {
                return this.pushStack(Q.apply(this, arguments))
            },
            first: function() {
                return this.eq(0)
            },
            last: function() {
                return this.eq(-1)
            },
            eq: function(e) {
                var t = this.length,
                    n = +e + (e < 0 ? t : 0);
                return this.pushStack(0 <= n && n < t ? [this[n]] : [])
            },
            end: function() {
                return this.prevObject || this.constructor()
            },
            push: Z,
            sort: V.sort,
            splice: V.splice
        }, oe.extend = oe.fn.extend = function() {
            var e, t, n, i, s, r, o = arguments[0] || {},
                a = 1,
                l = arguments.length,
                c = !1;
            for ("boolean" == typeof o && (c = o, o = arguments[a] || {}, a++), "object" == typeof o || oe.isFunction(o) || (o = {}), a === l && (o = this, a--); a < l; a++)
                if (null != (e = arguments[a]))
                    for (t in e) n = o[t], o !== (i = e[t]) && (c && i && (oe.isPlainObject(i) || (s = Array.isArray(i))) ? (r = s ? (s = !1, n && Array.isArray(n) ? n : []) : n && oe.isPlainObject(n) ? n : {}, o[t] = oe.extend(c, r, i)) : void 0 !== i && (o[t] = i));
            return o
        }, oe.extend({
            expando: "jQuery" + ("3.2.1" + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function(e) {
                throw new Error(e)
            },
            noop: function() {},
            isFunction: function(e) {
                return "function" === oe.type(e)
            },
            isWindow: function(e) {
                return null != e && e === e.window
            },
            isNumeric: function(e) {
                var t = oe.type(e);
                return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
            },
            isPlainObject: function(e) {
                var t, n;
                return !(!e || "[object Object]" !== te.call(e) || (t = X(e)) && ("function" != typeof(n = ne.call(t, "constructor") && t.constructor) || ie.call(n) !== se))
            },
            isEmptyObject: function(e) {
                var t;
                for (t in e) return !1;
                return !0
            },
            type: function(e) {
                return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? ee[te.call(e)] || "object" : typeof e
            },
            globalEval: function(e) {
                m(e)
            },
            camelCase: function(e) {
                return e.replace(le, "ms-").replace(ce, U)
            },
            each: function(e, t) {
                var n, i = 0;
                if (a(e))
                    for (n = e.length; i < n && !1 !== t.call(e[i], i, e[i]); i++);
                else
                    for (i in e)
                        if (!1 === t.call(e[i], i, e[i])) break;
                return e
            },
            trim: function(e) {
                return null == e ? "" : (e + "").replace(ae, "")
            },
            makeArray: function(e, t) {
                var n = t || [];
                return null != e && (a(Object(e)) ? oe.merge(n, "string" == typeof e ? [e] : e) : Z.call(n, e)), n
            },
            inArray: function(e, t, n) {
                return null == t ? -1 : J.call(t, e, n)
            },
            merge: function(e, t) {
                for (var n = +t.length, i = 0, s = e.length; i < n; i++) e[s++] = t[i];
                return e.length = s, e
            },
            grep: function(e, t, n) {
                for (var i = [], s = 0, r = e.length, o = !n; s < r; s++) !t(e[s], s) != o && i.push(e[s]);
                return i
            },
            map: function(e, t, n) {
                var i, s, r = 0,
                    o = [];
                if (a(e))
                    for (i = e.length; r < i; r++) null != (s = t(e[r], r, n)) && o.push(s);
                else
                    for (r in e) null != (s = t(e[r], r, n)) && o.push(s);
                return K.apply([], o)
            },
            guid: 1,
            proxy: function(e, t) {
                var n, i, s;
                if ("string" == typeof t && (n = e[t], t = e, e = n), oe.isFunction(e)) return i = Q.call(arguments, 2), (s = function() {
                    return e.apply(t || this, i.concat(Q.call(arguments)))
                }).guid = e.guid = e.guid || oe.guid++, s
            },
            now: Date.now,
            support: re
        }), "function" == typeof Symbol && (oe.fn[Symbol.iterator] = V[Symbol.iterator]), oe.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
            ee["[object " + t + "]"] = t.toLowerCase()
        });
        var de = function(n) {
            function w(e, t, n, i) {
                var s, r, o, a, l, c, d, u = t && t.ownerDocument,
                    h = t ? t.nodeType : 9;
                if (n = n || [], "string" != typeof e || !e || 1 !== h && 9 !== h && 11 !== h) return n;
                if (!i && ((t ? t.ownerDocument || t : z) !== A && M(t), t = t || A, I)) {
                    if (11 !== h && (l = me.exec(e)))
                        if (s = l[1]) {
                            if (9 === h) {
                                if (!(o = t.getElementById(s))) return n;
                                if (o.id === s) return n.push(o), n
                            } else if (u && (o = u.getElementById(s)) && j(t, o) && o.id === s) return n.push(o), n
                        } else {
                            if (l[2]) return Q.apply(n, t.getElementsByTagName(e)), n;
                            if ((s = l[3]) && v.getElementsByClassName && t.getElementsByClassName) return Q.apply(n, t.getElementsByClassName(s)), n
                        } if (v.qsa && !W[e + " "] && (!P || !P.test(e))) {
                        if (1 !== h) u = t, d = e;
                        else if ("object" !== t.nodeName.toLowerCase()) {
                            for ((a = t.getAttribute("id")) ? a = a.replace(ye, be) : t.setAttribute("id", a = H), r = (c = S(e)).length; r--;) c[r] = "#" + a + " " + f(c[r]);
                            d = c.join(","), u = ge.test(e) && p(t.parentNode) || t
                        }
                        if (d) try {
                            return Q.apply(n, u.querySelectorAll(d)), n
                        } catch (e) {} finally {
                            a === H && t.removeAttribute("id")
                        }
                    }
                }
                return _(e.replace(re, "$1"), t, n, i)
            }

            function e() {
                var i = [];
                return function e(t, n) {
                    return i.push(t + " ") > T.cacheLength && delete e[i.shift()], e[t + " "] = n
                }
            }

            function l(e) {
                return e[H] = !0, e
            }

            function s(e) {
                var t = A.createElement("fieldset");
                try {
                    return !!e(t)
                } catch (e) {
                    return !1
                } finally {
                    t.parentNode && t.parentNode.removeChild(t), t = null
                }
            }

            function t(e, t) {
                for (var n = e.split("|"), i = n.length; i--;) T.attrHandle[n[i]] = t
            }

            function c(e, t) {
                var n = t && e,
                    i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
                if (i) return i;
                if (n)
                    for (; n = n.nextSibling;)
                        if (n === t) return -1;
                return e ? 1 : -1
            }

            function i(t) {
                return function(e) {
                    return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && we(e) === t : e.disabled === t : "label" in e && e.disabled === t
                }
            }

            function r(o) {
                return l(function(r) {
                    return r = +r, l(function(e, t) {
                        for (var n, i = o([], e.length, r), s = i.length; s--;) e[n = i[s]] && (e[n] = !(t[n] = e[n]))
                    })
                })
            }

            function p(e) {
                return e && void 0 !== e.getElementsByTagName && e
            }

            function o() {}

            function f(e) {
                for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
                return i
            }

            function u(a, e, t) {
                var l = e.dir,
                    c = e.next,
                    d = c || l,
                    u = t && "parentNode" === d,
                    h = F++;
                return e.first ? function(e, t, n) {
                    for (; e = e[l];)
                        if (1 === e.nodeType || u) return a(e, t, n);
                    return !1
                } : function(e, t, n) {
                    var i, s, r, o = [q, h];
                    if (n) {
                        for (; e = e[l];)
                            if ((1 === e.nodeType || u) && a(e, t, n)) return !0
                    } else
                        for (; e = e[l];)
                            if (1 === e.nodeType || u)
                                if (s = (r = e[H] || (e[H] = {}))[e.uniqueID] || (r[e.uniqueID] = {}), c && c === e.nodeName.toLowerCase()) e = e[l] || e;
                                else {
                                    if ((i = s[d]) && i[0] === q && i[1] === h) return o[2] = i[2];
                                    if ((s[d] = o)[2] = a(e, t, n)) return !0
                                } return !1
                }
            }

            function h(s) {
                return 1 < s.length ? function(e, t, n) {
                    for (var i = s.length; i--;)
                        if (!s[i](e, t, n)) return !1;
                    return !0
                } : s[0]
            }

            function x(e, t, n, i, s) {
                for (var r, o = [], a = 0, l = e.length, c = null != t; a < l; a++)(r = e[a]) && (n && !n(r, i, s) || (o.push(r), c && t.push(a)));
                return o
            }

            function y(p, f, m, g, v, e) {
                return g && !g[H] && (g = y(g)), v && !v[H] && (v = y(v, e)), l(function(e, t, n, i) {
                    var s, r, o, a = [],
                        l = [],
                        c = t.length,
                        d = e || function(e, t, n) {
                            for (var i = 0, s = t.length; i < s; i++) w(e, t[i], n);
                            return n
                        }(f || "*", n.nodeType ? [n] : n, []),
                        u = !p || !e && f ? d : x(d, a, p, n, i),
                        h = m ? v || (e ? p : c || g) ? [] : t : u;
                    if (m && m(u, h, n, i), g)
                        for (s = x(h, l), g(s, [], n, i), r = s.length; r--;)(o = s[r]) && (h[l[r]] = !(u[l[r]] = o));
                    if (e) {
                        if (v || p) {
                            if (v) {
                                for (s = [], r = h.length; r--;)(o = h[r]) && s.push(u[r] = o);
                                v(null, h = [], s, i)
                            }
                            for (r = h.length; r--;)(o = h[r]) && -1 < (s = v ? Z(e, o) : a[r]) && (e[s] = !(t[s] = o))
                        }
                    } else h = x(h === t ? h.splice(c, h.length) : h), v ? v(null, t, h, i) : Q.apply(t, h)
                })
            }

            function m(e) {
                for (var s, t, n, i = e.length, r = T.relative[e[0].type], o = r || T.relative[" "], a = r ? 1 : 0, l = u(function(e) {
                        return e === s
                    }, o, !0), c = u(function(e) {
                        return -1 < Z(s, e)
                    }, o, !0), d = [function(e, t, n) {
                        var i = !r && (n || t !== k) || ((s = t).nodeType ? l(e, t, n) : c(e, t, n));
                        return s = null, i
                    }]; a < i; a++)
                    if (t = T.relative[e[a].type]) d = [u(h(d), t)];
                    else {
                        if ((t = T.filter[e[a].type].apply(null, e[a].matches))[H]) {
                            for (n = ++a; n < i && !T.relative[e[n].type]; n++);
                            return y(1 < a && h(d), 1 < a && f(e.slice(0, a - 1).concat({
                                value: " " === e[a - 2].type ? "*" : ""
                            })).replace(re, "$1"), t, a < n && m(e.slice(a, n)), n < i && m(e = e.slice(n)), n < i && f(e))
                        }
                        d.push(t)
                    } return h(d)
            }

            function g(e, t, n) {
                var i = "0x" + t - 65536;
                return i != i || n ? t : i < 0 ? String.fromCharCode(65536 + i) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            }

            function a() {
                M()
            }
            var d, v, T, b, C, S, E, _, k, D, $, M, A, O, I, P, L, N, j, H = "sizzle" + 1 * new Date,
                z = n.document,
                q = 0,
                F = 0,
                R = e(),
                Y = e(),
                W = e(),
                B = function(e, t) {
                    return e === t && ($ = !0), 0
                },
                U = {}.hasOwnProperty,
                V = [],
                G = V.pop,
                X = V.push,
                Q = V.push,
                K = V.slice,
                Z = function(e, t) {
                    for (var n = 0, i = e.length; n < i; n++)
                        if (e[n] === t) return n;
                    return -1
                },
                J = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                ee = "[\\x20\\t\\r\\n\\f]",
                te = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
                ne = "\\[" + ee + "*(" + te + ")(?:" + ee + "*([*^$|!~]?=)" + ee + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + te + "))|)" + ee + "*\\]",
                ie = ":(" + te + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ne + ")*)|.*)\\)|)",
                se = new RegExp(ee + "+", "g"),
                re = new RegExp("^" + ee + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ee + "+$", "g"),
                oe = new RegExp("^" + ee + "*," + ee + "*"),
                ae = new RegExp("^" + ee + "*([>+~]|" + ee + ")" + ee + "*"),
                le = new RegExp("=" + ee + "*([^\\]'\"]*?)" + ee + "*\\]", "g"),
                ce = new RegExp(ie),
                de = new RegExp("^" + te + "$"),
                ue = {
                    ID: new RegExp("^#(" + te + ")"),
                    CLASS: new RegExp("^\\.(" + te + ")"),
                    TAG: new RegExp("^(" + te + "|[*])"),
                    ATTR: new RegExp("^" + ne),
                    PSEUDO: new RegExp("^" + ie),
                    CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ee + "*(even|odd|(([+-]|)(\\d*)n|)" + ee + "*(?:([+-]|)" + ee + "*(\\d+)|))" + ee + "*\\)|)", "i"),
                    bool: new RegExp("^(?:" + J + ")$", "i"),
                    needsContext: new RegExp("^" + ee + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ee + "*((?:-\\d)?\\d*)" + ee + "*\\)|)(?=[^-]|$)", "i")
                },
                he = /^(?:input|select|textarea|button)$/i,
                pe = /^h\d$/i,
                fe = /^[^{]+\{\s*\[native \w/,
                me = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                ge = /[+~]/,
                ve = new RegExp("\\\\([\\da-f]{1,6}" + ee + "?|(" + ee + ")|.)", "ig"),
                ye = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
                be = function(e, t) {
                    return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
                },
                we = u(function(e) {
                    return !0 === e.disabled && ("form" in e || "label" in e)
                }, {
                    dir: "parentNode",
                    next: "legend"
                });
            try {
                Q.apply(V = K.call(z.childNodes), z.childNodes), V[z.childNodes.length].nodeType
            } catch (n) {
                Q = {
                    apply: V.length ? function(e, t) {
                        X.apply(e, K.call(t))
                    } : function(e, t) {
                        for (var n = e.length, i = 0; e[n++] = t[i++];);
                        e.length = n - 1
                    }
                }
            }
            for (d in v = w.support = {}, C = w.isXML = function(e) {
                    var t = e && (e.ownerDocument || e).documentElement;
                    return !!t && "HTML" !== t.nodeName
                }, M = w.setDocument = function(e) {
                    var t, n, i = e ? e.ownerDocument || e : z;
                    return i !== A && 9 === i.nodeType && i.documentElement && (O = (A = i).documentElement, I = !C(A), z !== A && (n = A.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", a, !1) : n.attachEvent && n.attachEvent("onunload", a)), v.attributes = s(function(e) {
                        return e.className = "i", !e.getAttribute("className")
                    }), v.getElementsByTagName = s(function(e) {
                        return e.appendChild(A.createComment("")), !e.getElementsByTagName("*").length
                    }), v.getElementsByClassName = fe.test(A.getElementsByClassName), v.getById = s(function(e) {
                        return O.appendChild(e).id = H, !A.getElementsByName || !A.getElementsByName(H).length
                    }), v.getById ? (T.filter.ID = function(e) {
                        var t = e.replace(ve, g);
                        return function(e) {
                            return e.getAttribute("id") === t
                        }
                    }, T.find.ID = function(e, t) {
                        if (void 0 !== t.getElementById && I) {
                            var n = t.getElementById(e);
                            return n ? [n] : []
                        }
                    }) : (T.filter.ID = function(e) {
                        var n = e.replace(ve, g);
                        return function(e) {
                            var t = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                            return t && t.value === n
                        }
                    }, T.find.ID = function(e, t) {
                        if (void 0 !== t.getElementById && I) {
                            var n, i, s, r = t.getElementById(e);
                            if (r) {
                                if ((n = r.getAttributeNode("id")) && n.value === e) return [r];
                                for (s = t.getElementsByName(e), i = 0; r = s[i++];)
                                    if ((n = r.getAttributeNode("id")) && n.value === e) return [r]
                            }
                            return []
                        }
                    }), T.find.TAG = v.getElementsByTagName ? function(e, t) {
                        return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : v.qsa ? t.querySelectorAll(e) : void 0
                    } : function(e, t) {
                        var n, i = [],
                            s = 0,
                            r = t.getElementsByTagName(e);
                        if ("*" !== e) return r;
                        for (; n = r[s++];) 1 === n.nodeType && i.push(n);
                        return i
                    }, T.find.CLASS = v.getElementsByClassName && function(e, t) {
                        if (void 0 !== t.getElementsByClassName && I) return t.getElementsByClassName(e)
                    }, L = [], P = [], (v.qsa = fe.test(A.querySelectorAll)) && (s(function(e) {
                        O.appendChild(e).innerHTML = "<a id='" + H + "'></a><select id='" + H + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && P.push("[*^$]=" + ee + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || P.push("\\[" + ee + "*(?:value|" + J + ")"), e.querySelectorAll("[id~=" + H + "-]").length || P.push("~="), e.querySelectorAll(":checked").length || P.push(":checked"), e.querySelectorAll("a#" + H + "+*").length || P.push(".#.+[+~]")
                    }), s(function(e) {
                        e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                        var t = A.createElement("input");
                        t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && P.push("name" + ee + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && P.push(":enabled", ":disabled"), O.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && P.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), P.push(",.*:")
                    })), (v.matchesSelector = fe.test(N = O.matches || O.webkitMatchesSelector || O.mozMatchesSelector || O.oMatchesSelector || O.msMatchesSelector)) && s(function(e) {
                        v.disconnectedMatch = N.call(e, "*"), N.call(e, "[s!='']:x"), L.push("!=", ie)
                    }), P = P.length && new RegExp(P.join("|")), L = L.length && new RegExp(L.join("|")), t = fe.test(O.compareDocumentPosition), j = t || fe.test(O.contains) ? function(e, t) {
                        var n = 9 === e.nodeType ? e.documentElement : e,
                            i = t && t.parentNode;
                        return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
                    } : function(e, t) {
                        if (t)
                            for (; t = t.parentNode;)
                                if (t === e) return !0;
                        return !1
                    }, B = t ? function(e, t) {
                        if (e === t) return $ = !0, 0;
                        var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                        return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !v.sortDetached && t.compareDocumentPosition(e) === n ? e === A || e.ownerDocument === z && j(z, e) ? -1 : t === A || t.ownerDocument === z && j(z, t) ? 1 : D ? Z(D, e) - Z(D, t) : 0 : 4 & n ? -1 : 1)
                    } : function(e, t) {
                        if (e === t) return $ = !0, 0;
                        var n, i = 0,
                            s = e.parentNode,
                            r = t.parentNode,
                            o = [e],
                            a = [t];
                        if (!s || !r) return e === A ? -1 : t === A ? 1 : s ? -1 : r ? 1 : D ? Z(D, e) - Z(D, t) : 0;
                        if (s === r) return c(e, t);
                        for (n = e; n = n.parentNode;) o.unshift(n);
                        for (n = t; n = n.parentNode;) a.unshift(n);
                        for (; o[i] === a[i];) i++;
                        return i ? c(o[i], a[i]) : o[i] === z ? -1 : a[i] === z ? 1 : 0
                    }), A
                }, w.matches = function(e, t) {
                    return w(e, null, null, t)
                }, w.matchesSelector = function(e, t) {
                    if ((e.ownerDocument || e) !== A && M(e), t = t.replace(le, "='$1']"), v.matchesSelector && I && !W[t + " "] && (!L || !L.test(t)) && (!P || !P.test(t))) try {
                        var n = N.call(e, t);
                        if (n || v.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
                    } catch (e) {}
                    return 0 < w(t, A, null, [e]).length
                }, w.contains = function(e, t) {
                    return (e.ownerDocument || e) !== A && M(e), j(e, t)
                }, w.attr = function(e, t) {
                    (e.ownerDocument || e) !== A && M(e);
                    var n = T.attrHandle[t.toLowerCase()],
                        i = n && U.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !I) : void 0;
                    return void 0 !== i ? i : v.attributes || !I ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
                }, w.escape = function(e) {
                    return (e + "").replace(ye, be)
                }, w.error = function(e) {
                    throw new Error("Syntax error, unrecognized expression: " + e)
                }, w.uniqueSort = function(e) {
                    var t, n = [],
                        i = 0,
                        s = 0;
                    if ($ = !v.detectDuplicates, D = !v.sortStable && e.slice(0), e.sort(B), $) {
                        for (; t = e[s++];) t === e[s] && (i = n.push(s));
                        for (; i--;) e.splice(n[i], 1)
                    }
                    return D = null, e
                }, b = w.getText = function(e) {
                    var t, n = "",
                        i = 0,
                        s = e.nodeType;
                    if (s) {
                        if (1 === s || 9 === s || 11 === s) {
                            if ("string" == typeof e.textContent) return e.textContent;
                            for (e = e.firstChild; e; e = e.nextSibling) n += b(e)
                        } else if (3 === s || 4 === s) return e.nodeValue
                    } else
                        for (; t = e[i++];) n += b(t);
                    return n
                }, (T = w.selectors = {
                    cacheLength: 50,
                    createPseudo: l,
                    match: ue,
                    attrHandle: {},
                    find: {},
                    relative: {
                        ">": {
                            dir: "parentNode",
                            first: !0
                        },
                        " ": {
                            dir: "parentNode"
                        },
                        "+": {
                            dir: "previousSibling",
                            first: !0
                        },
                        "~": {
                            dir: "previousSibling"
                        }
                    },
                    preFilter: {
                        ATTR: function(e) {
                            return e[1] = e[1].replace(ve, g), e[3] = (e[3] || e[4] || e[5] || "").replace(ve, g), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                        },
                        CHILD: function(e) {
                            return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || w.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && w.error(e[0]), e
                        },
                        PSEUDO: function(e) {
                            var t, n = !e[6] && e[2];
                            return ue.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && ce.test(n) && (t = S(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                        }
                    },
                    filter: {
                        TAG: function(e) {
                            var t = e.replace(ve, g).toLowerCase();
                            return "*" === e ? function() {
                                return !0
                            } : function(e) {
                                return e.nodeName && e.nodeName.toLowerCase() === t
                            }
                        },
                        CLASS: function(e) {
                            var t = R[e + " "];
                            return t || (t = new RegExp("(^|" + ee + ")" + e + "(" + ee + "|$)")) && R(e, function(e) {
                                return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                            })
                        },
                        ATTR: function(n, i, s) {
                            return function(e) {
                                var t = w.attr(e, n);
                                return null == t ? "!=" === i : !i || (t += "", "=" === i ? t === s : "!=" === i ? t !== s : "^=" === i ? s && 0 === t.indexOf(s) : "*=" === i ? s && -1 < t.indexOf(s) : "$=" === i ? s && t.slice(-s.length) === s : "~=" === i ? -1 < (" " + t.replace(se, " ") + " ").indexOf(s) : "|=" === i && (t === s || t.slice(0, s.length + 1) === s + "-"))
                            }
                        },
                        CHILD: function(f, e, t, m, g) {
                            var v = "nth" !== f.slice(0, 3),
                                y = "last" !== f.slice(-4),
                                b = "of-type" === e;
                            return 1 === m && 0 === g ? function(e) {
                                return !!e.parentNode
                            } : function(e, t, n) {
                                var i, s, r, o, a, l, c = v != y ? "nextSibling" : "previousSibling",
                                    d = e.parentNode,
                                    u = b && e.nodeName.toLowerCase(),
                                    h = !n && !b,
                                    p = !1;
                                if (d) {
                                    if (v) {
                                        for (; c;) {
                                            for (o = e; o = o[c];)
                                                if (b ? o.nodeName.toLowerCase() === u : 1 === o.nodeType) return !1;
                                            l = c = "only" === f && !l && "nextSibling"
                                        }
                                        return !0
                                    }
                                    if (l = [y ? d.firstChild : d.lastChild], y && h) {
                                        for (p = (a = (i = (s = (r = (o = d)[H] || (o[H] = {}))[o.uniqueID] || (r[o.uniqueID] = {}))[f] || [])[0] === q && i[1]) && i[2], o = a && d.childNodes[a]; o = ++a && o && o[c] || (p = a = 0) || l.pop();)
                                            if (1 === o.nodeType && ++p && o === e) {
                                                s[f] = [q, a, p];
                                                break
                                            }
                                    } else if (h && (p = a = (i = (s = (r = (o = e)[H] || (o[H] = {}))[o.uniqueID] || (r[o.uniqueID] = {}))[f] || [])[0] === q && i[1]), !1 === p)
                                        for (;
                                            (o = ++a && o && o[c] || (p = a = 0) || l.pop()) && ((b ? o.nodeName.toLowerCase() !== u : 1 !== o.nodeType) || !++p || (h && ((s = (r = o[H] || (o[H] = {}))[o.uniqueID] || (r[o.uniqueID] = {}))[f] = [q, p]), o !== e)););
                                    return (p -= g) === m || p % m == 0 && 0 <= p / m
                                }
                            }
                        },
                        PSEUDO: function(e, r) {
                            var t, o = T.pseudos[e] || T.setFilters[e.toLowerCase()] || w.error("unsupported pseudo: " + e);
                            return o[H] ? o(r) : 1 < o.length ? (t = [e, e, "", r], T.setFilters.hasOwnProperty(e.toLowerCase()) ? l(function(e, t) {
                                for (var n, i = o(e, r), s = i.length; s--;) e[n = Z(e, i[s])] = !(t[n] = i[s])
                            }) : function(e) {
                                return o(e, 0, t)
                            }) : o
                        }
                    },
                    pseudos: {
                        not: l(function(e) {
                            var i = [],
                                s = [],
                                a = E(e.replace(re, "$1"));
                            return a[H] ? l(function(e, t, n, i) {
                                for (var s, r = a(e, null, i, []), o = e.length; o--;)(s = r[o]) && (e[o] = !(t[o] = s))
                            }) : function(e, t, n) {
                                return i[0] = e, a(i, null, n, s), i[0] = null, !s.pop()
                            }
                        }),
                        has: l(function(t) {
                            return function(e) {
                                return 0 < w(t, e).length
                            }
                        }),
                        contains: l(function(t) {
                            return t = t.replace(ve, g),
                                function(e) {
                                    return -1 < (e.textContent || e.innerText || b(e)).indexOf(t)
                                }
                        }),
                        lang: l(function(n) {
                            return de.test(n || "") || w.error("unsupported lang: " + n), n = n.replace(ve, g).toLowerCase(),
                                function(e) {
                                    var t;
                                    do {
                                        if (t = I ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
                                    } while ((e = e.parentNode) && 1 === e.nodeType);
                                    return !1
                                }
                        }),
                        target: function(e) {
                            var t = n.location && n.location.hash;
                            return t && t.slice(1) === e.id
                        },
                        root: function(e) {
                            return e === O
                        },
                        focus: function(e) {
                            return e === A.activeElement && (!A.hasFocus || A.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                        },
                        enabled: i(!1),
                        disabled: i(!0),
                        checked: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && !!e.checked || "option" === t && !!e.selected
                        },
                        selected: function(e) {
                            return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                        },
                        empty: function(e) {
                            for (e = e.firstChild; e; e = e.nextSibling)
                                if (e.nodeType < 6) return !1;
                            return !0
                        },
                        parent: function(e) {
                            return !T.pseudos.empty(e)
                        },
                        header: function(e) {
                            return pe.test(e.nodeName)
                        },
                        input: function(e) {
                            return he.test(e.nodeName)
                        },
                        button: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && "button" === e.type || "button" === t
                        },
                        text: function(e) {
                            var t;
                            return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                        },
                        first: r(function() {
                            return [0]
                        }),
                        last: r(function(e, t) {
                            return [t - 1]
                        }),
                        eq: r(function(e, t, n) {
                            return [n < 0 ? n + t : n]
                        }),
                        even: r(function(e, t) {
                            for (var n = 0; n < t; n += 2) e.push(n);
                            return e
                        }),
                        odd: r(function(e, t) {
                            for (var n = 1; n < t; n += 2) e.push(n);
                            return e
                        }),
                        lt: r(function(e, t, n) {
                            for (var i = n < 0 ? n + t : n; 0 <= --i;) e.push(i);
                            return e
                        }),
                        gt: r(function(e, t, n) {
                            for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
                            return e
                        })
                    }
                }).pseudos.nth = T.pseudos.eq, {
                    radio: !0,
                    checkbox: !0,
                    file: !0,
                    password: !0,
                    image: !0
                }) T.pseudos[d] = function(t) {
                return function(e) {
                    return "input" === e.nodeName.toLowerCase() && e.type === t
                }
            }(d);
            for (d in {
                    submit: !0,
                    reset: !0
                }) T.pseudos[d] = function(n) {
                return function(e) {
                    var t = e.nodeName.toLowerCase();
                    return ("input" === t || "button" === t) && e.type === n
                }
            }(d);
            return o.prototype = T.filters = T.pseudos, T.setFilters = new o, S = w.tokenize = function(e, t) {
                var n, i, s, r, o, a, l, c = Y[e + " "];
                if (c) return t ? 0 : c.slice(0);
                for (o = e, a = [], l = T.preFilter; o;) {
                    for (r in n && !(i = oe.exec(o)) || (i && (o = o.slice(i[0].length) || o), a.push(s = [])), n = !1, (i = ae.exec(o)) && (n = i.shift(), s.push({
                            value: n,
                            type: i[0].replace(re, " ")
                        }), o = o.slice(n.length)), T.filter) !(i = ue[r].exec(o)) || l[r] && !(i = l[r](i)) || (n = i.shift(), s.push({
                        value: n,
                        type: r,
                        matches: i
                    }), o = o.slice(n.length));
                    if (!n) break
                }
                return t ? o.length : o ? w.error(e) : Y(e, a).slice(0)
            }, E = w.compile = function(e, t) {
                var n, i = [],
                    s = [],
                    r = W[e + " "];
                if (!r) {
                    for (n = (t = t || S(e)).length; n--;)(r = m(t[n]))[H] ? i.push(r) : s.push(r);
                    (r = W(e, function(g, v) {
                        function e(e, t, n, i, s) {
                            var r, o, a, l = 0,
                                c = "0",
                                d = e && [],
                                u = [],
                                h = k,
                                p = e || b && T.find.TAG("*", s),
                                f = q += null == h ? 1 : Math.random() || .1,
                                m = p.length;
                            for (s && (k = t === A || t || s); c !== m && null != (r = p[c]); c++) {
                                if (b && r) {
                                    for (o = 0, t || r.ownerDocument === A || (M(r), n = !I); a = g[o++];)
                                        if (a(r, t || A, n)) {
                                            i.push(r);
                                            break
                                        } s && (q = f)
                                }
                                y && ((r = !a && r) && l--, e && d.push(r))
                            }
                            if (l += c, y && c !== l) {
                                for (o = 0; a = v[o++];) a(d, u, t, n);
                                if (e) {
                                    if (0 < l)
                                        for (; c--;) d[c] || u[c] || (u[c] = G.call(i));
                                    u = x(u)
                                }
                                Q.apply(i, u), s && !e && 0 < u.length && 1 < l + v.length && w.uniqueSort(i)
                            }
                            return s && (q = f, k = h), d
                        }
                        var y = 0 < v.length,
                            b = 0 < g.length;
                        return y ? l(e) : e
                    }(s, i))).selector = e
                }
                return r
            }, _ = w.select = function(e, t, n, i) {
                var s, r, o, a, l, c = "function" == typeof e && e,
                    d = !i && S(e = c.selector || e);
                if (n = n || [], 1 === d.length) {
                    if (2 < (r = d[0] = d[0].slice(0)).length && "ID" === (o = r[0]).type && 9 === t.nodeType && I && T.relative[r[1].type]) {
                        if (!(t = (T.find.ID(o.matches[0].replace(ve, g), t) || [])[0])) return n;
                        c && (t = t.parentNode), e = e.slice(r.shift().value.length)
                    }
                    for (s = ue.needsContext.test(e) ? 0 : r.length; s-- && (o = r[s], !T.relative[a = o.type]);)
                        if ((l = T.find[a]) && (i = l(o.matches[0].replace(ve, g), ge.test(r[0].type) && p(t.parentNode) || t))) {
                            if (r.splice(s, 1), !(e = i.length && f(r))) return Q.apply(n, i), n;
                            break
                        }
                }
                return (c || E(e, d))(i, t, !I, n, !t || ge.test(e) && p(t.parentNode) || t), n
            }, v.sortStable = H.split("").sort(B).join("") === H, v.detectDuplicates = !!$, M(), v.sortDetached = s(function(e) {
                return 1 & e.compareDocumentPosition(A.createElement("fieldset"))
            }), s(function(e) {
                return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
            }) || t("type|href|height|width", function(e, t, n) {
                if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
            }), v.attributes && s(function(e) {
                return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
            }) || t("value", function(e, t, n) {
                if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
            }), s(function(e) {
                return null == e.getAttribute("disabled")
            }) || t(J, function(e, t, n) {
                var i;
                if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
            }), w
        }(C);
        oe.find = de, oe.expr = de.selectors, oe.expr[":"] = oe.expr.pseudos, oe.uniqueSort = oe.unique = de.uniqueSort, oe.text = de.getText, oe.isXMLDoc = de.isXML, oe.contains = de.contains, oe.escapeSelector = de.escape;

        function ue(e, t, n) {
            for (var i = [], s = void 0 !== n;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (s && oe(e).is(n)) break;
                    i.push(e)
                } return i
        }

        function he(e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        }
        var pe = oe.expr.match.needsContext,
            fe = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
            me = /^.[^:#\[\.,]*$/;
        oe.filter = function(e, t, n) {
            var i = t[0];
            return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? oe.find.matchesSelector(i, e) ? [i] : [] : oe.find.matches(e, oe.grep(t, function(e) {
                return 1 === e.nodeType
            }))
        }, oe.fn.extend({
            find: function(e) {
                var t, n, i = this.length,
                    s = this;
                if ("string" != typeof e) return this.pushStack(oe(e).filter(function() {
                    for (t = 0; t < i; t++)
                        if (oe.contains(s[t], this)) return !0
                }));
                for (n = this.pushStack([]), t = 0; t < i; t++) oe.find(e, s[t], n);
                return 1 < i ? oe.uniqueSort(n) : n
            },
            filter: function(e) {
                return this.pushStack(t(this, e || [], !1))
            },
            not: function(e) {
                return this.pushStack(t(this, e || [], !0))
            },
            is: function(e) {
                return !!t(this, "string" == typeof e && pe.test(e) ? oe(e) : e || [], !1).length
            }
        });
        var ge, ve = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
        (oe.fn.init = function(e, t, n) {
            var i, s;
            if (!e) return this;
            if (n = n || ge, "string" != typeof e) return e.nodeType ? (this[0] = e, this.length = 1, this) : oe.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(oe) : oe.makeArray(e, this);
            if (!(i = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : ve.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
            if (i[1]) {
                if (t = t instanceof oe ? t[0] : t, oe.merge(this, oe.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : G, !0)), fe.test(i[1]) && oe.isPlainObject(t))
                    for (i in t) oe.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                return this
            }
            return (s = G.getElementById(i[2])) && (this[0] = s, this.length = 1), this
        }).prototype = oe.fn, ge = oe(G);
        var ye = /^(?:parents|prev(?:Until|All))/,
            be = {
                children: !0,
                contents: !0,
                next: !0,
                prev: !0
            };
        oe.fn.extend({
            has: function(e) {
                var t = oe(e, this),
                    n = t.length;
                return this.filter(function() {
                    for (var e = 0; e < n; e++)
                        if (oe.contains(this, t[e])) return !0
                })
            },
            closest: function(e, t) {
                var n, i = 0,
                    s = this.length,
                    r = [],
                    o = "string" != typeof e && oe(e);
                if (!pe.test(e))
                    for (; i < s; i++)
                        for (n = this[i]; n && n !== t; n = n.parentNode)
                            if (n.nodeType < 11 && (o ? -1 < o.index(n) : 1 === n.nodeType && oe.find.matchesSelector(n, e))) {
                                r.push(n);
                                break
                            } return this.pushStack(1 < r.length ? oe.uniqueSort(r) : r)
            },
            index: function(e) {
                return e ? "string" == typeof e ? J.call(oe(e), this[0]) : J.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            },
            add: function(e, t) {
                return this.pushStack(oe.uniqueSort(oe.merge(this.get(), oe(e, t))))
            },
            addBack: function(e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
            }
        }), oe.each({
            parent: function(e) {
                var t = e.parentNode;
                return t && 11 !== t.nodeType ? t : null
            },
            parents: function(e) {
                return ue(e, "parentNode")
            },
            parentsUntil: function(e, t, n) {
                return ue(e, "parentNode", n)
            },
            next: function(e) {
                return n(e, "nextSibling")
            },
            prev: function(e) {
                return n(e, "previousSibling")
            },
            nextAll: function(e) {
                return ue(e, "nextSibling")
            },
            prevAll: function(e) {
                return ue(e, "previousSibling")
            },
            nextUntil: function(e, t, n) {
                return ue(e, "nextSibling", n)
            },
            prevUntil: function(e, t, n) {
                return ue(e, "previousSibling", n)
            },
            siblings: function(e) {
                return he((e.parentNode || {}).firstChild, e)
            },
            children: function(e) {
                return he(e.firstChild)
            },
            contents: function(e) {
                return c(e, "iframe") ? e.contentDocument : (c(e, "template") && (e = e.content || e), oe.merge([], e.childNodes))
            }
        }, function(i, s) {
            oe.fn[i] = function(e, t) {
                var n = oe.map(this, s, e);
                return "Until" !== i.slice(-5) && (t = e), t && "string" == typeof t && (n = oe.filter(t, n)), 1 < this.length && (be[i] || oe.uniqueSort(n), ye.test(i) && n.reverse()), this.pushStack(n)
            }
        });
        var we = /[^\x20\t\r\n\f]+/g;
        oe.Callbacks = function(i) {
            i = "string" == typeof i ? function(e) {
                var n = {};
                return oe.each(e.match(we) || [], function(e, t) {
                    n[t] = !0
                }), n
            }(i) : oe.extend({}, i);

            function n() {
                for (r = r || i.once, t = s = !0; a.length; l = -1)
                    for (e = a.shift(); ++l < o.length;) !1 === o[l].apply(e[0], e[1]) && i.stopOnFalse && (l = o.length, e = !1);
                i.memory || (e = !1), s = !1, r && (o = e ? [] : "")
            }
            var s, e, t, r, o = [],
                a = [],
                l = -1,
                c = {
                    add: function() {
                        return o && (e && !s && (l = o.length - 1, a.push(e)), function n(e) {
                            oe.each(e, function(e, t) {
                                oe.isFunction(t) ? i.unique && c.has(t) || o.push(t) : t && t.length && "string" !== oe.type(t) && n(t)
                            })
                        }(arguments), e && !s && n()), this
                    },
                    remove: function() {
                        return oe.each(arguments, function(e, t) {
                            for (var n; - 1 < (n = oe.inArray(t, o, n));) o.splice(n, 1), n <= l && l--
                        }), this
                    },
                    has: function(e) {
                        return e ? -1 < oe.inArray(e, o) : 0 < o.length
                    },
                    empty: function() {
                        return o = o && [], this
                    },
                    disable: function() {
                        return r = a = [], o = e = "", this
                    },
                    disabled: function() {
                        return !o
                    },
                    lock: function() {
                        return r = a = [], e || s || (o = e = ""), this
                    },
                    locked: function() {
                        return !!r
                    },
                    fireWith: function(e, t) {
                        return r || (t = [e, (t = t || []).slice ? t.slice() : t], a.push(t), s || n()), this
                    },
                    fire: function() {
                        return c.fireWith(this, arguments), this
                    },
                    fired: function() {
                        return !!t
                    }
                };
            return c
        }, oe.extend({
            Deferred: function(e) {
                var r = [
                        ["notify", "progress", oe.Callbacks("memory"), oe.Callbacks("memory"), 2],
                        ["resolve", "done", oe.Callbacks("once memory"), oe.Callbacks("once memory"), 0, "resolved"],
                        ["reject", "fail", oe.Callbacks("once memory"), oe.Callbacks("once memory"), 1, "rejected"]
                    ],
                    s = "pending",
                    o = {
                        state: function() {
                            return s
                        },
                        always: function() {
                            return a.done(arguments).fail(arguments), this
                        },
                        catch: function(e) {
                            return o.then(null, e)
                        },
                        pipe: function() {
                            var s = arguments;
                            return oe.Deferred(function(i) {
                                oe.each(r, function(e, t) {
                                    var n = oe.isFunction(s[t[4]]) && s[t[4]];
                                    a[t[1]](function() {
                                        var e = n && n.apply(this, arguments);
                                        e && oe.isFunction(e.promise) ? e.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[t[0] + "With"](this, n ? [e] : arguments)
                                    })
                                }), s = null
                            }).promise()
                        },
                        then: function(t, n, i) {
                            function l(s, r, o, a) {
                                return function() {
                                    function e() {
                                        var e, t;
                                        if (!(s < c)) {
                                            if ((e = o.apply(n, i)) === r.promise()) throw new TypeError("Thenable self-resolution");
                                            t = e && ("object" == typeof e || "function" == typeof e) && e.then, oe.isFunction(t) ? a ? t.call(e, l(c, r, d, a), l(c, r, u, a)) : (c++, t.call(e, l(c, r, d, a), l(c, r, u, a), l(c, r, d, r.notifyWith))) : (o !== d && (n = void 0, i = [e]), (a || r.resolveWith)(n, i))
                                        }
                                    }
                                    var n = this,
                                        i = arguments,
                                        t = a ? e : function() {
                                            try {
                                                e()
                                            } catch (e) {
                                                oe.Deferred.exceptionHook && oe.Deferred.exceptionHook(e, t.stackTrace), c <= s + 1 && (o !== u && (n = void 0, i = [e]), r.rejectWith(n, i))
                                            }
                                        };
                                    s ? t() : (oe.Deferred.getStackHook && (t.stackTrace = oe.Deferred.getStackHook()), C.setTimeout(t))
                                }
                            }
                            var c = 0;
                            return oe.Deferred(function(e) {
                                r[0][3].add(l(0, e, oe.isFunction(i) ? i : d, e.notifyWith)), r[1][3].add(l(0, e, oe.isFunction(t) ? t : d)), r[2][3].add(l(0, e, oe.isFunction(n) ? n : u))
                            }).promise()
                        },
                        promise: function(e) {
                            return null != e ? oe.extend(e, o) : o
                        }
                    },
                    a = {};
                return oe.each(r, function(e, t) {
                    var n = t[2],
                        i = t[5];
                    o[t[1]] = n.add, i && n.add(function() {
                        s = i
                    }, r[3 - e][2].disable, r[0][2].lock), n.add(t[3].fire), a[t[0]] = function() {
                        return a[t[0] + "With"](this === a ? void 0 : this, arguments), this
                    }, a[t[0] + "With"] = n.fireWith
                }), o.promise(a), e && e.call(a, a), a
            },
            when: function(e) {
                function t(t) {
                    return function(e) {
                        s[t] = this, r[t] = 1 < arguments.length ? Q.call(arguments) : e, --n || o.resolveWith(s, r)
                    }
                }
                var n = arguments.length,
                    i = n,
                    s = Array(i),
                    r = Q.call(arguments),
                    o = oe.Deferred();
                if (n <= 1 && (l(e, o.done(t(i)).resolve, o.reject, !n), "pending" === o.state() || oe.isFunction(r[i] && r[i].then))) return o.then();
                for (; i--;) l(r[i], t(i), o.reject);
                return o.promise()
            }
        });
        var xe = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
        oe.Deferred.exceptionHook = function(e, t) {
            C.console && C.console.warn && e && xe.test(e.name) && C.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
        }, oe.readyException = function(e) {
            C.setTimeout(function() {
                throw e
            })
        };
        var Te = oe.Deferred();
        oe.fn.ready = function(e) {
            return Te.then(e).catch(function(e) {
                oe.readyException(e)
            }), this
        }, oe.extend({
            isReady: !1,
            readyWait: 1,
            ready: function(e) {
                (!0 === e ? --oe.readyWait : oe.isReady) || ((oe.isReady = !0) !== e && 0 < --oe.readyWait || Te.resolveWith(G, [oe]))
            }
        }), oe.ready.then = Te.then, "complete" === G.readyState || "loading" !== G.readyState && !G.documentElement.doScroll ? C.setTimeout(oe.ready) : (G.addEventListener("DOMContentLoaded", i), C.addEventListener("load", i));

        function Ce(e) {
            return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
        }
        var Se = function(e, t, n, i, s, r, o) {
            var a = 0,
                l = e.length,
                c = null == n;
            if ("object" === oe.type(n))
                for (a in s = !0, n) Se(e, t, a, n[a], !0, r, o);
            else if (void 0 !== i && (s = !0, oe.isFunction(i) || (o = !0), c && (t = o ? (t.call(e, i), null) : (c = t, function(e, t, n) {
                    return c.call(oe(e), n)
                })), t))
                for (; a < l; a++) t(e[a], n, o ? i : i.call(e[a], a, t(e[a], n)));
            return s ? e : c ? t.call(e) : l ? t(e[0], n) : r
        };
        s.uid = 1, s.prototype = {
            cache: function(e) {
                var t = e[this.expando];
                return t || (t = {}, Ce(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                    value: t,
                    configurable: !0
                }))), t
            },
            set: function(e, t, n) {
                var i, s = this.cache(e);
                if ("string" == typeof t) s[oe.camelCase(t)] = n;
                else
                    for (i in t) s[oe.camelCase(i)] = t[i];
                return s
            },
            get: function(e, t) {
                return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][oe.camelCase(t)]
            },
            access: function(e, t, n) {
                return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
            },
            remove: function(e, t) {
                var n, i = e[this.expando];
                if (void 0 !== i) {
                    if (void 0 !== t) {
                        n = (t = Array.isArray(t) ? t.map(oe.camelCase) : (t = oe.camelCase(t)) in i ? [t] : t.match(we) || []).length;
                        for (; n--;) delete i[t[n]]
                    }
                    void 0 !== t && !oe.isEmptyObject(i) || (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
                }
            },
            hasData: function(e) {
                var t = e[this.expando];
                return void 0 !== t && !oe.isEmptyObject(t)
            }
        };
        var Ee = new s,
            _e = new s,
            ke = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            De = /[A-Z]/g;
        oe.extend({
            hasData: function(e) {
                return _e.hasData(e) || Ee.hasData(e)
            },
            data: function(e, t, n) {
                return _e.access(e, t, n)
            },
            removeData: function(e, t) {
                _e.remove(e, t)
            },
            _data: function(e, t, n) {
                return Ee.access(e, t, n)
            },
            _removeData: function(e, t) {
                Ee.remove(e, t)
            }
        }), oe.fn.extend({
            data: function(n, e) {
                var t, i, s, r = this[0],
                    o = r && r.attributes;
                if (void 0 !== n) return "object" == typeof n ? this.each(function() {
                    _e.set(this, n)
                }) : Se(this, function(e) {
                    var t;
                    if (r && void 0 === e) {
                        if (void 0 !== (t = _e.get(r, n))) return t;
                        if (void 0 !== (t = h(r, n))) return t
                    } else this.each(function() {
                        _e.set(this, n, e)
                    })
                }, null, e, 1 < arguments.length, null, !0);
                if (this.length && (s = _e.get(r), 1 === r.nodeType && !Ee.get(r, "hasDataAttrs"))) {
                    for (t = o.length; t--;) o[t] && 0 === (i = o[t].name).indexOf("data-") && (i = oe.camelCase(i.slice(5)), h(r, i, s[i]));
                    Ee.set(r, "hasDataAttrs", !0)
                }
                return s
            },
            removeData: function(e) {
                return this.each(function() {
                    _e.remove(this, e)
                })
            }
        }), oe.extend({
            queue: function(e, t, n) {
                var i;
                if (e) return t = (t || "fx") + "queue", i = Ee.get(e, t), n && (!i || Array.isArray(n) ? i = Ee.access(e, t, oe.makeArray(n)) : i.push(n)), i || []
            },
            dequeue: function(e, t) {
                t = t || "fx";
                var n = oe.queue(e, t),
                    i = n.length,
                    s = n.shift(),
                    r = oe._queueHooks(e, t);
                "inprogress" === s && (s = n.shift(), i--), s && ("fx" === t && n.unshift("inprogress"), delete r.stop, s.call(e, function() {
                    oe.dequeue(e, t)
                }, r)), !i && r && r.empty.fire()
            },
            _queueHooks: function(e, t) {
                var n = t + "queueHooks";
                return Ee.get(e, n) || Ee.access(e, n, {
                    empty: oe.Callbacks("once memory").add(function() {
                        Ee.remove(e, [t + "queue", n])
                    })
                })
            }
        }), oe.fn.extend({
            queue: function(t, n) {
                var e = 2;
                return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? oe.queue(this[0], t) : void 0 === n ? this : this.each(function() {
                    var e = oe.queue(this, t, n);
                    oe._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && oe.dequeue(this, t)
                })
            },
            dequeue: function(e) {
                return this.each(function() {
                    oe.dequeue(this, e)
                })
            },
            clearQueue: function(e) {
                return this.queue(e || "fx", [])
            },
            promise: function(e, t) {
                function n() {
                    --s || r.resolveWith(o, [o])
                }
                var i, s = 1,
                    r = oe.Deferred(),
                    o = this,
                    a = this.length;
                for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;)(i = Ee.get(o[a], e + "queueHooks")) && i.empty && (s++, i.empty.add(n));
                return n(), r.promise(t)
            }
        });

        function $e(e, t) {
            return "none" === (e = t || e).style.display || "" === e.style.display && oe.contains(e.ownerDocument, e) && "none" === oe.css(e, "display")
        }

        function Me(e, t, n, i) {
            var s, r, o = {};
            for (r in t) o[r] = e.style[r], e.style[r] = t[r];
            for (r in s = n.apply(e, i || []), t) e.style[r] = o[r];
            return s
        }
        var Ae = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            Oe = new RegExp("^(?:([+-])=|)(" + Ae + ")([a-z%]*)$", "i"),
            Ie = ["Top", "Right", "Bottom", "Left"],
            Pe = {};
        oe.fn.extend({
            show: function() {
                return v(this, !0)
            },
            hide: function() {
                return v(this)
            },
            toggle: function(e) {
                return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                    $e(this) ? oe(this).show() : oe(this).hide()
                })
            }
        });
        var Le = /^(?:checkbox|radio)$/i,
            Ne = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
            je = /^$|\/(?:java|ecma)script/i,
            He = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                thead: [1, "<table>", "</table>"],
                col: [2, "<table><colgroup>", "</colgroup></table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: [0, "", ""]
            };
        He.optgroup = He.option, He.tbody = He.tfoot = He.colgroup = He.caption = He.thead, He.th = He.td;
        var ze, qe, Fe = /<|&#?\w+;/;
        ze = G.createDocumentFragment().appendChild(G.createElement("div")), (qe = G.createElement("input")).setAttribute("type", "radio"), qe.setAttribute("checked", "checked"), qe.setAttribute("name", "t"), ze.appendChild(qe), re.checkClone = ze.cloneNode(!0).cloneNode(!0).lastChild.checked, ze.innerHTML = "<textarea>x</textarea>", re.noCloneChecked = !!ze.cloneNode(!0).lastChild.defaultValue;
        var Re = G.documentElement,
            Ye = /^key/,
            We = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
            Be = /^([^.]*)(?:\.(.+)|)/;
        oe.event = {
            global: {},
            add: function(t, e, n, i, s) {
                var r, o, a, l, c, d, u, h, p, f, m, g = Ee.get(t);
                if (g)
                    for (n.handler && (n = (r = n).handler, s = r.selector), s && oe.find.matchesSelector(Re, s), n.guid || (n.guid = oe.guid++), (l = g.events) || (l = g.events = {}), (o = g.handle) || (o = g.handle = function(e) {
                            return void 0 !== oe && oe.event.triggered !== e.type ? oe.event.dispatch.apply(t, arguments) : void 0
                        }), c = (e = (e || "").match(we) || [""]).length; c--;) p = m = (a = Be.exec(e[c]) || [])[1], f = (a[2] || "").split(".").sort(), p && (u = oe.event.special[p] || {}, p = (s ? u.delegateType : u.bindType) || p, u = oe.event.special[p] || {}, d = oe.extend({
                        type: p,
                        origType: m,
                        data: i,
                        handler: n,
                        guid: n.guid,
                        selector: s,
                        needsContext: s && oe.expr.match.needsContext.test(s),
                        namespace: f.join(".")
                    }, r), (h = l[p]) || ((h = l[p] = []).delegateCount = 0, u.setup && !1 !== u.setup.call(t, i, f, o) || t.addEventListener && t.addEventListener(p, o)), u.add && (u.add.call(t, d), d.handler.guid || (d.handler.guid = n.guid)), s ? h.splice(h.delegateCount++, 0, d) : h.push(d), oe.event.global[p] = !0)
            },
            remove: function(e, t, n, i, s) {
                var r, o, a, l, c, d, u, h, p, f, m, g = Ee.hasData(e) && Ee.get(e);
                if (g && (l = g.events)) {
                    for (c = (t = (t || "").match(we) || [""]).length; c--;)
                        if (p = m = (a = Be.exec(t[c]) || [])[1], f = (a[2] || "").split(".").sort(), p) {
                            for (u = oe.event.special[p] || {}, h = l[p = (i ? u.delegateType : u.bindType) || p] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), o = r = h.length; r--;) d = h[r], !s && m !== d.origType || n && n.guid !== d.guid || a && !a.test(d.namespace) || i && i !== d.selector && ("**" !== i || !d.selector) || (h.splice(r, 1), d.selector && h.delegateCount--, u.remove && u.remove.call(e, d));
                            o && !h.length && (u.teardown && !1 !== u.teardown.call(e, f, g.handle) || oe.removeEvent(e, p, g.handle), delete l[p])
                        } else
                            for (p in l) oe.event.remove(e, p + t[c], n, i, !0);
                    oe.isEmptyObject(l) && Ee.remove(e, "handle events")
                }
            },
            dispatch: function(e) {
                var t, n, i, s, r, o, a = oe.event.fix(e),
                    l = new Array(arguments.length),
                    c = (Ee.get(this, "events") || {})[a.type] || [],
                    d = oe.event.special[a.type] || {};
                for (l[0] = a, t = 1; t < arguments.length; t++) l[t] = arguments[t];
                if (a.delegateTarget = this, !d.preDispatch || !1 !== d.preDispatch.call(this, a)) {
                    for (o = oe.event.handlers.call(this, a, c), t = 0;
                        (s = o[t++]) && !a.isPropagationStopped();)
                        for (a.currentTarget = s.elem, n = 0;
                            (r = s.handlers[n++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (i = ((oe.event.special[r.origType] || {}).handle || r.handler).apply(s.elem, l)) && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
                    return d.postDispatch && d.postDispatch.call(this, a), a.result
                }
            },
            handlers: function(e, t) {
                var n, i, s, r, o, a = [],
                    l = t.delegateCount,
                    c = e.target;
                if (l && c.nodeType && !("click" === e.type && 1 <= e.button))
                    for (; c !== this; c = c.parentNode || this)
                        if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
                            for (r = [], o = {}, n = 0; n < l; n++) void 0 === o[s = (i = t[n]).selector + " "] && (o[s] = i.needsContext ? -1 < oe(s, this).index(c) : oe.find(s, this, null, [c]).length), o[s] && r.push(i);
                            r.length && a.push({
                                elem: c,
                                handlers: r
                            })
                        } return c = this, l < t.length && a.push({
                    elem: c,
                    handlers: t.slice(l)
                }), a
            },
            addProp: function(t, e) {
                Object.defineProperty(oe.Event.prototype, t, {
                    enumerable: !0,
                    configurable: !0,
                    get: oe.isFunction(e) ? function() {
                        if (this.originalEvent) return e(this.originalEvent)
                    } : function() {
                        if (this.originalEvent) return this.originalEvent[t]
                    },
                    set: function(e) {
                        Object.defineProperty(this, t, {
                            enumerable: !0,
                            configurable: !0,
                            writable: !0,
                            value: e
                        })
                    }
                })
            },
            fix: function(e) {
                return e[oe.expando] ? e : new oe.Event(e)
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function() {
                        if (this !== o() && this.focus) return this.focus(), !1
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        if (this === o() && this.blur) return this.blur(), !1
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        if ("checkbox" === this.type && this.click && c(this, "input")) return this.click(), !1
                    },
                    _default: function(e) {
                        return c(e.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function(e) {
                        void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                    }
                }
            }
        }, oe.removeEvent = function(e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n)
        }, oe.Event = function(e, t) {
            return this instanceof oe.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? r : f, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && oe.extend(this, t), this.timeStamp = e && e.timeStamp || oe.now(), void(this[oe.expando] = !0)) : new oe.Event(e, t)
        }, oe.Event.prototype = {
            constructor: oe.Event,
            isDefaultPrevented: f,
            isPropagationStopped: f,
            isImmediatePropagationStopped: f,
            isSimulated: !1,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = r, e && !this.isSimulated && e.preventDefault()
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = r, e && !this.isSimulated && e.stopPropagation()
            },
            stopImmediatePropagation: function() {
                var e = this.originalEvent;
                this.isImmediatePropagationStopped = r, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
            }
        }, oe.each({
            altKey: !0,
            bubbles: !0,
            cancelable: !0,
            changedTouches: !0,
            ctrlKey: !0,
            detail: !0,
            eventPhase: !0,
            metaKey: !0,
            pageX: !0,
            pageY: !0,
            shiftKey: !0,
            view: !0,
            char: !0,
            charCode: !0,
            key: !0,
            keyCode: !0,
            button: !0,
            buttons: !0,
            clientX: !0,
            clientY: !0,
            offsetX: !0,
            offsetY: !0,
            pointerId: !0,
            pointerType: !0,
            screenX: !0,
            screenY: !0,
            targetTouches: !0,
            toElement: !0,
            touches: !0,
            which: function(e) {
                var t = e.button;
                return null == e.which && Ye.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && We.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
            }
        }, oe.event.addProp), oe.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout",
            pointerenter: "pointerover",
            pointerleave: "pointerout"
        }, function(e, s) {
            oe.event.special[e] = {
                delegateType: s,
                bindType: s,
                handle: function(e) {
                    var t, n = e.relatedTarget,
                        i = e.handleObj;
                    return n && (n === this || oe.contains(this, n)) || (e.type = i.origType, t = i.handler.apply(this, arguments), e.type = s), t
                }
            }
        }), oe.fn.extend({
            on: function(e, t, n, i) {
                return w(this, e, t, n, i)
            },
            one: function(e, t, n, i) {
                return w(this, e, t, n, i, 1)
            },
            off: function(e, t, n) {
                var i, s;
                if (e && e.preventDefault && e.handleObj) return i = e.handleObj, oe(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
                if ("object" != typeof e) return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = f), this.each(function() {
                    oe.event.remove(this, e, n, t)
                });
                for (s in e) this.off(s, t, e[s]);
                return this
            }
        });
        var Ue = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
            Ve = /<script|<style|<link/i,
            Ge = /checked\s*(?:[^=]|=\s*.checked.)/i,
            Xe = /^true\/(.*)/,
            Qe = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
        oe.extend({
            htmlPrefilter: function(e) {
                return e.replace(Ue, "<$1></$2>")
            },
            clone: function(e, t, n) {
                var i, s, r, o, a, l, c, d = e.cloneNode(!0),
                    u = oe.contains(e.ownerDocument, e);
                if (!(re.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || oe.isXMLDoc(e)))
                    for (o = g(d), i = 0, s = (r = g(e)).length; i < s; i++) a = r[i], l = o[i], void 0, "input" === (c = l.nodeName.toLowerCase()) && Le.test(a.type) ? l.checked = a.checked : "input" !== c && "textarea" !== c || (l.defaultValue = a.defaultValue);
                if (t)
                    if (n)
                        for (r = r || g(e), o = o || g(d), i = 0, s = r.length; i < s; i++) E(r[i], o[i]);
                    else E(e, d);
                return 0 < (o = g(d, "script")).length && y(o, !u && g(e, "script")), d
            },
            cleanData: function(e) {
                for (var t, n, i, s = oe.event.special, r = 0; void 0 !== (n = e[r]); r++)
                    if (Ce(n)) {
                        if (t = n[Ee.expando]) {
                            if (t.events)
                                for (i in t.events) s[i] ? oe.event.remove(n, i) : oe.removeEvent(n, i, t.handle);
                            n[Ee.expando] = void 0
                        }
                        n[_e.expando] && (n[_e.expando] = void 0)
                    }
            }
        }), oe.fn.extend({
            detach: function(e) {
                return k(this, e, !0)
            },
            remove: function(e) {
                return k(this, e)
            },
            text: function(e) {
                return Se(this, function(e) {
                    return void 0 === e ? oe.text(this) : this.empty().each(function() {
                        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                    })
                }, null, e, arguments.length)
            },
            append: function() {
                return _(this, arguments, function(e) {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || x(this, e).appendChild(e)
                })
            },
            prepend: function() {
                return _(this, arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = x(this, e);
                        t.insertBefore(e, t.firstChild)
                    }
                })
            },
            before: function() {
                return _(this, arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this)
                })
            },
            after: function() {
                return _(this, arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                })
            },
            empty: function() {
                for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (oe.cleanData(g(e, !1)), e.textContent = "");
                return this
            },
            clone: function(e, t) {
                return e = null != e && e, t = null == t ? e : t, this.map(function() {
                    return oe.clone(this, e, t)
                })
            },
            html: function(e) {
                return Se(this, function(e) {
                    var t = this[0] || {},
                        n = 0,
                        i = this.length;
                    if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                    if ("string" == typeof e && !Ve.test(e) && !He[(Ne.exec(e) || ["", ""])[1].toLowerCase()]) {
                        e = oe.htmlPrefilter(e);
                        try {
                            for (; n < i; n++) 1 === (t = this[n] || {}).nodeType && (oe.cleanData(g(t, !1)), t.innerHTML = e);
                            t = 0
                        } catch (e) {}
                    }
                    t && this.empty().append(e)
                }, null, e, arguments.length)
            },
            replaceWith: function() {
                var n = [];
                return _(this, arguments, function(e) {
                    var t = this.parentNode;
                    oe.inArray(this, n) < 0 && (oe.cleanData(g(this)), t && t.replaceChild(e, this))
                }, n)
            }
        }), oe.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function(e, o) {
            oe.fn[e] = function(e) {
                for (var t, n = [], i = oe(e), s = i.length - 1, r = 0; r <= s; r++) t = r === s ? this : this.clone(!0), oe(i[r])[o](t), Z.apply(n, t.get());
                return this.pushStack(n)
            }
        });
        var Ke, Ze, Je, et, tt, nt, it = /^margin/,
            st = new RegExp("^(" + Ae + ")(?!px)[a-z%]+$", "i"),
            rt = function(e) {
                var t = e.ownerDocument.defaultView;
                return t && t.opener || (t = C), t.getComputedStyle(e)
            };

        function ot() {
            if (nt) {
                nt.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", nt.innerHTML = "", Re.appendChild(tt);
                var e = C.getComputedStyle(nt);
                Ke = "1%" !== e.top, et = "2px" === e.marginLeft, Ze = "4px" === e.width, nt.style.marginRight = "50%", Je = "4px" === e.marginRight, Re.removeChild(tt), nt = null
            }
        }
        tt = G.createElement("div"), (nt = G.createElement("div")).style && (nt.style.backgroundClip = "content-box", nt.cloneNode(!0).style.backgroundClip = "", re.clearCloneStyle = "content-box" === nt.style.backgroundClip, tt.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", tt.appendChild(nt), oe.extend(re, {
            pixelPosition: function() {
                return ot(), Ke
            },
            boxSizingReliable: function() {
                return ot(), Ze
            },
            pixelMarginRight: function() {
                return ot(), Je
            },
            reliableMarginLeft: function() {
                return ot(), et
            }
        }));
        var at = /^(none|table(?!-c[ea]).+)/,
            lt = /^--/,
            ct = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            },
            dt = {
                letterSpacing: "0",
                fontWeight: "400"
            },
            ut = ["Webkit", "Moz", "ms"],
            ht = G.createElement("div").style;
        oe.extend({
            cssHooks: {
                opacity: {
                    get: function(e, t) {
                        if (t) {
                            var n = D(e, "opacity");
                            return "" === n ? "1" : n
                        }
                    }
                }
            },
            cssNumber: {
                animationIterationCount: !0,
                columnCount: !0,
                fillOpacity: !0,
                flexGrow: !0,
                flexShrink: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {
                float: "cssFloat"
            },
            style: function(e, t, n, i) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var s, r, o, a = oe.camelCase(t),
                        l = lt.test(t),
                        c = e.style;
                    return l || (t = M(a)), o = oe.cssHooks[t] || oe.cssHooks[a], void 0 === n ? o && "get" in o && void 0 !== (s = o.get(e, !1, i)) ? s : c[t] : ("string" == (r = typeof n) && (s = Oe.exec(n)) && s[1] && (n = p(e, t, s), r = "number"), void(null != n && n == n && ("number" === r && (n += s && s[3] || (oe.cssNumber[a] ? "" : "px")), re.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), o && "set" in o && void 0 === (n = o.set(e, n, i)) || (l ? c.setProperty(t, n) : c[t] = n))))
                }
            },
            css: function(e, t, n, i) {
                var s, r, o, a = oe.camelCase(t);
                return lt.test(t) || (t = M(a)), (o = oe.cssHooks[t] || oe.cssHooks[a]) && "get" in o && (s = o.get(e, !0, n)), void 0 === s && (s = D(e, t, i)), "normal" === s && t in dt && (s = dt[t]), "" === n || n ? (r = parseFloat(s), !0 === n || isFinite(r) ? r || 0 : s) : s
            }
        }), oe.each(["height", "width"], function(e, o) {
            oe.cssHooks[o] = {
                get: function(e, t, n) {
                    if (t) return !at.test(oe.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? I(e, o, n) : Me(e, ct, function() {
                        return I(e, o, n)
                    })
                },
                set: function(e, t, n) {
                    var i, s = n && rt(e),
                        r = n && O(e, o, n, "border-box" === oe.css(e, "boxSizing", !1, s), s);
                    return r && (i = Oe.exec(t)) && "px" !== (i[3] || "px") && (e.style[o] = t, t = oe.css(e, o)), A(0, t, r)
                }
            }
        }), oe.cssHooks.marginLeft = $(re.reliableMarginLeft, function(e, t) {
            if (t) return (parseFloat(D(e, "marginLeft")) || e.getBoundingClientRect().left - Me(e, {
                marginLeft: 0
            }, function() {
                return e.getBoundingClientRect().left
            })) + "px"
        }), oe.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function(s, r) {
            oe.cssHooks[s + r] = {
                expand: function(e) {
                    for (var t = 0, n = {}, i = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[s + Ie[t] + r] = i[t] || i[t - 2] || i[0];
                    return n
                }
            }, it.test(s) || (oe.cssHooks[s + r].set = A)
        }), oe.fn.extend({
            css: function(e, t) {
                return Se(this, function(e, t, n) {
                    var i, s, r = {},
                        o = 0;
                    if (Array.isArray(t)) {
                        for (i = rt(e), s = t.length; o < s; o++) r[t[o]] = oe.css(e, t[o], !1, i);
                        return r
                    }
                    return void 0 !== n ? oe.style(e, t, n) : oe.css(e, t)
                }, e, t, 1 < arguments.length)
            }
        }), ((oe.Tween = P).prototype = {
            constructor: P,
            init: function(e, t, n, i, s, r) {
                this.elem = e, this.prop = n, this.easing = s || oe.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (oe.cssNumber[n] ? "" : "px")
            },
            cur: function() {
                var e = P.propHooks[this.prop];
                return e && e.get ? e.get(this) : P.propHooks._default.get(this)
            },
            run: function(e) {
                var t, n = P.propHooks[this.prop];
                return this.options.duration ? this.pos = t = oe.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : P.propHooks._default.set(this), this
            }
        }).init.prototype = P.prototype, (P.propHooks = {
            _default: {
                get: function(e) {
                    var t;
                    return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = oe.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
                },
                set: function(e) {
                    oe.fx.step[e.prop] ? oe.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[oe.cssProps[e.prop]] && !oe.cssHooks[e.prop] ? e.elem[e.prop] = e.now : oe.style(e.elem, e.prop, e.now + e.unit)
                }
            }
        }).scrollTop = P.propHooks.scrollLeft = {
            set: function(e) {
                e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
            }
        }, oe.easing = {
            linear: function(e) {
                return e
            },
            swing: function(e) {
                return .5 - Math.cos(e * Math.PI) / 2
            },
            _default: "swing"
        }, oe.fx = P.prototype.init, oe.fx.step = {};
        var pt, ft, mt, gt, vt = /^(?:toggle|show|hide)$/,
            yt = /queueHooks$/;
        oe.Animation = oe.extend(z, {
            tweeners: {
                "*": [function(e, t) {
                    var n = this.createTween(e, t);
                    return p(n.elem, e, Oe.exec(t), n), n
                }]
            },
            tweener: function(e, t) {
                for (var n, i = 0, s = (e = oe.isFunction(e) ? (t = e, ["*"]) : e.match(we)).length; i < s; i++) n = e[i], z.tweeners[n] = z.tweeners[n] || [], z.tweeners[n].unshift(t)
            },
            prefilters: [function(e, t, n) {
                var i, s, r, o, a, l, c, d, u = "width" in t || "height" in t,
                    h = this,
                    p = {},
                    f = e.style,
                    m = e.nodeType && $e(e),
                    g = Ee.get(e, "fxshow");
                for (i in n.queue || (null == (o = oe._queueHooks(e, "fx")).unqueued && (o.unqueued = 0, a = o.empty.fire, o.empty.fire = function() {
                        o.unqueued || a()
                    }), o.unqueued++, h.always(function() {
                        h.always(function() {
                            o.unqueued--, oe.queue(e, "fx").length || o.empty.fire()
                        })
                    })), t)
                    if (s = t[i], vt.test(s)) {
                        if (delete t[i], r = r || "toggle" === s, s === (m ? "hide" : "show")) {
                            if ("show" !== s || !g || void 0 === g[i]) continue;
                            m = !0
                        }
                        p[i] = g && g[i] || oe.style(e, i)
                    } if ((l = !oe.isEmptyObject(t)) || !oe.isEmptyObject(p))
                    for (i in u && 1 === e.nodeType && (n.overflow = [f.overflow, f.overflowX, f.overflowY], null == (c = g && g.display) && (c = Ee.get(e, "display")), "none" === (d = oe.css(e, "display")) && (c ? d = c : (v([e], !0), c = e.style.display || c, d = oe.css(e, "display"), v([e]))), ("inline" === d || "inline-block" === d && null != c) && "none" === oe.css(e, "float") && (l || (h.done(function() {
                            f.display = c
                        }), null == c && (d = f.display, c = "none" === d ? "" : d)), f.display = "inline-block")), n.overflow && (f.overflow = "hidden", h.always(function() {
                            f.overflow = n.overflow[0], f.overflowX = n.overflow[1], f.overflowY = n.overflow[2]
                        })), l = !1, p) l || (g ? "hidden" in g && (m = g.hidden) : g = Ee.access(e, "fxshow", {
                        display: c
                    }), r && (g.hidden = !m), m && v([e], !0), h.done(function() {
                        for (i in m || v([e]), Ee.remove(e, "fxshow"), p) oe.style(e, i, p[i])
                    })), l = H(m ? g[i] : 0, i, h), i in g || (g[i] = l.start, m && (l.end = l.start, l.start = 0))
            }],
            prefilter: function(e, t) {
                t ? z.prefilters.unshift(e) : z.prefilters.push(e)
            }
        }), oe.speed = function(e, t, n) {
            var i = e && "object" == typeof e ? oe.extend({}, e) : {
                complete: n || !n && t || oe.isFunction(e) && e,
                duration: e,
                easing: n && t || t && !oe.isFunction(t) && t
            };
            return oe.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in oe.fx.speeds ? i.duration = oe.fx.speeds[i.duration] : i.duration = oe.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function() {
                oe.isFunction(i.old) && i.old.call(this), i.queue && oe.dequeue(this, i.queue)
            }, i
        }, oe.fn.extend({
            fadeTo: function(e, t, n, i) {
                return this.filter($e).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, n, i)
            },
            animate: function(t, e, n, i) {
                function s() {
                    var e = z(this, oe.extend({}, t), o);
                    (r || Ee.get(this, "finish")) && e.stop(!0)
                }
                var r = oe.isEmptyObject(t),
                    o = oe.speed(e, n, i);
                return s.finish = s, r || !1 === o.queue ? this.each(s) : this.queue(o.queue, s)
            },
            stop: function(s, e, r) {
                function o(e) {
                    var t = e.stop;
                    delete e.stop, t(r)
                }
                return "string" != typeof s && (r = e, e = s, s = void 0), e && !1 !== s && this.queue(s || "fx", []), this.each(function() {
                    var e = !0,
                        t = null != s && s + "queueHooks",
                        n = oe.timers,
                        i = Ee.get(this);
                    if (t) i[t] && i[t].stop && o(i[t]);
                    else
                        for (t in i) i[t] && i[t].stop && yt.test(t) && o(i[t]);
                    for (t = n.length; t--;) n[t].elem !== this || null != s && n[t].queue !== s || (n[t].anim.stop(r), e = !1, n.splice(t, 1));
                    !e && r || oe.dequeue(this, s)
                })
            },
            finish: function(o) {
                return !1 !== o && (o = o || "fx"), this.each(function() {
                    var e, t = Ee.get(this),
                        n = t[o + "queue"],
                        i = t[o + "queueHooks"],
                        s = oe.timers,
                        r = n ? n.length : 0;
                    for (t.finish = !0, oe.queue(this, o, []), i && i.stop && i.stop.call(this, !0), e = s.length; e--;) s[e].elem === this && s[e].queue === o && (s[e].anim.stop(!0), s.splice(e, 1));
                    for (e = 0; e < r; e++) n[e] && n[e].finish && n[e].finish.call(this);
                    delete t.finish
                })
            }
        }), oe.each(["toggle", "show", "hide"], function(e, i) {
            var s = oe.fn[i];
            oe.fn[i] = function(e, t, n) {
                return null == e || "boolean" == typeof e ? s.apply(this, arguments) : this.animate(j(i, !0), e, t, n)
            }
        }), oe.each({
            slideDown: j("show"),
            slideUp: j("hide"),
            slideToggle: j("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, i) {
            oe.fn[e] = function(e, t, n) {
                return this.animate(i, e, t, n)
            }
        }), oe.timers = [], oe.fx.tick = function() {
            var e, t = 0,
                n = oe.timers;
            for (pt = oe.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
            n.length || oe.fx.stop(), pt = void 0
        }, oe.fx.timer = function(e) {
            oe.timers.push(e), oe.fx.start()
        }, oe.fx.interval = 13, oe.fx.start = function() {
            ft || (ft = !0, L())
        }, oe.fx.stop = function() {
            ft = null
        }, oe.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, oe.fn.delay = function(i, e) {
            return i = oe.fx && oe.fx.speeds[i] || i, e = e || "fx", this.queue(e, function(e, t) {
                var n = C.setTimeout(e, i);
                t.stop = function() {
                    C.clearTimeout(n)
                }
            })
        }, mt = G.createElement("input"), gt = G.createElement("select").appendChild(G.createElement("option")), mt.type = "checkbox", re.checkOn = "" !== mt.value, re.optSelected = gt.selected, (mt = G.createElement("input")).value = "t", mt.type = "radio", re.radioValue = "t" === mt.value;
        var bt, wt = oe.expr.attrHandle;
        oe.fn.extend({
            attr: function(e, t) {
                return Se(this, oe.attr, e, t, 1 < arguments.length)
            },
            removeAttr: function(e) {
                return this.each(function() {
                    oe.removeAttr(this, e)
                })
            }
        }), oe.extend({
            attr: function(e, t, n) {
                var i, s, r = e.nodeType;
                if (3 !== r && 8 !== r && 2 !== r) return void 0 === e.getAttribute ? oe.prop(e, t, n) : (1 === r && oe.isXMLDoc(e) || (s = oe.attrHooks[t.toLowerCase()] || (oe.expr.match.bool.test(t) ? bt : void 0)), void 0 !== n ? null === n ? void oe.removeAttr(e, t) : s && "set" in s && void 0 !== (i = s.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : s && "get" in s && null !== (i = s.get(e, t)) ? i : null == (i = oe.find.attr(e, t)) ? void 0 : i)
            },
            attrHooks: {
                type: {
                    set: function(e, t) {
                        if (!re.radioValue && "radio" === t && c(e, "input")) {
                            var n = e.value;
                            return e.setAttribute("type", t), n && (e.value = n), t
                        }
                    }
                }
            },
            removeAttr: function(e, t) {
                var n, i = 0,
                    s = t && t.match(we);
                if (s && 1 === e.nodeType)
                    for (; n = s[i++];) e.removeAttribute(n)
            }
        }), bt = {
            set: function(e, t, n) {
                return !1 === t ? oe.removeAttr(e, n) : e.setAttribute(n, n), n
            }
        }, oe.each(oe.expr.match.bool.source.match(/\w+/g), function(e, t) {
            var o = wt[t] || oe.find.attr;
            wt[t] = function(e, t, n) {
                var i, s, r = t.toLowerCase();
                return n || (s = wt[r], wt[r] = i, i = null != o(e, t, n) ? r : null, wt[r] = s), i
            }
        });
        var xt = /^(?:input|select|textarea|button)$/i,
            Tt = /^(?:a|area)$/i;
        oe.fn.extend({
            prop: function(e, t) {
                return Se(this, oe.prop, e, t, 1 < arguments.length)
            },
            removeProp: function(e) {
                return this.each(function() {
                    delete this[oe.propFix[e] || e]
                })
            }
        }), oe.extend({
            prop: function(e, t, n) {
                var i, s, r = e.nodeType;
                if (3 !== r && 8 !== r && 2 !== r) return 1 === r && oe.isXMLDoc(e) || (t = oe.propFix[t] || t, s = oe.propHooks[t]), void 0 !== n ? s && "set" in s && void 0 !== (i = s.set(e, n, t)) ? i : e[t] = n : s && "get" in s && null !== (i = s.get(e, t)) ? i : e[t]
            },
            propHooks: {
                tabIndex: {
                    get: function(e) {
                        var t = oe.find.attr(e, "tabindex");
                        return t ? parseInt(t, 10) : xt.test(e.nodeName) || Tt.test(e.nodeName) && e.href ? 0 : -1
                    }
                }
            },
            propFix: {
                for: "htmlFor",
                class: "className"
            }
        }), re.optSelected || (oe.propHooks.selected = {
            get: function(e) {
                var t = e.parentNode;
                return t && t.parentNode && t.parentNode.selectedIndex, null
            },
            set: function(e) {
                var t = e.parentNode;
                t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
            }
        }), oe.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
            oe.propFix[this.toLowerCase()] = this
        }), oe.fn.extend({
            addClass: function(t) {
                var e, n, i, s, r, o, a, l = 0;
                if (oe.isFunction(t)) return this.each(function(e) {
                    oe(this).addClass(t.call(this, e, F(this)))
                });
                if ("string" == typeof t && t)
                    for (e = t.match(we) || []; n = this[l++];)
                        if (s = F(n), i = 1 === n.nodeType && " " + q(s) + " ") {
                            for (o = 0; r = e[o++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                            s !== (a = q(i)) && n.setAttribute("class", a)
                        } return this
            },
            removeClass: function(t) {
                var e, n, i, s, r, o, a, l = 0;
                if (oe.isFunction(t)) return this.each(function(e) {
                    oe(this).removeClass(t.call(this, e, F(this)))
                });
                if (!arguments.length) return this.attr("class", "");
                if ("string" == typeof t && t)
                    for (e = t.match(we) || []; n = this[l++];)
                        if (s = F(n), i = 1 === n.nodeType && " " + q(s) + " ") {
                            for (o = 0; r = e[o++];)
                                for (; - 1 < i.indexOf(" " + r + " ");) i = i.replace(" " + r + " ", " ");
                            s !== (a = q(i)) && n.setAttribute("class", a)
                        } return this
            },
            toggleClass: function(s, t) {
                var r = typeof s;
                return "boolean" == typeof t && "string" == r ? t ? this.addClass(s) : this.removeClass(s) : oe.isFunction(s) ? this.each(function(e) {
                    oe(this).toggleClass(s.call(this, e, F(this), t), t)
                }) : this.each(function() {
                    var e, t, n, i;
                    if ("string" == r)
                        for (t = 0, n = oe(this), i = s.match(we) || []; e = i[t++];) n.hasClass(e) ? n.removeClass(e) : n.addClass(e);
                    else void 0 !== s && "boolean" != r || ((e = F(this)) && Ee.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === s ? "" : Ee.get(this, "__className__") || ""))
                })
            },
            hasClass: function(e) {
                var t, n, i = 0;
                for (t = " " + e + " "; n = this[i++];)
                    if (1 === n.nodeType && -1 < (" " + q(F(n)) + " ").indexOf(t)) return !0;
                return !1
            }
        });
        var Ct = /\r/g;
        oe.fn.extend({
            val: function(n) {
                var i, e, s, t = this[0];
                return arguments.length ? (s = oe.isFunction(n), this.each(function(e) {
                    var t;
                    1 === this.nodeType && (null == (t = s ? n.call(this, e, oe(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = oe.map(t, function(e) {
                        return null == e ? "" : e + ""
                    })), (i = oe.valHooks[this.type] || oe.valHooks[this.nodeName.toLowerCase()]) && "set" in i && void 0 !== i.set(this, t, "value") || (this.value = t))
                })) : t ? (i = oe.valHooks[t.type] || oe.valHooks[t.nodeName.toLowerCase()]) && "get" in i && void 0 !== (e = i.get(t, "value")) ? e : "string" == typeof(e = t.value) ? e.replace(Ct, "") : null == e ? "" : e : void 0
            }
        }), oe.extend({
            valHooks: {
                option: {
                    get: function(e) {
                        var t = oe.find.attr(e, "value");
                        return null != t ? t : q(oe.text(e))
                    }
                },
                select: {
                    get: function(e) {
                        var t, n, i, s = e.options,
                            r = e.selectedIndex,
                            o = "select-one" === e.type,
                            a = o ? null : [],
                            l = o ? r + 1 : s.length;
                        for (i = r < 0 ? l : o ? r : 0; i < l; i++)
                            if (((n = s[i]).selected || i === r) && !n.disabled && (!n.parentNode.disabled || !c(n.parentNode, "optgroup"))) {
                                if (t = oe(n).val(), o) return t;
                                a.push(t)
                            } return a
                    },
                    set: function(e, t) {
                        for (var n, i, s = e.options, r = oe.makeArray(t), o = s.length; o--;)((i = s[o]).selected = -1 < oe.inArray(oe.valHooks.option.get(i), r)) && (n = !0);
                        return n || (e.selectedIndex = -1), r
                    }
                }
            }
        }), oe.each(["radio", "checkbox"], function() {
            oe.valHooks[this] = {
                set: function(e, t) {
                    if (Array.isArray(t)) return e.checked = -1 < oe.inArray(oe(e).val(), t)
                }
            }, re.checkOn || (oe.valHooks[this].get = function(e) {
                return null === e.getAttribute("value") ? "on" : e.value
            })
        });
        var St = /^(?:focusinfocus|focusoutblur)$/;
        oe.extend(oe.event, {
            trigger: function(e, t, n, i) {
                var s, r, o, a, l, c, d, u = [n || G],
                    h = ne.call(e, "type") ? e.type : e,
                    p = ne.call(e, "namespace") ? e.namespace.split(".") : [];
                if (r = o = n = n || G, 3 !== n.nodeType && 8 !== n.nodeType && !St.test(h + oe.event.triggered) && (-1 < h.indexOf(".") && (h = (p = h.split(".")).shift(), p.sort()), l = h.indexOf(":") < 0 && "on" + h, (e = e[oe.expando] ? e : new oe.Event(h, "object" == typeof e && e)).isTrigger = i ? 2 : 3, e.namespace = p.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : oe.makeArray(t, [e]), d = oe.event.special[h] || {}, i || !d.trigger || !1 !== d.trigger.apply(n, t))) {
                    if (!i && !d.noBubble && !oe.isWindow(n)) {
                        for (a = d.delegateType || h, St.test(a + h) || (r = r.parentNode); r; r = r.parentNode) u.push(r), o = r;
                        o === (n.ownerDocument || G) && u.push(o.defaultView || o.parentWindow || C)
                    }
                    for (s = 0;
                        (r = u[s++]) && !e.isPropagationStopped();) e.type = 1 < s ? a : d.bindType || h, (c = (Ee.get(r, "events") || {})[e.type] && Ee.get(r, "handle")) && c.apply(r, t), (c = l && r[l]) && c.apply && Ce(r) && (e.result = c.apply(r, t), !1 === e.result && e.preventDefault());
                    return e.type = h, i || e.isDefaultPrevented() || d._default && !1 !== d._default.apply(u.pop(), t) || !Ce(n) || l && oe.isFunction(n[h]) && !oe.isWindow(n) && ((o = n[l]) && (n[l] = null), n[oe.event.triggered = h](), oe.event.triggered = void 0, o && (n[l] = o)), e.result
                }
            },
            simulate: function(e, t, n) {
                var i = oe.extend(new oe.Event, n, {
                    type: e,
                    isSimulated: !0
                });
                oe.event.trigger(i, null, t)
            }
        }), oe.fn.extend({
            trigger: function(e, t) {
                return this.each(function() {
                    oe.event.trigger(e, t, this)
                })
            },
            triggerHandler: function(e, t) {
                var n = this[0];
                if (n) return oe.event.trigger(e, t, n, !0)
            }
        }), oe.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, n) {
            oe.fn[n] = function(e, t) {
                return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
            }
        }), oe.fn.extend({
            hover: function(e, t) {
                return this.mouseenter(e).mouseleave(t || e)
            }
        }), re.focusin = "onfocusin" in C, re.focusin || oe.each({
            focus: "focusin",
            blur: "focusout"
        }, function(n, i) {
            function s(e) {
                oe.event.simulate(i, e.target, oe.event.fix(e))
            }
            oe.event.special[i] = {
                setup: function() {
                    var e = this.ownerDocument || this,
                        t = Ee.access(e, i);
                    t || e.addEventListener(n, s, !0), Ee.access(e, i, (t || 0) + 1)
                },
                teardown: function() {
                    var e = this.ownerDocument || this,
                        t = Ee.access(e, i) - 1;
                    t ? Ee.access(e, i, t) : (e.removeEventListener(n, s, !0), Ee.remove(e, i))
                }
            }
        });
        var Et = C.location,
            _t = oe.now(),
            kt = /\?/;
        oe.parseXML = function(e) {
            var t;
            if (!e || "string" != typeof e) return null;
            try {
                t = (new C.DOMParser).parseFromString(e, "text/xml")
            } catch (e) {
                t = void 0
            }
            return t && !t.getElementsByTagName("parsererror").length || oe.error("Invalid XML: " + e), t
        };
        var Dt = /\[\]$/,
            $t = /\r?\n/g,
            Mt = /^(?:submit|button|image|reset|file)$/i,
            At = /^(?:input|select|textarea|keygen)/i;
        oe.param = function(e, t) {
            function n(e, t) {
                var n = oe.isFunction(t) ? t() : t;
                s[s.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
            }
            var i, s = [];
            if (Array.isArray(e) || e.jquery && !oe.isPlainObject(e)) oe.each(e, function() {
                n(this.name, this.value)
            });
            else
                for (i in e) R(i, e[i], t, n);
            return s.join("&")
        }, oe.fn.extend({
            serialize: function() {
                return oe.param(this.serializeArray())
            },
            serializeArray: function() {
                return this.map(function() {
                    var e = oe.prop(this, "elements");
                    return e ? oe.makeArray(e) : this
                }).filter(function() {
                    var e = this.type;
                    return this.name && !oe(this).is(":disabled") && At.test(this.nodeName) && !Mt.test(e) && (this.checked || !Le.test(e))
                }).map(function(e, t) {
                    var n = oe(this).val();
                    return null == n ? null : Array.isArray(n) ? oe.map(n, function(e) {
                        return {
                            name: t.name,
                            value: e.replace($t, "\r\n")
                        }
                    }) : {
                        name: t.name,
                        value: n.replace($t, "\r\n")
                    }
                }).get()
            }
        });
        var Ot = /%20/g,
            It = /#.*$/,
            Pt = /([?&])_=[^&]*/,
            Lt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
            Nt = /^(?:GET|HEAD)$/,
            jt = /^\/\//,
            Ht = {},
            zt = {},
            qt = "*/".concat("*"),
            Ft = G.createElement("a");
        Ft.href = Et.href, oe.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: Et.href,
                type: "GET",
                isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Et.protocol),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": qt,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /\bxml\b/,
                    html: /\bhtml/,
                    json: /\bjson\b/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": JSON.parse,
                    "text xml": oe.parseXML
                },
                flatOptions: {
                    url: !0,
                    context: !0
                }
            },
            ajaxSetup: function(e, t) {
                return t ? B(B(e, oe.ajaxSettings), t) : B(oe.ajaxSettings, e)
            },
            ajaxPrefilter: Y(Ht),
            ajaxTransport: Y(zt),
            ajax: function(e, t) {
                function n(e, t, n, i) {
                    var s, r, o, a, l, c = t;
                    f || (f = !0, p && C.clearTimeout(p), d = void 0, h = i || "", T.readyState = 0 < e ? 4 : 0, s = 200 <= e && e < 300 || 304 === e, n && (a = function(e, t, n) {
                        for (var i, s, r, o, a = e.contents, l = e.dataTypes;
                            "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
                        if (i)
                            for (s in a)
                                if (a[s] && a[s].test(i)) {
                                    l.unshift(s);
                                    break
                                } if (l[0] in n) r = l[0];
                        else {
                            for (s in n) {
                                if (!l[0] || e.converters[s + " " + l[0]]) {
                                    r = s;
                                    break
                                }
                                o = o || s
                            }
                            r = r || o
                        }
                        if (r) return r !== l[0] && l.unshift(r), n[r]
                    }(g, T, n)), a = function(e, t, n, i) {
                        var s, r, o, a, l, c = {},
                            d = e.dataTypes.slice();
                        if (d[1])
                            for (o in e.converters) c[o.toLowerCase()] = e.converters[o];
                        for (r = d.shift(); r;)
                            if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = d.shift())
                                if ("*" === r) r = l;
                                else if ("*" !== l && l !== r) {
                            if (!(o = c[l + " " + r] || c["* " + r]))
                                for (s in c)
                                    if ((a = s.split(" "))[1] === r && (o = c[l + " " + a[0]] || c["* " + a[0]])) {
                                        !0 === o ? o = c[s] : !0 !== c[s] && (r = a[0], d.unshift(a[1]));
                                        break
                                    } if (!0 !== o)
                                if (o && e.throws) t = o(t);
                                else try {
                                    t = o(t)
                                } catch (e) {
                                    return {
                                        state: "parsererror",
                                        error: o ? e : "No conversion from " + l + " to " + r
                                    }
                                }
                        }
                        return {
                            state: "success",
                            data: t
                        }
                    }(g, a, T, s), s ? (g.ifModified && ((l = T.getResponseHeader("Last-Modified")) && (oe.lastModified[u] = l), (l = T.getResponseHeader("etag")) && (oe.etag[u] = l)), 204 === e || "HEAD" === g.type ? c = "nocontent" : 304 === e ? c = "notmodified" : (c = a.state, r = a.data, s = !(o = a.error))) : (o = c, !e && c || (c = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (t || c) + "", s ? b.resolveWith(v, [r, c, T]) : b.rejectWith(v, [T, c, o]), T.statusCode(x), x = void 0, m && y.trigger(s ? "ajaxSuccess" : "ajaxError", [T, g, s ? r : o]), w.fireWith(v, [T, c]), m && (y.trigger("ajaxComplete", [T, g]), --oe.active || oe.event.trigger("ajaxStop")))
                }
                "object" == typeof e && (t = e, e = void 0), t = t || {};
                var d, u, h, i, p, s, f, m, r, o, g = oe.ajaxSetup({}, t),
                    v = g.context || g,
                    y = g.context && (v.nodeType || v.jquery) ? oe(v) : oe.event,
                    b = oe.Deferred(),
                    w = oe.Callbacks("once memory"),
                    x = g.statusCode || {},
                    a = {},
                    l = {},
                    c = "canceled",
                    T = {
                        readyState: 0,
                        getResponseHeader: function(e) {
                            var t;
                            if (f) {
                                if (!i)
                                    for (i = {}; t = Lt.exec(h);) i[t[1].toLowerCase()] = t[2];
                                t = i[e.toLowerCase()]
                            }
                            return null == t ? null : t
                        },
                        getAllResponseHeaders: function() {
                            return f ? h : null
                        },
                        setRequestHeader: function(e, t) {
                            return null == f && (e = l[e.toLowerCase()] = l[e.toLowerCase()] || e, a[e] = t), this
                        },
                        overrideMimeType: function(e) {
                            return null == f && (g.mimeType = e), this
                        },
                        statusCode: function(e) {
                            var t;
                            if (e)
                                if (f) T.always(e[T.status]);
                                else
                                    for (t in e) x[t] = [x[t], e[t]];
                            return this
                        },
                        abort: function(e) {
                            var t = e || c;
                            return d && d.abort(t), n(0, t), this
                        }
                    };
                if (b.promise(T), g.url = ((e || g.url || Et.href) + "").replace(jt, Et.protocol + "//"), g.type = t.method || t.type || g.method || g.type, g.dataTypes = (g.dataType || "*").toLowerCase().match(we) || [""], null == g.crossDomain) {
                    s = G.createElement("a");
                    try {
                        s.href = g.url, s.href = s.href, g.crossDomain = Ft.protocol + "//" + Ft.host != s.protocol + "//" + s.host
                    } catch (e) {
                        g.crossDomain = !0
                    }
                }
                if (g.data && g.processData && "string" != typeof g.data && (g.data = oe.param(g.data, g.traditional)), W(Ht, g, t, T), f) return T;
                for (r in (m = oe.event && g.global) && 0 == oe.active++ && oe.event.trigger("ajaxStart"), g.type = g.type.toUpperCase(), g.hasContent = !Nt.test(g.type), u = g.url.replace(It, ""), g.hasContent ? g.data && g.processData && 0 === (g.contentType || "").indexOf("application/x-www-form-urlencoded") && (g.data = g.data.replace(Ot, "+")) : (o = g.url.slice(u.length), g.data && (u += (kt.test(u) ? "&" : "?") + g.data, delete g.data), !1 === g.cache && (u = u.replace(Pt, "$1"), o = (kt.test(u) ? "&" : "?") + "_=" + _t++ + o), g.url = u + o), g.ifModified && (oe.lastModified[u] && T.setRequestHeader("If-Modified-Since", oe.lastModified[u]), oe.etag[u] && T.setRequestHeader("If-None-Match", oe.etag[u])), (g.data && g.hasContent && !1 !== g.contentType || t.contentType) && T.setRequestHeader("Content-Type", g.contentType), T.setRequestHeader("Accept", g.dataTypes[0] && g.accepts[g.dataTypes[0]] ? g.accepts[g.dataTypes[0]] + ("*" !== g.dataTypes[0] ? ", " + qt + "; q=0.01" : "") : g.accepts["*"]), g.headers) T.setRequestHeader(r, g.headers[r]);
                if (g.beforeSend && (!1 === g.beforeSend.call(v, T, g) || f)) return T.abort();
                if (c = "abort", w.add(g.complete), T.done(g.success), T.fail(g.error), d = W(zt, g, t, T)) {
                    if (T.readyState = 1, m && y.trigger("ajaxSend", [T, g]), f) return T;
                    g.async && 0 < g.timeout && (p = C.setTimeout(function() {
                        T.abort("timeout")
                    }, g.timeout));
                    try {
                        f = !1, d.send(a, n)
                    } catch (e) {
                        if (f) throw e;
                        n(-1, e)
                    }
                } else n(-1, "No Transport");
                return T
            },
            getJSON: function(e, t, n) {
                return oe.get(e, t, n, "json")
            },
            getScript: function(e, t) {
                return oe.get(e, void 0, t, "script")
            }
        }), oe.each(["get", "post"], function(e, s) {
            oe[s] = function(e, t, n, i) {
                return oe.isFunction(t) && (i = i || n, n = t, t = void 0), oe.ajax(oe.extend({
                    url: e,
                    type: s,
                    dataType: i,
                    data: t,
                    success: n
                }, oe.isPlainObject(e) && e))
            }
        }), oe._evalUrl = function(e) {
            return oe.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                cache: !0,
                async: !1,
                global: !1,
                throws: !0
            })
        }, oe.fn.extend({
            wrapAll: function(e) {
                var t;
                return this[0] && (oe.isFunction(e) && (e = e.call(this[0])), t = oe(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                    return e
                }).append(this)), this
            },
            wrapInner: function(n) {
                return oe.isFunction(n) ? this.each(function(e) {
                    oe(this).wrapInner(n.call(this, e))
                }) : this.each(function() {
                    var e = oe(this),
                        t = e.contents();
                    t.length ? t.wrapAll(n) : e.append(n)
                })
            },
            wrap: function(t) {
                var n = oe.isFunction(t);
                return this.each(function(e) {
                    oe(this).wrapAll(n ? t.call(this, e) : t)
                })
            },
            unwrap: function(e) {
                return this.parent(e).not("body").each(function() {
                    oe(this).replaceWith(this.childNodes)
                }), this
            }
        }), oe.expr.pseudos.hidden = function(e) {
            return !oe.expr.pseudos.visible(e)
        }, oe.expr.pseudos.visible = function(e) {
            return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
        }, oe.ajaxSettings.xhr = function() {
            try {
                return new C.XMLHttpRequest
            } catch (e) {}
        };
        var Rt = {
                0: 200,
                1223: 204
            },
            Yt = oe.ajaxSettings.xhr();
        re.cors = !!Yt && "withCredentials" in Yt, re.ajax = Yt = !!Yt, oe.ajaxTransport(function(s) {
            var r, o;
            if (re.cors || Yt && !s.crossDomain) return {
                send: function(e, t) {
                    var n, i = s.xhr();
                    if (i.open(s.type, s.url, s.async, s.username, s.password), s.xhrFields)
                        for (n in s.xhrFields) i[n] = s.xhrFields[n];
                    for (n in s.mimeType && i.overrideMimeType && i.overrideMimeType(s.mimeType), s.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) i.setRequestHeader(n, e[n]);
                    r = function(e) {
                        return function() {
                            r && (r = o = i.onload = i.onerror = i.onabort = i.onreadystatechange = null, "abort" === e ? i.abort() : "error" === e ? "number" != typeof i.status ? t(0, "error") : t(i.status, i.statusText) : t(Rt[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {
                                binary: i.response
                            } : {
                                text: i.responseText
                            }, i.getAllResponseHeaders()))
                        }
                    }, i.onload = r(), o = i.onerror = r("error"), void 0 !== i.onabort ? i.onabort = o : i.onreadystatechange = function() {
                        4 === i.readyState && C.setTimeout(function() {
                            r && o()
                        })
                    }, r = r("abort");
                    try {
                        i.send(s.hasContent && s.data || null)
                    } catch (e) {
                        if (r) throw e
                    }
                },
                abort: function() {
                    r && r()
                }
            }
        }), oe.ajaxPrefilter(function(e) {
            e.crossDomain && (e.contents.script = !1)
        }), oe.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /\b(?:java|ecma)script\b/
            },
            converters: {
                "text script": function(e) {
                    return oe.globalEval(e), e
                }
            }
        }), oe.ajaxPrefilter("script", function(e) {
            void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
        }), oe.ajaxTransport("script", function(n) {
            var i, s;
            if (n.crossDomain) return {
                send: function(e, t) {
                    i = oe("<script>").prop({
                        charset: n.scriptCharset,
                        src: n.url
                    }).on("load error", s = function(e) {
                        i.remove(), s = null, e && t("error" === e.type ? 404 : 200, e.type)
                    }), G.head.appendChild(i[0])
                },
                abort: function() {
                    s && s()
                }
            }
        });
        var Wt, Bt = [],
            Ut = /(=)\?(?=&|$)|\?\?/;
        oe.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var e = Bt.pop() || oe.expando + "_" + _t++;
                return this[e] = !0, e
            }
        }), oe.ajaxPrefilter("json jsonp", function(e, t, n) {
            var i, s, r, o = !1 !== e.jsonp && (Ut.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Ut.test(e.data) && "data");
            if (o || "jsonp" === e.dataTypes[0]) return i = e.jsonpCallback = oe.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, o ? e[o] = e[o].replace(Ut, "$1" + i) : !1 !== e.jsonp && (e.url += (kt.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), e.converters["script json"] = function() {
                return r || oe.error(i + " was not called"), r[0]
            }, e.dataTypes[0] = "json", s = C[i], C[i] = function() {
                r = arguments
            }, n.always(function() {
                void 0 === s ? oe(C).removeProp(i) : C[i] = s, e[i] && (e.jsonpCallback = t.jsonpCallback, Bt.push(i)), r && oe.isFunction(s) && s(r[0]), r = s = void 0
            }), "script"
        }), re.createHTMLDocument = ((Wt = G.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Wt.childNodes.length), oe.parseHTML = function(e, t, n) {
            return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (re.createHTMLDocument ? ((i = (t = G.implementation.createHTMLDocument("")).createElement("base")).href = G.location.href, t.head.appendChild(i)) : t = G), r = !n && [], (s = fe.exec(e)) ? [t.createElement(s[1])] : (s = b([e], t, r), r && r.length && oe(r).remove(), oe.merge([], s.childNodes)));
            var i, s, r
        }, oe.fn.load = function(e, t, n) {
            var i, s, r, o = this,
                a = e.indexOf(" ");
            return -1 < a && (i = q(e.slice(a)), e = e.slice(0, a)), oe.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (s = "POST"), 0 < o.length && oe.ajax({
                url: e,
                type: s || "GET",
                dataType: "html",
                data: t
            }).done(function(e) {
                r = arguments, o.html(i ? oe("<div>").append(oe.parseHTML(e)).find(i) : e)
            }).always(n && function(e, t) {
                o.each(function() {
                    n.apply(this, r || [e.responseText, t, e])
                })
            }), this
        }, oe.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
            oe.fn[t] = function(e) {
                return this.on(t, e)
            }
        }), oe.expr.pseudos.animated = function(t) {
            return oe.grep(oe.timers, function(e) {
                return t === e.elem
            }).length
        }, oe.offset = {
            setOffset: function(e, t, n) {
                var i, s, r, o, a, l, c = oe.css(e, "position"),
                    d = oe(e),
                    u = {};
                "static" === c && (e.style.position = "relative"), a = d.offset(), r = oe.css(e, "top"), l = oe.css(e, "left"), s = ("absolute" === c || "fixed" === c) && -1 < (r + l).indexOf("auto") ? (o = (i = d.position()).top, i.left) : (o = parseFloat(r) || 0, parseFloat(l) || 0), oe.isFunction(t) && (t = t.call(e, n, oe.extend({}, a))), null != t.top && (u.top = t.top - a.top + o), null != t.left && (u.left = t.left - a.left + s), "using" in t ? t.using.call(e, u) : d.css(u)
            }
        }, oe.fn.extend({
            offset: function(t) {
                if (arguments.length) return void 0 === t ? this : this.each(function(e) {
                    oe.offset.setOffset(this, t, e)
                });
                var e, n, i, s, r = this[0];
                return r ? r.getClientRects().length ? (i = r.getBoundingClientRect(), n = (e = r.ownerDocument).documentElement, s = e.defaultView, {
                    top: i.top + s.pageYOffset - n.clientTop,
                    left: i.left + s.pageXOffset - n.clientLeft
                }) : {
                    top: 0,
                    left: 0
                } : void 0
            },
            position: function() {
                if (this[0]) {
                    var e, t, n = this[0],
                        i = {
                            top: 0,
                            left: 0
                        };
                    return "fixed" === oe.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), c(e[0], "html") || (i = e.offset()), i = {
                        top: i.top + oe.css(e[0], "borderTopWidth", !0),
                        left: i.left + oe.css(e[0], "borderLeftWidth", !0)
                    }), {
                        top: t.top - i.top - oe.css(n, "marginTop", !0),
                        left: t.left - i.left - oe.css(n, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    for (var e = this.offsetParent; e && "static" === oe.css(e, "position");) e = e.offsetParent;
                    return e || Re
                })
            }
        }), oe.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(t, s) {
            var r = "pageYOffset" === s;
            oe.fn[t] = function(e) {
                return Se(this, function(e, t, n) {
                    var i;
                    return oe.isWindow(e) ? i = e : 9 === e.nodeType && (i = e.defaultView), void 0 === n ? i ? i[s] : e[t] : void(i ? i.scrollTo(r ? i.pageXOffset : n, r ? n : i.pageYOffset) : e[t] = n)
                }, t, e, arguments.length)
            }
        }), oe.each(["top", "left"], function(e, n) {
            oe.cssHooks[n] = $(re.pixelPosition, function(e, t) {
                if (t) return t = D(e, n), st.test(t) ? oe(e).position()[n] + "px" : t
            })
        }), oe.each({
            Height: "height",
            Width: "width"
        }, function(o, a) {
            oe.each({
                padding: "inner" + o,
                content: a,
                "": "outer" + o
            }, function(i, r) {
                oe.fn[r] = function(e, t) {
                    var n = arguments.length && (i || "boolean" != typeof e),
                        s = i || (!0 === e || !0 === t ? "margin" : "border");
                    return Se(this, function(e, t, n) {
                        var i;
                        return oe.isWindow(e) ? 0 === r.indexOf("outer") ? e["inner" + o] : e.document.documentElement["client" + o] : 9 === e.nodeType ? (i = e.documentElement, Math.max(e.body["scroll" + o], i["scroll" + o], e.body["offset" + o], i["offset" + o], i["client" + o])) : void 0 === n ? oe.css(e, t, s) : oe.style(e, t, n, s)
                    }, a, n ? e : void 0, n)
                }
            })
        }), oe.fn.extend({
            bind: function(e, t, n) {
                return this.on(e, null, t, n)
            },
            unbind: function(e, t) {
                return this.off(e, null, t)
            },
            delegate: function(e, t, n, i) {
                return this.on(t, e, n, i)
            },
            undelegate: function(e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            }
        }), oe.holdReady = function(e) {
            e ? oe.readyWait++ : oe.ready(!0)
        }, oe.isArray = Array.isArray, oe.parseJSON = JSON.parse, oe.nodeName = c, "function" == typeof define && define.amd && define("jquery", [], function() {
            return oe
        });
        var Vt = C.jQuery,
            Gt = C.$;
        return oe.noConflict = function(e) {
            return C.$ === oe && (C.$ = Gt), e && C.jQuery === oe && (C.jQuery = Vt), oe
        }, e || (C.jQuery = C.$ = oe), oe
    }), void 0 === jQuery.migrateMute && (jQuery.migrateMute = !0), function(a, n) {
        "use strict";

        function l(e) {
            var t = n.console;
            i[e] || (i[e] = !0, a.migrateWarnings.push(e), t && t.warn && !a.migrateMute && (t.warn("JQMIGRATE: " + e), a.migrateTrace && t.trace && t.trace()))
        }

        function t(e, t, n, i) {
            Object.defineProperty(e, t, {
                configurable: !0,
                enumerable: !0,
                get: function() {
                    return l(i), n
                }
            })
        }
        var e;
        a.migrateVersion = "3.0.0", (e = n.console && n.console.log && function() {
            n.console.log.apply(n.console, arguments)
        }) && (a && !/^[12]\./.test(a.fn.jquery) || e("JQMIGRATE: jQuery 3.0.0+ REQUIRED"), a.migrateWarnings && e("JQMIGRATE: Migrate plugin loaded multiple times"), e("JQMIGRATE: Migrate is installed" + (a.migrateMute ? "" : " with logging active") + ", version " + a.migrateVersion));
        var i = {};
        a.migrateWarnings = [], void 0 === a.migrateTrace && (a.migrateTrace = !0), a.migrateReset = function() {
            i = {}, a.migrateWarnings.length = 0
        }, "BackCompat" === document.compatMode && l("jQuery is not compatible with Quirks Mode");
        var s, r = a.fn.init,
            o = a.isNumeric,
            c = a.find,
            d = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,
            u = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g;
        for (s in a.fn.init = function(e) {
                var t = Array.prototype.slice.call(arguments);
                return "string" == typeof e && "#" === e && (l("jQuery( '#' ) is not a valid selector"), t[0] = []), r.apply(this, t)
            }, a.fn.init.prototype = a.fn, a.find = function(t) {
                var n = Array.prototype.slice.call(arguments);
                if ("string" == typeof t && d.test(t)) try {
                    document.querySelector(t)
                } catch (e) {
                    t = t.replace(u, function(e, t, n, i) {
                        return "[" + t + n + '"' + i + '"]'
                    });
                    try {
                        document.querySelector(t), l("Attribute selector with '#' must be quoted: " + n[0]), n[0] = t
                    } catch (e) {
                        l("Attribute selector with '#' was not fixed: " + n[0])
                    }
                }
                return c.apply(this, n)
            }, c) Object.prototype.hasOwnProperty.call(c, s) && (a.find[s] = c[s]);
        a.fn.size = function() {
            return l("jQuery.fn.size() is deprecated; use the .length property"), this.length
        }, a.parseJSON = function() {
            return l("jQuery.parseJSON is deprecated; use JSON.parse"), JSON.parse.apply(null, arguments)
        }, a.isNumeric = function(e) {
            var t, n, i = o(e),
                s = (n = (t = e) && t.toString(), !a.isArray(t) && 0 <= n - parseFloat(n) + 1);
            return i !== s && l("jQuery.isNumeric() should not be called on constructed objects"), s
        }, t(a, "unique", a.uniqueSort, "jQuery.unique is deprecated, use jQuery.uniqueSort"), t(a.expr, "filters", a.expr.pseudos, "jQuery.expr.filters is now jQuery.expr.pseudos"), t(a.expr, ":", a.expr.pseudos, 'jQuery.expr[":"] is now jQuery.expr.pseudos');
        var h = a.ajax;
        a.ajax = function() {
            var e = h.apply(this, arguments);
            return e.promise && (t(e, "success", e.done, "jQXHR.success is deprecated and removed"), t(e, "error", e.fail, "jQXHR.error is deprecated and removed"), t(e, "complete", e.always, "jQXHR.complete is deprecated and removed")), e
        };
        var p = a.fn.removeAttr,
            f = a.fn.toggleClass,
            m = /\S+/g;
        a.fn.removeAttr = function(e) {
            var n = this;
            return a.each(e.match(m), function(e, t) {
                a.expr.match.bool.test(t) && (l("jQuery.fn.removeAttr no longer sets boolean properties: " + t), n.prop(t, !1))
            }), p.apply(this, arguments)
        };
        var g = !(a.fn.toggleClass = function(t) {
            return void 0 !== t && "boolean" != typeof t ? f.apply(this, arguments) : (l("jQuery.fn.toggleClass( boolean ) is deprecated"), this.each(function() {
                var e = this.getAttribute && this.getAttribute("class") || "";
                e && a.data(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === t ? "" : a.data(this, "__className__") || "")
            }))
        });
        a.swap && a.each(["height", "width", "reliableMarginRight"], function(e, t) {
            var n = a.cssHooks[t] && a.cssHooks[t].get;
            n && (a.cssHooks[t].get = function() {
                var e;
                return g = !0, e = n.apply(this, arguments), g = !1, e
            })
        }), a.swap = function(e, t, n, i) {
            var s, r, o = {};
            for (r in g || l("jQuery.swap() is undocumented and deprecated"), t) o[r] = e.style[r], e.style[r] = t[r];
            for (r in s = n.apply(e, i || []), t) e.style[r] = o[r];
            return s
        };
        var v = a.data;
        a.data = function(e, t, n) {
            var i;
            return t && t !== a.camelCase(t) && ((i = a.hasData(e) && v.call(this, e)) && t in i) ? (l("jQuery.data() always sets/gets camelCased names: " + t), 2 < arguments.length && (i[t] = n), i[t]) : v.apply(this, arguments)
        };
        var y = a.Tween.prototype.run;
        a.Tween.prototype.run = function(e) {
            1 < a.easing[this.easing].length && (l('easing function "jQuery.easing.' + this.easing.toString() + '" should use only first argument'), a.easing[this.easing] = a.easing[this.easing].bind(a.easing, e, this.options.duration * e, 0, 1, this.options.duration)), y.apply(this, arguments)
        };
        var b = a.fn.load,
            w = a.event.fix;
        a.event.props = [], a.event.fixHooks = {}, a.event.fix = function(e) {
            var t, n = e.type,
                i = this.fixHooks[n],
                s = a.event.props;
            if (s.length)
                for (l("jQuery.event.props are deprecated and removed: " + s.join()); s.length;) a.event.addProp(s.pop());
            if (i && !i._migrated_ && (i._migrated_ = !0, l("jQuery.event.fixHooks are deprecated and removed: " + n), (s = i.props) && s.length))
                for (; s.length;) a.event.addProp(s.pop());
            return t = w.call(this, e), i && i.filter ? i.filter(t, e) : t
        }, a.each(["load", "unload", "error"], function(e, t) {
            a.fn[t] = function() {
                var e = Array.prototype.slice.call(arguments, 0);
                return "load" === t && "string" == typeof e[0] ? b.apply(this, e) : (l("jQuery.fn." + t + "() is deprecated"), e.splice(0, 0, t), arguments.length ? this.on.apply(this, e) : (this.triggerHandler.apply(this, e), this))
            }
        }), a(function() {
            a(document).triggerHandler("ready")
        }), a.event.special.ready = {
            setup: function() {
                this === document && l("'ready' event is deprecated")
            }
        }, a.fn.extend({
            bind: function(e, t, n) {
                return l("jQuery.fn.bind() is deprecated"), this.on(e, null, t, n)
            },
            unbind: function(e, t) {
                return l("jQuery.fn.unbind() is deprecated"), this.off(e, null, t)
            },
            delegate: function(e, t, n, i) {
                return l("jQuery.fn.delegate() is deprecated"), this.on(t, e, n, i)
            },
            undelegate: function(e, t, n) {
                return l("jQuery.fn.undelegate() is deprecated"), 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            }
        });
        var x = a.fn.offset;
        a.fn.offset = function() {
            var e, t = this[0],
                n = {
                    top: 0,
                    left: 0
                };
            return t && t.nodeType ? (e = (t.ownerDocument || document).documentElement, a.contains(e, t) ? x.apply(this, arguments) : (l("jQuery.fn.offset() requires an element connected to a document"), n)) : (l("jQuery.fn.offset() requires a valid DOM element"), n)
        };
        var T = a.param;
        a.param = function(e, t) {
            var n = a.ajaxSettings && a.ajaxSettings.traditional;
            return void 0 === t && n && (l("jQuery.param() no longer uses jQuery.ajaxSettings.traditional"), t = n), T.call(this, e, t)
        };
        var C = a.fn.andSelf || a.fn.addBack;
        a.fn.andSelf = function() {
            return l("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"), C.apply(this, arguments)
        };
        var S = a.Deferred,
            E = [
                ["resolve", "done", a.Callbacks("once memory"), a.Callbacks("once memory"), "resolved"],
                ["reject", "fail", a.Callbacks("once memory"), a.Callbacks("once memory"), "rejected"],
                ["notify", "progress", a.Callbacks("memory"), a.Callbacks("memory")]
            ];
        a.Deferred = function(e) {
            var r = S(),
                o = r.promise();
            return r.pipe = o.pipe = function() {
                var s = arguments;
                return l("deferred.pipe() is deprecated"), a.Deferred(function(i) {
                    a.each(E, function(e, t) {
                        var n = a.isFunction(s[e]) && s[e];
                        r[t[1]](function() {
                            var e = n && n.apply(this, arguments);
                            e && a.isFunction(e.promise) ? e.promise().done(i.resolve).fail(i.reject).progress(i.notify) : i[t[0] + "With"](this === o ? i.promise() : this, n ? [e] : arguments)
                        })
                    }), s = null
                }).promise()
            }, e && e.call(r, r), r
        }
    }(jQuery, window), function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
    }(function(p) {
        function f(e) {
            return g.raw ? e : encodeURIComponent(e)
        }

        function m(e, t) {
            var n = g.raw ? e : function(e) {
                0 === e.indexOf('"') && (e = e.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\"));
                try {
                    return e = decodeURIComponent(e.replace(i, " ")), g.json ? JSON.parse(e) : e
                } catch (e) {}
            }(e);
            return p.isFunction(t) ? t(n) : n
        }
        var i = /\+/g,
            g = p.cookie = function(e, t, n) {
                if (1 < arguments.length && !p.isFunction(t)) {
                    if ("number" == typeof(n = p.extend({}, g.defaults, n)).expires) {
                        var i = n.expires,
                            s = n.expires = new Date;
                        s.setMilliseconds(s.getMilliseconds() + 864e5 * i)
                    }
                    return document.cookie = [f(e), "=", function(e) {
                        return f(g.json ? JSON.stringify(e) : String(e))
                    }(t), n.expires ? "; expires=" + n.expires.toUTCString() : "", n.path ? "; path=" + n.path : "", n.domain ? "; domain=" + n.domain : "", n.secure ? "; secure" : ""].join("")
                }
                for (var r = e ? void 0 : {}, o = document.cookie ? document.cookie.split("; ") : [], a = 0, l = o.length; a < l; a++) {
                    var c = o[a].split("="),
                        d = (h = c.shift(), g.raw ? h : decodeURIComponent(h)),
                        u = c.join("=");
                    if (e === d) {
                        r = m(u, t);
                        break
                    }
                    e || void 0 === (u = m(u)) || (r[d] = u)
                }
                var h;
                return r
            };
        g.defaults = {}, p.removeCookie = function(e, t) {
            return p.cookie(e, "", p.extend({}, t, {
                expires: -1
            })), !p.cookie(e)
        }
    }), function() {
        var e, t, n, i, s, r, o, a, l, c;
        t = window.device, e = {}, window.device = e, i = window.document.documentElement, c = window.navigator.userAgent.toLowerCase(), e.ios = function() {
            return e.iphone() || e.ipod() || e.ipad()
        }, e.iphone = function() {
            return !e.windows() && s("iphone")
        }, e.ipod = function() {
            return s("ipod")
        }, e.ipad = function() {
            return s("ipad")
        }, e.android = function() {
            return !e.windows() && s("android")
        }, e.androidPhone = function() {
            return e.android() && s("mobile")
        }, e.androidTablet = function() {
            return e.android() && !s("mobile")
        }, e.blackberry = function() {
            return s("blackberry") || s("bb10") || s("rim")
        }, e.blackberryPhone = function() {
            return e.blackberry() && !s("tablet")
        }, e.blackberryTablet = function() {
            return e.blackberry() && s("tablet")
        }, e.windows = function() {
            return s("windows")
        }, e.windowsPhone = function() {
            return e.windows() && s("phone")
        }, e.windowsTablet = function() {
            return e.windows() && s("touch") && !e.windowsPhone()
        }, e.fxos = function() {
            return (s("(mobile;") || s("(tablet;")) && s("; rv:")
        }, e.fxosPhone = function() {
            return e.fxos() && s("mobile")
        }, e.fxosTablet = function() {
            return e.fxos() && s("tablet")
        }, e.meego = function() {
            return s("meego")
        }, e.cordova = function() {
            return window.cordova && "file:" === location.protocol
        }, e.nodeWebkit = function() {
            return "object" == typeof window.process
        }, e.mobile = function() {
            return e.androidPhone() || e.iphone() || e.ipod() || e.windowsPhone() || e.blackberryPhone() || e.fxosPhone() || e.meego()
        }, e.tablet = function() {
            return e.ipad() || e.androidTablet() || e.blackberryTablet() || e.windowsTablet() || e.fxosTablet()
        }, e.desktop = function() {
            return !e.tablet() && !e.mobile()
        }, e.television = function() {
            var e, t = ["googletv", "viera", "smarttv", "internet.tv", "netcast", "nettv", "appletv", "boxee", "kylo", "roku", "dlnadoc", "roku", "pov_tv", "hbbtv", "ce-html"];
            for (e = 0; e < t.length;) {
                if (s(t[e])) return !0;
                e++
            }
            return !1
        }, e.portrait = function() {
            return 1 < window.innerHeight / window.innerWidth
        }, e.landscape = function() {
            return window.innerHeight / window.innerWidth < 1
        }, e.noConflict = function() {
            return window.device = t, this
        }, s = function(e) {
            return -1 !== c.indexOf(e)
        }, o = function(e) {
            var t;
            return t = new RegExp(e, "i"), i.className.match(t)
        }, n = function(e) {
            var t = null;
            o(e) || (t = i.className.replace(/^\s+|\s+$/g, ""), i.className = t + " " + e)
        }, l = function(e) {
            o(e) && (i.className = i.className.replace(" " + e, ""))
        }, e.ios() ? e.ipad() ? n("ios ipad tablet") : e.iphone() ? n("ios iphone mobile") : e.ipod() && n("ios ipod mobile") : e.android() ? n(e.androidTablet() ? "android tablet" : "android mobile") : e.blackberry() ? n(e.blackberryTablet() ? "blackberry tablet" : "blackberry mobile") : e.windows() ? n(e.windowsTablet() ? "windows tablet" : e.windowsPhone() ? "windows mobile" : "desktop") : e.fxos() ? n(e.fxosTablet() ? "fxos tablet" : "fxos mobile") : e.meego() ? n("meego mobile") : e.nodeWebkit() ? n("node-webkit") : e.television() ? n("television") : e.desktop() && n("desktop"), e.cordova() && n("cordova"), r = function() {
            e.landscape() ? (l("portrait"), n("landscape")) : (l("landscape"), n("portrait"))
        }, a = Object.prototype.hasOwnProperty.call(window, "onorientationchange") ? "orientationchange" : "resize", window.addEventListener ? window.addEventListener(a, r, !1) : window.attachEvent ? window.attachEvent(a, r) : window[a] = r, r(), "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function() {
            return e
        }) : "undefined" != typeof module && module.exports ? module.exports = e : window.device = e
    }.call(this), function(o, t) {
        var n, i = o([]),
            s = o.resize = o.extend(o.resize, {}),
            a = "setTimeout",
            r = "resize",
            l = r + "-special-event",
            c = "delay",
            d = "throttleWindow";
        s[c] = 250, s[d] = !0, o.event.special[r] = {
            setup: function() {
                if (!s[d] && this[a]) return !1;
                var e = o(this);
                i = i.add(e), o.data(this, l, {
                    w: e.width(),
                    h: e.height()
                }), 1 === i.length && function e() {
                    n = t[a](function() {
                        i.each(function() {
                            var e = o(this),
                                t = e.width(),
                                n = e.height(),
                                i = o.data(this, l);
                            t === i.w && n === i.h || e.trigger(r, [i.w = t, i.h = n])
                        }), e()
                    }, s[c])
                }()
            },
            teardown: function() {
                if (!s[d] && this[a]) return !1;
                var e = o(this);
                i = i.not(e), e.removeData(l), i.length || clearTimeout(n)
            },
            add: function(e) {
                if (!s[d] && this[a]) return !1;
                var r;

                function t(e, t, n) {
                    var i = o(this),
                        s = o.data(this, l);
                    s.w = void 0 !== t ? t : i.width(), s.h = void 0 !== n ? n : i.height(), r.apply(this, arguments)
                }
                if (o.isFunction(e)) return r = e, t;
                r = e.handler, e.handler = t
            }
        }
    }(jQuery, this), function(t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof module && "object" == typeof module.exports ? exports = t(require("jquery")) : t(jQuery)
    }(function(t) {
        function n(e) {
            var t = 7.5625,
                n = 2.75;
            return e < 1 / n ? t * e * e : e < 2 / n ? t * (e -= 1.5 / n) * e + .75 : e < 2.5 / n ? t * (e -= 2.25 / n) * e + .9375 : t * (e -= 2.625 / n) * e + .984375
        }
        t.easing.jswing = t.easing.swing;
        var i = Math.pow,
            s = Math.sqrt,
            r = Math.sin,
            o = Math.cos,
            a = Math.PI,
            l = 1.70158,
            c = 1.525 * l,
            d = 2 * a / 3,
            u = 2 * a / 4.5;
        t.extend(t.easing, {
            def: "easeOutQuad",
            swing: function(e) {
                return t.easing[t.easing.def](e)
            },
            easeInQuad: function(e) {
                return e * e
            },
            easeOutQuad: function(e) {
                return 1 - (1 - e) * (1 - e)
            },
            easeInOutQuad: function(e) {
                return e < .5 ? 2 * e * e : 1 - i(-2 * e + 2, 2) / 2
            },
            easeInCubic: function(e) {
                return e * e * e
            },
            easeOutCubic: function(e) {
                return 1 - i(1 - e, 3)
            },
            easeInOutCubic: function(e) {
                return e < .5 ? 4 * e * e * e : 1 - i(-2 * e + 2, 3) / 2
            },
            easeInQuart: function(e) {
                return e * e * e * e
            },
            easeOutQuart: function(e) {
                return 1 - i(1 - e, 4)
            },
            easeInOutQuart: function(e) {
                return e < .5 ? 8 * e * e * e * e : 1 - i(-2 * e + 2, 4) / 2
            },
            easeInQuint: function(e) {
                return e * e * e * e * e
            },
            easeOutQuint: function(e) {
                return 1 - i(1 - e, 5)
            },
            easeInOutQuint: function(e) {
                return e < .5 ? 16 * e * e * e * e * e : 1 - i(-2 * e + 2, 5) / 2
            },
            easeInSine: function(e) {
                return 1 - o(e * a / 2)
            },
            easeOutSine: function(e) {
                return r(e * a / 2)
            },
            easeInOutSine: function(e) {
                return -(o(a * e) - 1) / 2
            },
            easeInExpo: function(e) {
                return 0 === e ? 0 : i(2, 10 * e - 10)
            },
            easeOutExpo: function(e) {
                return 1 === e ? 1 : 1 - i(2, -10 * e)
            },
            easeInOutExpo: function(e) {
                return 0 === e ? 0 : 1 === e ? 1 : e < .5 ? i(2, 20 * e - 10) / 2 : (2 - i(2, -20 * e + 10)) / 2
            },
            easeInCirc: function(e) {
                return 1 - s(1 - i(e, 2))
            },
            easeOutCirc: function(e) {
                return s(1 - i(e - 1, 2))
            },
            easeInOutCirc: function(e) {
                return e < .5 ? (1 - s(1 - i(2 * e, 2))) / 2 : (s(1 - i(-2 * e + 2, 2)) + 1) / 2
            },
            easeInElastic: function(e) {
                return 0 === e ? 0 : 1 === e ? 1 : -i(2, 10 * e - 10) * r((10 * e - 10.75) * d)
            },
            easeOutElastic: function(e) {
                return 0 === e ? 0 : 1 === e ? 1 : i(2, -10 * e) * r((10 * e - .75) * d) + 1
            },
            easeInOutElastic: function(e) {
                return 0 === e ? 0 : 1 === e ? 1 : e < .5 ? -i(2, 20 * e - 10) * r((20 * e - 11.125) * u) / 2 : i(2, -20 * e + 10) * r((20 * e - 11.125) * u) / 2 + 1
            },
            easeInBack: function(e) {
                return 2.70158 * e * e * e - l * e * e
            },
            easeOutBack: function(e) {
                return 1 + 2.70158 * i(e - 1, 3) + l * i(e - 1, 2)
            },
            easeInOutBack: function(e) {
                return e < .5 ? i(2 * e, 2) * (7.189819 * e - c) / 2 : (i(2 * e - 2, 2) * ((1 + c) * (2 * e - 2) + c) + 2) / 2
            },
            easeInBounce: function(e) {
                return 1 - n(1 - e)
            },
            easeOutBounce: n,
            easeInOutBounce: function(e) {
                return e < .5 ? (1 - n(1 - 2 * e)) / 2 : (1 + n(2 * e - 1)) / 2
            }
        })
    }), function(e) {
        "function" == typeof define && define.amd && define.amd.jQuery ? define(["jquery"], e) : e("undefined" != typeof module && module.exports ? require("jquery") : jQuery)
    }(function(ae) {
        "use strict";

        function i(e, a) {
            function t(e) {
                if (!(!0 === Q.data(Oe + "_intouch") || 0 < ae(e.target).closest(a.excludedElements, Q).length)) {
                    var t = e.originalEvent ? e.originalEvent : e;
                    if (!t.pointerType || "mouse" != t.pointerType || 0 != a.fallbackToMouseEvents) {
                        var n, i = t.touches,
                            s = i ? i[0] : t;
                        return K = Ee, i ? Z = i.length : !1 !== a.preventDefaultEvents && e.preventDefault(), G = R = F = null, U = 1, V = B = W = Y = q = 0, X = function() {
                            var e = {};
                            return e[le] = $(le), e[ce] = $(ce), e[de] = $(de), e[ue] = $(ue), e
                        }(), C(), _(0, s), !i || Z === a.fingers || a.fingers === Ce || m() ? (ee = I(), 2 == Z && (_(1, i[1]), W = B = A(J[0].start, J[1].start)), (a.swipeStatus || a.pinchStatus) && (n = c(t, K))) : n = !1, !1 === n ? (c(t, K = De), n) : (a.hold && (oe = setTimeout(ae.proxy(function() {
                            Q.trigger("hold", [t.target]), a.hold && (n = a.hold.call(Q, t, t.target))
                        }, this), a.longTapThreshold)), E(!0), null)
                    }
                }
            }

            function n(e) {
                var t = e.originalEvent ? e.originalEvent : e;
                if (K !== ke && K !== De && !S()) {
                    var n, i = t.touches,
                        s = k(i ? i[0] : t);
                    if (te = I(), i && (Z = i.length), a.hold && clearTimeout(oe), K = _e, 2 == Z && (0 == W ? (_(1, i[1]), W = B = A(J[0].start, J[1].start)) : (k(i[1]), B = A(J[0].end, J[1].end), J[0].end, J[1].end, G = U < 1 ? pe : he), U = function(e, t) {
                            return (t / e * 1).toFixed(2)
                        }(W, B), V = Math.abs(W - B)), Z === a.fingers || a.fingers === Ce || !i || m()) {
                        if (F = O(s.start, s.end), function(e, t) {
                                if (!1 !== a.preventDefaultEvents)
                                    if (a.allowPageScroll === fe) e.preventDefault();
                                    else {
                                        var n = a.allowPageScroll === me;
                                        switch (t) {
                                            case le:
                                                (a.swipeLeft && n || !n && a.allowPageScroll != xe) && e.preventDefault();
                                                break;
                                            case ce:
                                                (a.swipeRight && n || !n && a.allowPageScroll != xe) && e.preventDefault();
                                                break;
                                            case de:
                                                (a.swipeUp && n || !n && a.allowPageScroll != Te) && e.preventDefault();
                                                break;
                                            case ue:
                                                (a.swipeDown && n || !n && a.allowPageScroll != Te) && e.preventDefault()
                                        }
                                    }
                            }(e, R = O(s.last, s.end)), q = function(e, t) {
                                return Math.round(Math.sqrt(Math.pow(t.x - e.x, 2) + Math.pow(t.y - e.y, 2)))
                            }(s.start, s.end), Y = M(), function(e, t) {
                                e != fe && (t = Math.max(t, D(e)), X[e].distance = t)
                            }(F, q), n = c(t, K), !a.triggerOnTouchEnd || a.triggerOnTouchLeave) {
                            var r = !0;
                            if (a.triggerOnTouchLeave) {
                                var o = function(e) {
                                    var t = (e = ae(e)).offset();
                                    return {
                                        left: t.left,
                                        right: t.left + e.outerWidth(),
                                        top: t.top,
                                        bottom: t.top + e.outerHeight()
                                    }
                                }(this);
                                r = function(e, t) {
                                    return e.x > t.left && e.x < t.right && e.y > t.top && e.y < t.bottom
                                }(s.end, o)
                            }!a.triggerOnTouchEnd && r ? K = l(_e) : a.triggerOnTouchLeave && !r && (K = l(ke)), K != De && K != ke || c(t, K)
                        }
                    } else c(t, K = De);
                    !1 === n && c(t, K = De)
                }
            }

            function i(e) {
                var t = e.originalEvent ? e.originalEvent : e,
                    n = t.touches;
                if (n) {
                    if (n.length && !S()) return function(e) {
                        ne = I(), ie = e.touches.length + 1
                    }(t), !0;
                    if (n.length && S()) return !0
                }
                return S() && (Z = ie), te = I(), Y = M(), h() || !u() ? c(t, K = De) : a.triggerOnTouchEnd || !1 === a.triggerOnTouchEnd && K === _e ? (!1 !== a.preventDefaultEvents && !1 !== e.cancelable && e.preventDefault(), c(t, K = ke)) : !a.triggerOnTouchEnd && w() ? d(t, K = ke, ye) : K === _e && c(t, K = De), E(!1), null
            }

            function s() {
                B = W = ee = te = Z = 0, U = 1, C(), E(!1)
            }

            function r(e) {
                var t = e.originalEvent ? e.originalEvent : e;
                a.triggerOnTouchLeave && c(t, K = l(ke))
            }

            function o() {
                Q.unbind(L, t), Q.unbind(z, s), Q.unbind(N, n), Q.unbind(j, i), H && Q.unbind(H, r), E(!1)
            }

            function l(e) {
                var t = e,
                    n = p(),
                    i = u(),
                    s = h();
                return !n || s ? t = De : !i || e != _e || a.triggerOnTouchEnd && !a.triggerOnTouchLeave ? !i && e == ke && a.triggerOnTouchLeave && (t = De) : t = ke, t
            }

            function c(e, t) {
                var n, i = e.touches;
                return (!g() || !v()) && !v() || (n = d(e, t, ge)), (!f() || !m()) && !m() || !1 === n || (n = d(e, t, ve)), T() && x() && !1 !== n ? n = d(e, t, be) : Y > a.longTapThreshold && q < Se && a.longTap && !1 !== n ? n = d(e, t, we) : 1 !== Z && $e || !(isNaN(q) || q < a.threshold) || !w() || !1 === n || (n = d(e, t, ye)), t === De && s(), t === ke && (i && i.length || s()), n
            }

            function d(e, t, n) {
                var i;
                if (n == ge) {
                    if (Q.trigger("swipeStatus", [t, F || null, q || 0, Y || 0, Z, J, R]), a.swipeStatus && !1 === (i = a.swipeStatus.call(Q, e, t, F || null, q || 0, Y || 0, Z, J, R))) return !1;
                    if (t == ke && g()) {
                        if (clearTimeout(re), clearTimeout(oe), Q.trigger("swipe", [F, q, Y, Z, J, R]), a.swipe && !1 === (i = a.swipe.call(Q, e, F, q, Y, Z, J, R))) return !1;
                        switch (F) {
                            case le:
                                Q.trigger("swipeLeft", [F, q, Y, Z, J, R]), a.swipeLeft && (i = a.swipeLeft.call(Q, e, F, q, Y, Z, J, R));
                                break;
                            case ce:
                                Q.trigger("swipeRight", [F, q, Y, Z, J, R]), a.swipeRight && (i = a.swipeRight.call(Q, e, F, q, Y, Z, J, R));
                                break;
                            case de:
                                Q.trigger("swipeUp", [F, q, Y, Z, J, R]), a.swipeUp && (i = a.swipeUp.call(Q, e, F, q, Y, Z, J, R));
                                break;
                            case ue:
                                Q.trigger("swipeDown", [F, q, Y, Z, J, R]), a.swipeDown && (i = a.swipeDown.call(Q, e, F, q, Y, Z, J, R))
                        }
                    }
                }
                if (n == ve) {
                    if (Q.trigger("pinchStatus", [t, G || null, V || 0, Y || 0, Z, U, J]), a.pinchStatus && !1 === (i = a.pinchStatus.call(Q, e, t, G || null, V || 0, Y || 0, Z, U, J))) return !1;
                    if (t == ke && f()) switch (G) {
                        case he:
                            Q.trigger("pinchIn", [G || null, V || 0, Y || 0, Z, U, J]), a.pinchIn && (i = a.pinchIn.call(Q, e, G || null, V || 0, Y || 0, Z, U, J));
                            break;
                        case pe:
                            Q.trigger("pinchOut", [G || null, V || 0, Y || 0, Z, U, J]), a.pinchOut && (i = a.pinchOut.call(Q, e, G || null, V || 0, Y || 0, Z, U, J))
                    }
                }
                return n == ye ? t !== De && t !== ke || (clearTimeout(re), clearTimeout(oe), x() && !T() ? (se = I(), re = setTimeout(ae.proxy(function() {
                    se = null, Q.trigger("tap", [e.target]), a.tap && (i = a.tap.call(Q, e, e.target))
                }, this), a.doubleTapThreshold)) : (se = null, Q.trigger("tap", [e.target]), a.tap && (i = a.tap.call(Q, e, e.target)))) : n == be ? t !== De && t !== ke || (clearTimeout(re), clearTimeout(oe), se = null, Q.trigger("doubletap", [e.target]), a.doubleTap && (i = a.doubleTap.call(Q, e, e.target))) : n == we && (t !== De && t !== ke || (clearTimeout(re), se = null, Q.trigger("longtap", [e.target]), a.longTap && (i = a.longTap.call(Q, e, e.target)))), i
            }

            function u() {
                var e = !0;
                return null !== a.threshold && (e = q >= a.threshold), e
            }

            function h() {
                var e = !1;
                return null !== a.cancelThreshold && null !== F && (e = D(F) - q >= a.cancelThreshold), e
            }

            function p() {
                return !(a.maxTimeThreshold && Y >= a.maxTimeThreshold)
            }

            function f() {
                var e = y(),
                    t = b(),
                    n = null === a.pinchThreshold || V >= a.pinchThreshold;
                return e && t && n
            }

            function m() {
                return !!(a.pinchStatus || a.pinchIn || a.pinchOut)
            }

            function g() {
                var e = p(),
                    t = u(),
                    n = y(),
                    i = b();
                return !h() && i && n && t && e
            }

            function v() {
                return !!(a.swipe || a.swipeStatus || a.swipeLeft || a.swipeRight || a.swipeUp || a.swipeDown)
            }

            function y() {
                return Z === a.fingers || a.fingers === Ce || !$e
            }

            function b() {
                return 0 !== J[0].end.x
            }

            function w() {
                return !!a.tap
            }

            function x() {
                return !!a.doubleTap
            }

            function T() {
                if (null == se) return !1;
                var e = I();
                return x() && e - se <= a.doubleTapThreshold
            }

            function C() {
                ie = ne = 0
            }

            function S() {
                var e = !1;
                return ne && I() - ne <= a.fingerReleaseThreshold && (e = !0), e
            }

            function E(e) {
                Q && (!0 === e ? (Q.bind(N, n), Q.bind(j, i), H && Q.bind(H, r)) : (Q.unbind(N, n, !1), Q.unbind(j, i, !1), H && Q.unbind(H, r, !1)), Q.data(Oe + "_intouch", !0 === e))
            }

            function _(e, t) {
                var n = {
                    start: {
                        x: 0,
                        y: 0
                    },
                    last: {
                        x: 0,
                        y: 0
                    },
                    end: {
                        x: 0,
                        y: 0
                    }
                };
                return n.start.x = n.last.x = n.end.x = t.pageX || t.clientX, n.start.y = n.last.y = n.end.y = t.pageY || t.clientY, J[e] = n
            }

            function k(e) {
                var t = void 0 !== e.identifier ? e.identifier : 0,
                    n = function(e) {
                        return J[e] || null
                    }(t);
                return null === n && (n = _(t, e)), n.last.x = n.end.x, n.last.y = n.end.y, n.end.x = e.pageX || e.clientX, n.end.y = e.pageY || e.clientY, n
            }

            function D(e) {
                if (X[e]) return X[e].distance
            }

            function $(e) {
                return {
                    direction: e,
                    distance: 0
                }
            }

            function M() {
                return te - ee
            }

            function A(e, t) {
                var n = Math.abs(e.x - t.x),
                    i = Math.abs(e.y - t.y);
                return Math.round(Math.sqrt(n * n + i * i))
            }

            function O(e, t) {
                if (function(e, t) {
                        return e.x == t.x && e.y == t.y
                    }(e, t)) return fe;
                var n = function(e, t) {
                    var n = e.x - t.x,
                        i = t.y - e.y,
                        s = Math.atan2(i, n),
                        r = Math.round(180 * s / Math.PI);
                    return r < 0 && (r = 360 - Math.abs(r)), r
                }(e, t);
                return n <= 45 && 0 <= n ? le : n <= 360 && 315 <= n ? le : 135 <= n && n <= 225 ? ce : 45 < n && n < 135 ? ue : de
            }

            function I() {
                return (new Date).getTime()
            }
            a = ae.extend({}, a);
            var P = $e || Ae || !a.fallbackToMouseEvents,
                L = P ? Ae ? Me ? "MSPointerDown" : "pointerdown" : "touchstart" : "mousedown",
                N = P ? Ae ? Me ? "MSPointerMove" : "pointermove" : "touchmove" : "mousemove",
                j = P ? Ae ? Me ? "MSPointerUp" : "pointerup" : "touchend" : "mouseup",
                H = P ? Ae ? "mouseleave" : null : "mouseleave",
                z = Ae ? Me ? "MSPointerCancel" : "pointercancel" : "touchcancel",
                q = 0,
                F = null,
                R = null,
                Y = 0,
                W = 0,
                B = 0,
                U = 1,
                V = 0,
                G = 0,
                X = null,
                Q = ae(e),
                K = "start",
                Z = 0,
                J = {},
                ee = 0,
                te = 0,
                ne = 0,
                ie = 0,
                se = 0,
                re = null,
                oe = null;
            try {
                Q.bind(L, t), Q.bind(z, s)
            } catch (e) {
                ae.error("events not supported " + L + "," + z + " on jQuery.swipe")
            }
            this.enable = function() {
                return this.disable(), Q.bind(L, t), Q.bind(z, s), Q
            }, this.disable = function() {
                return o(), Q
            }, this.destroy = function() {
                o(), Q.data(Oe, null), Q = null
            }, this.option = function(e, t) {
                if ("object" == typeof e) a = ae.extend(a, e);
                else if (void 0 !== a[e]) {
                    if (void 0 === t) return a[e];
                    a[e] = t
                } else {
                    if (!e) return a;
                    ae.error("Option " + e + " does not exist on jQuery.swipe.options")
                }
                return null
            }
        }
        var le = "left",
            ce = "right",
            de = "up",
            ue = "down",
            he = "in",
            pe = "out",
            fe = "none",
            me = "auto",
            ge = "swipe",
            ve = "pinch",
            ye = "tap",
            be = "doubletap",
            we = "longtap",
            xe = "horizontal",
            Te = "vertical",
            Ce = "all",
            Se = 10,
            Ee = "start",
            _e = "move",
            ke = "end",
            De = "cancel",
            $e = "ontouchstart" in window,
            Me = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled && !$e,
            Ae = (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && !$e,
            Oe = "TouchSwipe";
        ae.fn.swipe = function(e) {
            var t = ae(this),
                n = t.data(Oe);
            if (n && "string" == typeof e) {
                if (n[e]) return n[e].apply(n, Array.prototype.slice.call(arguments, 1));
                ae.error("Method " + e + " does not exist on jQuery.swipe")
            } else if (n && "object" == typeof e) n.option.apply(n, arguments);
            else if (!(n || "object" != typeof e && e)) return function(n) {
                return !n || void 0 !== n.allowPageScroll || void 0 === n.swipe && void 0 === n.swipeStatus || (n.allowPageScroll = fe), void 0 !== n.click && void 0 === n.tap && (n.tap = n.click), n = n || {}, n = ae.extend({}, ae.fn.swipe.defaults, n), this.each(function() {
                    var e = ae(this),
                        t = e.data(Oe);
                    t || (t = new i(this, n), e.data(Oe, t))
                })
            }.apply(this, arguments);
            return t
        }, ae.fn.swipe.version = "1.6.18", ae.fn.swipe.defaults = {
            fingers: 1,
            threshold: 75,
            cancelThreshold: null,
            pinchThreshold: 20,
            maxTimeThreshold: null,
            fingerReleaseThreshold: 250,
            longTapThreshold: 500,
            doubleTapThreshold: 200,
            swipe: null,
            swipeLeft: null,
            swipeRight: null,
            swipeUp: null,
            swipeDown: null,
            swipeStatus: null,
            pinchIn: null,
            pinchOut: null,
            pinchStatus: null,
            click: null,
            tap: null,
            doubleTap: null,
            longTap: null,
            hold: null,
            triggerOnTouchEnd: !0,
            triggerOnTouchLeave: !1,
            allowPageScroll: "auto",
            fallbackToMouseEvents: !0,
            excludedElements: ".noSwipe",
            preventDefaultEvents: !0
        }, ae.fn.swipe.phases = {
            PHASE_START: Ee,
            PHASE_MOVE: _e,
            PHASE_END: ke,
            PHASE_CANCEL: De
        }, ae.fn.swipe.directions = {
            LEFT: le,
            RIGHT: ce,
            UP: de,
            DOWN: ue,
            IN: he,
            OUT: pe
        }, ae.fn.swipe.pageScroll = {
            NONE: fe,
            HORIZONTAL: xe,
            VERTICAL: Te,
            AUTO: me
        }, ae.fn.swipe.fingers = {
            ONE: 1,
            TWO: 2,
            THREE: 3,
            FOUR: 4,
            FIVE: 5,
            ALL: Ce
        }
    }), function(e, t) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.Popper = t()
    }(this, function() {
        "use strict";

        function r(e) {
            return e && "[object Function]" === {}.toString.call(e)
        }

        function w(e, t) {
            if (1 !== e.nodeType) return [];
            var n = getComputedStyle(e, null);
            return t ? n[t] : n
        }

        function p(e) {
            return "HTML" === e.nodeName ? e : e.parentNode || e.host
        }

        function m(e) {
            if (!e) return document.body;
            switch (e.nodeName) {
                case "HTML":
                case "BODY":
                    return e.ownerDocument.body;
                case "#document":
                    return e.body
            }
            var t = w(e),
                n = t.overflow,
                i = t.overflowX,
                s = t.overflowY;
            return /(auto|scroll|overlay)/.test(n + s + i) ? e : m(p(e))
        }

        function g(e) {
            return 11 === e ? W : 10 === e ? B : W || B
        }

        function y(e) {
            if (!e) return document.documentElement;
            for (var t = g(10) ? document.body : null, n = e.offsetParent; n === t && e.nextElementSibling;) n = (e = e.nextElementSibling).offsetParent;
            var i = n && n.nodeName;
            return i && "BODY" !== i && "HTML" !== i ? -1 !== ["TD", "TABLE"].indexOf(n.nodeName) && "static" === w(n, "position") ? y(n) : n : e ? e.ownerDocument.documentElement : document.documentElement
        }

        function l(e) {
            return null === e.parentNode ? e : l(e.parentNode)
        }

        function f(e, t) {
            if (!(e && e.nodeType && t && t.nodeType)) return document.documentElement;
            var n = e.compareDocumentPosition(t) & Node.DOCUMENT_POSITION_FOLLOWING,
                i = n ? e : t,
                s = n ? t : e,
                r = document.createRange();
            r.setStart(i, 0), r.setEnd(s, 0);
            var o = r.commonAncestorContainer;
            if (e !== o && t !== o || i.contains(s)) return function(e) {
                var t = e.nodeName;
                return "BODY" !== t && ("HTML" === t || y(e.firstElementChild) === e)
            }(o) ? o : y(o);
            var a = l(e);
            return a.host ? f(a.host, t) : f(e, l(t).host)
        }

        function v(e, t) {
            var n = "top" === (1 < arguments.length && void 0 !== t ? t : "top") ? "scrollTop" : "scrollLeft",
                i = e.nodeName;
            if ("BODY" !== i && "HTML" !== i) return e[n];
            var s = e.ownerDocument.documentElement;
            return (e.ownerDocument.scrollingElement || s)[n]
        }

        function u(e, t) {
            var n = "x" === t ? "Left" : "Top",
                i = "Left" == n ? "Right" : "Bottom";
            return parseFloat(e["border" + n + "Width"], 10) + parseFloat(e["border" + i + "Width"], 10)
        }

        function i(e, t, n, i) {
            return j(t["offset" + e], t["scroll" + e], n["client" + e], n["offset" + e], n["scroll" + e], g(10) ? n["offset" + e] + i["margin" + ("Height" === e ? "Top" : "Left")] + i["margin" + ("Height" === e ? "Bottom" : "Right")] : 0)
        }

        function b() {
            var e = document.body,
                t = document.documentElement,
                n = g(10) && getComputedStyle(t);
            return {
                height: i("Height", e, t, n),
                width: i("Width", e, t, n)
            }
        }

        function x(e) {
            return V({}, e, {
                right: e.left + e.width,
                bottom: e.top + e.height
            })
        }

        function T(e) {
            var t = {};
            try {
                if (g(10)) {
                    t = e.getBoundingClientRect();
                    var n = v(e, "top"),
                        i = v(e, "left");
                    t.top += n, t.left += i, t.bottom += n, t.right += i
                } else t = e.getBoundingClientRect()
            } catch (e) {}
            var s = {
                    left: t.left,
                    top: t.top,
                    width: t.right - t.left,
                    height: t.bottom - t.top
                },
                r = "HTML" === e.nodeName ? b() : {},
                o = r.width || e.clientWidth || s.right - s.left,
                a = r.height || e.clientHeight || s.bottom - s.top,
                l = e.offsetWidth - o,
                c = e.offsetHeight - a;
            if (l || c) {
                var d = w(e);
                l -= u(d, "x"), c -= u(d, "y"), s.width -= l, s.height -= c
            }
            return x(s)
        }

        function C(e, t, n) {
            var i = 2 < arguments.length && void 0 !== n && n,
                s = g(10),
                r = "HTML" === t.nodeName,
                o = T(e),
                a = T(t),
                l = m(e),
                c = w(t),
                d = parseFloat(c.borderTopWidth, 10),
                u = parseFloat(c.borderLeftWidth, 10);
            i && "HTML" === t.nodeName && (a.top = j(a.top, 0), a.left = j(a.left, 0));
            var h = x({
                top: o.top - a.top - d,
                left: o.left - a.left - u,
                width: o.width,
                height: o.height
            });
            if (h.marginTop = 0, h.marginLeft = 0, !s && r) {
                var p = parseFloat(c.marginTop, 10),
                    f = parseFloat(c.marginLeft, 10);
                h.top -= d - p, h.bottom -= d - p, h.left -= u - f, h.right -= u - f, h.marginTop = p, h.marginLeft = f
            }
            return (s && !i ? t.contains(l) : t === l && "BODY" !== l.nodeName) && (h = function(e, t, n) {
                var i = 2 < arguments.length && void 0 !== n && n,
                    s = v(t, "top"),
                    r = v(t, "left"),
                    o = i ? -1 : 1;
                return e.top += s * o, e.bottom += s * o, e.left += r * o, e.right += r * o, e
            }(h, t)), h
        }

        function S(e) {
            if (!e || !e.parentElement || g()) return document.documentElement;
            for (var t = e.parentElement; t && "none" === w(t, "transform");) t = t.parentElement;
            return t || document.documentElement
        }

        function E(e, t, n, i, s) {
            var r = 4 < arguments.length && void 0 !== s && s,
                o = {
                    top: 0,
                    left: 0
                },
                a = r ? S(e) : f(e, t);
            if ("viewport" === i) o = function(e, t) {
                var n = 1 < arguments.length && void 0 !== t && t,
                    i = e.ownerDocument.documentElement,
                    s = C(e, i),
                    r = j(i.clientWidth, window.innerWidth || 0),
                    o = j(i.clientHeight, window.innerHeight || 0),
                    a = n ? 0 : v(i),
                    l = n ? 0 : v(i, "left");
                return x({
                    top: a - s.top + s.marginTop,
                    left: l - s.left + s.marginLeft,
                    width: r,
                    height: o
                })
            }(a, r);
            else {
                var l;
                "scrollParent" === i ? "BODY" === (l = m(p(t))).nodeName && (l = e.ownerDocument.documentElement) : l = "window" === i ? e.ownerDocument.documentElement : i;
                var c = C(l, a, r);
                if ("HTML" !== l.nodeName || function e(t) {
                        var n = t.nodeName;
                        return "BODY" !== n && "HTML" !== n && ("fixed" === w(t, "position") || e(p(t)))
                    }(a)) o = c;
                else {
                    var d = b(),
                        u = d.height,
                        h = d.width;
                    o.top += c.top - c.marginTop, o.bottom = u + c.top, o.left += c.left - c.marginLeft, o.right = h + c.left
                }
            }
            return o.left += n, o.top += n, o.right -= n, o.bottom -= n, o
        }

        function a(e, t, i, n, s, r) {
            var o = 5 < arguments.length && void 0 !== r ? r : 0;
            if (-1 === e.indexOf("auto")) return e;
            var a = E(i, n, o, s),
                l = {
                    top: {
                        width: a.width,
                        height: t.top - a.top
                    },
                    right: {
                        width: a.right - t.right,
                        height: a.height
                    },
                    bottom: {
                        width: a.width,
                        height: a.bottom - t.bottom
                    },
                    left: {
                        width: t.left - a.left,
                        height: a.height
                    }
                },
                c = Object.keys(l).map(function(e) {
                    return V({
                        key: e
                    }, l[e], {
                        area: function(e) {
                            return e.width * e.height
                        }(l[e])
                    })
                }).sort(function(e, t) {
                    return t.area - e.area
                }),
                d = c.filter(function(e) {
                    var t = e.width,
                        n = e.height;
                    return t >= i.clientWidth && n >= i.clientHeight
                }),
                u = 0 < d.length ? d[0].key : c[0].key,
                h = e.split("-")[1];
            return u + (h ? "-" + h : "")
        }

        function c(e, t, n, i) {
            var s = 3 < arguments.length && void 0 !== i ? i : null;
            return C(n, s ? S(t) : f(t, n), s)
        }

        function _(e) {
            var t = getComputedStyle(e),
                n = parseFloat(t.marginTop) + parseFloat(t.marginBottom),
                i = parseFloat(t.marginLeft) + parseFloat(t.marginRight);
            return {
                width: e.offsetWidth + i,
                height: e.offsetHeight + n
            }
        }

        function k(e) {
            var t = {
                left: "right",
                right: "left",
                bottom: "top",
                top: "bottom"
            };
            return e.replace(/left|right|bottom|top/g, function(e) {
                return t[e]
            })
        }

        function D(e, t, n) {
            n = n.split("-")[0];
            var i = _(e),
                s = {
                    width: i.width,
                    height: i.height
                },
                r = -1 !== ["right", "left"].indexOf(n),
                o = r ? "top" : "left",
                a = r ? "left" : "top",
                l = r ? "height" : "width",
                c = r ? "width" : "height";
            return s[o] = t[o] + t[l] / 2 - i[l] / 2, s[a] = n === a ? t[a] - i[c] : t[k(a)], s
        }

        function $(e, t) {
            return Array.prototype.find ? e.find(t) : e.filter(t)[0]
        }

        function M(e, n, t) {
            return (void 0 === t ? e : e.slice(0, function(e, t, n) {
                if (Array.prototype.findIndex) return e.findIndex(function(e) {
                    return e[t] === n
                });
                var i = $(e, function(e) {
                    return e[t] === n
                });
                return e.indexOf(i)
            }(e, "name", t))).forEach(function(e) {
                e.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
                var t = e.function || e.fn;
                e.enabled && r(t) && (n.offsets.popper = x(n.offsets.popper), n.offsets.reference = x(n.offsets.reference), n = t(n, e))
            }), n
        }

        function e(e, n) {
            return e.some(function(e) {
                var t = e.name;
                return e.enabled && t === n
            })
        }

        function A(e) {
            for (var t = [!1, "ms", "Webkit", "Moz", "O"], n = e.charAt(0).toUpperCase() + e.slice(1), i = 0; i < t.length; i++) {
                var s = t[i],
                    r = s ? "" + s + n : e;
                if (void 0 !== document.body.style[r]) return r
            }
            return null
        }

        function o(e) {
            var t = e.ownerDocument;
            return t ? t.defaultView : window
        }

        function t(e, t, n, i) {
            n.updateBound = i, o(e).addEventListener("resize", n.updateBound, {
                passive: !0
            });
            var s = m(e);
            return function e(t, n, i, s) {
                var r = "BODY" === t.nodeName,
                    o = r ? t.ownerDocument.defaultView : t;
                o.addEventListener(n, i, {
                    passive: !0
                }), r || e(m(o.parentNode), n, i, s), s.push(o)
            }(s, "scroll", n.updateBound, n.scrollParents), n.scrollElement = s, n.eventsEnabled = !0, n
        }

        function n() {
            this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = function(e, t) {
                return o(e).removeEventListener("resize", t.updateBound), t.scrollParents.forEach(function(e) {
                    e.removeEventListener("scroll", t.updateBound)
                }), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t
            }(this.reference, this.state))
        }

        function d(e) {
            return "" !== e && !isNaN(parseFloat(e)) && isFinite(e)
        }

        function h(n, i) {
            Object.keys(i).forEach(function(e) {
                var t = ""; - 1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(e) && d(i[e]) && (t = "px"), n.style[e] = i[e] + t
            })
        }

        function O(e, t, n) {
            var i = $(e, function(e) {
                    return e.name === t
                }),
                s = !!i && e.some(function(e) {
                    return e.name === n && e.enabled && e.order < i.order
                });
            if (!s) {
                var r = "`" + t + "`";
                console.warn("`" + n + "` modifier is required by " + r + " modifier in order to work, be sure to include it before " + r + "!")
            }
            return s
        }

        function s(e, t) {
            var n = 1 < arguments.length && void 0 !== t && t,
                i = X.indexOf(e),
                s = X.slice(i + 1).concat(X.slice(0, i));
            return n ? s.reverse() : s
        }

        function I(e, s, r, t) {
            var o = [0, 0],
                a = -1 !== ["right", "left"].indexOf(t),
                n = e.split(/(\+|\-)/).map(function(e) {
                    return e.trim()
                }),
                i = n.indexOf($(n, function(e) {
                    return -1 !== e.search(/,|\s/)
                }));
            n[i] && -1 === n[i].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
            var l = /\s*,\s*|\s+/,
                c = -1 === i ? [n] : [n.slice(0, i).concat([n[i].split(l)[0]]), [n[i].split(l)[1]].concat(n.slice(i + 1))];
            return (c = c.map(function(e, t) {
                var n = (1 === t ? !a : a) ? "height" : "width",
                    i = !1;
                return e.reduce(function(e, t) {
                    return "" === e[e.length - 1] && -1 !== ["+", "-"].indexOf(t) ? (e[e.length - 1] = t, i = !0, e) : i ? (e[e.length - 1] += t, i = !1, e) : e.concat(t)
                }, []).map(function(e) {
                    return function(e, t, n, i) {
                        var s, r = e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
                            o = +r[1],
                            a = r[2];
                        if (!o) return e;
                        if (0 !== a.indexOf("%")) return "vh" !== a && "vw" !== a ? o : ("vh" === a ? j(document.documentElement.clientHeight, window.innerHeight || 0) : j(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * o;
                        switch (a) {
                            case "%p":
                                s = n;
                                break;
                            case "%":
                            case "%r":
                            default:
                                s = i
                        }
                        return x(s)[t] / 100 * o
                    }(e, n, s, r)
                })
            })).forEach(function(n, i) {
                n.forEach(function(e, t) {
                    d(e) && (o[i] += e * ("-" === n[t - 1] ? -1 : 1))
                })
            }), o
        }
        for (var P = Math.min, L = Math.round, N = Math.floor, j = Math.max, H = "undefined" != typeof window && "undefined" != typeof document, z = ["Edge", "Trident", "Firefox"], q = 0, F = 0; F < z.length; F += 1)
            if (H && 0 <= navigator.userAgent.indexOf(z[F])) {
                q = 1;
                break
            }
        function R(e, t, n) {
            return t in e ? Object.defineProperty(e, t, {
                value: n,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = n, e
        }
        var Y = H && window.Promise ? function(e) {
                var t = !1;
                return function() {
                    t || (t = !0, window.Promise.resolve().then(function() {
                        t = !1, e()
                    }))
                }
            } : function(e) {
                var t = !1;
                return function() {
                    t || (t = !0, setTimeout(function() {
                        t = !1, e()
                    }, q))
                }
            },
            W = H && !(!window.MSInputMethodContext || !document.documentMode),
            B = H && /MSIE 10/.test(navigator.userAgent),
            U = function(e, t, n) {
                return t && te(e.prototype, t), n && te(e, n), e
            },
            V = Object.assign || function(e) {
                for (var t, n = 1; n < arguments.length; n++)
                    for (var i in t = arguments[n]) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
                return e
            },
            G = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
            X = G.slice(3),
            Q = "flip",
            K = "clockwise",
            Z = "counterclockwise",
            J = (U(ee, [{
                key: "update",
                value: function() {
                    return function() {
                        if (!this.state.isDestroyed) {
                            var e = {
                                instance: this,
                                styles: {},
                                arrowStyles: {},
                                attributes: {},
                                flipped: !1,
                                offsets: {}
                            };
                            e.offsets.reference = c(this.state, this.popper, this.reference, this.options.positionFixed), e.placement = a(this.options.placement, e.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), e.originalPlacement = e.placement, e.positionFixed = this.options.positionFixed, e.offsets.popper = D(this.popper, e.offsets.reference, e.placement), e.offsets.popper.position = this.options.positionFixed ? "fixed" : "absolute", e = M(this.modifiers, e), this.state.isCreated ? this.options.onUpdate(e) : (this.state.isCreated = !0, this.options.onCreate(e))
                        }
                    }.call(this)
                }
            }, {
                key: "destroy",
                value: function() {
                    return function() {
                        return this.state.isDestroyed = !0, e(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.position = "", this.popper.style.top = "", this.popper.style.left = "", this.popper.style.right = "", this.popper.style.bottom = "", this.popper.style.willChange = "", this.popper.style[A("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this
                    }.call(this)
                }
            }, {
                key: "enableEventListeners",
                value: function() {
                    return function() {
                        this.state.eventsEnabled || (this.state = t(this.reference, this.options, this.state, this.scheduleUpdate))
                    }.call(this)
                }
            }, {
                key: "disableEventListeners",
                value: function() {
                    return n.call(this)
                }
            }]), ee);

        function ee(e, t) {
            var n = this,
                i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
            (function(e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            })(this, ee), this.scheduleUpdate = function() {
                return requestAnimationFrame(n.update)
            }, this.update = Y(this.update.bind(this)), this.options = V({}, ee.Defaults, i), this.state = {
                isDestroyed: !1,
                isCreated: !1,
                scrollParents: []
            }, this.reference = e && e.jquery ? e[0] : e, this.popper = t && t.jquery ? t[0] : t, this.options.modifiers = {}, Object.keys(V({}, ee.Defaults.modifiers, i.modifiers)).forEach(function(e) {
                n.options.modifiers[e] = V({}, ee.Defaults.modifiers[e] || {}, i.modifiers ? i.modifiers[e] : {})
            }), this.modifiers = Object.keys(this.options.modifiers).map(function(e) {
                return V({
                    name: e
                }, n.options.modifiers[e])
            }).sort(function(e, t) {
                return e.order - t.order
            }), this.modifiers.forEach(function(e) {
                e.enabled && r(e.onLoad) && e.onLoad(n.reference, n.popper, n.options, e, n.state)
            }), this.update();
            var s = this.options.eventsEnabled;
            s && this.enableEventListeners(), this.state.eventsEnabled = s
        }

        function te(e, t) {
            for (var n, i = 0; i < t.length; i++)(n = t[i]).enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
        }
        return J.Utils = ("undefined" == typeof window ? global : window).PopperUtils, J.placements = G, J.Defaults = {
            placement: "bottom",
            positionFixed: !1,
            eventsEnabled: !0,
            removeOnDestroy: !1,
            onCreate: function() {},
            onUpdate: function() {},
            modifiers: {
                shift: {
                    order: 100,
                    enabled: !0,
                    fn: function(e) {
                        var t = e.placement,
                            n = t.split("-")[0],
                            i = t.split("-")[1];
                        if (i) {
                            var s = e.offsets,
                                r = s.reference,
                                o = s.popper,
                                a = -1 !== ["bottom", "top"].indexOf(n),
                                l = a ? "left" : "top",
                                c = a ? "width" : "height",
                                d = {
                                    start: R({}, l, r[l]),
                                    end: R({}, l, r[l] + r[c] - o[c])
                                };
                            e.offsets.popper = V({}, o, d[i])
                        }
                        return e
                    }
                },
                offset: {
                    order: 200,
                    enabled: !0,
                    fn: function(e, t) {
                        var n, i = t.offset,
                            s = e.placement,
                            r = e.offsets,
                            o = r.popper,
                            a = r.reference,
                            l = s.split("-")[0];
                        return n = d(+i) ? [+i, 0] : I(i, o, a, l), "left" === l ? (o.top += n[0], o.left -= n[1]) : "right" === l ? (o.top += n[0], o.left += n[1]) : "top" === l ? (o.left += n[0], o.top -= n[1]) : "bottom" === l && (o.left += n[0], o.top += n[1]), e.popper = o, e
                    },
                    offset: 0
                },
                preventOverflow: {
                    order: 300,
                    enabled: !0,
                    fn: function(e, i) {
                        var t = i.boundariesElement || y(e.instance.popper);
                        e.instance.reference === t && (t = y(t));
                        var n = A("transform"),
                            s = e.instance.popper.style,
                            r = s.top,
                            o = s.left,
                            a = s[n];
                        s.top = "", s.left = "", s[n] = "";
                        var l = E(e.instance.popper, e.instance.reference, i.padding, t, e.positionFixed);
                        s.top = r, s.left = o, s[n] = a, i.boundaries = l;
                        var c = i.priority,
                            d = e.offsets.popper,
                            u = {
                                primary: function(e) {
                                    var t = d[e];
                                    return d[e] < l[e] && !i.escapeWithReference && (t = j(d[e], l[e])), R({}, e, t)
                                },
                                secondary: function(e) {
                                    var t = "right" === e ? "left" : "top",
                                        n = d[t];
                                    return d[e] > l[e] && !i.escapeWithReference && (n = P(d[t], l[e] - ("right" === e ? d.width : d.height))), R({}, t, n)
                                }
                            };
                        return c.forEach(function(e) {
                            var t = -1 === ["left", "top"].indexOf(e) ? "secondary" : "primary";
                            d = V({}, d, u[t](e))
                        }), e.offsets.popper = d, e
                    },
                    priority: ["left", "right", "top", "bottom"],
                    padding: 5,
                    boundariesElement: "scrollParent"
                },
                keepTogether: {
                    order: 400,
                    enabled: !0,
                    fn: function(e) {
                        var t = e.offsets,
                            n = t.popper,
                            i = t.reference,
                            s = e.placement.split("-")[0],
                            r = N,
                            o = -1 !== ["top", "bottom"].indexOf(s),
                            a = o ? "right" : "bottom",
                            l = o ? "left" : "top",
                            c = o ? "width" : "height";
                        return n[a] < r(i[l]) && (e.offsets.popper[l] = r(i[l]) - n[c]), n[l] > r(i[a]) && (e.offsets.popper[l] = r(i[a])), e
                    }
                },
                arrow: {
                    order: 500,
                    enabled: !0,
                    fn: function(e, t) {
                        var n;
                        if (!O(e.instance.modifiers, "arrow", "keepTogether")) return e;
                        var i = t.element;
                        if ("string" == typeof i) {
                            if (!(i = e.instance.popper.querySelector(i))) return e
                        } else if (!e.instance.popper.contains(i)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), e;
                        var s = e.placement.split("-")[0],
                            r = e.offsets,
                            o = r.popper,
                            a = r.reference,
                            l = -1 !== ["left", "right"].indexOf(s),
                            c = l ? "height" : "width",
                            d = l ? "Top" : "Left",
                            u = d.toLowerCase(),
                            h = l ? "left" : "top",
                            p = l ? "bottom" : "right",
                            f = _(i)[c];
                        a[p] - f < o[u] && (e.offsets.popper[u] -= o[u] - (a[p] - f)), a[u] + f > o[p] && (e.offsets.popper[u] += a[u] + f - o[p]), e.offsets.popper = x(e.offsets.popper);
                        var m = a[u] + a[c] / 2 - f / 2,
                            g = w(e.instance.popper),
                            v = parseFloat(g["margin" + d], 10),
                            y = parseFloat(g["border" + d + "Width"], 10),
                            b = m - e.offsets.popper[u] - v - y;
                        return b = j(P(o[c] - f, b), 0), e.arrowElement = i, e.offsets.arrow = (R(n = {}, u, L(b)), R(n, h, ""), n), e
                    },
                    element: "[x-arrow]"
                },
                flip: {
                    order: 600,
                    enabled: !0,
                    fn: function(p, f) {
                        if (e(p.instance.modifiers, "inner")) return p;
                        if (p.flipped && p.placement === p.originalPlacement) return p;
                        var m = E(p.instance.popper, p.instance.reference, f.padding, f.boundariesElement, p.positionFixed),
                            g = p.placement.split("-")[0],
                            v = k(g),
                            y = p.placement.split("-")[1] || "",
                            b = [];
                        switch (f.behavior) {
                            case Q:
                                b = [g, v];
                                break;
                            case K:
                                b = s(g);
                                break;
                            case Z:
                                b = s(g, !0);
                                break;
                            default:
                                b = f.behavior
                        }
                        return b.forEach(function(e, t) {
                            if (g !== e || b.length === t + 1) return p;
                            g = p.placement.split("-")[0], v = k(g);
                            var n = p.offsets.popper,
                                i = p.offsets.reference,
                                s = N,
                                r = "left" === g && s(n.right) > s(i.left) || "right" === g && s(n.left) < s(i.right) || "top" === g && s(n.bottom) > s(i.top) || "bottom" === g && s(n.top) < s(i.bottom),
                                o = s(n.left) < s(m.left),
                                a = s(n.right) > s(m.right),
                                l = s(n.top) < s(m.top),
                                c = s(n.bottom) > s(m.bottom),
                                d = "left" === g && o || "right" === g && a || "top" === g && l || "bottom" === g && c,
                                u = -1 !== ["top", "bottom"].indexOf(g),
                                h = !!f.flipVariations && (u && "start" === y && o || u && "end" === y && a || !u && "start" === y && l || !u && "end" === y && c);
                            (r || d || h) && (p.flipped = !0, (r || d) && (g = b[t + 1]), h && (y = function(e) {
                                return "end" === e ? "start" : "start" === e ? "end" : e
                            }(y)), p.placement = g + (y ? "-" + y : ""), p.offsets.popper = V({}, p.offsets.popper, D(p.instance.popper, p.offsets.reference, p.placement)), p = M(p.instance.modifiers, p, "flip"))
                        }), p
                    },
                    behavior: "flip",
                    padding: 5,
                    boundariesElement: "viewport"
                },
                inner: {
                    order: 700,
                    enabled: !1,
                    fn: function(e) {
                        var t = e.placement,
                            n = t.split("-")[0],
                            i = e.offsets,
                            s = i.popper,
                            r = i.reference,
                            o = -1 !== ["left", "right"].indexOf(n),
                            a = -1 === ["top", "left"].indexOf(n);
                        return s[o ? "left" : "top"] = r[n] - (a ? s[o ? "width" : "height"] : 0), e.placement = k(t), e.offsets.popper = x(s), e
                    }
                },
                hide: {
                    order: 800,
                    enabled: !0,
                    fn: function(e) {
                        if (!O(e.instance.modifiers, "hide", "preventOverflow")) return e;
                        var t = e.offsets.reference,
                            n = $(e.instance.modifiers, function(e) {
                                return "preventOverflow" === e.name
                            }).boundaries;
                        if (t.bottom < n.top || t.left > n.right || t.top > n.bottom || t.right < n.left) {
                            if (!0 === e.hide) return e;
                            e.hide = !0, e.attributes["x-out-of-boundaries"] = ""
                        } else {
                            if (!1 === e.hide) return e;
                            e.hide = !1, e.attributes["x-out-of-boundaries"] = !1
                        }
                        return e
                    }
                },
                computeStyle: {
                    order: 850,
                    enabled: !0,
                    fn: function(e, t) {
                        var n = t.x,
                            i = t.y,
                            s = e.offsets.popper,
                            r = $(e.instance.modifiers, function(e) {
                                return "applyStyle" === e.name
                            }).gpuAcceleration;
                        void 0 !== r && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
                        var o, a, l = void 0 === r ? t.gpuAcceleration : r,
                            c = T(y(e.instance.popper)),
                            d = {
                                position: s.position
                            },
                            u = {
                                left: N(s.left),
                                top: L(s.top),
                                bottom: L(s.bottom),
                                right: N(s.right)
                            },
                            h = "bottom" === n ? "top" : "bottom",
                            p = "right" === i ? "left" : "right",
                            f = A("transform");
                        if (a = "bottom" == h ? -c.height + u.bottom : u.top, o = "right" == p ? -c.width + u.right : u.left, l && f) d[f] = "translate3d(" + o + "px, " + a + "px, 0)", d[h] = 0, d[p] = 0, d.willChange = "transform";
                        else {
                            var m = "bottom" == h ? -1 : 1,
                                g = "right" == p ? -1 : 1;
                            d[h] = a * m, d[p] = o * g, d.willChange = h + ", " + p
                        }
                        var v = {
                            "x-placement": e.placement
                        };
                        return e.attributes = V({}, v, e.attributes), e.styles = V({}, d, e.styles), e.arrowStyles = V({}, e.offsets.arrow, e.arrowStyles), e
                    },
                    gpuAcceleration: !0,
                    x: "bottom",
                    y: "right"
                },
                applyStyle: {
                    order: 900,
                    enabled: !0,
                    fn: function(e) {
                        return h(e.instance.popper, e.styles),
                            function(t, n) {
                                Object.keys(n).forEach(function(e) {
                                    !1 === n[e] ? t.removeAttribute(e) : t.setAttribute(e, n[e])
                                })
                            }(e.instance.popper, e.attributes), e.arrowElement && Object.keys(e.arrowStyles).length && h(e.arrowElement, e.arrowStyles), e
                    },
                    onLoad: function(e, t, n, i, s) {
                        var r = c(s, t, e, n.positionFixed),
                            o = a(n.placement, r, t, e, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);
                        return t.setAttribute("x-placement", o), h(t, {
                            position: n.positionFixed ? "fixed" : "absolute"
                        }), n
                    },
                    gpuAcceleration: void 0
                }
            }
        }, J
    }), function(e, t) {
        "object" == typeof exports && "undefined" != typeof module ? t(exports, require("jquery"), require("popper.js")) : "function" == typeof define && define.amd ? define(["exports", "jquery", "popper.js"], t) : t(e.bootstrap = {}, e.jQuery, e.Popper)
    }(this, function(e, t, d) {
        "use strict";

        function i(e, t) {
            for (var n = 0; n < t.length; n++) {
                var i = t[n];
                i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
            }
        }

        function r(e, t, n) {
            return t && i(e.prototype, t), n && i(e, n), e
        }

        function o(s) {
            for (var e = 1; e < arguments.length; e++) {
                var r = null != arguments[e] ? arguments[e] : {},
                    t = Object.keys(r);
                "function" == typeof Object.getOwnPropertySymbols && (t = t.concat(Object.getOwnPropertySymbols(r).filter(function(e) {
                    return Object.getOwnPropertyDescriptor(r, e).enumerable
                }))), t.forEach(function(e) {
                    var t, n, i;
                    t = s, i = r[n = e], n in t ? Object.defineProperty(t, n, {
                        value: i,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : t[n] = i
                })
            }
            return s
        }
        t = t && t.hasOwnProperty("default") ? t.default : t, d = d && d.hasOwnProperty("default") ? d.default : d;
        var s, n, a, l, c, u, h, p, f, m, g, v, y, b, w, x, T, C, S, E, _, k, D, $, M, A, O, I, P, L, N, j, H, z, q, F, R, Y, W, B, U, V, G, X, Q, K, Z, J, ee, te, ne, ie, se, re, oe, ae, le, ce, de, ue, he, pe, fe, me, ge, ve, ye, be, we, xe, Te, Ce, Se, Ee, _e, ke, De, $e, Me, Ae, Oe, Ie, Pe, Le, Ne, je, He, ze, qe, Fe, Re, Ye, We, Be, Ue, Ve, Ge, Xe, Qe, Ke, Ze, Je, et, tt, nt, it, st, rt, ot, at, lt, ct, dt, ut, ht, pt, ft, mt, gt, vt, yt, bt, wt, xt, Tt, Ct, St, Et, _t, kt, Dt, $t, Mt, At, Ot, It, Pt, Lt, Nt = (Pt = "transitionend", Lt = {
                TRANSITION_END: "bsTransitionEnd",
                getUID: function(e) {
                    for (; e += ~~(1e6 * Math.random()), document.getElementById(e););
                    return e
                },
                getSelectorFromElement: function(e) {
                    var t = e.getAttribute("data-target");
                    t && "#" !== t || (t = e.getAttribute("href") || "");
                    try {
                        return document.querySelector(t) ? t : null
                    } catch (e) {
                        return null
                    }
                },
                getTransitionDurationFromElement: function(e) {
                    if (!e) return 0;
                    var t = It(e).css("transition-duration");
                    return parseFloat(t) ? (t = t.split(",")[0], 1e3 * parseFloat(t)) : 0
                },
                reflow: function(e) {
                    return e.offsetHeight
                },
                triggerTransitionEnd: function(e) {
                    It(e).trigger(Pt)
                },
                supportsTransitionEnd: function() {
                    return Boolean(Pt)
                },
                isElement: function(e) {
                    return (e[0] || e).nodeType
                },
                typeCheckConfig: function(e, t, n) {
                    for (var i in n)
                        if (Object.prototype.hasOwnProperty.call(n, i)) {
                            var s = n[i],
                                r = t[i],
                                o = r && Lt.isElement(r) ? "element" : (a = r, {}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());
                            if (!new RegExp(s).test(o)) throw new Error(e.toUpperCase() + ': Option "' + i + '" provided type "' + o + '" but expected type "' + s + '".')
                        } var a
                }
            }, (It = t).fn.emulateTransitionEnd = function(e) {
                var t = this,
                    n = !1;
                return It(this).one(Lt.TRANSITION_END, function() {
                    n = !0
                }), setTimeout(function() {
                    n || Lt.triggerTransitionEnd(t)
                }, e), this
            }, It.event.special[Lt.TRANSITION_END] = {
                bindType: Pt,
                delegateType: Pt,
                handle: function(e) {
                    if (It(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
                }
            }, Lt),
            jt = (n = "alert", l = "." + (a = "bs.alert"), c = (s = t).fn[n], u = {
                CLOSE: "close" + l,
                CLOSED: "closed" + l,
                CLICK_DATA_API: "click" + l + ".data-api"
            }, "alert", "fade", "show", (Ot = tn.prototype).close = function(e) {
                var t = this._element;
                e && (t = this._getRootElement(e)), this._triggerCloseEvent(t).isDefaultPrevented() || this._removeElement(t)
            }, Ot.dispose = function() {
                s.removeData(this._element, a), this._element = null
            }, Ot._getRootElement = function(e) {
                var t = Nt.getSelectorFromElement(e),
                    n = !1;
                return t && (n = document.querySelector(t)), n = n || s(e).closest(".alert")[0]
            }, Ot._triggerCloseEvent = function(e) {
                var t = s.Event(u.CLOSE);
                return s(e).trigger(t), t
            }, Ot._removeElement = function(t) {
                var n = this;
                if (s(t).removeClass("show"), s(t).hasClass("fade")) {
                    var e = Nt.getTransitionDurationFromElement(t);
                    s(t).one(Nt.TRANSITION_END, function(e) {
                        return n._destroyElement(t, e)
                    }).emulateTransitionEnd(e)
                } else this._destroyElement(t)
            }, Ot._destroyElement = function(e) {
                s(e).detach().trigger(u.CLOSED).remove()
            }, tn._jQueryInterface = function(n) {
                return this.each(function() {
                    var e = s(this),
                        t = e.data(a);
                    t || (t = new tn(this), e.data(a, t)), "close" === n && t[n](this)
                })
            }, tn._handleDismiss = function(t) {
                return function(e) {
                    e && e.preventDefault(), t.close(this)
                }
            }, r(tn, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }]), h = tn, s(document).on(u.CLICK_DATA_API, '[data-dismiss="alert"]', h._handleDismiss(new h)), s.fn[n] = h._jQueryInterface, s.fn[n].Constructor = h, s.fn[n].noConflict = function() {
                return s.fn[n] = c, h._jQueryInterface
            }, h),
            Ht = (f = "button", g = "." + (m = "bs.button"), v = ".data-api", y = (p = t).fn[f], b = "active", "btn", w = '[data-toggle^="button"]', '[data-toggle="buttons"]', "input", ".active", ".btn", x = {
                CLICK_DATA_API: "click" + g + v,
                FOCUS_BLUR_DATA_API: "focus" + g + v + " blur" + g + v
            }, (At = en.prototype).toggle = function() {
                var e = !0,
                    t = !0,
                    n = p(this._element).closest('[data-toggle="buttons"]')[0];
                if (n) {
                    var i = this._element.querySelector("input");
                    if (i) {
                        if ("radio" === i.type)
                            if (i.checked && this._element.classList.contains(b)) e = !1;
                            else {
                                var s = n.querySelector(".active");
                                s && p(s).removeClass(b)
                            } if (e) {
                            if (i.hasAttribute("disabled") || n.hasAttribute("disabled") || i.classList.contains("disabled") || n.classList.contains("disabled")) return;
                            i.checked = !this._element.classList.contains(b), p(i).trigger("change")
                        }
                        i.focus(), t = !1
                    }
                }
                t && this._element.setAttribute("aria-pressed", !this._element.classList.contains(b)), e && p(this._element).toggleClass(b)
            }, At.dispose = function() {
                p.removeData(this._element, m), this._element = null
            }, en._jQueryInterface = function(t) {
                return this.each(function() {
                    var e = p(this).data(m);
                    e || (e = new en(this), p(this).data(m, e)), "toggle" === t && e[t]()
                })
            }, r(en, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }]), T = en, p(document).on(x.CLICK_DATA_API, w, function(e) {
                e.preventDefault();
                var t = e.target;
                p(t).hasClass("btn") || (t = p(t).closest(".btn")), T._jQueryInterface.call(p(t), "toggle")
            }).on(x.FOCUS_BLUR_DATA_API, w, function(e) {
                var t = p(e.target).closest(".btn")[0];
                p(t).toggleClass("focus", /^focus(in)?$/.test(e.type))
            }), p.fn[f] = T._jQueryInterface, p.fn[f].Constructor = T, p.fn[f].noConflict = function() {
                return p.fn[f] = y, T._jQueryInterface
            }, T),
            zt = (S = "carousel", _ = "." + (E = "bs.carousel"), k = ".data-api", D = (C = t).fn[S], $ = {
                interval: 5e3,
                keyboard: !0,
                slide: !1,
                pause: "hover",
                wrap: !0
            }, M = {
                interval: "(number|boolean)",
                keyboard: "boolean",
                slide: "(boolean|string)",
                pause: "(string|boolean)",
                wrap: "boolean"
            }, A = "next", O = "prev", "left", "right", I = {
                SLIDE: "slide" + _,
                SLID: "slid" + _,
                KEYDOWN: "keydown" + _,
                MOUSEENTER: "mouseenter" + _,
                MOUSELEAVE: "mouseleave" + _,
                TOUCHEND: "touchend" + _,
                LOAD_DATA_API: "load" + _ + k,
                CLICK_DATA_API: "click" + _ + k
            }, "carousel", P = "active", "slide", "carousel-item-right", "carousel-item-left", "carousel-item-next", "carousel-item-prev", ".active", L = ".active.carousel-item", ".carousel-item", ".carousel-item-next, .carousel-item-prev", ".carousel-indicators", "[data-slide], [data-slide-to]", '[data-ride="carousel"]', (Mt = Jt.prototype).next = function() {
                this._isSliding || this._slide(A)
            }, Mt.nextWhenVisible = function() {
                !document.hidden && C(this._element).is(":visible") && "hidden" !== C(this._element).css("visibility") && this.next()
            }, Mt.prev = function() {
                this._isSliding || this._slide(O)
            }, Mt.pause = function(e) {
                e || (this._isPaused = !0), this._element.querySelector(".carousel-item-next, .carousel-item-prev") && (Nt.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
            }, Mt.cycle = function(e) {
                e || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
            }, Mt.to = function(e) {
                var t = this;
                this._activeElement = this._element.querySelector(L);
                var n = this._getItemIndex(this._activeElement);
                if (!(e > this._items.length - 1 || e < 0))
                    if (this._isSliding) C(this._element).one(I.SLID, function() {
                        return t.to(e)
                    });
                    else {
                        if (n === e) return this.pause(), void this.cycle();
                        var i = n < e ? A : O;
                        this._slide(i, this._items[e])
                    }
            }, Mt.dispose = function() {
                C(this._element).off(_), C.removeData(this._element, E), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
            }, Mt._getConfig = function(e) {
                return e = o({}, $, e), Nt.typeCheckConfig(S, e, M), e
            }, Mt._addEventListeners = function() {
                var t = this;
                this._config.keyboard && C(this._element).on(I.KEYDOWN, function(e) {
                    return t._keydown(e)
                }), "hover" === this._config.pause && (C(this._element).on(I.MOUSEENTER, function(e) {
                    return t.pause(e)
                }).on(I.MOUSELEAVE, function(e) {
                    return t.cycle(e)
                }), "ontouchstart" in document.documentElement && C(this._element).on(I.TOUCHEND, function() {
                    t.pause(), t.touchTimeout && clearTimeout(t.touchTimeout), t.touchTimeout = setTimeout(function(e) {
                        return t.cycle(e)
                    }, 500 + t._config.interval)
                }))
            }, Mt._keydown = function(e) {
                if (!/input|textarea/i.test(e.target.tagName)) switch (e.which) {
                    case 37:
                        e.preventDefault(), this.prev();
                        break;
                    case 39:
                        e.preventDefault(), this.next()
                }
            }, Mt._getItemIndex = function(e) {
                return this._items = e && e.parentNode ? [].slice.call(e.parentNode.querySelectorAll(".carousel-item")) : [], this._items.indexOf(e)
            }, Mt._getItemByDirection = function(e, t) {
                var n = e === A,
                    i = e === O,
                    s = this._getItemIndex(t),
                    r = this._items.length - 1;
                if ((i && 0 === s || n && s === r) && !this._config.wrap) return t;
                var o = (s + (e === O ? -1 : 1)) % this._items.length;
                return -1 == o ? this._items[this._items.length - 1] : this._items[o]
            }, Mt._triggerSlideEvent = function(e, t) {
                var n = this._getItemIndex(e),
                    i = this._getItemIndex(this._element.querySelector(L)),
                    s = C.Event(I.SLIDE, {
                        relatedTarget: e,
                        direction: t,
                        from: i,
                        to: n
                    });
                return C(this._element).trigger(s), s
            }, Mt._setActiveIndicatorElement = function(e) {
                if (this._indicatorsElement) {
                    var t = [].slice.call(this._indicatorsElement.querySelectorAll(".active"));
                    C(t).removeClass(P);
                    var n = this._indicatorsElement.children[this._getItemIndex(e)];
                    n && C(n).addClass(P)
                }
            }, Mt._slide = function(e, t) {
                var n, i, s, r = this,
                    o = this._element.querySelector(L),
                    a = this._getItemIndex(o),
                    l = t || o && this._getItemByDirection(e, o),
                    c = this._getItemIndex(l),
                    d = Boolean(this._interval);
                if (s = e === A ? (n = "carousel-item-left", i = "carousel-item-next", "left") : (n = "carousel-item-right", i = "carousel-item-prev", "right"), l && C(l).hasClass(P)) this._isSliding = !1;
                else if (!this._triggerSlideEvent(l, s).isDefaultPrevented() && o && l) {
                    this._isSliding = !0, d && this.pause(), this._setActiveIndicatorElement(l);
                    var u = C.Event(I.SLID, {
                        relatedTarget: l,
                        direction: s,
                        from: a,
                        to: c
                    });
                    if (C(this._element).hasClass("slide")) {
                        C(l).addClass(i), Nt.reflow(l), C(o).addClass(n), C(l).addClass(n);
                        var h = Nt.getTransitionDurationFromElement(o);
                        C(o).one(Nt.TRANSITION_END, function() {
                            C(l).removeClass(n + " " + i).addClass(P), C(o).removeClass(P + " " + i + " " + n), r._isSliding = !1, setTimeout(function() {
                                return C(r._element).trigger(u)
                            }, 0)
                        }).emulateTransitionEnd(h)
                    } else C(o).removeClass(P), C(l).addClass(P), this._isSliding = !1, C(this._element).trigger(u);
                    d && this.cycle()
                }
            }, Jt._jQueryInterface = function(i) {
                return this.each(function() {
                    var e = C(this).data(E),
                        t = o({}, $, C(this).data());
                    "object" == typeof i && (t = o({}, t, i));
                    var n = "string" == typeof i ? i : t.slide;
                    if (e || (e = new Jt(this, t), C(this).data(E, e)), "number" == typeof i) e.to(i);
                    else if ("string" == typeof n) {
                        if (void 0 === e[n]) throw new TypeError('No method named "' + n + '"');
                        e[n]()
                    } else t.interval && (e.pause(), e.cycle())
                })
            }, Jt._dataApiClickHandler = function(e) {
                var t = Nt.getSelectorFromElement(this);
                if (t) {
                    var n = C(t)[0];
                    if (n && C(n).hasClass("carousel")) {
                        var i = o({}, C(n).data(), C(this).data()),
                            s = this.getAttribute("data-slide-to");
                        s && (i.interval = !1), Jt._jQueryInterface.call(C(n), i), s && C(n).data(E).to(s), e.preventDefault()
                    }
                }
            }, r(Jt, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }, {
                key: "Default",
                get: function() {
                    return $
                }
            }]), N = Jt, C(document).on(I.CLICK_DATA_API, "[data-slide], [data-slide-to]", N._dataApiClickHandler), C(window).on(I.LOAD_DATA_API, function() {
                for (var e = [].slice.call(document.querySelectorAll('[data-ride="carousel"]')), t = 0, n = e.length; t < n; t++) {
                    var i = C(e[t]);
                    N._jQueryInterface.call(i, i.data())
                }
            }), C.fn[S] = N._jQueryInterface, C.fn[S].Constructor = N, C.fn[S].noConflict = function() {
                return C.fn[S] = D, N._jQueryInterface
            }, N),
            qt = (H = "collapse", q = "." + (z = "bs.collapse"), F = (j = t).fn[H], R = {
                toggle: !0,
                parent: ""
            }, Y = {
                toggle: "boolean",
                parent: "(string|element)"
            }, W = {
                SHOW: "show" + q,
                SHOWN: "shown" + q,
                HIDE: "hide" + q,
                HIDDEN: "hidden" + q,
                CLICK_DATA_API: "click" + q + ".data-api"
            }, B = "show", U = "collapse", V = "collapsing", G = "collapsed", "width", "height", ".show, .collapsing", X = '[data-toggle="collapse"]', ($t = Zt.prototype).toggle = function() {
                j(this._element).hasClass(B) ? this.hide() : this.show()
            }, $t.show = function() {
                var e, t, n = this;
                if (!(this._isTransitioning || j(this._element).hasClass(B) || (this._parent && 0 === (e = [].slice.call(this._parent.querySelectorAll(".show, .collapsing")).filter(function(e) {
                        return e.getAttribute("data-parent") === n._config.parent
                    })).length && (e = null), e && (t = j(e).not(this._selector).data(z)) && t._isTransitioning))) {
                    var i = j.Event(W.SHOW);
                    if (j(this._element).trigger(i), !i.isDefaultPrevented()) {
                        e && (Zt._jQueryInterface.call(j(e).not(this._selector), "hide"), t || j(e).data(z, null));
                        var s = this._getDimension();
                        j(this._element).removeClass(U).addClass(V), this._element.style[s] = 0, this._triggerArray.length && j(this._triggerArray).removeClass(G).attr("aria-expanded", !0), this.setTransitioning(!0);
                        var r = "scroll" + (s[0].toUpperCase() + s.slice(1)),
                            o = Nt.getTransitionDurationFromElement(this._element);
                        j(this._element).one(Nt.TRANSITION_END, function() {
                            j(n._element).removeClass(V).addClass(U).addClass(B), n._element.style[s] = "", n.setTransitioning(!1), j(n._element).trigger(W.SHOWN)
                        }).emulateTransitionEnd(o), this._element.style[s] = this._element[r] + "px"
                    }
                }
            }, $t.hide = function() {
                var e = this;
                if (!this._isTransitioning && j(this._element).hasClass(B)) {
                    var t = j.Event(W.HIDE);
                    if (j(this._element).trigger(t), !t.isDefaultPrevented()) {
                        var n = this._getDimension();
                        this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", Nt.reflow(this._element), j(this._element).addClass(V).removeClass(U).removeClass(B);
                        var i = this._triggerArray.length;
                        if (0 < i)
                            for (var s = 0; s < i; s++) {
                                var r = this._triggerArray[s],
                                    o = Nt.getSelectorFromElement(r);
                                null !== o && (j([].slice.call(document.querySelectorAll(o))).hasClass(B) || j(r).addClass(G).attr("aria-expanded", !1))
                            }
                        this.setTransitioning(!0), this._element.style[n] = "";
                        var a = Nt.getTransitionDurationFromElement(this._element);
                        j(this._element).one(Nt.TRANSITION_END, function() {
                            e.setTransitioning(!1), j(e._element).removeClass(V).addClass(U).trigger(W.HIDDEN)
                        }).emulateTransitionEnd(a)
                    }
                }
            }, $t.setTransitioning = function(e) {
                this._isTransitioning = e
            }, $t.dispose = function() {
                j.removeData(this._element, z), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
            }, $t._getConfig = function(e) {
                return (e = o({}, R, e)).toggle = Boolean(e.toggle), Nt.typeCheckConfig(H, e, Y), e
            }, $t._getDimension = function() {
                return j(this._element).hasClass("width") ? "width" : "height"
            }, $t._getParent = function() {
                var n = this,
                    e = null;
                Nt.isElement(this._config.parent) ? (e = this._config.parent, void 0 !== this._config.parent.jquery && (e = this._config.parent[0])) : e = document.querySelector(this._config.parent);
                var t = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
                    i = [].slice.call(e.querySelectorAll(t));
                return j(i).each(function(e, t) {
                    n._addAriaAndCollapsedClass(Zt._getTargetFromElement(t), [t])
                }), e
            }, $t._addAriaAndCollapsedClass = function(e, t) {
                if (e) {
                    var n = j(e).hasClass(B);
                    t.length && j(t).toggleClass(G, !n).attr("aria-expanded", n)
                }
            }, Zt._getTargetFromElement = function(e) {
                var t = Nt.getSelectorFromElement(e);
                return t ? document.querySelector(t) : null
            }, Zt._jQueryInterface = function(i) {
                return this.each(function() {
                    var e = j(this),
                        t = e.data(z),
                        n = o({}, R, e.data(), "object" == typeof i && i ? i : {});
                    if (!t && n.toggle && /show|hide/.test(i) && (n.toggle = !1), t || (t = new Zt(this, n), e.data(z, t)), "string" == typeof i) {
                        if (void 0 === t[i]) throw new TypeError('No method named "' + i + '"');
                        t[i]()
                    }
                })
            }, r(Zt, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }, {
                key: "Default",
                get: function() {
                    return R
                }
            }]), Q = Zt, j(document).on(W.CLICK_DATA_API, X, function(e) {
                "A" === e.currentTarget.tagName && e.preventDefault();
                var n = j(this),
                    t = Nt.getSelectorFromElement(this),
                    i = [].slice.call(document.querySelectorAll(t));
                j(i).each(function() {
                    var e = j(this),
                        t = e.data(z) ? "toggle" : n.data();
                    Q._jQueryInterface.call(e, t)
                })
            }), j.fn[H] = Q._jQueryInterface, j.fn[H].Constructor = Q, j.fn[H].noConflict = function() {
                return j.fn[H] = F, Q._jQueryInterface
            }, Q),
            Ft = (Z = "dropdown", ee = "." + (J = "bs.dropdown"), te = ".data-api", ne = (K = t).fn[Z], ie = new RegExp("38|40|27"), se = {
                HIDE: "hide" + ee,
                HIDDEN: "hidden" + ee,
                SHOW: "show" + ee,
                SHOWN: "shown" + ee,
                CLICK: "click" + ee,
                CLICK_DATA_API: "click" + ee + te,
                KEYDOWN_DATA_API: "keydown" + ee + te,
                KEYUP_DATA_API: "keyup" + ee + te
            }, re = "disabled", oe = "show", "dropup", "dropright", "dropleft", ae = "dropdown-menu-right", "position-static", le = '[data-toggle="dropdown"]', ".dropdown form", ce = ".dropdown-menu", ".navbar-nav", ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)", "top-start", "top-end", "bottom-start", "bottom-end", "right-start", "left-start", de = {
                offset: 0,
                flip: !0,
                boundary: "scrollParent",
                reference: "toggle",
                display: "dynamic"
            }, ue = {
                offset: "(number|string|function)",
                flip: "boolean",
                boundary: "(string|element)",
                reference: "(string|element)",
                display: "string"
            }, (Dt = Kt.prototype).toggle = function() {
                if (!this._element.disabled && !K(this._element).hasClass(re)) {
                    var e = Kt._getParentFromElement(this._element),
                        t = K(this._menu).hasClass(oe);
                    if (Kt._clearMenus(), !t) {
                        var n = {
                                relatedTarget: this._element
                            },
                            i = K.Event(se.SHOW, n);
                        if (K(e).trigger(i), !i.isDefaultPrevented()) {
                            if (!this._inNavbar) {
                                if (void 0 === d) throw new TypeError("Bootstrap dropdown require Popper.js (https://popper.js.org)");
                                var s = this._element;
                                "parent" === this._config.reference ? s = e : Nt.isElement(this._config.reference) && (s = this._config.reference, void 0 !== this._config.reference.jquery && (s = this._config.reference[0])), "scrollParent" !== this._config.boundary && K(e).addClass("position-static"), this._popper = new d(s, this._menu, this._getPopperConfig())
                            }
                            "ontouchstart" in document.documentElement && 0 === K(e).closest(".navbar-nav").length && K(document.body).children().on("mouseover", null, K.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), K(this._menu).toggleClass(oe), K(e).toggleClass(oe).trigger(K.Event(se.SHOWN, n))
                        }
                    }
                }
            }, Dt.dispose = function() {
                K.removeData(this._element, J), K(this._element).off(ee), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null)
            }, Dt.update = function() {
                this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
            }, Dt._addEventListeners = function() {
                var t = this;
                K(this._element).on(se.CLICK, function(e) {
                    e.preventDefault(), e.stopPropagation(), t.toggle()
                })
            }, Dt._getConfig = function(e) {
                return e = o({}, this.constructor.Default, K(this._element).data(), e), Nt.typeCheckConfig(Z, e, this.constructor.DefaultType), e
            }, Dt._getMenuElement = function() {
                if (!this._menu) {
                    var e = Kt._getParentFromElement(this._element);
                    e && (this._menu = e.querySelector(ce))
                }
                return this._menu
            }, Dt._getPlacement = function() {
                var e = K(this._element.parentNode),
                    t = "bottom-start";
                return e.hasClass("dropup") ? (t = "top-start", K(this._menu).hasClass(ae) && (t = "top-end")) : e.hasClass("dropright") ? t = "right-start" : e.hasClass("dropleft") ? t = "left-start" : K(this._menu).hasClass(ae) && (t = "bottom-end"), t
            }, Dt._detectNavbar = function() {
                return 0 < K(this._element).closest(".navbar").length
            }, Dt._getPopperConfig = function() {
                var t = this,
                    e = {};
                "function" == typeof this._config.offset ? e.fn = function(e) {
                    return e.offsets = o({}, e.offsets, t._config.offset(e.offsets) || {}), e
                } : e.offset = this._config.offset;
                var n = {
                    placement: this._getPlacement(),
                    modifiers: {
                        offset: e,
                        flip: {
                            enabled: this._config.flip
                        },
                        preventOverflow: {
                            boundariesElement: this._config.boundary
                        }
                    }
                };
                return "static" === this._config.display && (n.modifiers.applyStyle = {
                    enabled: !1
                }), n
            }, Kt._jQueryInterface = function(t) {
                return this.each(function() {
                    var e = K(this).data(J);
                    if (e || (e = new Kt(this, "object" == typeof t ? t : null), K(this).data(J, e)), "string" == typeof t) {
                        if (void 0 === e[t]) throw new TypeError('No method named "' + t + '"');
                        e[t]()
                    }
                })
            }, Kt._clearMenus = function(e) {
                if (!e || 3 !== e.which && ("keyup" !== e.type || 9 === e.which))
                    for (var t = [].slice.call(document.querySelectorAll(le)), n = 0, i = t.length; n < i; n++) {
                        var s = Kt._getParentFromElement(t[n]),
                            r = K(t[n]).data(J),
                            o = {
                                relatedTarget: t[n]
                            };
                        if (e && "click" === e.type && (o.clickEvent = e), r) {
                            var a = r._menu;
                            if (K(s).hasClass(oe) && !(e && ("click" === e.type && /input|textarea/i.test(e.target.tagName) || "keyup" === e.type && 9 === e.which) && K.contains(s, e.target))) {
                                var l = K.Event(se.HIDE, o);
                                K(s).trigger(l), l.isDefaultPrevented() || ("ontouchstart" in document.documentElement && K(document.body).children().off("mouseover", null, K.noop), t[n].setAttribute("aria-expanded", "false"), K(a).removeClass(oe), K(s).removeClass(oe).trigger(K.Event(se.HIDDEN, o)))
                            }
                        }
                    }
            }, Kt._getParentFromElement = function(e) {
                var t, n = Nt.getSelectorFromElement(e);
                return n && (t = document.querySelector(n)), t || e.parentNode
            }, Kt._dataApiKeydownHandler = function(e) {
                if ((/input|textarea/i.test(e.target.tagName) ? !(32 === e.which || 27 !== e.which && (40 !== e.which && 38 !== e.which || K(e.target).closest(ce).length)) : ie.test(e.which)) && (e.preventDefault(), e.stopPropagation(), !this.disabled && !K(this).hasClass(re))) {
                    var t = Kt._getParentFromElement(this),
                        n = K(t).hasClass(oe);
                    if ((n || 27 === e.which && 32 === e.which) && (!n || 27 !== e.which && 32 !== e.which)) {
                        var i = [].slice.call(t.querySelectorAll(".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)"));
                        if (0 !== i.length) {
                            var s = i.indexOf(e.target);
                            38 === e.which && 0 < s && s--, 40 === e.which && s < i.length - 1 && s++, s < 0 && (s = 0), i[s].focus()
                        }
                    } else {
                        if (27 === e.which) {
                            var r = t.querySelector(le);
                            K(r).trigger("focus")
                        }
                        K(this).trigger("click")
                    }
                }
            }, r(Kt, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }, {
                key: "Default",
                get: function() {
                    return de
                }
            }, {
                key: "DefaultType",
                get: function() {
                    return ue
                }
            }]), he = Kt, K(document).on(se.KEYDOWN_DATA_API, le, he._dataApiKeydownHandler).on(se.KEYDOWN_DATA_API, ce, he._dataApiKeydownHandler).on(se.CLICK_DATA_API + " " + se.KEYUP_DATA_API, he._clearMenus).on(se.CLICK_DATA_API, le, function(e) {
                e.preventDefault(), e.stopPropagation(), he._jQueryInterface.call(K(this), "toggle")
            }).on(se.CLICK_DATA_API, ".dropdown form", function(e) {
                e.stopPropagation()
            }), K.fn[Z] = he._jQueryInterface, K.fn[Z].Constructor = he, K.fn[Z].noConflict = function() {
                return K.fn[Z] = ne, he._jQueryInterface
            }, he),
            Rt = (fe = "modal", ge = "." + (me = "bs.modal"), ve = (pe = t).fn[fe], ye = {
                backdrop: !0,
                keyboard: !0,
                focus: !0,
                show: !0
            }, be = {
                backdrop: "(boolean|string)",
                keyboard: "boolean",
                focus: "boolean",
                show: "boolean"
            }, we = {
                HIDE: "hide" + ge,
                HIDDEN: "hidden" + ge,
                SHOW: "show" + ge,
                SHOWN: "shown" + ge,
                FOCUSIN: "focusin" + ge,
                RESIZE: "resize" + ge,
                CLICK_DISMISS: "click.dismiss" + ge,
                KEYDOWN_DISMISS: "keydown.dismiss" + ge,
                MOUSEUP_DISMISS: "mouseup.dismiss" + ge,
                MOUSEDOWN_DISMISS: "mousedown.dismiss" + ge,
                CLICK_DATA_API: "click" + ge + ".data-api"
            }, "modal-scrollbar-measure", "modal-backdrop", xe = "modal-open", Te = "fade", Ce = "show", ".modal-dialog", '[data-toggle="modal"]', '[data-dismiss="modal"]', Se = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top", Ee = ".sticky-top", (kt = Qt.prototype).toggle = function(e) {
                return this._isShown ? this.hide() : this.show(e)
            }, kt.show = function(e) {
                var t = this;
                if (!this._isTransitioning && !this._isShown) {
                    pe(this._element).hasClass(Te) && (this._isTransitioning = !0);
                    var n = pe.Event(we.SHOW, {
                        relatedTarget: e
                    });
                    pe(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), pe(document.body).addClass(xe), this._setEscapeEvent(), this._setResizeEvent(), pe(this._element).on(we.CLICK_DISMISS, '[data-dismiss="modal"]', function(e) {
                        return t.hide(e)
                    }), pe(this._dialog).on(we.MOUSEDOWN_DISMISS, function() {
                        pe(t._element).one(we.MOUSEUP_DISMISS, function(e) {
                            pe(e.target).is(t._element) && (t._ignoreBackdropClick = !0)
                        })
                    }), this._showBackdrop(function() {
                        return t._showElement(e)
                    }))
                }
            }, kt.hide = function(e) {
                var t = this;
                if (e && e.preventDefault(), !this._isTransitioning && this._isShown) {
                    var n = pe.Event(we.HIDE);
                    if (pe(this._element).trigger(n), this._isShown && !n.isDefaultPrevented()) {
                        this._isShown = !1;
                        var i = pe(this._element).hasClass(Te);
                        if (i && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), pe(document).off(we.FOCUSIN), pe(this._element).removeClass(Ce), pe(this._element).off(we.CLICK_DISMISS), pe(this._dialog).off(we.MOUSEDOWN_DISMISS), i) {
                            var s = Nt.getTransitionDurationFromElement(this._element);
                            pe(this._element).one(Nt.TRANSITION_END, function(e) {
                                return t._hideModal(e)
                            }).emulateTransitionEnd(s)
                        } else this._hideModal()
                    }
                }
            }, kt.dispose = function() {
                pe.removeData(this._element, me), pe(window, document, this._element, this._backdrop).off(ge), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null
            }, kt.handleUpdate = function() {
                this._adjustDialog()
            }, kt._getConfig = function(e) {
                return e = o({}, ye, e), Nt.typeCheckConfig(fe, e, be), e
            }, kt._showElement = function(e) {
                var t = this,
                    n = pe(this._element).hasClass(Te);

                function i() {
                    t._config.focus && t._element.focus(), t._isTransitioning = !1, pe(t._element).trigger(s)
                }
                this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, n && Nt.reflow(this._element), pe(this._element).addClass(Ce), this._config.focus && this._enforceFocus();
                var s = pe.Event(we.SHOWN, {
                    relatedTarget: e
                });
                if (n) {
                    var r = Nt.getTransitionDurationFromElement(this._element);
                    pe(this._dialog).one(Nt.TRANSITION_END, i).emulateTransitionEnd(r)
                } else i()
            }, kt._enforceFocus = function() {
                var t = this;
                pe(document).off(we.FOCUSIN).on(we.FOCUSIN, function(e) {
                    document !== e.target && t._element !== e.target && 0 === pe(t._element).has(e.target).length && t._element.focus()
                })
            }, kt._setEscapeEvent = function() {
                var t = this;
                this._isShown && this._config.keyboard ? pe(this._element).on(we.KEYDOWN_DISMISS, function(e) {
                    27 === e.which && (e.preventDefault(), t.hide())
                }) : this._isShown || pe(this._element).off(we.KEYDOWN_DISMISS)
            }, kt._setResizeEvent = function() {
                var t = this;
                this._isShown ? pe(window).on(we.RESIZE, function(e) {
                    return t.handleUpdate(e)
                }) : pe(window).off(we.RESIZE)
            }, kt._hideModal = function() {
                var e = this;
                this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop(function() {
                    pe(document.body).removeClass(xe), e._resetAdjustments(), e._resetScrollbar(), pe(e._element).trigger(we.HIDDEN)
                })
            }, kt._removeBackdrop = function() {
                this._backdrop && (pe(this._backdrop).remove(), this._backdrop = null)
            }, kt._showBackdrop = function(e) {
                var t = this,
                    n = pe(this._element).hasClass(Te) ? Te : "";
                if (this._isShown && this._config.backdrop) {
                    if (this._backdrop = document.createElement("div"), this._backdrop.className = "modal-backdrop", n && this._backdrop.classList.add(n), pe(this._backdrop).appendTo(document.body), pe(this._element).on(we.CLICK_DISMISS, function(e) {
                            t._ignoreBackdropClick ? t._ignoreBackdropClick = !1 : e.target === e.currentTarget && ("static" === t._config.backdrop ? t._element.focus() : t.hide())
                        }), n && Nt.reflow(this._backdrop), pe(this._backdrop).addClass(Ce), !e) return;
                    if (!n) return void e();
                    var i = Nt.getTransitionDurationFromElement(this._backdrop);
                    pe(this._backdrop).one(Nt.TRANSITION_END, e).emulateTransitionEnd(i)
                } else if (!this._isShown && this._backdrop) {
                    pe(this._backdrop).removeClass(Ce);
                    var s = function() {
                        t._removeBackdrop(), e && e()
                    };
                    if (pe(this._element).hasClass(Te)) {
                        var r = Nt.getTransitionDurationFromElement(this._backdrop);
                        pe(this._backdrop).one(Nt.TRANSITION_END, s).emulateTransitionEnd(r)
                    } else s()
                } else e && e()
            }, kt._adjustDialog = function() {
                var e = this._element.scrollHeight > document.documentElement.clientHeight;
                !this._isBodyOverflowing && e && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !e && (this._element.style.paddingRight = this._scrollbarWidth + "px")
            }, kt._resetAdjustments = function() {
                this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
            }, kt._checkScrollbar = function() {
                var e = document.body.getBoundingClientRect();
                this._isBodyOverflowing = e.left + e.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
            }, kt._setScrollbar = function() {
                var s = this;
                if (this._isBodyOverflowing) {
                    var e = [].slice.call(document.querySelectorAll(Se)),
                        t = [].slice.call(document.querySelectorAll(Ee));
                    pe(e).each(function(e, t) {
                        var n = t.style.paddingRight,
                            i = pe(t).css("padding-right");
                        pe(t).data("padding-right", n).css("padding-right", parseFloat(i) + s._scrollbarWidth + "px")
                    }), pe(t).each(function(e, t) {
                        var n = t.style.marginRight,
                            i = pe(t).css("margin-right");
                        pe(t).data("margin-right", n).css("margin-right", parseFloat(i) - s._scrollbarWidth + "px")
                    });
                    var n = document.body.style.paddingRight,
                        i = pe(document.body).css("padding-right");
                    pe(document.body).data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px")
                }
            }, kt._resetScrollbar = function() {
                var e = [].slice.call(document.querySelectorAll(Se));
                pe(e).each(function(e, t) {
                    var n = pe(t).data("padding-right");
                    pe(t).removeData("padding-right"), t.style.paddingRight = n || ""
                });
                var t = [].slice.call(document.querySelectorAll(Ee));
                pe(t).each(function(e, t) {
                    var n = pe(t).data("margin-right");
                    void 0 !== n && pe(t).css("margin-right", n).removeData("margin-right")
                });
                var n = pe(document.body).data("padding-right");
                pe(document.body).removeData("padding-right"), document.body.style.paddingRight = n || ""
            }, kt._getScrollbarWidth = function() {
                var e = document.createElement("div");
                e.className = "modal-scrollbar-measure", document.body.appendChild(e);
                var t = e.getBoundingClientRect().width - e.clientWidth;
                return document.body.removeChild(e), t
            }, Qt._jQueryInterface = function(n, i) {
                return this.each(function() {
                    var e = pe(this).data(me),
                        t = o({}, ye, pe(this).data(), "object" == typeof n && n ? n : {});
                    if (e || (e = new Qt(this, t), pe(this).data(me, e)), "string" == typeof n) {
                        if (void 0 === e[n]) throw new TypeError('No method named "' + n + '"');
                        e[n](i)
                    } else t.show && e.show(i)
                })
            }, r(Qt, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }, {
                key: "Default",
                get: function() {
                    return ye
                }
            }]), _e = Qt, pe(document).on(we.CLICK_DATA_API, '[data-toggle="modal"]', function(e) {
                var t, n = this,
                    i = Nt.getSelectorFromElement(this);
                i && (t = document.querySelector(i));
                var s = pe(t).data(me) ? "toggle" : o({}, pe(t).data(), pe(this).data());
                "A" !== this.tagName && "AREA" !== this.tagName || e.preventDefault();
                var r = pe(t).one(we.SHOW, function(e) {
                    e.isDefaultPrevented() || r.one(we.HIDDEN, function() {
                        pe(n).is(":visible") && n.focus()
                    })
                });
                _e._jQueryInterface.call(pe(t), s, this)
            }), pe.fn[fe] = _e._jQueryInterface, pe.fn[fe].Constructor = _e, pe.fn[fe].noConflict = function() {
                return pe.fn[fe] = ve, _e._jQueryInterface
            }, _e),
            Yt = (De = "tooltip", Me = "." + ($e = "bs.tooltip"), Ae = (ke = t).fn[De], Oe = "bs-tooltip", Ie = new RegExp("(^|\\s)" + Oe + "\\S+", "g"), Ne = {
                animation: !0,
                template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
                trigger: "hover focus",
                title: "",
                delay: 0,
                html: (Le = {
                    AUTO: "auto",
                    TOP: "top",
                    RIGHT: "right",
                    BOTTOM: "bottom",
                    LEFT: "left"
                }, !1),
                selector: (Pe = {
                    animation: "boolean",
                    template: "string",
                    title: "(string|element|function)",
                    trigger: "string",
                    delay: "(number|object)",
                    html: "boolean",
                    selector: "(string|boolean)",
                    placement: "(string|function)",
                    offset: "(number|string)",
                    container: "(string|element|boolean)",
                    fallbackPlacement: "(string|array)",
                    boundary: "(string|element)"
                }, !1),
                placement: "top",
                offset: 0,
                container: !1,
                fallbackPlacement: "flip",
                boundary: "scrollParent"
            }, "out", He = {
                HIDE: "hide" + Me,
                HIDDEN: "hidden" + Me,
                SHOW: (je = "show") + Me,
                SHOWN: "shown" + Me,
                INSERTED: "inserted" + Me,
                CLICK: "click" + Me,
                FOCUSIN: "focusin" + Me,
                FOCUSOUT: "focusout" + Me,
                MOUSEENTER: "mouseenter" + Me,
                MOUSELEAVE: "mouseleave" + Me
            }, ze = "fade", qe = "show", ".tooltip-inner", ".arrow", Fe = "hover", Re = "focus", "click", "manual", (_t = Xt.prototype).enable = function() {
                this._isEnabled = !0
            }, _t.disable = function() {
                this._isEnabled = !1
            }, _t.toggleEnabled = function() {
                this._isEnabled = !this._isEnabled
            }, _t.toggle = function(e) {
                if (this._isEnabled)
                    if (e) {
                        var t = this.constructor.DATA_KEY,
                            n = ke(e.currentTarget).data(t);
                        n || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), ke(e.currentTarget).data(t, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n)
                    } else {
                        if (ke(this.getTipElement()).hasClass(qe)) return void this._leave(null, this);
                        this._enter(null, this)
                    }
            }, _t.dispose = function() {
                clearTimeout(this._timeout), ke.removeData(this.element, this.constructor.DATA_KEY), ke(this.element).off(this.constructor.EVENT_KEY), ke(this.element).closest(".modal").off("hide.bs.modal"), this.tip && ke(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, (this._activeTrigger = null) !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
            }, _t.show = function() {
                var t = this;
                if ("none" === ke(this.element).css("display")) throw new Error("Please use show on visible elements");
                var e = ke.Event(this.constructor.Event.SHOW);
                if (this.isWithContent() && this._isEnabled) {
                    ke(this.element).trigger(e);
                    var n = ke.contains(this.element.ownerDocument.documentElement, this.element);
                    if (e.isDefaultPrevented() || !n) return;
                    var i = this.getTipElement(),
                        s = Nt.getUID(this.constructor.NAME);
                    i.setAttribute("id", s), this.element.setAttribute("aria-describedby", s), this.setContent(), this.config.animation && ke(i).addClass(ze);
                    var r = "function" == typeof this.config.placement ? this.config.placement.call(this, i, this.element) : this.config.placement,
                        o = this._getAttachment(r);
                    this.addAttachmentClass(o);
                    var a = !1 === this.config.container ? document.body : ke(document).find(this.config.container);
                    ke(i).data(this.constructor.DATA_KEY, this), ke.contains(this.element.ownerDocument.documentElement, this.tip) || ke(i).appendTo(a), ke(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new d(this.element, i, {
                        placement: o,
                        modifiers: {
                            offset: {
                                offset: this.config.offset
                            },
                            flip: {
                                behavior: this.config.fallbackPlacement
                            },
                            arrow: {
                                element: ".arrow"
                            },
                            preventOverflow: {
                                boundariesElement: this.config.boundary
                            }
                        },
                        onCreate: function(e) {
                            e.originalPlacement !== e.placement && t._handlePopperPlacementChange(e)
                        },
                        onUpdate: function(e) {
                            t._handlePopperPlacementChange(e)
                        }
                    }), ke(i).addClass(qe), "ontouchstart" in document.documentElement && ke(document.body).children().on("mouseover", null, ke.noop);
                    var l = function() {
                        t.config.animation && t._fixTransition();
                        var e = t._hoverState;
                        t._hoverState = null, ke(t.element).trigger(t.constructor.Event.SHOWN), "out" === e && t._leave(null, t)
                    };
                    if (ke(this.tip).hasClass(ze)) {
                        var c = Nt.getTransitionDurationFromElement(this.tip);
                        ke(this.tip).one(Nt.TRANSITION_END, l).emulateTransitionEnd(c)
                    } else l()
                }
            }, _t.hide = function(e) {
                function t() {
                    n._hoverState !== je && i.parentNode && i.parentNode.removeChild(i), n._cleanTipClass(), n.element.removeAttribute("aria-describedby"), ke(n.element).trigger(n.constructor.Event.HIDDEN), null !== n._popper && n._popper.destroy(), e && e()
                }
                var n = this,
                    i = this.getTipElement(),
                    s = ke.Event(this.constructor.Event.HIDE);
                if (ke(this.element).trigger(s), !s.isDefaultPrevented()) {
                    if (ke(i).removeClass(qe), "ontouchstart" in document.documentElement && ke(document.body).children().off("mouseover", null, ke.noop), this._activeTrigger.click = !1, this._activeTrigger[Re] = !1, this._activeTrigger[Fe] = !1, ke(this.tip).hasClass(ze)) {
                        var r = Nt.getTransitionDurationFromElement(i);
                        ke(i).one(Nt.TRANSITION_END, t).emulateTransitionEnd(r)
                    } else t();
                    this._hoverState = ""
                }
            }, _t.update = function() {
                null !== this._popper && this._popper.scheduleUpdate()
            }, _t.isWithContent = function() {
                return Boolean(this.getTitle())
            }, _t.addAttachmentClass = function(e) {
                ke(this.getTipElement()).addClass(Oe + "-" + e)
            }, _t.getTipElement = function() {
                return this.tip = this.tip || ke(this.config.template)[0], this.tip
            }, _t.setContent = function() {
                var e = this.getTipElement();
                this.setElementContent(ke(e.querySelectorAll(".tooltip-inner")), this.getTitle()), ke(e).removeClass(ze + " " + qe)
            }, _t.setElementContent = function(e, t) {
                var n = this.config.html;
                "object" == typeof t && (t.nodeType || t.jquery) ? n ? ke(t).parent().is(e) || e.empty().append(t) : e.text(ke(t).text()) : e[n ? "html" : "text"](t)
            }, _t.getTitle = function() {
                var e = this.element.getAttribute("data-original-title");
                return e = e || ("function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title)
            }, _t._getAttachment = function(e) {
                return Le[e.toUpperCase()]
            }, _t._setListeners = function() {
                var i = this;
                this.config.trigger.split(" ").forEach(function(e) {
                    if ("click" === e) ke(i.element).on(i.constructor.Event.CLICK, i.config.selector, function(e) {
                        return i.toggle(e)
                    });
                    else if ("manual" !== e) {
                        var t = e === Fe ? i.constructor.Event.MOUSEENTER : i.constructor.Event.FOCUSIN,
                            n = e === Fe ? i.constructor.Event.MOUSELEAVE : i.constructor.Event.FOCUSOUT;
                        ke(i.element).on(t, i.config.selector, function(e) {
                            return i._enter(e)
                        }).on(n, i.config.selector, function(e) {
                            return i._leave(e)
                        })
                    }
                    ke(i.element).closest(".modal").on("hide.bs.modal", function() {
                        return i.hide()
                    })
                }), this.config.selector ? this.config = o({}, this.config, {
                    trigger: "manual",
                    selector: ""
                }) : this._fixTitle()
            }, _t._fixTitle = function() {
                var e = typeof this.element.getAttribute("data-original-title");
                !this.element.getAttribute("title") && "string" == e || (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
            }, _t._enter = function(e, t) {
                var n = this.constructor.DATA_KEY;
                (t = t || ke(e.currentTarget).data(n)) || (t = new this.constructor(e.currentTarget, this._getDelegateConfig()), ke(e.currentTarget).data(n, t)), e && (t._activeTrigger["focusin" === e.type ? Re : Fe] = !0), ke(t.getTipElement()).hasClass(qe) || t._hoverState === je ? t._hoverState = je : (clearTimeout(t._timeout), t._hoverState = je, t.config.delay && t.config.delay.show ? t._timeout = setTimeout(function() {
                    t._hoverState === je && t.show()
                }, t.config.delay.show) : t.show())
            }, _t._leave = function(e, t) {
                var n = this.constructor.DATA_KEY;
                (t = t || ke(e.currentTarget).data(n)) || (t = new this.constructor(e.currentTarget, this._getDelegateConfig()), ke(e.currentTarget).data(n, t)), e && (t._activeTrigger["focusout" === e.type ? Re : Fe] = !1), t._isWithActiveTrigger() || (clearTimeout(t._timeout), t._hoverState = "out", t.config.delay && t.config.delay.hide ? t._timeout = setTimeout(function() {
                    "out" === t._hoverState && t.hide()
                }, t.config.delay.hide) : t.hide())
            }, _t._isWithActiveTrigger = function() {
                for (var e in this._activeTrigger)
                    if (this._activeTrigger[e]) return !0;
                return !1
            }, _t._getConfig = function(e) {
                return "number" == typeof(e = o({}, this.constructor.Default, ke(this.element).data(), "object" == typeof e && e ? e : {})).delay && (e.delay = {
                    show: e.delay,
                    hide: e.delay
                }), "number" == typeof e.title && (e.title = e.title.toString()), "number" == typeof e.content && (e.content = e.content.toString()), Nt.typeCheckConfig(De, e, this.constructor.DefaultType), e
            }, _t._getDelegateConfig = function() {
                var e = {};
                if (this.config)
                    for (var t in this.config) this.constructor.Default[t] !== this.config[t] && (e[t] = this.config[t]);
                return e
            }, _t._cleanTipClass = function() {
                var e = ke(this.getTipElement()),
                    t = e.attr("class").match(Ie);
                null !== t && t.length && e.removeClass(t.join(""))
            }, _t._handlePopperPlacementChange = function(e) {
                var t = e.instance;
                this.tip = t.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(e.placement))
            }, _t._fixTransition = function() {
                var e = this.getTipElement(),
                    t = this.config.animation;
                null === e.getAttribute("x-placement") && (ke(e).removeClass(ze), this.config.animation = !1, this.hide(), this.show(), this.config.animation = t)
            }, Xt._jQueryInterface = function(n) {
                return this.each(function() {
                    var e = ke(this).data($e),
                        t = "object" == typeof n && n;
                    if ((e || !/dispose|hide/.test(n)) && (e || (e = new Xt(this, t), ke(this).data($e, e)), "string" == typeof n)) {
                        if (void 0 === e[n]) throw new TypeError('No method named "' + n + '"');
                        e[n]()
                    }
                })
            }, r(Xt, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }, {
                key: "Default",
                get: function() {
                    return Ne
                }
            }, {
                key: "NAME",
                get: function() {
                    return De
                }
            }, {
                key: "DATA_KEY",
                get: function() {
                    return $e
                }
            }, {
                key: "Event",
                get: function() {
                    return He
                }
            }, {
                key: "EVENT_KEY",
                get: function() {
                    return Me
                }
            }, {
                key: "DefaultType",
                get: function() {
                    return Pe
                }
            }]), Ye = Xt, ke.fn[De] = Ye._jQueryInterface, ke.fn[De].Constructor = Ye, ke.fn[De].noConflict = function() {
                return ke.fn[De] = Ae, Ye._jQueryInterface
            }, Ye),
            Wt = (Be = "popover", Ve = "." + (Ue = "bs.popover"), Ge = (We = t).fn[Be], Xe = "bs-popover", Qe = new RegExp("(^|\\s)" + Xe + "\\S+", "g"), Ke = o({}, Yt.Default, {
                placement: "right",
                trigger: "click",
                content: "",
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
            }), Ze = o({}, Yt.DefaultType, {
                content: "(string|element|function)"
            }), "fade", ".popover-header", ".popover-body", Je = {
                HIDE: "hide" + Ve,
                HIDDEN: "hidden" + Ve,
                SHOW: "show" + Ve,
                SHOWN: "shown" + Ve,
                INSERTED: "inserted" + Ve,
                CLICK: "click" + Ve,
                FOCUSIN: "focusin" + Ve,
                FOCUSOUT: "focusout" + Ve,
                MOUSEENTER: "mouseenter" + Ve,
                MOUSELEAVE: "mouseleave" + Ve
            }, et = function(e) {
                var t, n;

                function i() {
                    return e.apply(this, arguments) || this
                }
                n = e, (t = i).prototype = Object.create(n.prototype), (t.prototype.constructor = t).__proto__ = n;
                var s = i.prototype;
                return s.isWithContent = function() {
                    return this.getTitle() || this._getContent()
                }, s.addAttachmentClass = function(e) {
                    We(this.getTipElement()).addClass(Xe + "-" + e)
                }, s.getTipElement = function() {
                    return this.tip = this.tip || We(this.config.template)[0], this.tip
                }, s.setContent = function() {
                    var e = We(this.getTipElement());
                    this.setElementContent(e.find(".popover-header"), this.getTitle());
                    var t = this._getContent();
                    "function" == typeof t && (t = t.call(this.element)), this.setElementContent(e.find(".popover-body"), t), e.removeClass("fade show")
                }, s._getContent = function() {
                    return this.element.getAttribute("data-content") || this.config.content
                }, s._cleanTipClass = function() {
                    var e = We(this.getTipElement()),
                        t = e.attr("class").match(Qe);
                    null !== t && 0 < t.length && e.removeClass(t.join(""))
                }, i._jQueryInterface = function(n) {
                    return this.each(function() {
                        var e = We(this).data(Ue),
                            t = "object" == typeof n ? n : null;
                        if ((e || !/destroy|hide/.test(n)) && (e || (e = new i(this, t), We(this).data(Ue, e)), "string" == typeof n)) {
                            if (void 0 === e[n]) throw new TypeError('No method named "' + n + '"');
                            e[n]()
                        }
                    })
                }, r(i, null, [{
                    key: "VERSION",
                    get: function() {
                        return "4.1.3"
                    }
                }, {
                    key: "Default",
                    get: function() {
                        return Ke
                    }
                }, {
                    key: "NAME",
                    get: function() {
                        return Be
                    }
                }, {
                    key: "DATA_KEY",
                    get: function() {
                        return Ue
                    }
                }, {
                    key: "Event",
                    get: function() {
                        return Je
                    }
                }, {
                    key: "EVENT_KEY",
                    get: function() {
                        return Ve
                    }
                }, {
                    key: "DefaultType",
                    get: function() {
                        return Ze
                    }
                }]), i
            }(Yt), We.fn[Be] = et._jQueryInterface, We.fn[Be].Constructor = et, We.fn[Be].noConflict = function() {
                return We.fn[Be] = Ge, et._jQueryInterface
            }, et),
            Bt = (nt = "scrollspy", st = "." + (it = "bs.scrollspy"), rt = (tt = t).fn[nt], ot = {
                offset: 10,
                method: "auto",
                target: ""
            }, at = {
                offset: "number",
                method: "string",
                target: "(string|element)"
            }, lt = {
                ACTIVATE: "activate" + st,
                SCROLL: "scroll" + st,
                LOAD_DATA_API: "load" + st + ".data-api"
            }, "dropdown-item", ct = "active", '[data-spy="scroll"]', ".active", dt = ".nav, .list-group", ut = ".nav-link", ".nav-item", ht = ".list-group-item", ".dropdown", ".dropdown-item", ".dropdown-toggle", "offset", pt = "position", (Et = Gt.prototype).refresh = function() {
                var t = this,
                    e = this._scrollElement === this._scrollElement.window ? "offset" : pt,
                    s = "auto" === this._config.method ? e : this._config.method,
                    r = s === pt ? this._getScrollTop() : 0;
                this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), [].slice.call(document.querySelectorAll(this._selector)).map(function(e) {
                    var t, n = Nt.getSelectorFromElement(e);
                    if (n && (t = document.querySelector(n)), t) {
                        var i = t.getBoundingClientRect();
                        if (i.width || i.height) return [tt(t)[s]().top + r, n]
                    }
                    return null
                }).filter(function(e) {
                    return e
                }).sort(function(e, t) {
                    return e[0] - t[0]
                }).forEach(function(e) {
                    t._offsets.push(e[0]), t._targets.push(e[1])
                })
            }, Et.dispose = function() {
                tt.removeData(this._element, it), tt(this._scrollElement).off(st), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
            }, Et._getConfig = function(e) {
                if ("string" != typeof(e = o({}, ot, "object" == typeof e && e ? e : {})).target) {
                    var t = tt(e.target).attr("id");
                    t || (t = Nt.getUID(nt), tt(e.target).attr("id", t)), e.target = "#" + t
                }
                return Nt.typeCheckConfig(nt, e, at), e
            }, Et._getScrollTop = function() {
                return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
            }, Et._getScrollHeight = function() {
                return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
            }, Et._getOffsetHeight = function() {
                return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
            }, Et._process = function() {
                var e = this._getScrollTop() + this._config.offset,
                    t = this._getScrollHeight(),
                    n = this._config.offset + t - this._getOffsetHeight();
                if (this._scrollHeight !== t && this.refresh(), n <= e) {
                    var i = this._targets[this._targets.length - 1];
                    this._activeTarget !== i && this._activate(i)
                } else {
                    if (this._activeTarget && e < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();
                    for (var s = this._offsets.length; s--;) this._activeTarget !== this._targets[s] && e >= this._offsets[s] && (void 0 === this._offsets[s + 1] || e < this._offsets[s + 1]) && this._activate(this._targets[s])
                }
            }, Et._activate = function(t) {
                this._activeTarget = t, this._clear();
                var e = this._selector.split(",");
                e = e.map(function(e) {
                    return e + '[data-target="' + t + '"],' + e + '[href="' + t + '"]'
                });
                var n = tt([].slice.call(document.querySelectorAll(e.join(","))));
                n.hasClass("dropdown-item") ? (n.closest(".dropdown").find(".dropdown-toggle").addClass(ct), n.addClass(ct)) : (n.addClass(ct), n.parents(dt).prev(ut + ", " + ht).addClass(ct), n.parents(dt).prev(".nav-item").children(ut).addClass(ct)), tt(this._scrollElement).trigger(lt.ACTIVATE, {
                    relatedTarget: t
                })
            }, Et._clear = function() {
                var e = [].slice.call(document.querySelectorAll(this._selector));
                tt(e).filter(".active").removeClass(ct)
            }, Gt._jQueryInterface = function(t) {
                return this.each(function() {
                    var e = tt(this).data(it);
                    if (e || (e = new Gt(this, "object" == typeof t && t), tt(this).data(it, e)), "string" == typeof t) {
                        if (void 0 === e[t]) throw new TypeError('No method named "' + t + '"');
                        e[t]()
                    }
                })
            }, r(Gt, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }, {
                key: "Default",
                get: function() {
                    return ot
                }
            }]), ft = Gt, tt(window).on(lt.LOAD_DATA_API, function() {
                for (var e = [].slice.call(document.querySelectorAll('[data-spy="scroll"]')), t = e.length; t--;) {
                    var n = tt(e[t]);
                    ft._jQueryInterface.call(n, n.data())
                }
            }), tt.fn[nt] = ft._jQueryInterface, tt.fn[nt].Constructor = ft, tt.fn[nt].noConflict = function() {
                return tt.fn[nt] = rt, ft._jQueryInterface
            }, ft),
            Ut = (vt = "." + (gt = "bs.tab"), yt = (mt = t).fn.tab, bt = {
                HIDE: "hide" + vt,
                HIDDEN: "hidden" + vt,
                SHOW: "show" + vt,
                SHOWN: "shown" + vt,
                CLICK_DATA_API: "click" + vt + ".data-api"
            }, "dropdown-menu", wt = "active", "disabled", "fade", "show", ".dropdown", ".nav, .list-group", xt = ".active", Tt = "> li > .active", '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', ".dropdown-toggle", "> .dropdown-menu .active", (St = Vt.prototype).show = function() {
                var n = this;
                if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && mt(this._element).hasClass(wt) || mt(this._element).hasClass("disabled"))) {
                    var e, i, t = mt(this._element).closest(".nav, .list-group")[0],
                        s = Nt.getSelectorFromElement(this._element);
                    if (t) {
                        var r = "UL" === t.nodeName ? Tt : xt;
                        i = (i = mt.makeArray(mt(t).find(r)))[i.length - 1]
                    }
                    var o = mt.Event(bt.HIDE, {
                            relatedTarget: this._element
                        }),
                        a = mt.Event(bt.SHOW, {
                            relatedTarget: i
                        });
                    if (i && mt(i).trigger(o), mt(this._element).trigger(a), !a.isDefaultPrevented() && !o.isDefaultPrevented()) {
                        s && (e = document.querySelector(s)), this._activate(this._element, t);
                        var l = function() {
                            var e = mt.Event(bt.HIDDEN, {
                                    relatedTarget: n._element
                                }),
                                t = mt.Event(bt.SHOWN, {
                                    relatedTarget: i
                                });
                            mt(i).trigger(e), mt(n._element).trigger(t)
                        };
                        e ? this._activate(e, e.parentNode, l) : l()
                    }
                }
            }, St.dispose = function() {
                mt.removeData(this._element, gt), this._element = null
            }, St._activate = function(e, t, n) {
                function i() {
                    return s._transitionComplete(e, r, n)
                }
                var s = this,
                    r = ("UL" === t.nodeName ? mt(t).find(Tt) : mt(t).children(xt))[0],
                    o = n && r && mt(r).hasClass("fade");
                if (r && o) {
                    var a = Nt.getTransitionDurationFromElement(r);
                    mt(r).one(Nt.TRANSITION_END, i).emulateTransitionEnd(a)
                } else i()
            }, St._transitionComplete = function(e, t, n) {
                if (t) {
                    mt(t).removeClass("show " + wt);
                    var i = mt(t.parentNode).find("> .dropdown-menu .active")[0];
                    i && mt(i).removeClass(wt), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !1)
                }
                if (mt(e).addClass(wt), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !0), Nt.reflow(e), mt(e).addClass("show"), e.parentNode && mt(e.parentNode).hasClass("dropdown-menu")) {
                    var s = mt(e).closest(".dropdown")[0];
                    if (s) {
                        var r = [].slice.call(s.querySelectorAll(".dropdown-toggle"));
                        mt(r).addClass(wt)
                    }
                    e.setAttribute("aria-expanded", !0)
                }
                n && n()
            }, Vt._jQueryInterface = function(n) {
                return this.each(function() {
                    var e = mt(this),
                        t = e.data(gt);
                    if (t || (t = new Vt(this), e.data(gt, t)), "string" == typeof n) {
                        if (void 0 === t[n]) throw new TypeError('No method named "' + n + '"');
                        t[n]()
                    }
                })
            }, r(Vt, null, [{
                key: "VERSION",
                get: function() {
                    return "4.1.3"
                }
            }]), Ct = Vt, mt(document).on(bt.CLICK_DATA_API, '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', function(e) {
                e.preventDefault(), Ct._jQueryInterface.call(mt(this), "show")
            }), mt.fn.tab = Ct._jQueryInterface, mt.fn.tab.Constructor = Ct, mt.fn.tab.noConflict = function() {
                return mt.fn.tab = yt, Ct._jQueryInterface
            }, Ct);

        function Vt(e) {
            this._element = e
        }

        function Gt(e, t) {
            var n = this;
            this._element = e, this._scrollElement = "BODY" === e.tagName ? window : e, this._config = this._getConfig(t), this._selector = this._config.target + " " + ut + "," + this._config.target + " " + ht + "," + this._config.target + " .dropdown-item", this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, tt(this._scrollElement).on(lt.SCROLL, function(e) {
                return n._process(e)
            }), this.refresh(), this._process()
        }

        function Xt(e, t) {
            if (void 0 === d) throw new TypeError("Bootstrap tooltips require Popper.js (https://popper.js.org)");
            this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = e, this.config = this._getConfig(t), this.tip = null, this._setListeners()
        }

        function Qt(e, t) {
            this._config = this._getConfig(t), this._element = e, this._dialog = e.querySelector(".modal-dialog"), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._scrollbarWidth = 0
        }

        function Kt(e, t) {
            this._element = e, this._popper = null, this._config = this._getConfig(t), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
        }

        function Zt(t, e) {
            this._isTransitioning = !1, this._element = t, this._config = this._getConfig(e), this._triggerArray = j.makeArray(document.querySelectorAll('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'));
            for (var n = [].slice.call(document.querySelectorAll(X)), i = 0, s = n.length; i < s; i++) {
                var r = n[i],
                    o = Nt.getSelectorFromElement(r),
                    a = [].slice.call(document.querySelectorAll(o)).filter(function(e) {
                        return e === t
                    });
                null !== o && 0 < a.length && (this._selector = o, this._triggerArray.push(r))
            }
            this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
        }

        function Jt(e, t) {
            this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(t), this._element = C(e)[0], this._indicatorsElement = this._element.querySelector(".carousel-indicators"), this._addEventListeners()
        }

        function en(e) {
            this._element = e
        }

        function tn(e) {
            this._element = e
        }! function(e) {
            if (void 0 === e) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
            var t = e.fn.jquery.split(" ")[0].split(".");
            if (t[0] < 2 && t[1] < 9 || 1 === t[0] && 9 === t[1] && t[2] < 1 || 4 <= t[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
        }(t), e.Util = Nt, e.Alert = jt, e.Button = Ht, e.Carousel = zt, e.Collapse = qt, e.Dropdown = Ft, e.Modal = Rt, e.Popover = Wt, e.Scrollspy = Bt, e.Tab = Ut, e.Tooltip = Yt, Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("utils/MapUtils", t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.MapUtils = {
            iterateOverMap: function(e, t) {
                var n = 0;
                for (var i in e) e.hasOwnProperty(i) && "__size__" !== i && (t.call(e, i, e[i], n), n++)
            },
            exists: function(e, t) {
                for (var n = !1, i = 0; !n && i < e.length;) n = t == e[i], i++;
                return n
            },
            put: function(e, t, n) {
                e.__size__ || (e.__size__ = 0), e[t] || e.__size__++, e[t] = n
            },
            isEmpty: function(e) {
                for (var t in e)
                    if (e.hasOwnProperty(t)) return !1;
                return !0
            }
        })
    }(this, function() {
        return {
            iterateOverMap: function(e, t) {
                var n = 0;
                for (var i in e) e.hasOwnProperty(i) && "__size__" !== i && (t.call(e, i, e[i], n), n++)
            },
            exists: function(e, t) {
                for (var n = !1, i = 0; !n && i < e.length;) n = t == e[i], i++;
                return n
            },
            put: function(e, t, n) {
                e.__size__ || (e.__size__ = 0), e[t] || e.__size__++, e[t] = n
            },
            isEmpty: function(e) {
                for (var t in e)
                    if (e.hasOwnProperty(t)) return !1;
                return !0
            }
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("utils/DOMUtils", t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.DOMUtils = t())
    }(this, function() {
        return {
            friendlyInputNames: {
                form: "The form",
                select: "The select box",
                textarea: "The text area",
                checkbox: "The checkbox",
                radio: "The radio button",
                text: "The text field",
                password: "The password",
                email: "The email",
                url: "The URL",
                number: "The number",
                datetime: "The datetime",
                "datetime-local": "The local datetime",
                date: "The date",
                month: "The month",
                time: "The time",
                week: "The week",
                range: "The range",
                tel: "The telephone number",
                color: "The color"
            },
            getElementsByAttribute: function(e, t, n, i) {
                for (var s, r, o = "*" == t && e.all ? e.all : e.getElementsByTagName(t), a = [], l = void 0 !== i ? new RegExp("(^|\\s)" + i + "(\\s|$)") : null, c = 0; c < o.length; c++) "string" == typeof(r = (s = o[c]).getAttribute && s.getAttribute(n)) && 0 < r.length && (void 0 === i || l && l.test(r)) && a.push(s);
                return a
            },
            getAttributeValueForElement: function(e, t) {
                var n = e.getAttribute && e.getAttribute(t) || null;
                if (!n)
                    for (var i = e.attributes, s = 0; s < i.length; s++) i[s].nodeName === t && (n = i[s].nodeValue);
                return n
            },
            generateRandomId: function() {
                return "regula-generated-" + Math.floor(1e6 * Math.random())
            },
            supportsHTML5Validation: function() {
                return "function" == typeof document.createElement("input").checkValidity
            }
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("service/GroupService", t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.GroupService = t())
    }(this, function() {
        return {
            Group: {
                Default: 0
            },
            ReverseGroup: {
                0: "Default"
            },
            deletedGroupIndices: [],
            firstCustomGroupIndex: 1
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("utils/ArrayUtils", t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.ArrayUtils = t())
    }(this, function() {
        return {
            explode: function(e, t) {
                for (var n = "", i = 0; i < e.length; i++) n += e[i] + t;
                return n.replace(new RegExp(t + "$"), "")
            }
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("service/ExceptionService", ["utils/ArrayUtils"], t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.ExceptionService = t(e.regulaModules.ArrayUtils))
    }(this, function(i) {
        var e = {
            IllegalArgumentException: function(e) {
                this.name = "IllegalArgumentException", this.message = e
            },
            ConstraintDefinitionException: function(e) {
                this.name = "ConstraintDefinitionException", this.message = e
            },
            BindException: function(e) {
                this.name = "BindException", this.message = e
            },
            MissingFeatureException: function(e) {
                this.name = "MissingFeatureException", this.message = e
            }
        };
        for (var t in e)
            if (e.hasOwnProperty(t)) {
                var n = e[t];
                n.prototype = new Error, n.prototype.constructor = n
            } return {
            Exception: e,
            generateExceptionMessage: function(e, t, n) {
                var i = "";
                return null != e ? (i = e.id, i += "" == t || null == t || null == t ? ": " : "." + t + ": ") : "" != t && null != t && null != t && (i = "@" + t + ": "), i + n
            },
            explodeParameters: function(e) {
                var t = "Function received: {";
                for (var n in e) e.hasOwnProperty(n) && ("string" == typeof e[n] ? t += n + ": " + e[n] + ", " : e[n] instanceof Array && (t += n + ": [" + i.explode(e[n], ", ") + "], "));
                return t = t.replace(/, $/, "") + "}"
            }
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("service/ValidationService", ["utils/DOMUtils", "utils/MapUtils", "service/GroupService", "service/ExceptionService", "utils/ArrayUtils"], t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.ValidationService = t(e.regulaModules.DOMUtils, e.regulaModules.MapUtils, e.regulaModules.GroupService, e.regulaModules.ExceptionService, e.regulaModules.ArrayUtils))
    }(this, function(u, x, o, f, m) {
        function n(i, e) {
            var s = e[i],
                t = i.replace(/(^[A-Z]+)/, function(e) {
                    return e.toLowerCase()
                });
            s.async ? j[t] = function(e, t, n) {
                if (void 0 === n) throw new f.Exception.IllegalArgumentException(i + " is an asynchronous constraint, but you have not provided a callback.");
                return s.validator.call(e, t, j, n)
            } : j[t] = function(e, t) {
                return s.validator.call(e, t, j)
            }
        }

        function T(e, t) {
            var n = A.validateEmptyFields;
            return void 0 !== t.ignoreEmpty && (n = !t.ignoreEmpty), !s.blank.call(e) || !!n
        }

        function i(e) {
            var t = {
                    YMD: {
                        Year: 0,
                        Month: 1,
                        Day: 2
                    },
                    MDY: {
                        Month: 0,
                        Day: 1,
                        Year: 2
                    },
                    DMY: {
                        Day: 0,
                        Month: 1,
                        Year: 2
                    }
                } [e.format],
                n = e.separator;
            void 0 === e.separator && (n = /\//.test(this.value) ? "/" : /\./.test(this.value) ? "." : / /.test(this.value) ? " " : /[^0-9]+/);
            var i = this.value.split(n),
                s = new Date(i[t.Year], i[t.Month] - 1, i[t.Day]),
                r = new Date;
            return void 0 !== e.date && (i = e.date.split(n), r = new Date(i[t.Year], i[t.Month] - 1, i[t.Day])), {
                dateToValidate: s,
                dateToTestAgainst: r
            }
        }

        function e() {
            return !this.validity.typeMismatch
        }

        function a(e) {
            var t = {
                asyncContexts: [],
                syncContexts: []
            };
            for (var n in P)
                if (P.hasOwnProperty(n)) {
                    var i = P[n];
                    for (var s in i)
                        if (i.hasOwnProperty(s))
                            if (document.getElementById(s)) {
                                var r = i[s];
                                for (var o in r)
                                    if (r.hasOwnProperty(o)) {
                                        var a = E(n, s, o);
                                        a.async ? t.asyncContexts.push(a) : t.syncContexts.push(a)
                                    }
                            } else delete i[s]
                } return C(t = b(t), e)
        }

        function l(e) {
            var t = {
                asyncContexts: [],
                syncContexts: []
            };
            for (var n in P)
                if (P.hasOwnProperty(n)) {
                    var i = P[n];
                    for (var s in i)
                        if (i.hasOwnProperty(s)) {
                            if (i[s][e.constraintType]) {
                                var r = E(n, s, e.constraintType);
                                r.async ? t.asyncContexts.push(r) : t.syncContexts.push(r)
                            }
                        }
                } return C(t = b(t), e)
        }

        function c(e) {
            var t = {},
                n = {
                    asyncContexts: [],
                    syncContexts: []
                };
            for (var i in P)
                if (P.hasOwnProperty(i))
                    for (var s = P[i], r = 0; r < e.elementIds.length; r++) {
                        void 0 === t[d = e.elementIds[r]] && (t[d] = 0);
                        var o = s[d];
                        if (void 0 !== o)
                            for (var a in t[d]++, o)
                                if (o.hasOwnProperty(a)) {
                                    var l = E(i, d, a);
                                    l.async ? n.asyncContexts.push(l) : n.syncContexts.push(l)
                                }
                    }
            var c = [];
            for (var d in t) t.hasOwnProperty(d) && 0 === t[d] && c.push(d);
            if (0 < c.length) throw new f.Exception.IllegalArgumentException("No constraints have been bound to the specified elements: " + m.explode(c) + ". " + f.explodeParameters(e));
            return C(n = b(n), e)
        }

        function d(e) {
            var t = [],
                n = {
                    asyncContexts: [],
                    syncContexts: []
                };
            for (var i in P)
                if (P.hasOwnProperty(i))
                    for (var s = P[i], r = 0; r < e.elementIds.length; r++) {
                        var o = e.elementIds[r];
                        if (void 0 !== s[o]) {
                            var a = E(i, o, e.constraintType);
                            a.async ? n.asyncContexts.push(a) : n.syncContexts.push(a)
                        } else t.push(o)
                    }
            if (0 < t.length) throw new f.Exception.IllegalArgumentException("No constraints have been bound to the specified elements: " + m.explode(t) + ". " + f.explodeParameters(e));
            return C(n = b(n), e)
        }

        function h(e) {
            for (var t = !1, n = {
                    groupedContexts: {}
                }, i = 0; i < e.groups.length;) {
                var s = e.groups[i],
                    r = P[s];
                if (void 0 === r) throw new f.Exception.IllegalArgumentException("Undefined group in group list. " + f.explodeParameters(e));
                for (var o in r)
                    if (r.hasOwnProperty(o)) {
                        var a = r[o];
                        for (var l in a)
                            if (a.hasOwnProperty(l)) {
                                var c = E(s, o, l);
                                n.groupedContexts[s] || (n.groupedContexts[s] = {
                                    asyncContexts: [],
                                    syncContexts: []
                                }), c.async ? (t = !0, n.groupedContexts[s].asyncContexts.push(c)) : n.groupedContexts[s].syncContexts.push(c)
                            }
                    } i++
            }
            var d = w(n);
            return e.groups = d.groups, S(e, n = d.uniqueConstraintsToValidate, t)
        }

        function p(e) {
            for (var t = !1, n = {
                    groupedContexts: {}
                }, i = 0; i < e.groups.length;) {
                var s = e.groups[i],
                    r = P[s];
                if (void 0 === r) throw new f.Exception.IllegalArgumentException("Undefined group in group list. " + f.explodeParameters(e));
                var o = !1;
                for (var a in r)
                    if (r.hasOwnProperty(a)) {
                        if (r[a][e.constraintType]) {
                            o = !0;
                            var l = E(s, a, e.constraintType);
                            n.groupedContexts[s] || (n.groupedContexts[s] = {
                                asyncContexts: [],
                                syncContexts: []
                            }), l.async ? (t = !0, n.groupedContexts[s].asyncContexts.push(l)) : n.groupedContexts[s].syncContexts.push(l)
                        }
                    } if (!o) throw new f.Exception.IllegalArgumentException("Constraint " + e.constraintType + " has not been bound to any element under group " + s + ". " + f.explodeParameters(e));
                i++
            }
            var c = w(n);
            return e.groups = c.groups, S(e, n = c.uniqueConstraintsToValidate, t)
        }

        function g(e) {
            for (var t = [], n = [], i = !1, s = {
                    groupedContexts: {}
                }, r = 0; r < e.groups.length;) {
                var o = e.groups[r],
                    a = P[o];
                if (!a) throw new f.Exception.IllegalArgumentException("Undefined group in group list. " + f.explodeParameters(e));
                for (var l = 0; l < e.elementIds.length; l++) {
                    var c = e.elementIds[l],
                        d = a[c];
                    if (d) {
                        for (var u in d)
                            if (d.hasOwnProperty(u)) {
                                var h = E(o, c, u);
                                s.groupedContexts[o] || (s.groupedContexts[o] = {
                                    asyncContexts: [],
                                    syncContexts: []
                                }), h.async ? (i = !0, s.groupedContexts[o].asyncContexts.push(h)) : s.groupedContexts[o].syncContexts.push(h)
                            }
                    } else t.push(o), n.push(c)
                }
                r++
            }
            if (0 < t.length) throw new f.Exception.IllegalArgumentException("The following elements: " + m.explode(n) + " were not found in one or more of the following group(s): [" + m.explode(t, ",").replace(/,/g, ", ") + "]. " + f.explodeParameters(e));
            var p = w(s);
            return e.groups = p.groups, S(e, s = p.uniqueConstraintsToValidate, i)
        }

        function v(e) {
            for (var t = !1, n = {
                    groupedContexts: {}
                }, i = 0; i < e.groups.length;) {
                for (var s = e.groups[i], r = 0; r < e.elementIds.length; r++) {
                    var o = E(s, e.elementIds[r], e.constraintType);
                    n.groupedContexts[s] || (n.groupedContexts[s] = {
                        asyncContexts: [],
                        syncContexts: []
                    }), o.async ? (t = !0, n.groupedContexts[s].asyncContexts.push(o)) : n.groupedContexts[s].syncContexts.push(o)
                }
                i++
            }
            var a = w(n);
            return e.groups = a.groups, S(e, n = a.uniqueConstraintsToValidate, t)
        }

        function y(e) {
            var t = !0;
            L[e.elementId] || (L[e.elementId] = {});
            var n = document.getElementById(e.elementId).cloneNode(!1),
                i = n.name.replace(/\s/g, "");
            return void 0 !== n.type && "radio" === n.type.toLowerCase() && "" !== i && N[i] || (N[i] = {}), L[e.elementId][e.elementConstraint] || N[i][e.elementConstraint] || (t = !1, L[e.elementId][e.elementConstraint] = !0, void 0 !== n.type && "radio" === n.type.toLowerCase() && "" !== i && (N[i][e.elementConstraint] = !0)), t
        }

        function b(e) {
            for (var t = {
                    asyncContexts: [],
                    syncContexts: []
                }, n = 0; n < e.syncContexts.length; n++) {
                y(i = e.syncContexts[n]) || t.syncContexts.push(i)
            }
            for (n = 0; n < e.asyncContexts.length; n++) {
                var i;
                y(i = e.asyncContexts[n]) || t.asyncContexts.push(i)
            }
            return t
        }

        function w(e) {
            var t = [],
                n = {
                    groupedContexts: {}
                };
            for (var i in e.groupedContexts)
                if (e.groupedContexts.hasOwnProperty(i)) {
                    for (var s = 0; s < e.groupedContexts[i].syncContexts.length; s++) {
                        y(r = e.groupedContexts[i].syncContexts[s]) || (n.groupedContexts[i] || (n.groupedContexts[i] = {
                            asyncContexts: [],
                            syncContexts: []
                        }), n.groupedContexts[i].syncContexts.push(r), -1 == t.indexOf(i) && t.push(i))
                    }
                    for (s = 0; s < e.groupedContexts[i].asyncContexts.length; s++) {
                        var r;
                        y(r = e.groupedContexts[i].asyncContexts[s]) || (n.groupedContexts[i] || (n.groupedContexts[i] = {
                            asyncContexts: [],
                            syncContexts: []
                        }), n.groupedContexts[i].asyncContexts.push(r), -1 == t.indexOf(i) && t.push(i))
                    }
                } return {
                groups: t,
                uniqueConstraintsToValidate: n
            }
        }

        function C(e, t) {
            var n = [];
            if (0 < e.syncContexts.length && (n = function(e) {
                    var t = [],
                        n = 0;
                    for (; n < e.syncContexts.length;) {
                        var i = e.syncContexts[n],
                            s = _(i.group, i.elementId, i.elementConstraint, i.params);
                        s && t.push(s), n++
                    }
                    return t
                }(e)), 0 < e.asyncContexts.length) {
                if (!t.callback) throw new f.Exception.IllegalArgumentException("One or more constraints to be validated are asynchronous, but a callback has not been provided.");
                ! function(t, n) {
                    function e(e) {
                        s++, e && i.push(e), s === t.asyncContexts.length && n(i)
                    }
                    for (var i = [], s = 0, r = 0; r < t.asyncContexts.length; r++) {
                        var o = t.asyncContexts[r];
                        k(o.group, o.elementId, o.elementConstraint, o.params, e)
                    }
                }(e, function(e) {
                    n = 0 < n.length ? n.concat(e) : e, t.callback(n)
                })
            } else t.callback && t.callback(n);
            return n
        }

        function S(t, e, n) {
            var i = function(e, t, n) {
                var i = [],
                    s = 0,
                    r = !0;
                for (; s < e.length && r;) {
                    for (var o = e[s], a = n.groupedContexts[o].syncContexts, l = 0; l < a.length; l++) {
                        var c = a[l],
                            d = _(c.group, c.elementId, c.elementConstraint, c.params);
                        d && i.push(d)
                    }
                    s++, r = 0 == i.length || t && 0 != i.length
                }
                return i
            }(t.groups, t.independent, e);
            if (n) {
                if (!t.callback) throw new f.Exception.IllegalArgumentException("One or more constraints to be validated are asynchronous, but a callback has not been provided.");
                if (!t.independent && 0 < i.length) {
                    var s = i[0].group,
                        r = e.groupedContexts[s];
                    e.groupedContexts = {}, e.groupedContexts[s] = r
                }! function(l, c, d, u) {
                    var h = [],
                        p = !0;
                    ! function t(n) {
                        if (n < l.length && p) {
                            for (var e = l[n], i = d.groupedContexts[e].asyncContexts, s = 0, r = 0; r < i.length; r++) {
                                var o = i[r];
                                k(o.group, o.elementId, o.elementConstraint, o.params, a)
                            }

                            function a(e) {
                                s++, e && h.push(e), s === i.length && (p = 0 === h.length || c && 0 != h.length, t(++n))
                            }
                        } else u(h)
                    }(0)
                }(t.groups, t.independent, e, function(e) {
                    i = 0 < i.length ? i.concat(e) : e, t.callback(i)
                })
            } else t.callback && t.callback(i);
            return i
        }

        function E(e, t, n) {
            var i = P[e];
            if (!i) throw new f.Exception.IllegalArgumentException("Undefined group in group list (group: " + e + ", elementId: " + t + ", constraint: " + n + ")");
            var s = i[t];
            if (!s) throw new f.Exception.IllegalArgumentException("No constraints have been defined for the element with id: " + t + " in group " + e);
            var r = s[n];
            if (!r) throw new f.Exception.IllegalArgumentException("Constraint " + n + " in group " + e + " hasn't been bound to the element with id " + t);
            return {
                group: e,
                elementId: t,
                elementConstraint: n,
                params: r,
                async: I[n].async
            }
        }

        function _(e, t, n, i) {
            var s, r = D(e, t, n, i),
                o = "";
            if (r.constraintPassed || (o = M(t, n, i), s = {
                    group: e,
                    constraintName: n,
                    formSpecific: I[n].formSpecific,
                    custom: I[n].custom,
                    compound: I[n].compound,
                    async: I[n].async,
                    composingConstraintViolations: r.composingConstraintViolations || [],
                    constraintParameters: i,
                    failingElements: r.failingElements,
                    message: o
                }), A.enableHTML5Validation)
                for (var a = 0; a < r.failingElements.length; a++) r.failingElements[a].setCustomValidity("");
            return s
        }

        function k(i, s, r, o, a) {
            var l;
            $(i, s, r, o, function(e) {
                var t = "";
                if (e.constraintPassed || (t = M(s, r, o), l = {
                        group: i,
                        constraintName: r,
                        formSpecific: I[r].formSpecific,
                        custom: I[r].custom,
                        compound: I[r].compound,
                        async: I[r].async,
                        composingConstraintViolations: e.composingConstraintViolations || [],
                        constraintParameters: o,
                        failingElements: e.failingElements,
                        message: t
                    }), A.enableHTML5Validation)
                    for (var n = 0; n < e.failingElements.length; n++) e.failingElements[n].setCustomValidity("");
                a(l)
            })
        }

        function D(e, t, n, i) {
            var s = !1,
                r = [],
                o = document.getElementById(t),
                a = [];
            I[n].formSpecific ? s = 0 == (r = I[n].validator.call(o, i, j)).length : (s = I[n].compound ? 0 == (a = I[n].validator.call(o, i, e, I[n], null)).length : I[n].validator.call(o, i, j)) || r.push(o);
            var l = o.cloneNode(!1).name.replace(/\s/g, ""),
                c = o.cloneNode(!1).type;
            void 0 !== c && "radio" === c.toLowerCase() && "" !== l && (r = u.getElementsByAttribute(document.body, "input", "name", l.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")));
            var d = {
                constraintName: n,
                constraintPassed: s,
                failingElements: r
            };
            return I[n].reportAsSingleViolation || (d.composingConstraintViolations = a), d
        }

        function $(e, t, a, n, i) {
            function s(e, t, n, i) {
                var s = l.cloneNode(!1).name.replace(/\s/g, ""),
                    r = l.cloneNode(!1).type;
                void 0 !== r && "radio" === r.toLowerCase() && "" !== s && (n = u.getElementsByAttribute(document.body, "input", "name", s));
                var o = {
                    constraintName: a,
                    constraintPassed: e,
                    failingElements: n
                };
                I[a].reportAsSingleViolation || (o.composingConstraintViolations = t), i(o)
            }
            var l = document.getElementById(t);
            I[a].formSpecific ? I[a].validator.call(l, n, j, function(e) {
                s(0 === e.length, null, e, i)
            }) : I[a].compound ? I[a].validator.call(l, n, e, I[a], function(e) {
                var t = [],
                    n = 0 === e.length;
                n || t.push(l), s(n, e, t, i)
            }) : I[a].validator.call(l, n, j, function(e) {
                var t = [];
                e || t.push(l), s(e, null, t, i)
            })
        }

        function M(e, t, n) {
            var i = document.getElementById(e),
                s = "";
            for (var r in s = n.message ? n.message : n.msg ? n.msg : I[t].defaultMessage, n)
                if (n.hasOwnProperty(r)) {
                    var o = new RegExp("{" + r + "}", "g");
                    s = s.replace(o, n[r])
                } if (I[t].compound && void 0 !== I[t].composingConstraints)
                for (var a = 0; a < I[t].composingConstraints.length; a++) {
                    var l = I[t].composingConstraints[a];
                    for (var r in l.params)
                        if (l.params.hasOwnProperty(r)) {
                            o = new RegExp("{" + r + "}", "g");
                            s = s.replace(o, l.params[r])
                        }
                }
            if (/{label}/.test(s)) {
                var c = u.friendlyInputNames[i.cloneNode(!1).tagName.toLowerCase()];
                c = c || u.friendlyInputNames[i.cloneNode(!1).type.toLowerCase()], s = (s = s.replace(/{label}/, c)).replace(/{flags}/g, "")
            }
            return s = s.replace(/\\\"/g, '"')
        }
        var A = {},
            O = {},
            I = {},
            P = {},
            L = {},
            N = {},
            j = {},
            s = {
                checked: function(e) {
                    var t = !1;
                    if ("radio" === this.type.toLowerCase() && "" !== this.name.replace(/\s/g, ""))
                        for (var n = u.getElementsByAttribute(document.body, "input", "name", this.name.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")), i = 0; i < n.length && !t;) t = n[i].checked, i++;
                    else t = this.checked;
                    return t
                },
                selected: function(e) {
                    return 0 < this.selectedIndex
                },
                max: function(e) {
                    var t = !0;
                    return T(this, e) && (t = parseFloat(this.value) <= parseFloat(e.value)), t
                },
                min: function(e) {
                    var t = !0;
                    return T(this, e) && (t = parseFloat(this.value) >= parseFloat(e.value)), t
                },
                range: function(e) {
                    var t = !0;
                    return T(this, e) && (t = "" != this.value.replace(/\s/g, "") && parseFloat(this.value) <= parseFloat(e.max) && parseFloat(this.value) >= parseFloat(e.min)), t
                },
                notBlank: function(e) {
                    return "" != this.value.replace(/\s/g, "")
                },
                blank: function(e) {
                    return "" === this.value.replace(/\s/g, "")
                },
                matches: function(e) {
                    var t, n = !0;
                    T(this, e) && (t = "string" == typeof e.regex ? e.regex.replace(/^\//, "").replace(/\/$/, "") : e.regex, n = (void 0 !== e.flags ? new RegExp(t.toString().replace(/^\//, "").replace(/\/[^\/]*$/, ""), e.flags) : new RegExp(t)).test(this.value));
                    return n
                },
                email: function(e) {
                    var t = !0;
                    return T(this, e) && (t = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i.test(this.value)), t
                },
                alpha: function(e) {
                    var t = !0;
                    return T(this, e) && (t = /^[A-Za-z]+$/.test(this.value)), t
                },
                numeric: function(e) {
                    var t = !0;
                    return T(this, e) && (t = /^[0-9]+$/.test(this.value)), t
                },
                integer: function(e) {
                    var t = !0;
                    return T(this, e) && (t = /^-?[0-9]+$/.test(this.value)), t
                },
                real: function(e) {
                    var t = !0;
                    return T(this, e) && (t = /^-?([0-9]+(\.[0-9]+)?|\.[0-9]+)$/.test(this.value)), t
                },
                alphaNumeric: function(e) {
                    var t = !0;
                    return T(this, e) && (t = /^[0-9A-Za-z]+$/.test(this.value)), t
                },
                completelyFilled: function(e) {
                    for (var t = [], n = 0; n < this.elements.length; n++) {
                        var i = this.elements[n];
                        s.required.call(i) || t.push(i)
                    }
                    return t
                },
                passwordsMatch: function(e) {
                    var t = [],
                        n = document.getElementById(e.field1),
                        i = document.getElementById(e.field2);
                    return n.value != i.value && (t = [n, i]), t
                },
                required: function(e) {
                    var t = !0;
                    return this.tagName && ("select" === this.tagName.toLowerCase() ? t = s.selected.call(this) : "checkbox" === this.type.toLowerCase() || "radio" === this.type.toLowerCase() ? t = s.checked.call(this) : "input" !== this.tagName.toLowerCase() && "textarea" !== this.tagName.toLowerCase() || "button" == this.type.toLowerCase() || (t = s.notBlank.call(this))), t
                },
                length: function(e) {
                    var t = !0;
                    return T(this, e) && (t = this.value.length >= e.min && this.value.length <= e.max), t
                },
                digits: function(e) {
                    var t = !0;
                    if (T(this, e)) {
                        var n = this.value.replace(/\s/g, ""),
                            i = n.split(/\./);
                        t = !1, 0 < n.length && (1 == i.length && (i[1] = ""), t = !(0 < e.integer) || i[0].length <= e.integer, 0 < e.fraction && (t = t && i[1].length <= e.fraction))
                    }
                    return t
                },
                past: function(e) {
                    var t = !0;
                    if (T(this, e)) {
                        var n = i.call(this, e);
                        t = n.dateToValidate < n.dateToTestAgainst
                    }
                    return t
                },
                future: function(e) {
                    var t = !0;
                    if (T(this, e)) {
                        var n = i.call(this, e);
                        t = n.dateToValidate > n.dateToTestAgainst
                    }
                    return t
                },
                url: function(e) {
                    var t = !0;
                    return T(this, e) && (t = /^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(this.value)), t
                },
                step: function(e) {
                    var t = !0;
                    if (T(this, e)) {
                        var n = parseFloat(this.value),
                            i = parseFloat(e.max),
                            s = parseFloat(e.min),
                            r = parseFloat(e.value);
                        t = n <= i && s <= n && n % r == 0
                    }
                    return t
                },
                html5Required: function(e) {
                    return !this.validity.valueMissing
                },
                html5Email: e,
                html5URL: e,
                html5Number: e,
                html5DateTime: e,
                html5DateTimeLocal: e,
                html5Date: e,
                html5Month: e,
                html5Time: e,
                html5Week: e,
                html5Range: e,
                html5Tel: e,
                html5Color: e,
                html5Pattern: function(e) {
                    return !this.validity.patternMismatch
                },
                html5MaxLength: function(e) {
                    return !this.validity.tooLong
                },
                html5Min: function(e) {
                    return !this.validity.rowUnderflow
                },
                html5Max: function(e) {
                    return !this.validity.rowOverflow
                },
                html5Step: function(e) {
                    return !this.validity.stepMismatch
                }
            };
        return {
            Validator: s,
            init: function(e) {
                A = e.config, O = e.ReverseConstraint, I = e.constraintDefinitions, P = e.boundConstraints
            },
            wrapValidatorWithEmptyCheck: function(s) {
                return function(e, t, n) {
                    var i = !0;
                    return T(this, e) && (i = s.call(this, e, t, n)), i
                }
            },
            initializePublicValidators: function(e) {
                for (var t in e) e.hasOwnProperty(t) && n(t, e)
            },
            compoundValidator: function(s, i, a, r) {
                function e(e, t) {
                    var n = {};
                    for (var i in e) e.hasOwnProperty(i) && "__size__" != i && x.put(n, i, e[i]);
                    if (0 < t.length)
                        for (var i in s) s.hasOwnProperty(i) && "__size__" != i && x.put(n, i, s[i]);
                    return n
                }

                function o(e, t, n, i) {
                    var s = O[p.constraintType],
                        r = M(t, s, i),
                        o = {
                            group: n,
                            constraintName: e.constraintName,
                            custom: I[s].custom,
                            compound: I[s].compound,
                            async: I[s].async,
                            constraintParameters: p.params,
                            failingElements: e.failingElements,
                            message: r
                        };
                    return a.reportAsSingleViolation || (o.composingConstraintViolations = e.composingConstraintViolations || []), o
                }
                for (var t = [], l = [], n = 0; n < a.composingConstraints.length; n++) {
                    var c = a.composingConstraints[n],
                        d = O[c.constraintType];
                    I[d].async ? l.push(c) : t.push(c)
                }
                var u = null,
                    h = this;
                if (T(this, s)) {
                    if (0 < t.length) {
                        u = [];
                        for (n = 0; n < t.length; n++) {
                            var p = t[n],
                                f = O[p.constraintType],
                                m = e(p.params, a.params),
                                g = D(i, h.id, f, m);
                            if (!g.constraintPassed) {
                                var v = o(g, h.id, i, m);
                                if (A.enableHTML5Validation)
                                    for (var y = 0; y < g.failingElements.length; y++) g.failingElements[y].setCustomValidity(v.message);
                                u.push(v)
                            }
                        }
                    }
                    if (0 < l.length) {
                        null === u && (u = []);
                        var b = 0;
                        for (n = 0; n < l.length; n++) {
                            p = l[n], f = O[p.constraintType], m = e(p.params, a.params);
                            $(i, h.id, f, m, w)
                        }

                        function w(e) {
                            if (!e.constraintPassed) {
                                var t = o(e, h.id, i, m);
                                if (A.enableHTML5Validation)
                                    for (var n = 0; n < e.failingElements.length; n++) e.failingElements[n].setCustomValidity(t.message);
                                u.push(t)
                            }++b === l.length && r(u)
                        }
                    }
                } else u = [];
                return u
            },
            validate: function(e) {
                L = {}, N = {};
                var t, n, i = {
                    "000": a,
                    "001": l,
                    "010": c,
                    "011": d,
                    100: h,
                    101: p,
                    110: g,
                    111: v
                };
                if (e && void 0 !== e || (e = {}), void 0 === e.independent && (e.independent = !0), void 0 !== e.constraintType && (e.constraintType = O[e.constraintType]), void 0 !== e.groups) {
                    var s = e.groups;
                    e.groups = [];
                    for (var r = 0; r < s.length; r++) e.groups.push(o.ReverseGroup[s[r]])
                }
                if (void 0 !== e.elements) {
                    e.elementIds = [];
                    for (r = 0; r < e.elements.length; r++) e.elementIds.push(e.elements[r].id)
                } else void 0 !== e.elementId && (e.elementIds = [e.elementId]);
                return i[(n = "", n += null == ((t = e).groups || null) ? "0" : "1", n += null == (t.elementIds || null) ? "0" : "1", n += null == ((void 0 === t.constraintType ? null : t.constraintType) || null) ? "0" : "1")](e)
            },
            runValidatorFor: D,
            interpolateConstraintDefaultMessage: M,
            createPublicValidator: n
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("domain/CompositionGraph", t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.CompositionGraph = t())
    }(this, function() {
        var r = {},
            o = {
                visited: !1,
                name: "RootNode",
                type: -1,
                parents: [],
                children: []
            };
        return {
            ROOT: -1,
            addNode: function(e) {
                var t = e.type,
                    n = e.name,
                    i = e.parent,
                    s = void 0 === r[t] ? {
                        visited: !1,
                        name: n,
                        type: t,
                        parents: [],
                        children: []
                    } : r[t];
                null == i ? o.children.push(s) : (i.children.push(s), s.parents.push(i)), r[t] = s
            },
            getNodeByType: function(e) {
                var t = r[e];
                return void 0 === t ? null : t
            },
            analyze: function(e) {
                var t = function e(t, n) {
                    var i = {
                        cycle: !1,
                        path: n
                    };
                    if (t.visited) i.cycle = !0;
                    else {
                        t.visited = !0;
                        for (var s = 0; s < t.children.length && !i.cycle;) i = e(t.children[s], n + "." + t.children[s].name), s++
                    }
                    return i
                }(e, e.name);
                return t.cycle || function e(t) {
                    t.visited = !1;
                    for (var n = 0; n < t.children.length; n++) e(t.children[n])
                }(o), t
            },
            getRoot: function() {
                return o
            },
            setRoot: function(e) {
                o = e
            },
            clone: function() {
                var r = {},
                    e = function e(t, n) {
                        var i = void 0 === r[t.type] ? {
                            visited: t.visited,
                            name: t.name,
                            type: t.type,
                            parents: [],
                            children: []
                        } : r[t.type];
                        null !== n && i.parents.push(n);
                        for (var s = 0; s < t.children.length; s++) i.children.push(e(t.children[s], i));
                        return r[t.type] = i
                    }(o, null);
                return {
                    typeToNodeMap: r,
                    root: e
                }
            },
            initializeFromClone: function(e) {
                r = e.typeToNodeMap, o = e.root
            }
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("service/ConstraintService", ["service/ValidationService", "domain/CompositionGraph", "service/ExceptionService", "utils/MapUtils", "utils/ArrayUtils"], t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.ConstraintService = t(e.regulaModules.ValidationService, e.regulaModules.CompositionGraph, e.regulaModules.ExceptionService, e.regulaModules.MapUtils, e.regulaModules.ArrayUtils))
    }(this, function(c, d, h, p, a) {
        function f(e, t, n) {
            var i = {
                error: !1,
                message: ""
            };
            n.__size__ < t.params.length && (i = {
                error: !0,
                message: h.generateExceptionMessage(e, v[t.constraintType], "@" + v[t.constraintType] + " expects at least " + t.params.length + " parameter(s). However, you have provided only " + n.__size__),
                data: null
            });
            for (var s = [], r = 0; r < t.params.length; r++) {
                var o = t.params[r];
                void 0 === n[o] && s.push(o)
            }
            return 0 < s.length && (i = {
                error: !0,
                message: h.generateExceptionMessage(e, v[t.constraintType], "You seem to have provided some optional or required parameters for @" + v[t.constraintType] + ", but you are still missing the following " + s.length + " required parameter(s): " + a.explode(s, ", ")),
                data: null
            }), i
        }

        function u(e, t) {
            var n = d.getNodeByType(g[e]);
            null == n && (d.addNode({
                type: g[e],
                name: e,
                parent: null
            }), n = d.getNodeByType(g[e]));
            for (var i = 0; i < n.children.length; i++) {
                for (var s = n.children[i], r = [], o = 0; o < s.parents.length; o++) s.parents[o] !== n && r.push(s.parents[o]);
                s.parents = r
            }
            n.children = [];
            for (i = 0; i < t.length; i++) {
                var a = v[t[i].constraintType],
                    l = y[a];
                d.addNode({
                    type: l.constraintType,
                    name: v[l.constraintType],
                    parent: n
                })
            }
        }

        function m(e, t, n) {
            for (var i = 0; i < t.length; i++) {
                if (void 0 === t[i].constraintType) throw new h.Exception.ConstraintDefinitionException("In compound constraint " + e + ": A composing constraint has no constraint type specified.");
                var s = t[i],
                    r = v[s.constraintType],
                    o = {
                        __size__: 0
                    };
                for (var a in s.params = s.params || {}, s.params) s.params.hasOwnProperty(a) && p.put(o, a, s.params[a]);
                var l = 0;
                for (var c in s.params) s.params.hasOwnProperty(c) && l++;
                s.params.__size__ = l;
                for (var d = 0; d < n.length; d++) p.put(o, n[d], null);
                var u = f(null, y[r], o);
                if (u.error) throw new h.Exception.ConstraintDefinitionException("In compound constraint " + e + ": " + u.message)
            }
        }
        var g = {},
            v = {},
            r = 0;
        ! function(e) {
            for (var t = 0; t < e.length; t++) g[e[t]] = t, v[t] = e[t];
            r = t, g.Between = g.row, g.Matches = g.Pattern, g.Empty = g.Blank, g.NotEmpty = g.NotBlank, g.IsAlpha = g.Alpha, g.IsNumeric = g.Numeric, g.IsAlphaNumeric = g.AlphaNumeric
        }(["Checked", "Selected", "Max", "Min", "Range", "Between", "NotBlank", "NotEmpty", "Blank", "Empty", "Pattern", "Matches", "Email", "Alpha", "IsAlpha", "Numeric", "IsNumeric", "AlphaNumeric", "IsAlphaNumeric", "Integer", "Real", "CompletelyFilled", "PasswordsMatch", "Required", "Length", "Digits", "Past", "Future", "Step", "URL", "HTML5Required", "HTML5Email", "HTML5URL", "HTML5MaxLength", "HTML5Pattern", "HTML5Min", "HTML5Max", "HTML5Step"]);
        var y = {
            Checked: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.checked,
                constraintType: g.Checked,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} needs to be checked."
            },
            Selected: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.selected,
                constraintType: g.Selected,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} needs to be selected."
            },
            Max: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.max,
                constraintType: g.Max,
                custom: !1,
                compound: !1,
                params: ["value"],
                defaultMessage: "{label} needs to be lesser than or equal to {value}."
            },
            Min: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.min,
                constraintType: g.Min,
                custom: !1,
                compound: !1,
                params: ["value"],
                defaultMessage: "{label} needs to be greater than or equal to {value}."
            },
            Range: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.row,
                constraintType: g.row,
                custom: !1,
                compound: !1,
                params: ["min", "max"],
                defaultMessage: "{label} needs to be between {min} and {max}."
            },
            NotBlank: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.notBlank,
                constraintType: g.NotBlank,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} cannot be blank."
            },
            Blank: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.blank,
                constraintType: g.Blank,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} needs to be blank."
            },
            Pattern: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.matches,
                constraintType: g.Pattern,
                custom: !1,
                compound: !1,
                params: ["regex"],
                defaultMessage: "{label} needs to match {regex}{flags}."
            },
            Email: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.email,
                constraintType: g.Email,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} is not a valid email."
            },
            Alpha: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.alpha,
                constraintType: g.Alpha,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} can only contain letters."
            },
            Numeric: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.numeric,
                constraintType: g.Numeric,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "Only numbers are required"
            },
            AlphaNumeric: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.alphaNumeric,
                constraintType: g.AlphaNumeric,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} can only contain numbers and letters."
            },
            Integer: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.integer,
                constraintType: g.Integer,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} must be an integer."
            },
            Real: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.real,
                constraintType: g.Real,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} must be a real number."
            },
            CompletelyFilled: {
                async: !1,
                html5: !1,
                formSpecific: !0,
                validator: c.Validator.completelyFilled,
                constraintType: g.CompletelyFilled,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} must be completely filled."
            },
            PasswordsMatch: {
                async: !1,
                html5: !1,
                formSpecific: !0,
                validator: c.Validator.passwordsMatch,
                constraintType: g.PasswordsMatch,
                custom: !1,
                compound: !1,
                params: ["field1", "field2"],
                defaultMessage: "Passwords do not match."
            },
            Required: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.required,
                constraintType: g.Required,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} is required."
            },
            Length: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.length,
                constraintType: g.Length,
                custom: !1,
                compound: !1,
                params: ["min", "max"],
                defaultMessage: "{label} length must be between {min} and {max}."
            },
            Digits: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.digits,
                constraintType: g.Digits,
                custom: !1,
                compound: !1,
                params: ["integer", "fraction"],
                defaultMessage: "{label} must have up to {integer} digits and {fraction} fractional digits."
            },
            Past: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.past,
                constraintType: g.Past,
                custom: !1,
                compound: !1,
                params: ["format"],
                defaultMessage: "{label} must be in the past."
            },
            Future: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.future,
                constraintType: g.Future,
                custom: !1,
                compound: !1,
                params: ["format"],
                defaultMessage: "{label} must be in the future."
            },
            Step: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.step,
                constraintType: g.Step,
                custom: !1,
                compound: !1,
                params: ["min", "max", "value"],
                defaultMessage: "{label} must be equal to {min} or greater, and equal to {max} or lesser, at increments of {value}."
            },
            URL: {
                async: !1,
                html5: !1,
                formSpecific: !1,
                validator: c.Validator.url,
                constraintType: g.URL,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} must be a valid URL."
            },
            HTML5Required: {
                async: !1,
                html5: !0,
                inputType: null,
                attribute: "required",
                formSpecific: !1,
                validator: c.Validator.html5Required,
                constraintType: g.HTML5Required,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} is required."
            },
            HTML5Email: {
                async: !1,
                html5: !0,
                inputType: "email",
                attribute: null,
                formSpecific: !1,
                validator: c.Validator.html5Email,
                constraintType: g.HTML5Email,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} is not a valid email."
            },
            HTML5Pattern: {
                async: !1,
                html5: !0,
                inputType: null,
                attribute: "pattern",
                formSpecific: !1,
                validator: c.Validator.html5Pattern,
                constraintType: g.HTML5Pattern,
                custom: !1,
                compound: !1,
                params: ["pattern"],
                defaultMessage: "{label} needs to match {pattern}."
            },
            HTML5URL: {
                async: !1,
                html5: !0,
                inputType: "url",
                attribute: null,
                formSpecific: !1,
                validator: c.Validator.html5URL,
                constraintType: g.HTML5URL,
                custom: !1,
                compound: !1,
                params: [],
                defaultMessage: "{label} is not a valid URL."
            },
            HTML5MaxLength: {
                async: !1,
                html5: !0,
                inputType: null,
                attribute: "maxlength",
                formSpecific: !1,
                validator: c.Validator.html5MaxLength,
                constraintType: g.HTML5MaxLength,
                custom: !1,
                compound: !1,
                params: ["maxlength"],
                defaultMessage: "{label} must be less than {maxlength} characters."
            },
            HTML5Min: {
                async: !1,
                html5: !0,
                inputType: null,
                attribute: "min",
                formSpecific: !1,
                validator: c.Validator.html5Min,
                constraintType: g.HTML5Min,
                custom: !1,
                compound: !1,
                params: ["min"],
                defaultMessage: "{label} needs to be greater than or equal to {min}."
            },
            HTML5Max: {
                async: !1,
                html5: !0,
                inputType: null,
                attribute: "max",
                formSpecific: !1,
                validator: c.Validator.html5Max,
                constraintType: g.HTML5Max,
                custom: !1,
                compound: !1,
                params: ["max"],
                defaultMessage: "{label} needs to be lesser than or equal to {max}."
            },
            HTML5Step: {
                async: !1,
                html5: !0,
                inputType: null,
                attribute: "step",
                formSpecific: !1,
                validator: c.Validator.html5Step,
                constraintType: g.HTML5Step,
                custom: !1,
                compound: !1,
                params: ["step"],
                defaultMessage: "{label} must be equal to the minimum value or greater at increments of {step}."
            }
        };
        return {
            Constraint: g,
            ReverseConstraint: v,
            firstCustomConstraintIndex: r,
            constraintDefinitions: y,
            override: function(e) {
                var r = void 0 === e.async ? y[e.name].async : e.async,
                    t = e.validator;
                e.validatorRedefined && !e.formSpecific && (t = c.wrapValidatorWithEmptyCheck(t));
                var n = d.getNodeByType(e.constraintType);
                if (e.compound) {
                    m(e.name, e.composingConstraints, e.params);
                    var i = d.clone();
                    u(e.name, e.composingConstraints);
                    var s = d.analyze(n);
                    if (s.cycle) throw d.initializeFromClone(i), new h.Exception.ConstraintDefinitionException("regula.override: The overriding composing-constraints you have specified have created a cyclic composition: " + s.path);
                    r = !1;
                    for (var o = 0; o < e.composingConstraints.length && !r;) {
                        var a = e.composingConstraints[o],
                            l = y[v[a.constraintType]];
                        r = l.async, o++
                    }
                }
                null !== n && function e(t) {
                    for (var n = 0; n < t.parents.length; n++) {
                        var i = t.parents[n];
                        if (i.type !== d.ROOT) {
                            var s = v[i.type];
                            y[s].async = r, e(i)
                        }
                    }
                }(n), y[e.name] = {
                    async: r,
                    formSpecific: e.formSpecific,
                    constraintType: g[e.name],
                    custom: !0,
                    compound: e.compound,
                    params: e.params,
                    composingConstraints: e.composingConstraints,
                    defaultMessage: e.defaultMessage,
                    validator: t
                }, y[e.name].custom && e.validatorRedefined && c.createPublicValidator(e.name, y)
            },
            custom: function(e) {
                g[e.name] = r, v[r++] = e.name;
                var t = e.validator;
                e.formSpecific || (t = c.wrapValidatorWithEmptyCheck(e.validator)), y[e.name] = {
                    async: e.async,
                    formSpecific: e.formSpecific,
                    validator: t,
                    constraintType: g[e.name],
                    custom: !0,
                    compound: !1,
                    params: e.params,
                    defaultMessage: e.defaultMessage
                }, c.createPublicValidator(e.name, y)
            },
            compound: function(e) {
                m(e.name, e.constraints, e.params);
                for (var t = !1, n = 0; n < e.constraints.length && !t;) {
                    var i = e.constraints[n],
                        s = v[i.constraintType];
                    t = t || y[s].async, n++
                }
                g[e.name] = r, v[r++] = e.name, y[e.name] = {
                    async: t,
                    formSpecific: e.formSpecific,
                    constraintType: g[e.name],
                    custom: !0,
                    compound: !0,
                    params: e.params,
                    reportAsSingleViolation: e.reportAsSingleViolation,
                    composingConstraints: e.constraints,
                    defaultMessage: e.defaultMessage,
                    validator: c.compoundValidator
                }, c.createPublicValidator(e.name, y), u(e.name, e.constraints)
            },
            verifyConstraintDefinition: function(e, t, n) {
                var i = {
                        successful: !0,
                        message: "",
                        data: null
                    },
                    s = e.cloneNode(!1);
                if ("form" != s.tagName.toLowerCase() || y[t].formSpecific)
                    if ("form" != s.tagName.toLowerCase() && y[t].formSpecific) i = {
                        successful: !1,
                        message: h.generateExceptionMessage(e, t, "@" + t + " is a form constraint, but you are trying to bind it to a non-form element"),
                        data: null
                    };
                    else if ((void 0 === s.type || "checkbox" != s.type.toLowerCase() && "radio" != s.type.toLowerCase()) && "Checked" == t) i = {
                    successful: !1,
                    message: h.generateExceptionMessage(e, t, "@" + t + " is only applicable to checkboxes and radio buttons. You are trying to bind it to an input element that is neither a checkbox nor a radio button."),
                    data: null
                };
                else if ("select" != s.tagName.toLowerCase() && "Selected" == t) i = {
                    successful: !1,
                    message: h.generateExceptionMessage(e, t, "@" + t + " is only applicable to select boxes. You are trying to bind it to an input element that is not a select box."),
                    data: null
                };
                else {
                    var r = f(e, y[t], n);
                    r.error ? i = {
                        successful: !1,
                        message: r.message,
                        data: null
                    } : i.data = n
                } else i = {
                    successful: !1,
                    message: h.generateExceptionMessage(e, t, "@" + t + " is not a form constraint, but you are trying to bind it to a form"),
                    data: null
                };
                return i
            },
            verifyParameterCountMatches: f
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("parser/Parser", ["utils/MapUtils", "service/ExceptionService", "service/ConstraintService"], t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.Parser = t(e.regulaModules.MapUtils, e.regulaModules.ExceptionService, e.regulaModules.ConstraintService))
    }(this, function(f, m, g) {
        function v(e) {
            return e ? e.replace(/^\s+/, "").replace(/\s+$/, "") : ""
        }

        function y(e) {
            return e[0]
        }
        return {
            parse: function(o, e) {
                function a(e) {
                    var t = {
                        successful: !0,
                        message: "",
                        data: null
                    };
                    return /[A-Za-z_]/.test(e) && void 0 !== e && null != e || (t = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Invalid starting character"),
                        data: null
                    }), t
                }

                function l(e) {
                    var t = {
                        successful: !0,
                        message: "",
                        data: null
                    };
                    return /[0-9A-Za-z_]/.test(e) || (t = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Invalid character in identifier. Can only include 0-9, A-Z, a-z, and _") + " " + t.message,
                        data: null
                    }), t
                }

                function c(e) {
                    var t = function(e) {
                        var t = v(e.shift());
                        0 == t.length && (t = e.shift());
                        var n = {
                            successful: !1,
                            message: m.generateExceptionMessage(o, p, "Invalid starting character for parameter name. Can only include A-Z, a-z, and _"),
                            data: null
                        };
                        if (void 0 !== t)
                            if ((n = a(t.charAt(0))).successful) {
                                for (var i = 1; i < t.length && n.successful;) n = l(t.charAt(i)), i++;
                                n.successful && (n.data = t)
                            } else n = {
                                successful: !1,
                                message: m.generateExceptionMessage(o, p, "Invalid starting character for parameter name. Can only include A-Z, a-z, and _") + " " + n.message,
                                data: null
                            };
                        return n
                    }(e);
                    if (t.successful) {
                        var n = t.data,
                            i = e.shift();
                        "=" == i ? (t = function(e) {
                            0 == v(y(e)).length && e.shift();
                            var t = {
                                successful: !0,
                                message: "",
                                data: []
                            };
                            if (")" == y(e)) t = {
                                successful: !1,
                                message: m.generateExceptionMessage(o, p, "Parameter value expected") + " " + t.message,
                                data: null
                            };
                            else {
                                var n = (t = function(e) {
                                    var t = function(e) {
                                        var t = e.shift(),
                                            n = {
                                                successful: !0,
                                                message: "",
                                                data: null
                                            };
                                        return "-" == t ? (n = s(e)).successful && (n.data = t + n.data) : (e.unshift(t), n = {
                                            successful: !1,
                                            message: m.generateExceptionMessage(o, p, "Not a negative number"),
                                            data: null
                                        }), n
                                    }(e);
                                    return t.successful || (t = s(e)).successful || (t = {
                                        successful: !1,
                                        message: m.generateExceptionMessage(o, p, "Parameter value is not a number") + " " + t.message,
                                        data: null
                                    }), t
                                }(e)).message;
                                t.successful || ((t = function(e) {
                                    var t = e.shift(),
                                        n = "",
                                        i = {
                                            successful: !0,
                                            message: "",
                                            data: null
                                        };
                                    if ('"' == t) {
                                        for (var s = !1; 0 < e.length && i.successful && !s;) '"' == y(e) ? (s = !0, e.shift()) : (i = u(e), n += i.data);
                                        s || (i = {
                                            successful: !1,
                                            message: m.generateExceptionMessage(o, p, "Unterminated string literal"),
                                            data: null
                                        })
                                    } else e.unshift(t), i = {
                                        successful: !1,
                                        message: m.generateExceptionMessage(o, p, "Invalid quoted string"),
                                        data: null
                                    };
                                    return i.successful = i.successful && s, i.data = n, i
                                }(e)).message = t.message + " " + n, n = t.message, t.successful || ((t = function(e) {
                                    var t = "",
                                        n = e.shift(),
                                        i = {
                                            successful: !0,
                                            message: "",
                                            data: null
                                        };
                                    if ("/" == n) {
                                        t = n;
                                        for (var s = !1; 0 < e.length && i.successful && !s;) "/" == y(e) ? (t += e.shift(), s = !0) : (i = u(e), t += i.data);
                                        s || (i = {
                                            successful: !1,
                                            message: m.generateExceptionMessage(o, p, "Unterminated regex literal"),
                                            data: null
                                        })
                                    } else e.unshift(n), i = {
                                        successful: !1,
                                        message: m.generateExceptionMessage(o, p, "Not a regular expression"),
                                        data: null
                                    };
                                    return i.successful = i.successful && s, i.data = t, i
                                }(e)).message = t.message + " " + n, n = t.message, t.successful || ((t = function(e) {
                                    var t = e.shift(),
                                        n = {
                                            successful: !0,
                                            message: "",
                                            data: null
                                        };
                                    return n = "true" == v(t) || "false" == v(t) ? {
                                        successful: !0,
                                        message: "",
                                        data: "true" === t
                                    } : (e.unshift(t), {
                                        successful: !1,
                                        message: m.generateExceptionMessage(o, p, "Not a boolean"),
                                        data: null
                                    }), n
                                }(e)).message = t.message + " " + n, n = t.message, t.successful || ((t = function(e) {
                                    var t = [],
                                        n = e.shift(),
                                        i = {
                                            successful: !0,
                                            message: "",
                                            data: null
                                        };
                                    if ("[" == n)
                                        if (0 == v(y(e)).length && e.shift(), (i = "]" == y(e) ? {
                                                successful: !0,
                                                message: "",
                                                data: ""
                                            } : h(e)).successful) {
                                            for (t.push(i.data), 0 == v(y(e)).length && e.shift(); 0 < e.length && "," == y(e) && i.successful;) e.shift(), i = h(e), t.push(i.data), 0 == v(y(e)).length && e.shift();
                                            i.data = t, 0 == v(n = e.shift()).length && e.shift(), "]" != n && (i = {
                                                successful: !1,
                                                message: m.generateExceptionMessage(o, p, "Cannot find matching closing ] in group definition") + " " + i.message,
                                                data: null
                                            })
                                        } else i = {
                                            successful: !1,
                                            message: m.generateExceptionMessage(o, p, "Invalid group definition") + " " + i.message,
                                            data: null
                                        };
                                    else e.unshift(n), i = {
                                        successful: !1,
                                        message: m.generateExceptionMessage(o, p, "Not a valid group definition"),
                                        data: null
                                    };
                                    return i
                                }(e)).message = t.message + " " + n, n = t.message, t.successful || (t = {
                                    successful: !1,
                                    message: m.generateExceptionMessage(o, p, "Parameter value must be a number, quoted string, regular expression, or a boolean") + " " + n,
                                    data: null
                                })))))
                            }
                            return t
                        }(e)).successful ? t.data = {
                            name: n,
                            value: t.data
                        } : t = {
                            successful: !1,
                            message: m.generateExceptionMessage(o, p, "Invalid parameter value") + " " + t.message,
                            data: null
                        } : (e.unshift(i), t = {
                            successful: !1,
                            message: m.generateExceptionMessage(o, p, "'=' expected after parameter name " + t.message),
                            data: null
                        })
                    } else t = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Invalid parameter name. You might have unmatched parentheses") + " " + t.message,
                        data: null
                    };
                    return t
                }

                function s(e) {
                    var t = null;
                    if ("." != y(e)) {
                        if (t = r(e), "." == y(e)) {
                            var n = t.data;
                            (t = i(e)).successful && (t.data = n + t.data)
                        }
                    } else t = i(e);
                    return t.successful || (t = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Not a positive number") + " " + t.message,
                        data: null
                    }), t
                }

                function i(e) {
                    var t = e.shift(),
                        n = r(e);
                    return n.successful ? n.data = t + n.data : n = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Not a valid fraction"),
                        data: null
                    }, n
                }

                function r(e) {
                    var t = v(e.shift()),
                        n = d(t.charAt(0));
                    if (n.successful) {
                        for (var i = 1; i < t.length && n.successful;) n = d(t.charAt(i)), i++;
                        n.successful && (n.data = t)
                    } else e.unshift(t), n = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Not a valid integer") + " " + n.message,
                        data: []
                    };
                    return n
                }

                function d(e) {
                    var t = {
                        successful: !0,
                        message: "",
                        data: null
                    };
                    return /[0-9]/.test(e) || (t = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Not a valid digit"),
                        data: null
                    }), t
                }

                function u(e) {
                    var t = "",
                        n = e.shift();
                    return "\\" == n && (t = e.shift()), {
                        successful: !0,
                        message: "",
                        data: n + t
                    }
                }

                function h(e) {
                    var t = {
                            successful: !0,
                            message: "",
                            data: ""
                        },
                        n = v(e.shift());
                    if (0 == n.length && (n = e.shift()), (t = a(n.charAt(0))).successful) {
                        for (var i = 1; i < n.length && t.successful;) t = l(n.charAt(i)), i++;
                        t.successful && (t.data = n)
                    } else t = {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Invalid starting character for group name. Can only include A-Z, a-z, and _") + " " + t.message,
                        data: null
                    };
                    return t
                }
                var p = "";
                return function(e) {
                    for (var t, n, i, s = {
                            successful: !0,
                            message: "",
                            data: null
                        }, r = []; 0 < e.length && s.successful;) n = {
                        successful: !(i = n = void 0),
                        message: "",
                        data: null
                    }, 0 == v(i = (t = e).shift()).length && (i = t.shift()), s = n = "@" == i ? function(e) {
                        var t = {
                                Between: "Range",
                                Matches: "Pattern",
                                Empty: "Blank",
                                NotEmpty: "NotBlank",
                                IsAlpha: "Alpha",
                                IsNumeric: "Integer",
                                IsAlphaNumeric: "AlphaNumeric"
                            },
                            n = function(e) {
                                var t = v(e.shift()),
                                    n = a(t.charAt(0));
                                if (n.successful) {
                                    for (var i = 1; i < t.length && n.successful;) n = l(t.charAt(i)), i++;
                                    n.successful && (n.data = t)
                                } else n = {
                                    successful: !1,
                                    message: m.generateExceptionMessage(o, p, "Invalid starting character for constraint name. Can only include A-Z, a-z, and _") + " " + n.message,
                                    data: null
                                };
                                return n
                            }(e);
                        if (n.successful)
                            if (p = n.data, p = t[p] ? t[p] : p, g.constraintDefinitions[p]) {
                                if ((n = function(e) {
                                        var t = {
                                            successful: !0,
                                            message: "",
                                            data: {}
                                        };
                                        if ("(" == y(e)) {
                                            e.shift();
                                            var n = {};
                                            if (")" == y(e)) e.shift();
                                            else if ((t = c(e)).successful) {
                                                for (f.put(n, t.data.name, t.data.value), 0 == v(y(e)).length && e.shift(); 0 < e.length && "," == y(e) && t.successful;) e.shift(), (t = c(e)).successful && (f.put(n, t.data.name, t.data.value), 0 == v(y(e)).length && e.shift());
                                                if (t.successful) {
                                                    var i = e.shift();
                                                    0 == v(i).length && (i = e.shift()), ")" != i ? t = {
                                                        successful: !1,
                                                        message: m.generateExceptionMessage(o, p, "Cannot find matching closing ) in parameter list") + " " + t.message,
                                                        data: null
                                                    } : t.data = n
                                                }
                                            } else t = {
                                                successful: !1,
                                                message: m.generateExceptionMessage(o, p, "Invalid parameter definition") + " " + t.message,
                                                data: null
                                            }
                                        } else void 0 !== y(e) && "@" != y(e) && (t = {
                                            successful: !1,
                                            message: m.generateExceptionMessage(o, p, "Unexpected character '" + y(e) + "' after constraint definition") + " " + t.message,
                                            data: null
                                        });
                                        return t
                                    }(e)).successful && (n = g.verifyConstraintDefinition(o, p, n.data)).successful) {
                                    var i = n.data;
                                    n.data = {
                                        element: o,
                                        constraintName: p,
                                        definedParameters: i
                                    }
                                }
                            } else n = {
                                successful: !1,
                                message: m.generateExceptionMessage(o, p, "I cannot find the specified constraint name. If this is a custom constraint, you need to define it before you bind to it") + " " + n.message,
                                data: null
                            };
                        else n = {
                            successful: !1,
                            message: m.generateExceptionMessage(o, p, "Invalid constraint name in constraint definition") + " " + n.message,
                            data: null
                        };
                        return n
                    }(t) : {
                        successful: !1,
                        message: m.generateExceptionMessage(o, p, "Invalid constraint. Constraint definitions need to start with '@'") + " " + n.message,
                        data: null
                    }, r.push(s.data);
                    return s.data = r, s
                }(function(e) {
                    for (var t = e.str, n = e.delimiters.split(""), i = e.returnDelimiters || !1, s = e.returnEmptyTokens || !1, r = [], o = 0, a = 0; a < t.length; a++)
                        if (f.exists(n, t.charAt(a))) {
                            var l;
                            0 == (l = t.substring(o, a)).length ? s && r.push(l) : r.push(l), i && r.push(t.charAt(a)), o = a + 1
                        } return o < t.length && (0 == (l = t.substring(o, t.length)).length ? s && r.push(l) : r.push(l)), r
                }({
                    str: v(e.replace(/\s*\n\s*/g, "")),
                    delimiters: '@()[]=,"\\/-\\.',
                    returnDelimiters: !0,
                    returnEmptyTokens: !1
                }))
            }
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("service/BindingService", ["utils/MapUtils", "service/GroupService", "utils/DOMUtils", "parser/Parser", "service/ConstraintService", "service/ExceptionService"], t) : (void 0 === e.regulaModules && (e.regulaModules = {}), e.regulaModules.BindingService = t(e.regulaModules.MapUtils, e.regulaModules.GroupService, e.regulaModules.DOMUtils, e.regulaModules.Parser, e.regulaModules.ConstraintService, e.regulaModules.ExceptionService))
    }(this, function(m, g, u, c, v, y) {
        function e() {
            x = {
                Default: {}
            }
        }

        function b(e, t) {
            if (m.isEmpty(x[t][e]) && (delete x[t][e], m.isEmpty(x[t]))) {
                delete x[t];
                var n = g.Group[t];
                delete g.Group[t], delete g.ReverseGroup[n], g.deletedGroupIndices.push(n)
            }
        }

        function h(e) {
            var t = {
                    successful: !0,
                    message: "",
                    data: null
                },
                n = void 0 !== e.cloneNode ? e.cloneNode(!1) : e,
                i = null;
            return void 0 !== n.tagName && (i = n.tagName.toLowerCase()), "form" !== i && "select" !== i && "textarea" !== i && "input" !== i ? t = {
                successful: !1,
                message: i + "#" + e.id + " is not an input, select, textarea, or form element! Validation constraints can only be attached to input, select, textarea, or form elements.",
                data: null
            } : "input" === i && null === e.getAttribute("type") && (t = {
                successful: !1,
                message: i + "#" + e.id + " does not have a type attribute.",
                data: null
            }), t
        }

        function o(e) {
            var t, n = e.element;
            t = null === n ? u.getElementsByAttribute(document.body, "*", "data-constraints") : [n];
            for (var i = {
                    successful: !0,
                    message: "",
                    data: null
                }, s = 0; s < t.length && i.successful;)
                if ((i = h(n = t[s])).successful) {
                    n.id || (n.id = u.generateRandomId());
                    var r = n.getAttribute("data-constraints");
                    if (null !== r && (i = c.parse(n, r)).successful && null !== i.data)
                        for (var o = i.data, a = 0; i.successful && a < o.length;) {
                            var l = o[a];
                            i = w(l.element, l.constraintName, l.definedParameters), a++
                        }
                    s++
                } return i
        }

        function a(e, t) {
            function n(e, t, n) {
                var i = x[g.ReverseGroup[g.Group.Default]][e.id][v.ReverseConstraint[t]].groups,
                    s = [];
                n.groups ? s = n.groups : s.push(g.ReverseGroup[g.Group.Default]), m.exists(s, g.ReverseGroup[g.Group.Default]) || s.push(g.ReverseGroup[g.Group.Default]);
                for (var r = function(e, t) {
                        for (var n = [], i = 0; i < t.length; i++) m.exists(e, t[i]) || n.push(t[i]);
                        return n
                    }(s, function(e, t) {
                        for (var n = {}, i = [], s = 0; s < e.length; s++) i.push(e[s]), n[e[s]] = !0;
                        for (var r = 0; r < t.length; r++) n[t[r]] || i.push(t[r]);
                        return i
                    }(i, s)), o = 0; o < r.length; o++) {
                    var a = r[o];
                    delete x[a][e.id][v.ReverseConstraint[t]], b(e.id, a)
                }
            }
            var i = {
                    successful: !0,
                    message: "",
                    data: null
                },
                s = t.element,
                r = e.overwriteConstraint || !1,
                o = e.overwriteParameters || !1,
                a = e.constraintType,
                l = e.params || {},
                c = {
                    __size__: 0
                },
                d = l.groups;
            if (void 0 === a) i = {
                successful: !1,
                message: "regula.bind expects a valid constraint type for each constraint in constraints attribute of the options argument. " + y.explodeParameters(t),
                data: null
            };
            else if (l && l.groups)
                if (l.groups instanceof Array) {
                    for (var u = [], h = 0; h < l.groups.length && i.successful;) "string" == typeof l.groups[h] ? u.push(l.groups[h]) : void 0 !== g.ReverseGroup[l.groups[h]] ? u.push(g.ReverseGroup[l.groups[h]]) : i = {
                        successful: !1,
                        message: "Invalid group: " + l.groups[h] + ". " + y.explodeParameters(t),
                        data: null
                    }, h++;
                    i.successful && (l.groups = u)
                } else i = {
                    successful: !1,
                    message: "The groups parameter must be an array of enums or strings " + y.explodeParameters(t),
                    data: null
                };
            if (i.successful) {
                if (x[g.ReverseGroup[g.Group.Default]][s.id] && x[g.ReverseGroup[g.Group.Default]][s.id][v.ReverseConstraint[a]])
                    if (r) {
                        for (var p in l) l.hasOwnProperty(p) && m.put(c, p, l[p]);
                        (i = v.verifyConstraintDefinition(s, v.ReverseConstraint[a], c)).successful && n(s, a, l)
                    } else {
                        var f = x[g.ReverseGroup[g.Group.Default]][s.id][v.ReverseConstraint[a]];
                        for (var p in f) f.hasOwnProperty(p) && m.put(c, p, f[p]);
                        if (o) {
                            for (var p in l) l.hasOwnProperty(p) && m.put(c, p, l[p]);
                            (i = v.verifyConstraintDefinition(s, v.ReverseConstraint[a], c)).successful && n(s, a, c)
                        } else
                            for (var p in l) l.hasOwnProperty(p) && (f[p] || m.put(c, p, l[p]))
                    }
                else {
                    for (var p in l) l.hasOwnProperty(p) && m.put(c, p, l[p]);
                    i = v.verifyConstraintDefinition(s, v.ReverseConstraint[a], c)
                }
                i.successful && (i = w(s, v.ReverseConstraint[a], c))
            }
            return l.groups = d, i
        }

        function w(e, t, n) {
            var i = {
                successful: !0,
                message: "",
                data: null
            };
            n.groups || m.put(n, "groups", [g.ReverseGroup[g.Group.Default]]);
            var s = n.groups; - 1 === s.indexOf(g.ReverseGroup[g.Group.Default]) && (s.push(g.ReverseGroup[g.Group.Default]), n.groups = s);
            for (var r = 0; r < s.length; r++) {
                var o = s[r];
                if (!x[o]) {
                    var a = -1;
                    a = 0 < g.deletedGroupIndices.length ? g.deletedGroupIndices.pop() : g.firstCustomGroupIndex++, g.Group[o] = a, g.ReverseGroup[a] = o, x[o] = {}
                }
                x[o][e.id] || (x[o][e.id] = {}), x[o][e.id][t] = n
            }
            if (v.constraintDefinitions[t].html5)
                if (null !== e.getAttribute("type") && null !== v.constraintDefinitions[t].inputType && e.getAttribute("type") !== v.constraintDefinitions[t].inputType) i = {
                    successful: !1,
                    message: y.generateExceptionMessage(e, t, "Element type of " + e.getAttribute("type") + " conflicts with type of constraint @" + t + ": " + v.constraintDefinitions[t].inputType),
                    data: null
                };
                else {
                    var l = v.constraintDefinitions[t].attribute,
                        c = v.constraintDefinitions[t].inputType;
                    (null !== l && null === e.getAttribute(l) || null !== c && null === e.getAttribute("type")) && function(e, t, n) {
                        if (t === v.ReverseConstraint[v.Constraint.HTML5Required]) e.setAttribute("required", "true");
                        else
                            for (var i = v.constraintDefinitions[t], s = 0; s < i.params.length; s++) e.setAttribute(i.params[s], n[i.params[s]]);
                        var r = e.getAttribute("class");
                        /regula-modified/.test(r) || e.setAttribute("class", r + " regula-modified")
                    }(e, t, n)
                } return i
        }
        var x = null;
        return {
            initializeBoundConstraints: function() {
                null === x && e()
            },
            resetBoundConstraints: e,
            getBoundConstraints: function() {
                return x
            },
            removeElementAndGroupFromBoundConstraintsIfEmpty: b,
            bindAfterParsing: o,
            bindHTML5ValidationConstraints: function(e) {
                function t(e, t, n) {
                    for (var i = 0; i < t.length; i++) {
                        var s = t[i];
                        s.id || (s.id = u.generateRandomId()), e[s.id] || (e[s.id] = []);
                        var r = {
                            constraint: n.constraint,
                            params: {}
                        };
                        null === n.value && (r.params[n.attribute] = u.getAttributeValueForElement(s, n.attribute)), e[s.id].push(r)
                    }
                }
                var n = e.element,
                    o = {
                        successful: !0,
                        message: "",
                        data: null
                    },
                    i = [{
                        attribute: "required",
                        value: null,
                        constraint: v.Constraint.HTML5Required
                    }, {
                        attribute: "type",
                        value: "email",
                        constraint: v.Constraint.HTML5Email
                    }, {
                        attribute: "type",
                        value: "url",
                        constraint: v.Constraint.HTML5URL
                    }, {
                        attribute: "pattern",
                        value: null,
                        constraint: v.Constraint.HTML5Pattern
                    }, {
                        attribute: "maxlength",
                        value: null,
                        constraint: v.Constraint.HTML5MaxLength
                    }, {
                        attribute: "min",
                        value: null,
                        constraint: v.Constraint.HTML5Min
                    }, {
                        attribute: "max",
                        value: null,
                        constraint: v.Constraint.HTML5Max
                    }, {
                        attribute: "step",
                        value: null,
                        constraint: v.Constraint.HTML5Step
                    }],
                    s = {
                        email: v.Constraint.HTML5Email,
                        url: v.Constraint.HTML5URL
                    },
                    r = {};
                if (null === n)
                    for (var a = 0; a < i.length; a++) {
                        t(r, null == (l = i[a]).value ? u.getElementsByAttribute(document.body, "*", l.attribute) : u.getElementsByAttribute(document.body, "*", l.attribute, l.value), l)
                    } else if (n.id || (n.id = u.generateRandomId()), (o = h(n)).successful) {
                        r[n.id] = [];
                        for (a = 0; a < i.length; a++) {
                            var l;
                            if (null === (l = i[a]).value) {
                                if (null != u.getAttributeValueForElement(n, l.attribute)) {
                                    var c = {
                                        constraint: l.constraint,
                                        params: {}
                                    };
                                    c.params[l.attribute] = u.getAttributeValueForElement(n, l.attribute), r[n.id].push(c)
                                }
                            } else {
                                var d = u.getAttributeValueForElement(n, l.attribute);
                                null != d && void 0 !== s[d] && r[n.id].push({
                                    constraint: s[d],
                                    params: {}
                                })
                            }
                        }
                    } return m.iterateOverMap(r, function(e, t, n) {
                    for (var i = document.getElementById(e), s = 0; s < t.length; s++) {
                        var r = t[s];
                        o = w(i, v.ReverseConstraint[r.constraint], r.params)
                    }
                }), o
            },
            bindFromOptions: function(e) {
                var t = {
                        successful: !0,
                        message: "",
                        data: null
                    },
                    n = e.element,
                    i = e.constraints || [],
                    s = n && n.tagName ? n.tagName.toLowerCase() : null;
                if (n)
                    if (1 !== n.nodeType) t = {
                        successful: !1,
                        message: "regula.bind: element attribute is expected to be an HTMLElement, but was of unexpected type: " + typeof n + ". " + y.explodeParameters(e),
                        data: null
                    };
                    else if ("form" != s && "select" != s && "textarea" != s && "input" != s) t = {
                    successful: !1,
                    message: s + "#" + n.id + " is not an input, select, textarea, or form element! Validation constraints can only be attached to input, select, textarea, or form elements. " + y.explodeParameters(e),
                    data: null
                };
                else if (0 < i.length)
                    for (var r = 0; r < i.length && t.successful;) t = a(i[r], e), r++;
                else t = o({
                    element: n
                });
                else t = {
                    successful: !1,
                    message: "regula.bind expects a non-null element attribute in the options argument. " + y.explodeParameters(e),
                    data: null
                };
                return t
            },
            unbind: function(e) {
                for (var t = !1, n = 0; n < e.elements.length; n++) {
                    var i = e.elements[n].id,
                        s = e.constraints || [];
                    if (0 == s.length)
                        for (var r in x) x.hasOwnProperty(r) && void 0 !== x[r][i] && (delete x[r][i], "Default" !== r && b(i, r), t = !0);
                    else
                        for (var o = 0; o < s.length; o++) {
                            var a = s[o];
                            for (var r in x) x.hasOwnProperty(r) && void 0 !== x[r][i] && (delete x[r][i][v.ReverseConstraint[a]], "Default" !== r && b(i, r), t = !0)
                        }
                }
                if (0 < e.elements.length && !t) throw new y.Exception.IllegalArgumentException("Element with id " + i + " does not have any constraints bound to it. " + y.explodeParameters(e))
            },
            isBound: function(e) {
                var t = e.elementId,
                    n = e.group,
                    i = e.constraint,
                    s = void 0 !== x[g.ReverseGroup[g.Group.Default]][t];
                if (s && void 0 !== n && void 0 === i) s = void 0 !== (r = g.ReverseGroup[n]) && void 0 !== x[r][t];
                else if (s && void 0 === n && void 0 !== i) {
                    s = void 0 !== (o = v.ReverseConstraint[i]) && void 0 !== x[g.ReverseGroup[g.Group.Default]][t][o]
                } else if (s && void 0 !== n && void 0 !== i) {
                    var r = g.ReverseGroup[n],
                        o = v.ReverseConstraint[i];
                    s = void 0 !== r && void 0 !== o && void 0 !== x[r][t] && void 0 !== x[r][t][o]
                }
                return s
            }
        }
    }), function(e, t) {
        "function" == typeof define && define.amd ? define("regula", ["utils/MapUtils", "utils/DOMUtils", "service/BindingService", "service/ExceptionService", "service/ConstraintService", "service/ValidationService", "service/GroupService"], t) : (e.regula = t(e.regulaModules.MapUtils, e.regulaModules.DOMUtils, e.regulaModules.BindingService, e.regulaModules.ExceptionService, e.regulaModules.ConstraintService, e.regulaModules.ValidationService, e.regulaModules.GroupService), e.regula._modules = e.regulaModules, e.regulaModules = void 0)
    }(this, function(t, s, r, d, u, i, e) {
        var o = {
            validateEmptyFields: !0,
            enableHTML5Validation: !0,
            debug: !1
        };
        return r.initializeBoundConstraints(), i.initializePublicValidators(u.constraintDefinitions), {
            configure: function(e) {
                t.iterateOverMap(e, function(e, t, n) {
                    void 0 !== o[e] && (o[e] = t)
                })
            },
            bind: function(e) {
                var t = {
                    successful: !0,
                    message: "",
                    data: null
                };
                if (void 0 !== e && e) {
                    var n = e.elements;
                    if (void 0 !== n && n)
                        for (var i = 0; t.successful && i < n.length;) e.element = n[i], o.enableHTML5Validation && s.supportsHTML5Validation() && (t = r.bindHTML5ValidationConstraints({
                            element: e.element
                        })), t.successful ? (t = r.bindFromOptions(e)).successful || (t.message = "regula.bind: Element " + (i + 1) + " of " + n.length + " failed: " + t.message) : t.message = "regula.bind: Failed binding HTML5 validation constraints: Element " + (i + 1) + " of " + n.length + " failed: " + t.message, i++;
                    else o.enableHTML5Validation && s.supportsHTML5Validation() && void 0 !== e.element && null !== e.element && (t = r.bindHTML5ValidationConstraints({
                        element: e.element
                    })), t.successful && (t = r.bindFromOptions(e))
                } else r.resetBoundConstraints(), o.enableHTML5Validation && s.supportsHTML5Validation() && (t = r.bindHTML5ValidationConstraints({
                    element: null
                })), t.successful && (t = r.bindAfterParsing({
                    element: null
                }));
                if (!t.successful) throw new d.Exception.BindException(t.message)
            },
            unbind: function(e) {
                if (void 0 !== e && e) {
                    if (void 0 === e.elementId && void 0 === e.elements) throw new d.Exception.IllegalArgumentException("regula.unbind requires an elementId attribute, or an elements attribute if options are provided");
                    if (!(void 0 === e.elements || e.elements instanceof Array)) throw new d.Exception.IllegalArgumentException("regula.unbind expects the elements attribute to be an array, if it is provided");
                    if (void 0 === e.elements && (e.elements = [document.getElementById(e.elementId)], null === e.elements[0])) throw new d.Exception.IllegalArgumentException("Element with id " + e.elementId + " does not have any constraints bound to it. " + d.explodeParameters(e));
                    r.unbind(e)
                } else r.resetBoundConstraints()
            },
            isBound: function(e) {
                if (void 0 === e) throw new d.Exception.IllegalArgumentException("regula.isBound expects options");
                var t = e.element,
                    n = e.elementId;
                if (void 0 === t && void 0 === n) throw new d.Exception.IllegalArgumentException("regula.isBound expects at the very least, either an element or elementId attribute");
                if (e.hasOwnProperty("constraint") && void 0 === e.constraint) throw new d.Exception.IllegalArgumentException("Undefined constraint was supplied as a parameter");
                if (e.hasOwnProperty("group") && void 0 === e.group) throw new d.Exception.IllegalArgumentException("Undefined group was supplied as a parameter");
                return void 0 !== t && (n = t.id), r.isBound({
                    elementId: n,
                    group: e.group,
                    constraint: e.constraint
                })
            },
            validate: function(e, t) {
                i.init({
                    config: o,
                    ReverseConstraint: u.ReverseConstraint,
                    constraintDefinitions: u.constraintDefinitions,
                    boundConstraints: r.getBoundConstraints()
                });
                var n = [];
                if (void 0 === e || void 0 === e.groups || e.groups instanceof Array) {
                    if (void 0 !== e && void 0 !== e.groups && 0 == e.groups.length) throw new d.Exception.IllegalArgumentException("regula.validate: If a groups attribute is provided, it must not be empty.");
                    if (void 0 !== e && e.hasOwnProperty("constraintType") && void 0 === e.constraintType) throw new d.Exception.IllegalArgumentException("regula.validate: If a constraintType attribute is provided, it cannot be undefined.");
                    if (void 0 === t && "function" == typeof e && (e = {
                            callback: e
                        }), void 0 !== t && (e.callback = t), void 0 !== e && void 0 !== e.elements) {
                        if (!(e.elements instanceof Array)) throw new d.Exception.IllegalArgumentException("regula.validate: If an elements attribute is provided, it must be an array.");
                        if (0 == e.elements.length) throw new d.Exception.IllegalArgumentException("regula.validate: If an elements attribute is provided, it must not be empty.");
                        n = i.validate(e)
                    } else n = i.validate(e);
                    return n
                }
                throw new d.Exception.IllegalArgumentException("regula.validate: If a groups attribute is provided, it must be an array.")
            },
            custom: function(e) {
                if (!e) throw new d.Exception.IllegalArgumentException("regula.custom expects options");
                var t = e.name,
                    n = e.formSpecific || !1,
                    i = e.validator,
                    s = e.params || [],
                    r = e.defaultMessage || "",
                    o = void 0 !== e.async && e.async;
                if (!t) throw new d.Exception.IllegalArgumentException("regula.custom expects a name attribute in the options argument");
                if ("string" != typeof t) throw new d.Exception.IllegalArgumentException("regula.custom expects the name attribute in the options argument to be a string");
                if (0 == t.replace(/\s/g, "").length) throw new d.Exception.IllegalArgumentException("regula.custom cannot accept an empty string for the name attribute in the options argument");
                if ("boolean" != typeof n) throw new d.Exception.IllegalArgumentException("regula.custom expects the formSpecific attribute in the options argument to be a boolean");
                if (!i) throw new d.Exception.IllegalArgumentException("regula.custom expects a validator attribute in the options argument");
                if ("function" != typeof i) throw new d.Exception.IllegalArgumentException("regula.custom expects the validator attribute in the options argument to be a function");
                if (s.constructor.toString().indexOf("Array") < 0) throw new d.Exception.IllegalArgumentException("regula.custom expects the params attribute in the options argument to be an array");
                if ("string" != typeof r) throw new d.Exception.IllegalArgumentException("regula.custom expects the defaultMessage attribute in the options argument to be a string");
                if (u.constraintDefinitions[t]) throw new d.Exception.IllegalArgumentException("There is already a constraint called " + t + ". If you wish to override this constraint, use regula.override");
                u.custom({
                    async: o,
                    name: t,
                    formSpecific: n,
                    validator: i,
                    custom: !0,
                    compound: !1,
                    params: s,
                    defaultMessage: r
                })
            },
            compound: function(e) {
                if (!e) throw new d.Exception.IllegalArgumentException("regula.compound expects options");
                var t = e.name,
                    n = e.constraints || [],
                    i = e.formSpecific || !1,
                    s = e.defaultMessage || "",
                    r = e.params || [],
                    o = void 0 !== e.reportAsSingleViolation && e.reportAsSingleViolation;
                if (!t) throw new d.Exception.IllegalArgumentException("regula.compound expects a name attribute in the options argument");
                if ("string" != typeof t) throw new d.Exception.IllegalArgumentException("regula.compound expects name to be a string parameter");
                if (r.constructor.toString().indexOf("Array") < 0) throw new d.Exception.IllegalArgumentException("regula.compound expects the params attribute in the options argument to be an array");
                if (0 == n.length) throw new d.Exception.IllegalArgumentException("regula.compound expects an array of composing constraints under a constraints attribute in the options argument");
                if (u.constraintDefinitions[t]) throw new d.Exception.IllegalArgumentException("regula.compound: There is already a constraint called " + t + ". If you wish to override this constraint, use regula.override");
                u.compound({
                    name: t,
                    formSpecific: i,
                    params: r,
                    reportAsSingleViolation: o,
                    constraints: n,
                    defaultMessage: s
                })
            },
            override: function(e) {
                if (!e) throw new d.Exception.IllegalArgumentException("regula.override expects options");
                if (void 0 === e.constraintType) throw new d.Exception.IllegalArgumentException("regula.override expects a valid constraintType attribute in the options argument");
                var t = u.ReverseConstraint[e.constraintType];
                if (void 0 === t) throw new d.Exception.IllegalArgumentException("regula.override: I could not find the specified constraint. Perhaps it has not been defined? Function received: " + d.explodeParameters(e));
                var n = !1,
                    i = u.constraintDefinitions[t].formSpecific;
                u.constraintDefinitions[t].custom && (i = void 0 === e.formSpecific ? u.constraintDefinitions[t].formSpecific : e.formSpecific);
                var s = u.constraintDefinitions[t].custom && void 0 !== e.async ? e.async : u.constraintDefinitions[t].async,
                    r = u.constraintDefinitions[t].custom && e.params || u.constraintDefinitions[t].params,
                    o = e.defaultMessage || u.constraintDefinitions[t].defaultMessage,
                    a = u.constraintDefinitions[t].compound,
                    l = e.constraints || u.constraintDefinitions[t].constraints,
                    c = u.constraintDefinitions[t].validator;
                if (u.constraintDefinitions[t].custom && !u.constraintDefinitions[t].compound && void 0 !== e.validator && (c = e.validator, n = !0), "boolean" != typeof i) throw new d.Exception.IllegalArgumentException("regula.override expects the formSpecific attribute in the options argument to be a boolean");
                if ("function" != typeof c) throw new d.Exception.IllegalArgumentException("regula.override expects the validator attribute in the options argument to be a function");
                if (!(r instanceof Array)) throw new d.Exception.IllegalArgumentException("regula.override expects the params attribute in the options argument to be an array");
                if ("string" != typeof o) throw new d.Exception.IllegalArgumentException("regula.override expects the defaultMessage attribute in the options argument to be a string");
                u.override({
                    async: s,
                    formSpecific: i,
                    name: t,
                    constraintType: e.constraintType,
                    compound: a,
                    params: r,
                    composingConstraints: l,
                    defaultMessage: o,
                    validator: c,
                    validatorRedefined: n
                })
            },
            Constraint: u.Constraint,
            Group: e.Group,
            DateFormat: {
                DMY: "DMY",
                MDY: "MDY",
                YMD: "YMD"
            },
            Exception: d.Exception
        }
    }), function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
    }(function(t) {
        function e(e) {
            return i[e] ? i[e].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof e && e ? void n.error("Method " + e + " does not exist on jQuery.regula") : i.bind.apply(this, arguments)
        }
        var n = t,
            i = {
                bind: function(e) {
                    return this instanceof t && (e = e || {}, 0 < this.get().length && n.extend(!0, e, {
                        elements: this.get()
                    })), regula.bind(e), this
                },
                unbind: function(e) {
                    return this instanceof t && (e = e || {}, 0 < this.get().length && n.extend(!0, e, {
                        elements: this.get()
                    })), regula.unbind(e), this
                },
                isBound: function(e) {
                    return this instanceof t && (e = e || {}, 0 < this.get().length && n.extend(!0, e, {
                        element: this.get(0)
                    })), regula.isBound(e), this
                },
                validate: function(e) {
                    return this instanceof t && (e = e || {}, 0 < this.get().length && n.extend(!0, e, {
                        elements: this.get()
                    })), regula.validate(e)
                },
                custom: function(e) {
                    return regula.custom(e), this
                },
                compound: function(e) {
                    return regula.compound(e), this
                },
                override: function(e) {
                    return regula.override(e), this
                }
            };
        i.on = i.bind, i.off = i.unbind, n.fn.regula = e, n.regula = e
    }), function(e) {
        "use strict";
        "function" == typeof define && define.amd ? define(["../jquery"], e) : e("undefined" != typeof jQuery ? jQuery : window.Zepto)
    }(function(I) {
        "use strict";

        function n(e) {
            var t = e.data;
            e.isDefaultPrevented() || (e.preventDefault(), I(e.target).ajaxSubmit(t))
        }

        function i(e) {
            var t = e.target,
                n = I(t);
            if (!n.is("[type=submit],[type=image]")) {
                var i = n.closest("[type=submit]");
                if (0 === i.length) return;
                t = i[0]
            }
            var s = this;
            if ("image" == (s.clk = t).type)
                if (void 0 !== e.offsetX) s.clk_x = e.offsetX, s.clk_y = e.offsetY;
                else if ("function" == typeof I.fn.offset) {
                var r = n.offset();
                s.clk_x = e.pageX - r.left, s.clk_y = e.pageY - r.top
            } else s.clk_x = e.pageX - t.offsetLeft, s.clk_y = e.pageY - t.offsetTop;
            setTimeout(function() {
                s.clk = s.clk_x = s.clk_y = null
            }, 100)
        }

        function P() {
            if (I.fn.ajaxSubmit.debug) {
                var e = "[jquery.form] " + Array.prototype.join.call(arguments, "");
                window.console && window.console.log ? window.console.log(e) : window.opera && window.opera.postError && window.opera.postError(e)
            }
        }
        var b = {};
        b.fileapi = void 0 !== I("<input type='file'/>").get(0).files, b.formdata = void 0 !== window.FormData;
        var L = !!I.fn.prop;
        I.fn.attr2 = function() {
            if (!L) return this.attr.apply(this, arguments);
            var e = this.prop.apply(this, arguments);
            return e && e.jquery || "string" == typeof e ? e : this.attr.apply(this, arguments)
        }, I.fn.ajaxSubmit = function($) {
            function e(e) {
                function d(t) {
                    var n = null;
                    try {
                        t.contentWindow && (n = t.contentWindow.document)
                    } catch (e) {
                        P("cannot get iframe.contentWindow document: " + e)
                    }
                    if (n) return n;
                    try {
                        n = t.contentDocument ? t.contentDocument : t.document
                    } catch (e) {
                        P("cannot get iframe.contentDocument: " + e), n = t.document
                    }
                    return n
                }

                function t() {
                    var e = A.attr2("target"),
                        t = A.attr2("action"),
                        n = A.attr("enctype") || A.attr("encoding") || "multipart/form-data";
                    a.setAttribute("target", r), M && !/post/i.test(M) || a.setAttribute("method", "POST"), t != h.url && a.setAttribute("action", h.url), h.skipEncodingOverride || M && !/post/i.test(M) || A.attr({
                        encoding: "multipart/form-data",
                        enctype: "multipart/form-data"
                    }), h.timeout && (y = setTimeout(function() {
                        v = !0, u(w)
                    }, h.timeout));
                    var i = [];
                    try {
                        if (h.extraData)
                            for (var s in h.extraData) h.extraData.hasOwnProperty(s) && i.push(I.isPlainObject(h.extraData[s]) && h.extraData[s].hasOwnProperty("name") && h.extraData[s].hasOwnProperty("value") ? I('<input type="hidden" name="' + h.extraData[s].name + '">').val(h.extraData[s].value).appendTo(a)[0] : I('<input type="hidden" name="' + s + '">').val(h.extraData[s]).appendTo(a)[0]);
                        h.iframeTarget || f.appendTo("body"), m.attachEvent ? m.attachEvent("onload", u) : m.addEventListener("load", u, !1), setTimeout(function e() {
                            try {
                                var t = d(m).readyState;
                                P("state = " + t), t && "uninitialized" == t.toLowerCase() && setTimeout(e, 50)
                            } catch (e) {
                                P("Server abort: ", e, " (", e.name, ")"), u(x), y && clearTimeout(y), y = void 0
                            }
                        }, 15);
                        try {
                            a.submit()
                        } catch (e) {
                            document.createElement("form").submit.apply(a)
                        }
                    } finally {
                        a.setAttribute("action", t), a.setAttribute("enctype", n), e ? a.setAttribute("target", e) : A.removeAttr("target"), I(i).remove()
                    }
                }

                function u(e) {
                    if (!g.aborted && !S) {
                        if ((C = d(m)) || (P("cannot access response document"), e = x), e === w && g) return g.abort("timeout"), void b.reject(g, "timeout");
                        if (e == x && g) return g.abort("server abort"), void b.reject(g, "error", "server abort");
                        if (C && C.location.href != h.iframeSrc || v) {
                            m.detachEvent ? m.detachEvent("onload", u) : m.removeEventListener("load", u, !1);
                            var t, n = "success";
                            try {
                                if (v) throw "timeout";
                                var i = "xml" == h.dataType || C.XMLDocument || I.isXMLDoc(C);
                                if (P("isXml=" + i), !i && window.opera && (null === C.body || !C.body.innerHTML) && --E) return P("requeing onLoad callback, DOM not available"), void setTimeout(u, 250);
                                var s = C.body ? C.body : C.documentElement;
                                g.responseText = s ? s.innerHTML : null, g.responseXML = C.XMLDocument ? C.XMLDocument : C, i && (h.dataType = "xml"), g.getResponseHeader = function(e) {
                                    return {
                                        "content-type": h.dataType
                                    } [e.toLowerCase()]
                                }, s && (g.status = Number(s.getAttribute("status")) || g.status, g.statusText = s.getAttribute("statusText") || g.statusText);
                                var r = (h.dataType || "").toLowerCase(),
                                    o = /(json|script|text)/.test(r);
                                if (o || h.textarea) {
                                    var a = C.getElementsByTagName("textarea")[0];
                                    if (a) g.responseText = a.value, g.status = Number(a.getAttribute("status")) || g.status, g.statusText = a.getAttribute("statusText") || g.statusText;
                                    else if (o) {
                                        var l = C.getElementsByTagName("pre")[0],
                                            c = C.getElementsByTagName("body")[0];
                                        l ? g.responseText = l.textContent ? l.textContent : l.innerText : c && (g.responseText = c.textContent ? c.textContent : c.innerText)
                                    }
                                } else "xml" == r && !g.responseXML && g.responseText && (g.responseXML = _(g.responseText));
                                try {
                                    T = D(g, r, h)
                                } catch (e) {
                                    n = "parsererror", g.error = t = e || n
                                }
                            } catch (e) {
                                P("error caught: ", e), n = "error", g.error = t = e || n
                            }
                            g.aborted && (P("upload aborted"), n = null), g.status && (n = 200 <= g.status && g.status < 300 || 304 === g.status ? "success" : "error"), "success" === n ? (h.success && h.success.call(h.context, T, "success", g), b.resolve(g.responseText, "success", g), p && I.event.trigger("ajaxSuccess", [g, h])) : n && (void 0 === t && (t = g.statusText), h.error && h.error.call(h.context, g, n, t), b.reject(g, "error", t), p && I.event.trigger("ajaxError", [g, h, t])), p && I.event.trigger("ajaxComplete", [g, h]), p && !--I.active && I.event.trigger("ajaxStop"), h.complete && h.complete.call(h.context, g, n), S = !0, h.timeout && clearTimeout(y), setTimeout(function() {
                                h.iframeTarget ? f.attr("src", h.iframeSrc) : f.remove(), g.responseXML = null
                            }, 100)
                        }
                    }
                }
                var n, i, h, p, r, f, m, g, s, o, v, y, a = A[0],
                    b = I.Deferred();
                if (b.abort = function(e) {
                        g.abort(e)
                    }, e)
                    for (i = 0; i < O.length; i++) n = I(O[i]), L ? n.prop("disabled", !1) : n.removeAttr("disabled");
                if ((h = I.extend(!0, {}, I.ajaxSettings, $)).context = h.context || h, r = "jqFormIO" + (new Date).getTime(), h.iframeTarget ? (o = (f = I(h.iframeTarget)).attr2("name")) ? r = o : f.attr2("name", r) : (f = I('<iframe name="' + r + '" src="' + h.iframeSrc + '" />')).css({
                        position: "absolute",
                        top: "-1000px",
                        left: "-1000px"
                    }), m = f[0], g = {
                        aborted: 0,
                        responseText: null,
                        responseXML: null,
                        status: 0,
                        statusText: "n/a",
                        getAllResponseHeaders: function() {},
                        getResponseHeader: function() {},
                        setRequestHeader: function() {},
                        abort: function(e) {
                            var t = "timeout" === e ? "timeout" : "aborted";
                            P("aborting upload... " + t), this.aborted = 1;
                            try {
                                m.contentWindow.document.execCommand && m.contentWindow.document.execCommand("Stop")
                            } catch (e) {}
                            f.attr("src", h.iframeSrc), g.error = t, h.error && h.error.call(h.context, g, t, e), p && I.event.trigger("ajaxError", [g, h, t]), h.complete && h.complete.call(h.context, g, t)
                        }
                    }, (p = h.global) && 0 == I.active++ && I.event.trigger("ajaxStart"), p && I.event.trigger("ajaxSend", [g, h]), h.beforeSend && !1 === h.beforeSend.call(h.context, g, h)) return h.global && I.active--, b.reject(), b;
                if (g.aborted) return b.reject(), b;
                !(s = a.clk) || (o = s.name) && !s.disabled && (h.extraData = h.extraData || {}, h.extraData[o] = s.value, "image" == s.type && (h.extraData[o + ".x"] = a.clk_x, h.extraData[o + ".y"] = a.clk_y));
                var w = 1,
                    x = 2,
                    l = I("meta[name=csrf-token]").attr("content"),
                    c = I("meta[name=csrf-param]").attr("content");
                c && l && (h.extraData = h.extraData || {}, h.extraData[c] = l), h.forceSync ? t() : setTimeout(t, 10);
                var T, C, S, E = 50,
                    _ = I.parseXML || function(e, t) {
                        return window.ActiveXObject ? ((t = new ActiveXObject("Microsoft.XMLDOM")).async = "false", t.loadXML(e)) : t = (new DOMParser).parseFromString(e, "text/xml"), t && t.documentElement && "parsererror" != t.documentElement.nodeName ? t : null
                    },
                    k = I.parseJSON || function(e) {
                        return window.eval("(" + e + ")")
                    },
                    D = function(e, t, n) {
                        var i = e.getResponseHeader("content-type") || "",
                            s = "xml" === t || !t && 0 <= i.indexOf("xml"),
                            r = s ? e.responseXML : e.responseText;
                        return s && "parsererror" === r.documentElement.nodeName && I.error && I.error("parsererror"), n && n.dataFilter && (r = n.dataFilter(r, t)), "string" == typeof r && ("json" === t || !t && 0 <= i.indexOf("json") ? r = k(r) : ("script" === t || !t && 0 <= i.indexOf("javascript")) && I.globalEval(r)), r
                    };
                return b
            }
            if (!this.length) return P("ajaxSubmit: skipping submit process - no element selected"), this;
            var M, t, n, A = this;
            "function" == typeof $ ? $ = {
                success: $
            } : void 0 === $ && ($ = {}), M = $.type || this.attr2("method"), n = (n = (n = "string" == typeof(t = $.url || this.attr2("action")) ? I.trim(t) : "") || window.location.href || "") && (n.match(/^([^#]+)/) || [])[1], $ = I.extend(!0, {
                url: n,
                success: I.ajaxSettings.success,
                type: M || I.ajaxSettings.type,
                iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
            }, $);
            var i = {};
            if (this.trigger("form-pre-serialize", [this, $, i]), i.veto) return P("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this;
            if ($.beforeSerialize && !1 === $.beforeSerialize(this, $)) return P("ajaxSubmit: submit aborted via beforeSerialize callback"), this;
            var s = $.traditional;
            void 0 === s && (s = I.ajaxSettings.traditional);
            var r, O = [],
                o = this.formToArray($.semantic, O);
            if ($.data && ($.extraData = $.data, r = I.param($.data, s)), $.beforeSubmit && !1 === $.beforeSubmit(o, this, $)) return P("ajaxSubmit: submit aborted via beforeSubmit callback"), this;
            if (this.trigger("form-submit-validate", [o, this, $, i]), i.veto) return P("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this;
            var a = I.param(o, s);
            r && (a = a ? a + "&" + r : r), "GET" == $.type.toUpperCase() ? ($.url += (0 <= $.url.indexOf("?") ? "&" : "?") + a, $.data = null) : $.data = a;
            var l = [];
            if ($.resetForm && l.push(function() {
                    A.resetForm()
                }), $.clearForm && l.push(function() {
                    A.clearForm($.includeHidden)
                }), !$.dataType && $.target) {
                var c = $.success || function() {};
                l.push(function(e) {
                    var t = $.replaceTarget ? "replaceWith" : "html";
                    I($.target)[t](e).each(c, arguments)
                })
            } else $.success && l.push($.success);
            if ($.success = function(e, t, n) {
                    for (var i = $.context || this, s = 0, r = l.length; s < r; s++) l[s].apply(i, [e, t, n || A, A])
                }, $.error) {
                var d = $.error;
                $.error = function(e, t, n) {
                    var i = $.context || this;
                    d.apply(i, [e, t, n, A])
                }
            }
            if ($.complete) {
                var u = $.complete;
                $.complete = function(e, t) {
                    var n = $.context || this;
                    u.apply(n, [e, t, A])
                }
            }
            var h = 0 < I("input[type=file]:enabled", this).filter(function() {
                    return "" !== I(this).val()
                }).length,
                p = "multipart/form-data",
                f = A.attr("enctype") == p || A.attr("encoding") == p,
                m = b.fileapi && b.formdata;
            P("fileAPI :" + m);
            var g, v = (h || f) && !m;
            !1 !== $.iframe && ($.iframe || v) ? $.closeKeepAlive ? I.get($.closeKeepAlive, function() {
                g = e(o)
            }) : g = e(o) : g = (h || f) && m ? function(e) {
                for (var n = new FormData, t = 0; t < e.length; t++) n.append(e[t].name, e[t].value);
                if ($.extraData) {
                    var i = function(e) {
                        var t, n, i = I.param(e, $.traditional).split("&"),
                            s = i.length,
                            r = [];
                        for (t = 0; t < s; t++) i[t] = i[t].replace(/\+/g, " "), n = i[t].split("="), r.push([decodeURIComponent(n[0]), decodeURIComponent(n[1])]);
                        return r
                    }($.extraData);
                    for (t = 0; t < i.length; t++) i[t] && n.append(i[t][0], i[t][1])
                }
                $.data = null;
                var s = I.extend(!0, {}, I.ajaxSettings, $, {
                    contentType: !1,
                    processData: !1,
                    cache: !1,
                    type: M || "POST"
                });
                $.uploadProgress && (s.xhr = function() {
                    var e = I.ajaxSettings.xhr();
                    return e.upload && e.upload.addEventListener("progress", function(e) {
                        var t = 0,
                            n = e.loaded || e.position,
                            i = e.total;
                        e.lengthComputable && (t = Math.ceil(n / i * 100)), $.uploadProgress(e, n, i, t)
                    }, !1), e
                }), s.data = null;
                var r = s.beforeSend;
                return s.beforeSend = function(e, t) {
                    t.data = $.formData ? $.formData : n, r && r.call(this, e, t)
                }, I.ajax(s)
            }(o) : I.ajax($), A.removeData("jqxhr").data("jqxhr", g);
            for (var y = 0; y < O.length; y++) O[y] = null;
            return this.trigger("form-submit-notify", [this, $]), this
        }, I.fn.ajaxForm = function(e) {
            if ((e = e || {}).delegation = e.delegation && I.isFunction(I.fn.on), e.delegation || 0 !== this.length) return e.delegation ? (I(document).off("submit.form-plugin", this.selector, n).off("click.form-plugin", this.selector, i).on("submit.form-plugin", this.selector, e, n).on("click.form-plugin", this.selector, e, i), this) : this.ajaxFormUnbind().bind("submit.form-plugin", e, n).bind("click.form-plugin", e, i);
            var t = {
                s: this.selector,
                c: this.context
            };
            return !I.isReady && t.s ? (P("DOM not ready, queuing ajaxForm"), I(function() {
                I(t.s, t.c).ajaxForm(e)
            })) : P("terminating; zero elements found by selector" + (I.isReady ? "" : " (DOM not ready)")), this
        }, I.fn.ajaxFormUnbind = function() {
            return this.unbind("submit.form-plugin click.form-plugin")
        }, I.fn.formToArray = function(e, t) {
            var n = [];
            if (0 === this.length) return n;
            var i, s, r, o, a, l, c, d, u = this[0],
                h = this.attr("id"),
                p = e ? u.getElementsByTagName("*") : u.elements;
            if (p && !/MSIE [678]/.test(navigator.userAgent) && (p = I(p).get()), h && ((i = I(':input[form="' + h + '"]').get()).length && (p = (p || []).concat(i))), !p || !p.length) return n;
            for (s = 0, c = p.length; s < c; s++)
                if ((o = (l = p[s]).name) && !l.disabled)
                    if (e && u.clk && "image" == l.type) u.clk == l && (n.push({
                        name: o,
                        value: I(l).val(),
                        type: l.type
                    }), n.push({
                        name: o + ".x",
                        value: u.clk_x
                    }, {
                        name: o + ".y",
                        value: u.clk_y
                    }));
                    else if ((a = I.fieldValue(l, !0)) && a.constructor == Array)
                for (t && t.push(l), r = 0, d = a.length; r < d; r++) n.push({
                    name: o,
                    value: a[r]
                });
            else if (b.fileapi && "file" == l.type) {
                t && t.push(l);
                var f = l.files;
                if (f.length)
                    for (r = 0; r < f.length; r++) n.push({
                        name: o,
                        value: f[r],
                        type: l.type
                    });
                else n.push({
                    name: o,
                    value: "",
                    type: l.type
                })
            } else null != a && (t && t.push(l), n.push({
                name: o,
                value: a,
                type: l.type,
                required: l.required
            }));
            if (!e && u.clk) {
                var m = I(u.clk),
                    g = m[0];
                (o = g.name) && !g.disabled && "image" == g.type && (n.push({
                    name: o,
                    value: m.val()
                }), n.push({
                    name: o + ".x",
                    value: u.clk_x
                }, {
                    name: o + ".y",
                    value: u.clk_y
                }))
            }
            return n
        }, I.fn.formSerialize = function(e) {
            return I.param(this.formToArray(e))
        }, I.fn.fieldSerialize = function(s) {
            var r = [];
            return this.each(function() {
                var e = this.name;
                if (e) {
                    var t = I.fieldValue(this, s);
                    if (t && t.constructor == Array)
                        for (var n = 0, i = t.length; n < i; n++) r.push({
                            name: e,
                            value: t[n]
                        });
                    else null != t && r.push({
                        name: this.name,
                        value: t
                    })
                }
            }), I.param(r)
        }, I.fn.fieldValue = function(e) {
            for (var t = [], n = 0, i = this.length; n < i; n++) {
                var s = this[n],
                    r = I.fieldValue(s, e);
                null == r || r.constructor == Array && !r.length || (r.constructor == Array ? I.merge(t, r) : t.push(r))
            }
            return t
        }, I.fieldValue = function(e, t) {
            var n = e.name,
                i = e.type,
                s = e.tagName.toLowerCase();
            if (void 0 === t && (t = !0), t && (!n || e.disabled || "reset" == i || "button" == i || ("checkbox" == i || "radio" == i) && !e.checked || ("submit" == i || "image" == i) && e.form && e.form.clk != e || "select" == s && -1 == e.selectedIndex)) return null;
            if ("select" != s) return I(e).val();
            var r = e.selectedIndex;
            if (r < 0) return null;
            for (var o = [], a = e.options, l = "select-one" == i, c = l ? r + 1 : a.length, d = l ? r : 0; d < c; d++) {
                var u = a[d];
                if (u.selected) {
                    var h = u.value;
                    if (h = h || (u.attributes && u.attributes.value && !u.attributes.value.specified ? u.text : u.value), l) return h;
                    o.push(h)
                }
            }
            return o
        }, I.fn.clearForm = function(e) {
            return this.each(function() {
                I("input,select,textarea", this).clearFields(e)
            })
        }, I.fn.clearFields = I.fn.clearInputs = function(n) {
            var i = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
            return this.each(function() {
                var e = this.type,
                    t = this.tagName.toLowerCase();
                i.test(e) || "textarea" == t ? this.value = "" : "checkbox" == e || "radio" == e ? this.checked = !1 : "select" == t ? this.selectedIndex = -1 : "file" == e ? /MSIE/.test(navigator.userAgent) ? I(this).replaceWith(I(this).clone(!0)) : I(this).val("") : n && (!0 === n && /hidden/.test(e) || "string" == typeof n && I(this).is(n)) && (this.value = "")
            })
        }, I.fn.resetForm = function() {
            return this.each(function() {
                "function" != typeof this.reset && ("object" != typeof this.reset || this.reset.nodeType) || this.reset()
            })
        }, I.fn.enable = function(e) {
            return void 0 === e && (e = !0), this.each(function() {
                this.disabled = !e
            })
        }, I.fn.selected = function(n) {
            return void 0 === n && (n = !0), this.each(function() {
                var e = this.type;
                if ("checkbox" == e || "radio" == e) this.checked = n;
                else if ("option" == this.tagName.toLowerCase()) {
                    var t = I(this).parent("select");
                    n && t[0] && "select-one" == t[0].type && t.find("option").selected(!1), this.selected = n
                }
            })
        }, I.fn.ajaxSubmit.debug = !1
    }), function() {
        function e(e, t) {
            this.options = n.extend(!0, {}, this.Defaults, t), this.$element = n(e).addClass("rd-input-label"), this.$target = n("#" + this.$element.attr("for")), this.$win = n(s), this.$doc = n(i), this.initialize()
        }
        var n, i, s, r;
        n = window.jQuery, i = document, s = window, /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent), isWebkit = /safari|chrome/i.test(navigator.userAgent), e.prototype.Defaults = {
            callbacks: null
        }, e.prototype.initialize = function() {
            return this.$target.on("input", n.proxy(this.change, this)).on("focus", n.proxy(this.focus, this)).on("blur", n.proxy(this.blur, this)).on("hover", n.proxy(this.hover, this)).parents("form").on("reset", n.proxy(this.reset, this)), this.change(), this.hover(), this
        }, e.prototype.hover = function() {
            return isWebkit && (this.$target.is(":-webkit-autofill") ? this.$element.addClass("auto-fill") : this.$element.removeClass("auto-fill")), this
        }, e.prototype.change = function() {
            return isWebkit && (this.$target.is(":-webkit-autofill") ? this.$element.addClass("auto-fill") : this.$element.removeClass("auto-fill")), "" !== this.$target.val() ? (this.$element.hasClass("focus") || this.focus(), this.$element.addClass("not-empty")) : this.$element.removeClass("not-empty"), this
        }, e.prototype.focus = function() {
            return this.$element.addClass("focus"), this
        }, e.prototype.reset = function() {
            return setTimeout(n.proxy(this.blur, this)), this
        }, e.prototype.blur = function(e) {
            return "" === this.$target.val() && this.$element.removeClass("focus").removeClass("not-empty"), this
        }, r = e, n.fn.extend({
            RDInputLabel: function(t) {
                return this.each(function() {
                    var e;
                    return (e = n(this)).data("RDInputLabel") ? void 0 : e.data("RDInputLabel", new r(this, t))
                })
            }
        }), s.RDInputLabel = r, "undefined" != typeof module && null !== module ? module.exports = window.RDInputLabel : "function" == typeof define && define.amd && define(["jquery"], function() {
            "use strict";
            return window.RDInputLabel
        })
    }.call(this), function(e) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
    }(function(c) {
        "use strict";
        var s, r = window.Slick || {};
        s = 0, (r = function(e, t) {
            var n, i = this;
            i.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: c(e),
                appendDots: c(e),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(e, t) {
                    return c('<button type="button" data-role="none" role="button" tabindex="0" />').text(t + 1)
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            }, i.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            }, c.extend(i, i.initials), i.activeBreakpoint = null, i.animType = null, i.animProp = null, i.breakpoints = [], i.breakpointSettings = [], i.cssTransitions = !1, i.focussed = !1, i.interrupted = !1, i.hidden = "hidden", i.paused = !0, i.positionProp = null, i.respondTo = null, i.rowCount = 1, i.shouldClick = !0, i.$slider = c(e), i.$slidesCache = null, i.transformType = null, i.transitionType = null, i.visibilityChange = "visibilitychange", i.windowWidth = 0, i.windowTimer = null, n = c(e).data("slick") || {}, i.options = c.extend({}, i.defaults, t, n), i.currentSlide = i.options.initialSlide, i.originalSettings = i.options, void 0 !== document.mozHidden ? (i.hidden = "mozHidden", i.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (i.hidden = "webkitHidden", i.visibilityChange = "webkitvisibilitychange"), i.autoPlay = c.proxy(i.autoPlay, i), i.autoPlayClear = c.proxy(i.autoPlayClear, i), i.autoPlayIterator = c.proxy(i.autoPlayIterator, i), i.changeSlide = c.proxy(i.changeSlide, i), i.clickHandler = c.proxy(i.clickHandler, i), i.selectHandler = c.proxy(i.selectHandler, i), i.setPosition = c.proxy(i.setPosition, i), i.swipeHandler = c.proxy(i.swipeHandler, i), i.dragHandler = c.proxy(i.dragHandler, i), i.keyHandler = c.proxy(i.keyHandler, i), i.instanceUid = s++, i.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, i.registerBreakpoints(), i.init(!0)
        }).prototype.activateADA = function() {
            this.$slideTrack.find(".slick-active").attr({
                "aria-hidden": "false"
            }).find("a, input, button, select").attr({
                tabindex: "0"
            })
        }, r.prototype.addSlide = r.prototype.slickAdd = function(e, t, n) {
            var i = this;
            if ("boolean" == typeof t) n = t, t = null;
            else if (t < 0 || t >= i.slideCount) return !1;
            i.unload(), "number" == typeof t ? 0 === t && 0 === i.$slides.length ? c(e).appendTo(i.$slideTrack) : n ? c(e).insertBefore(i.$slides.eq(t)) : c(e).insertAfter(i.$slides.eq(t)) : !0 === n ? c(e).prependTo(i.$slideTrack) : c(e).appendTo(i.$slideTrack), i.$slides = i.$slideTrack.children(this.options.slide), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.append(i.$slides), i.$slides.each(function(e, t) {
                c(t).attr("data-slick-index", e)
            }), i.$slidesCache = i.$slides, i.reinit()
        }, r.prototype.animateHeight = function() {
            if (1 === this.options.slidesToShow && !0 === this.options.adaptiveHeight && !1 === this.options.vertical) {
                var e = this.$slides.eq(this.currentSlide).outerHeight(!0);
                this.$list.animate({
                    height: e
                }, this.options.speed)
            }
        }, r.prototype.animateSlide = function(e, t) {
            var n = {},
                i = this;
            i.animateHeight(), !0 === i.options.rtl && !1 === i.options.vertical && (e = -e), !1 === i.transformsEnabled ? !1 === i.options.vertical ? i.$slideTrack.animate({
                left: e
            }, i.options.speed, i.options.easing, t) : i.$slideTrack.animate({
                top: e
            }, i.options.speed, i.options.easing, t) : !1 === i.cssTransitions ? (!0 === i.options.rtl && (i.currentLeft = -i.currentLeft), c({
                animStart: i.currentLeft
            }).animate({
                animStart: e
            }, {
                duration: i.options.speed,
                easing: i.options.easing,
                step: function(e) {
                    e = Math.ceil(e), !1 === i.options.vertical ? n[i.animType] = "translate(" + e + "px, 0px)" : n[i.animType] = "translate(0px," + e + "px)", i.$slideTrack.css(n)
                },
                complete: function() {
                    t && t.call()
                }
            })) : (i.applyTransition(), e = Math.ceil(e), !1 === i.options.vertical ? n[i.animType] = "translate3d(" + e + "px, 0px, 0px)" : n[i.animType] = "translate3d(0px," + e + "px, 0px)", i.$slideTrack.css(n), t && setTimeout(function() {
                i.disableTransition(), t.call()
            }, i.options.speed))
        }, r.prototype.getNavTarget = function() {
            var e = this.options.asNavFor;
            return e && null !== e && (e = c(e).not(this.$slider)), e
        }, r.prototype.asNavFor = function(t) {
            var e = this.getNavTarget();
            null !== e && "object" == typeof e && e.each(function() {
                var e = c(this).slick("getSlick");
                e.unslicked || e.slideHandler(t, !0)
            })
        }, r.prototype.applyTransition = function(e) {
            var t = this,
                n = {};
            !1 === t.options.fade ? n[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : n[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, !1 === t.options.fade ? t.$slideTrack.css(n) : t.$slides.eq(e).css(n)
        }, r.prototype.autoPlay = function() {
            this.autoPlayClear(), this.slideCount > this.options.slidesToShow && (this.autoPlayTimer = setInterval(this.autoPlayIterator, this.options.autoplaySpeed))
        }, r.prototype.autoPlayClear = function() {
            this.autoPlayTimer && clearInterval(this.autoPlayTimer)
        }, r.prototype.autoPlayIterator = function() {
            var e = this,
                t = e.currentSlide + e.options.slidesToScroll;
            e.paused || e.interrupted || e.focussed || (!1 === e.options.infinite && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll, e.currentSlide - 1 == 0 && (e.direction = 1))), e.slideHandler(t))
        }, r.prototype.buildArrows = function() {
            var e = this;
            !0 === e.options.arrows && (e.$prevArrow = c(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = c(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
                "aria-disabled": "true",
                tabindex: "-1"
            }))
        }, r.prototype.buildDots = function() {
            var e, t, n = this;
            if (!0 === n.options.dots && n.slideCount > n.options.slidesToShow) {
                for (n.$slider.addClass("slick-dotted"), t = c("<ul />").addClass(n.options.dotsClass), e = 0; e <= n.getDotCount(); e += 1) t.append(c("<li />").append(n.options.customPaging.call(this, n, e)));
                n.$dots = t.appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
            }
        }, r.prototype.buildOut = function() {
            var e = this;
            e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function(e, t) {
                c(t).attr("data-slick-index", e).data("originalStyling", c(t).attr("style") || "")
            }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? c('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), c("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable")
        }, r.prototype.buildRows = function() {
            var e, t, n, i, s, r, o, a = this;
            if (i = document.createDocumentFragment(), r = a.$slider.children(), 1 < a.options.rows) {
                for (o = a.options.slidesPerRow * a.options.rows, s = Math.ceil(r.length / o), e = 0; e < s; e++) {
                    var l = document.createElement("div");
                    for (t = 0; t < a.options.rows; t++) {
                        var c = document.createElement("div");
                        for (n = 0; n < a.options.slidesPerRow; n++) {
                            var d = e * o + (t * a.options.slidesPerRow + n);
                            r.get(d) && c.appendChild(r.get(d))
                        }
                        l.appendChild(c)
                    }
                    i.appendChild(l)
                }
                a.$slider.empty().append(i), a.$slider.children().children().children().css({
                    width: 100 / a.options.slidesPerRow + "%",
                    display: "inline-block"
                })
            }
        }, r.prototype.checkResponsive = function(e, t) {
            var n, i, s, r = this,
                o = !1,
                a = r.$slider.width(),
                l = window.innerWidth || c(window).width();
            if ("window" === r.respondTo ? s = l : "slider" === r.respondTo ? s = a : "min" === r.respondTo && (s = Math.min(l, a)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
                for (n in i = null, r.breakpoints) r.breakpoints.hasOwnProperty(n) && (!1 === r.originalSettings.mobileFirst ? s < r.breakpoints[n] && (i = r.breakpoints[n]) : s > r.breakpoints[n] && (i = r.breakpoints[n]));
                null !== i ? null !== r.activeBreakpoint ? i === r.activeBreakpoint && !t || (r.activeBreakpoint = i, "unslick" === r.breakpointSettings[i] ? r.unslick(i) : (r.options = c.extend({}, r.originalSettings, r.breakpointSettings[i]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), o = i) : (r.activeBreakpoint = i, "unslick" === r.breakpointSettings[i] ? r.unslick(i) : (r.options = c.extend({}, r.originalSettings, r.breakpointSettings[i]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), o = i) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), o = i), e || !1 === o || r.$slider.trigger("breakpoint", [r, o])
            }
        }, r.prototype.changeSlide = function(e, t) {
            var n, i, s = this,
                r = c(e.currentTarget);
            switch (r.is("a") && e.preventDefault(), r.is("li") || (r = r.closest("li")), n = s.slideCount % s.options.slidesToScroll != 0 ? 0 : (s.slideCount - s.currentSlide) % s.options.slidesToScroll, e.data.message) {
                case "previous":
                    i = 0 == n ? s.options.slidesToScroll : s.options.slidesToShow - n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide - i, !1, t);
                    break;
                case "next":
                    i = 0 == n ? s.options.slidesToScroll : n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide + i, !1, t);
                    break;
                case "index":
                    var o = 0 === e.data.index ? 0 : e.data.index || r.index() * s.options.slidesToScroll;
                    s.slideHandler(s.checkNavigable(o), !1, t), r.children().trigger("focus");
                    break;
                default:
                    return
            }
        }, r.prototype.checkNavigable = function(e) {
            var t, n;
            if (n = 0, e > (t = this.getNavigableIndexes())[t.length - 1]) e = t[t.length - 1];
            else
                for (var i in t) {
                    if (e < t[i]) {
                        e = n;
                        break
                    }
                    n = t[i]
                }
            return e
        }, r.prototype.cleanUpEvents = function() {
            var e = this;
            e.options.dots && null !== e.$dots && c("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", c.proxy(e.interrupt, e, !0)).off("mouseleave.slick", c.proxy(e.interrupt, e, !1)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide)), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), c(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().off("click.slick", e.selectHandler), c(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), c(window).off("resize.slick.slick-" + e.instanceUid, e.resize), c("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), c(window).off("load.slick.slick-" + e.instanceUid, e.setPosition), c(document).off("ready.slick.slick-" + e.instanceUid, e.setPosition)
        }, r.prototype.cleanUpSlideEvents = function() {
            this.$list.off("mouseenter.slick", c.proxy(this.interrupt, this, !0)), this.$list.off("mouseleave.slick", c.proxy(this.interrupt, this, !1))
        }, r.prototype.cleanUpRows = function() {
            var e;
            1 < this.options.rows && ((e = this.$slides.children().children()).removeAttr("style"), this.$slider.empty().append(e))
        }, r.prototype.clickHandler = function(e) {
            !1 === this.shouldClick && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
        }, r.prototype.destroy = function(e) {
            var t = this;
            t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), c(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
                c(this).attr("style", c(this).data("originalStyling"))
            }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t])
        }, r.prototype.disableTransition = function(e) {
            var t = {};
            t[this.transitionType] = "", !1 === this.options.fade ? this.$slideTrack.css(t) : this.$slides.eq(e).css(t)
        }, r.prototype.fadeSlide = function(e, t) {
            var n = this;
            !1 === n.cssTransitions ? (n.$slides.eq(e).css({
                zIndex: n.options.zIndex
            }), n.$slides.eq(e).animate({
                opacity: 1
            }, n.options.speed, n.options.easing, t)) : (n.applyTransition(e), n.$slides.eq(e).css({
                opacity: 1,
                zIndex: n.options.zIndex
            }), t && setTimeout(function() {
                n.disableTransition(e), t.call()
            }, n.options.speed))
        }, r.prototype.fadeSlideOut = function(e) {
            !1 === this.cssTransitions ? this.$slides.eq(e).animate({
                opacity: 0,
                zIndex: this.options.zIndex - 2
            }, this.options.speed, this.options.easing) : (this.applyTransition(e), this.$slides.eq(e).css({
                opacity: 0,
                zIndex: this.options.zIndex - 2
            }))
        }, r.prototype.filterSlides = r.prototype.slickFilter = function(e) {
            null !== e && (this.$slidesCache = this.$slides, this.unload(), this.$slideTrack.children(this.options.slide).detach(), this.$slidesCache.filter(e).appendTo(this.$slideTrack), this.reinit())
        }, r.prototype.focusHandler = function() {
            var n = this;
            n.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(e) {
                e.stopImmediatePropagation();
                var t = c(this);
                setTimeout(function() {
                    n.options.pauseOnFocus && (n.focussed = t.is(":focus"), n.autoPlay())
                }, 0)
            })
        }, r.prototype.getCurrent = r.prototype.slickCurrentSlide = function() {
            return this.currentSlide
        }, r.prototype.getDotCount = function() {
            var e = this,
                t = 0,
                n = 0,
                i = 0;
            if (!0 === e.options.infinite)
                for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            else if (!0 === e.options.centerMode) i = e.slideCount;
            else if (e.options.asNavFor)
                for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            else i = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
            return i - 1
        }, r.prototype.getLeft = function(e) {
            var t, n, i, s = this,
                r = 0;
            return s.slideOffset = 0, n = s.$slides.first().outerHeight(!0), !0 === s.options.infinite ? (s.slideCount > s.options.slidesToShow && (s.slideOffset = s.slideWidth * s.options.slidesToShow * -1, r = n * s.options.slidesToShow * -1), s.slideCount % s.options.slidesToScroll != 0 && e + s.options.slidesToScroll > s.slideCount && s.slideCount > s.options.slidesToShow && (r = e > s.slideCount ? (s.slideOffset = (s.options.slidesToShow - (e - s.slideCount)) * s.slideWidth * -1, (s.options.slidesToShow - (e - s.slideCount)) * n * -1) : (s.slideOffset = s.slideCount % s.options.slidesToScroll * s.slideWidth * -1, s.slideCount % s.options.slidesToScroll * n * -1))) : e + s.options.slidesToShow > s.slideCount && (s.slideOffset = (e + s.options.slidesToShow - s.slideCount) * s.slideWidth, r = (e + s.options.slidesToShow - s.slideCount) * n), s.slideCount <= s.options.slidesToShow && (r = s.slideOffset = 0), !0 === s.options.centerMode && !0 === s.options.infinite ? s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2) - s.slideWidth : !0 === s.options.centerMode && (s.slideOffset = 0, s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2)), t = !1 === s.options.vertical ? e * s.slideWidth * -1 + s.slideOffset : e * n * -1 + r, !0 === s.options.variableWidth && (i = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(e) : s.$slideTrack.children(".slick-slide").eq(e + s.options.slidesToShow), t = !0 === s.options.rtl ? i[0] ? -1 * (s.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, !0 === s.options.centerMode && (i = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(e) : s.$slideTrack.children(".slick-slide").eq(e + s.options.slidesToShow + 1), t = !0 === s.options.rtl ? i[0] ? -1 * (s.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, t += (s.$list.width() - i.outerWidth()) / 2)), t
        }, r.prototype.getOption = r.prototype.slickGetOption = function(e) {
            return this.options[e]
        }, r.prototype.getNavigableIndexes = function() {
            var e, t = this,
                n = 0,
                i = 0,
                s = [];
            for (e = !1 === t.options.infinite ? t.slideCount : (n = -1 * t.options.slidesToScroll, i = -1 * t.options.slidesToScroll, 2 * t.slideCount); n < e;) s.push(n), n = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
            return s
        }, r.prototype.getSlick = function() {
            return this
        }, r.prototype.getSlideCount = function() {
            var n, i, s = this;
            return i = !0 === s.options.centerMode ? s.slideWidth * Math.floor(s.options.slidesToShow / 2) : 0, !0 === s.options.swipeToSlide ? (s.$slideTrack.find(".slick-slide").each(function(e, t) {
                return t.offsetLeft - i + c(t).outerWidth() / 2 > -1 * s.swipeLeft ? (n = t, !1) : void 0
            }), Math.abs(c(n).attr("data-slick-index") - s.currentSlide) || 1) : s.options.slidesToScroll
        }, r.prototype.goTo = r.prototype.slickGoTo = function(e, t) {
            this.changeSlide({
                data: {
                    message: "index",
                    index: parseInt(e)
                }
            }, t)
        }, r.prototype.init = function(e) {
            var t = this;
            c(t.$slider).hasClass("slick-initialized") || (c(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay())
        }, r.prototype.initADA = function() {
            var t = this;
            t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
                "aria-hidden": "true",
                tabindex: "-1"
            }).find("a, input, button, select").attr({
                tabindex: "-1"
            }), t.$slideTrack.attr("role", "listbox"), t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function(e) {
                c(this).attr({
                    role: "option",
                    "aria-describedby": "slick-slide" + t.instanceUid + e
                })
            }), null !== t.$dots && t.$dots.attr("role", "tablist").find("li").each(function(e) {
                c(this).attr({
                    role: "presentation",
                    "aria-selected": "false",
                    "aria-controls": "navigation" + t.instanceUid + e,
                    id: "slick-slide" + t.instanceUid + e
                })
            }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), t.activateADA()
        }, r.prototype.initArrowEvents = function() {
            !0 === this.options.arrows && this.slideCount > this.options.slidesToShow && (this.$prevArrow.off("click.slick").on("click.slick", {
                message: "previous"
            }, this.changeSlide), this.$nextArrow.off("click.slick").on("click.slick", {
                message: "next"
            }, this.changeSlide))
        }, r.prototype.initDotEvents = function() {
            var e = this;
            !0 === e.options.dots && e.slideCount > e.options.slidesToShow && c("li", e.$dots).on("click.slick", {
                message: "index"
            }, e.changeSlide), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && c("li", e.$dots).on("mouseenter.slick", c.proxy(e.interrupt, e, !0)).on("mouseleave.slick", c.proxy(e.interrupt, e, !1))
        }, r.prototype.initSlideEvents = function() {
            this.options.pauseOnHover && (this.$list.on("mouseenter.slick", c.proxy(this.interrupt, this, !0)), this.$list.on("mouseleave.slick", c.proxy(this.interrupt, this, !1)))
        }, r.prototype.initializeEvents = function() {
            var e = this;
            e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
                action: "start"
            }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
                action: "move"
            }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
                action: "end"
            }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
                action: "end"
            }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), c(document).on(e.visibilityChange, c.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().on("click.slick", e.selectHandler), c(window).on("orientationchange.slick.slick-" + e.instanceUid, c.proxy(e.orientationChange, e)), c(window).on("resize.slick.slick-" + e.instanceUid, c.proxy(e.resize, e)), c("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), c(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), c(document).on("ready.slick.slick-" + e.instanceUid, e.setPosition)
        }, r.prototype.initUI = function() {
            !0 === this.options.arrows && this.slideCount > this.options.slidesToShow && (this.$prevArrow.show(), this.$nextArrow.show()), !0 === this.options.dots && this.slideCount > this.options.slidesToShow && this.$dots.show()
        }, r.prototype.keyHandler = function(e) {
            e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === this.options.accessibility ? this.changeSlide({
                data: {
                    message: !0 === this.options.rtl ? "next" : "previous"
                }
            }) : 39 === e.keyCode && !0 === this.options.accessibility && this.changeSlide({
                data: {
                    message: !0 === this.options.rtl ? "previous" : "next"
                }
            }))
        }, r.prototype.lazyLoad = function() {
            function e(e) {
                c("img[data-lazy]", e).each(function() {
                    var e = c(this),
                        t = c(this).attr("data-lazy"),
                        n = document.createElement("img");
                    n.onload = function() {
                        e.animate({
                            opacity: 0
                        }, 100, function() {
                            e.attr("src", t).animate({
                                opacity: 1
                            }, 200, function() {
                                e.removeAttr("data-lazy").removeClass("slick-loading")
                            }), i.$slider.trigger("lazyLoaded", [i, e, t])
                        })
                    }, n.onerror = function() {
                        e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), i.$slider.trigger("lazyLoadError", [i, e, t])
                    }, n.src = t
                })
            }
            var t, n, i = this;
            !0 === i.options.centerMode ? n = !0 === i.options.infinite ? (t = i.currentSlide + (i.options.slidesToShow / 2 + 1)) + i.options.slidesToShow + 2 : (t = Math.max(0, i.currentSlide - (i.options.slidesToShow / 2 + 1)), i.options.slidesToShow / 2 + 1 + 2 + i.currentSlide) : (t = i.options.infinite ? i.options.slidesToShow + i.currentSlide : i.currentSlide, n = Math.ceil(t + i.options.slidesToShow), !0 === i.options.fade && (0 < t && t--, n <= i.slideCount && n++)), e(i.$slider.find(".slick-slide").slice(t, n)), i.slideCount <= i.options.slidesToShow ? e(i.$slider.find(".slick-slide")) : i.currentSlide >= i.slideCount - i.options.slidesToShow ? e(i.$slider.find(".slick-cloned").slice(0, i.options.slidesToShow)) : 0 === i.currentSlide && e(i.$slider.find(".slick-cloned").slice(-1 * i.options.slidesToShow))
        }, r.prototype.loadSlider = function() {
            this.setPosition(), this.$slideTrack.css({
                opacity: 1
            }), this.$slider.removeClass("slick-loading"), this.initUI(), "progressive" === this.options.lazyLoad && this.progressiveLazyLoad()
        }, r.prototype.next = r.prototype.slickNext = function() {
            this.changeSlide({
                data: {
                    message: "next"
                }
            })
        }, r.prototype.orientationChange = function() {
            this.checkResponsive(), this.setPosition()
        }, r.prototype.pause = r.prototype.slickPause = function() {
            this.autoPlayClear(), this.paused = !0
        }, r.prototype.play = r.prototype.slickPlay = function() {
            this.autoPlay(), this.options.autoplay = !0, this.paused = !1, this.focussed = !1, this.interrupted = !1
        }, r.prototype.postSlide = function(e) {
            var t = this;
            t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && t.initADA())
        }, r.prototype.prev = r.prototype.slickPrev = function() {
            this.changeSlide({
                data: {
                    message: "previous"
                }
            })
        }, r.prototype.preventDefault = function(e) {
            e.preventDefault()
        }, r.prototype.progressiveLazyLoad = function(e) {
            e = e || 1;
            var t, n, i, s = this,
                r = c("img[data-lazy]", s.$slider);
            r.length ? (t = r.first(), n = t.attr("data-lazy"), (i = document.createElement("img")).onload = function() {
                t.attr("src", n).removeAttr("data-lazy").removeClass("slick-loading"), !0 === s.options.adaptiveHeight && s.setPosition(), s.$slider.trigger("lazyLoaded", [s, t, n]), s.progressiveLazyLoad()
            }, i.onerror = function() {
                e < 3 ? setTimeout(function() {
                    s.progressiveLazyLoad(e + 1)
                }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), s.$slider.trigger("lazyLoadError", [s, t, n]), s.progressiveLazyLoad())
            }, i.src = n) : s.$slider.trigger("allImagesLoaded", [s])
        }, r.prototype.refresh = function(e) {
            var t, n, i = this;
            n = i.slideCount - i.options.slidesToShow, !i.options.infinite && i.currentSlide > n && (i.currentSlide = n), i.slideCount <= i.options.slidesToShow && (i.currentSlide = 0), t = i.currentSlide, i.destroy(!0), c.extend(i, i.initials, {
                currentSlide: t
            }), i.init(), e || i.changeSlide({
                data: {
                    message: "index",
                    index: t
                }
            }, !1)
        }, r.prototype.registerBreakpoints = function() {
            var e, t, n, i = this,
                s = i.options.responsive || null;
            if ("array" === c.type(s) && s.length) {
                for (e in i.respondTo = i.options.respondTo || "window", s)
                    if (n = i.breakpoints.length - 1, t = s[e].breakpoint, s.hasOwnProperty(e)) {
                        for (; 0 <= n;) i.breakpoints[n] && i.breakpoints[n] === t && i.breakpoints.splice(n, 1), n--;
                        i.breakpoints.push(t), i.breakpointSettings[t] = s[e].settings
                    } i.breakpoints.sort(function(e, t) {
                    return i.options.mobileFirst ? e - t : t - e
                })
            }
        }, r.prototype.reinit = function() {
            var e = this;
            e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
        }, r.prototype.resize = function() {
            var e = this;
            c(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function() {
                e.windowWidth = c(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
            }, 50))
        }, r.prototype.removeSlide = r.prototype.slickRemove = function(e, t, n) {
            var i = this;
            return e = "boolean" == typeof e ? !0 === (t = e) ? 0 : i.slideCount - 1 : !0 === t ? --e : e, !(i.slideCount < 1 || e < 0 || e > i.slideCount - 1) && (i.unload(), !0 === n ? i.$slideTrack.children().remove() : i.$slideTrack.children(this.options.slide).eq(e).remove(), i.$slides = i.$slideTrack.children(this.options.slide), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.append(i.$slides), i.$slidesCache = i.$slides, void i.reinit())
        }, r.prototype.setCSS = function(e) {
            var t, n, i = this,
                s = {};
            !0 === i.options.rtl && (e = -e), t = "left" == i.positionProp ? Math.ceil(e) + "px" : "0px", n = "top" == i.positionProp ? Math.ceil(e) + "px" : "0px", s[i.positionProp] = e, !1 === i.transformsEnabled || (!(s = {}) === i.cssTransitions ? s[i.animType] = "translate(" + t + ", " + n + ")" : s[i.animType] = "translate3d(" + t + ", " + n + ", 0px)"), i.$slideTrack.css(s)
        }, r.prototype.setDimensions = function() {
            var e = this;
            !1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({
                padding: "0px " + e.options.centerPadding
            }) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), !0 === e.options.centerMode && e.$list.css({
                padding: e.options.centerPadding + " 0px"
            })), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
            var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
            !1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
        }, r.prototype.setFade = function() {
            var n, i = this;
            i.$slides.each(function(e, t) {
                n = i.slideWidth * e * -1, !0 === i.options.rtl ? c(t).css({
                    position: "relative",
                    right: n,
                    top: 0,
                    zIndex: i.options.zIndex - 2,
                    opacity: 0
                }) : c(t).css({
                    position: "relative",
                    left: n,
                    top: 0,
                    zIndex: i.options.zIndex - 2,
                    opacity: 0
                })
            }), i.$slides.eq(i.currentSlide).css({
                zIndex: i.options.zIndex - 1,
                opacity: 1
            })
        }, r.prototype.setHeight = function() {
            if (1 === this.options.slidesToShow && !0 === this.options.adaptiveHeight && !1 === this.options.vertical) {
                var e = this.$slides.eq(this.currentSlide).outerHeight(!0);
                this.$list.css("height", e)
            }
        }, r.prototype.setOption = r.prototype.slickSetOption = function() {
            var e, t, n, i, s, r = this,
                o = !1;
            if ("object" === c.type(arguments[0]) ? (n = arguments[0], o = arguments[1], s = "multiple") : "string" === c.type(arguments[0]) && (i = arguments[1], o = arguments[2], "responsive" === (n = arguments[0]) && "array" === c.type(arguments[1]) ? s = "responsive" : void 0 !== arguments[1] && (s = "single")), "single" === s) r.options[n] = i;
            else if ("multiple" === s) c.each(n, function(e, t) {
                r.options[e] = t
            });
            else if ("responsive" === s)
                for (t in i)
                    if ("array" !== c.type(r.options.responsive)) r.options.responsive = [i[t]];
                    else {
                        for (e = r.options.responsive.length - 1; 0 <= e;) r.options.responsive[e].breakpoint === i[t].breakpoint && r.options.responsive.splice(e, 1), e--;
                        r.options.responsive.push(i[t])
                    } o && (r.unload(), r.reinit())
        }, r.prototype.setPosition = function() {
            this.setDimensions(), this.setHeight(), !1 === this.options.fade ? this.setCSS(this.getLeft(this.currentSlide)) : this.setFade(), this.$slider.trigger("setPosition", [this])
        }, r.prototype.setProps = function() {
            var e = this,
                t = document.body.style;
            e.positionProp = !0 === e.options.vertical ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || !0 !== e.options.useCSS || (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && !1 !== e.animType
        }, r.prototype.setSlideClasses = function(e) {
            var t, n, i, s, r = this;
            n = r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), r.$slides.eq(e).addClass("slick-current"), !0 === r.options.centerMode ? (t = Math.floor(r.options.slidesToShow / 2), !0 === r.options.infinite && (t <= e && e <= r.slideCount - 1 - t ? r.$slides.slice(e - t, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (i = r.options.slidesToShow + e, n.slice(i - t + 1, i + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? n.eq(n.length - 1 - r.options.slidesToShow).addClass("slick-center") : e === r.slideCount - 1 && n.eq(r.options.slidesToShow).addClass("slick-center")), r.$slides.eq(e).addClass("slick-center")) : 0 <= e && e <= r.slideCount - r.options.slidesToShow ? r.$slides.slice(e, e + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : n.length <= r.options.slidesToShow ? n.addClass("slick-active").attr("aria-hidden", "false") : (s = r.slideCount % r.options.slidesToShow, i = !0 === r.options.infinite ? r.options.slidesToShow + e : e, r.options.slidesToShow == r.options.slidesToScroll && r.slideCount - e < r.options.slidesToShow ? n.slice(i - (r.options.slidesToShow - s), i + s).addClass("slick-active").attr("aria-hidden", "false") : n.slice(i, i + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === r.options.lazyLoad && r.lazyLoad()
        }, r.prototype.setupInfinite = function() {
            var e, t, n, i = this;
            if (!0 === i.options.fade && (i.options.centerMode = !1), !0 === i.options.infinite && !1 === i.options.fade && (t = null, i.slideCount > i.options.slidesToShow)) {
                for (n = !0 === i.options.centerMode ? i.options.slidesToShow + 1 : i.options.slidesToShow, e = i.slideCount; e > i.slideCount - n; e -= 1) t = e - 1, c(i.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - i.slideCount).prependTo(i.$slideTrack).addClass("slick-cloned");
                for (e = 0; e < n; e += 1) t = e, c(i.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + i.slideCount).appendTo(i.$slideTrack).addClass("slick-cloned");
                i.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                    c(this).attr("id", "")
                })
            }
        }, r.prototype.interrupt = function(e) {
            e || this.autoPlay(), this.interrupted = e
        }, r.prototype.selectHandler = function(e) {
            var t = c(e.target).is(".slick-slide") ? c(e.target) : c(e.target).parents(".slick-slide"),
                n = parseInt(t.attr("data-slick-index"));
            return n = n || 0, this.slideCount <= this.options.slidesToShow ? (this.setSlideClasses(n), void this.asNavFor(n)) : void this.slideHandler(n)
        }, r.prototype.slideHandler = function(e, t, n) {
            var i, s, r, o, a, l = null,
                c = this;
            return t = t || !1, !0 === c.animating && !0 === c.options.waitForAnimate || !0 === c.options.fade && c.currentSlide === e || c.slideCount <= c.options.slidesToShow ? void 0 : (!1 === t && c.asNavFor(e), i = e, l = c.getLeft(i), o = c.getLeft(c.currentSlide), c.currentLeft = null === c.swipeLeft ? o : c.swipeLeft, !1 === c.options.infinite && !1 === c.options.centerMode && (e < 0 || e > c.getDotCount() * c.options.slidesToScroll) ? void(!1 === c.options.fade && (i = c.currentSlide, !0 !== n ? c.animateSlide(o, function() {
                c.postSlide(i)
            }) : c.postSlide(i))) : !1 === c.options.infinite && !0 === c.options.centerMode && (e < 0 || e > c.slideCount - c.options.slidesToScroll) ? void(!1 === c.options.fade && (i = c.currentSlide, !0 !== n ? c.animateSlide(o, function() {
                c.postSlide(i)
            }) : c.postSlide(i))) : (c.options.autoplay && clearInterval(c.autoPlayTimer), s = i < 0 ? c.slideCount % c.options.slidesToScroll != 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + i : i >= c.slideCount ? c.slideCount % c.options.slidesToScroll != 0 ? 0 : i - c.slideCount : i, c.animating = !0, c.$slider.trigger("beforeChange", [c, c.currentSlide, s]), r = c.currentSlide, c.currentSlide = s, c.setSlideClasses(c.currentSlide), c.options.asNavFor && ((a = (a = c.getNavTarget()).slick("getSlick")).slideCount <= a.options.slidesToShow && a.setSlideClasses(c.currentSlide)), c.updateDots(), c.updateArrows(), !0 === c.options.fade ? (!0 !== n ? (c.fadeSlideOut(r), c.fadeSlide(s, function() {
                c.postSlide(s)
            })) : c.postSlide(s), void c.animateHeight()) : void(!0 !== n ? c.animateSlide(l, function() {
                c.postSlide(s)
            }) : c.postSlide(s))))
        }, r.prototype.startLoad = function() {
            var e = this;
            !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
        }, r.prototype.swipeDirection = function() {
            var e, t, n, i;
            return e = this.touchObject.startX - this.touchObject.curX, t = this.touchObject.startY - this.touchObject.curY, n = Math.atan2(t, e), (i = Math.round(180 * n / Math.PI)) < 0 && (i = 360 - Math.abs(i)), i <= 45 && 0 <= i ? !1 === this.options.rtl ? "left" : "right" : i <= 360 && 315 <= i ? !1 === this.options.rtl ? "left" : "right" : 135 <= i && i <= 225 ? !1 === this.options.rtl ? "right" : "left" : !0 === this.options.verticalSwiping ? 35 <= i && i <= 135 ? "down" : "up" : "vertical"
        }, r.prototype.swipeEnd = function(e) {
            var t, n, i = this;
            if (i.dragging = !1, i.interrupted = !1, i.shouldClick = !(10 < i.touchObject.swipeLength), void 0 === i.touchObject.curX) return !1;
            if (!0 === i.touchObject.edgeHit && i.$slider.trigger("edge", [i, i.swipeDirection()]), i.touchObject.swipeLength >= i.touchObject.minSwipe) {
                switch (n = i.swipeDirection()) {
                    case "left":
                    case "down":
                        t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide + i.getSlideCount()) : i.currentSlide + i.getSlideCount(), i.currentDirection = 0;
                        break;
                    case "right":
                    case "up":
                        t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide - i.getSlideCount()) : i.currentSlide - i.getSlideCount(), i.currentDirection = 1
                }
                "vertical" != n && (i.slideHandler(t), i.touchObject = {}, i.$slider.trigger("swipe", [i, n]))
            } else i.touchObject.startX !== i.touchObject.curX && (i.slideHandler(i.currentSlide), i.touchObject = {})
        }, r.prototype.swipeHandler = function(e) {
            var t = this;
            if (!(!1 === t.options.swipe || "ontouchend" in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
                case "start":
                    t.swipeStart(e);
                    break;
                case "move":
                    t.swipeMove(e);
                    break;
                case "end":
                    t.swipeEnd(e)
            }
        }, r.prototype.swipeMove = function(e) {
            var t, n, i, s, r, o = this;
            return r = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!o.dragging || r && 1 !== r.length) && (t = o.getLeft(o.currentSlide), o.touchObject.curX = void 0 !== r ? r[0].pageX : e.clientX, o.touchObject.curY = void 0 !== r ? r[0].pageY : e.clientY, o.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(o.touchObject.curX - o.touchObject.startX, 2))), !0 === o.options.verticalSwiping && (o.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(o.touchObject.curY - o.touchObject.startY, 2)))), "vertical" !== (n = o.swipeDirection()) ? (void 0 !== e.originalEvent && 4 < o.touchObject.swipeLength && e.preventDefault(), s = (!1 === o.options.rtl ? 1 : -1) * (o.touchObject.curX > o.touchObject.startX ? 1 : -1), !0 === o.options.verticalSwiping && (s = o.touchObject.curY > o.touchObject.startY ? 1 : -1), i = o.touchObject.swipeLength, (o.touchObject.edgeHit = !1) === o.options.infinite && (0 === o.currentSlide && "right" === n || o.currentSlide >= o.getDotCount() && "left" === n) && (i = o.touchObject.swipeLength * o.options.edgeFriction, o.touchObject.edgeHit = !0), !1 === o.options.vertical ? o.swipeLeft = t + i * s : o.swipeLeft = t + i * (o.$list.height() / o.listWidth) * s, !0 === o.options.verticalSwiping && (o.swipeLeft = t + i * s), !0 !== o.options.fade && !1 !== o.options.touchMove && (!0 === o.animating ? (o.swipeLeft = null, !1) : void o.setCSS(o.swipeLeft))) : void 0)
        }, r.prototype.swipeStart = function(e) {
            var t, n = this;
            return n.interrupted = !0, 1 !== n.touchObject.fingerCount || n.slideCount <= n.options.slidesToShow ? !(n.touchObject = {}) : (void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), n.touchObject.startX = n.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, n.touchObject.startY = n.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, void(n.dragging = !0))
        }, r.prototype.unfilterSlides = r.prototype.slickUnfilter = function() {
            null !== this.$slidesCache && (this.unload(), this.$slideTrack.children(this.options.slide).detach(), this.$slidesCache.appendTo(this.$slideTrack), this.reinit())
        }, r.prototype.unload = function() {
            var e = this;
            c(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
        }, r.prototype.unslick = function(e) {
            this.$slider.trigger("unslick", [this, e]), this.destroy()
        }, r.prototype.updateArrows = function() {
            var e = this;
            Math.floor(e.options.slidesToShow / 2), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
        }, r.prototype.updateDots = function() {
            null !== this.$dots && (this.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), this.$dots.find("li").eq(Math.floor(this.currentSlide / this.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
        }, r.prototype.visibility = function() {
            this.options.autoplay && (document[this.hidden] ? this.interrupted = !0 : this.interrupted = !1)
        }, c.fn.slick = function() {
            var e, t, n = arguments[0],
                i = Array.prototype.slice.call(arguments, 1),
                s = this.length;
            for (e = 0; e < s; e++)
                if ("object" == typeof n || void 0 === n ? this[e].slick = new r(this[e], n) : t = this[e].slick[n].apply(this[e].slick, i), void 0 !== t) return t;
            return this
        }
    }), function() {
        var s, p, n, i, r;

        function e(e, t) {
            this.options = p.extend(!0, {}, this.Defaults, t), this.$element = p(e), this.$clone = null, this.$win = p(i), this.$doc = p(n), this.currentLayout = this.options.layout, this.loaded = !1, this.focusOnHover = this.options.focusOnHover, this.focusTimer = !1, this.cloneTimer = !1, this.isStuck = !1, this.initialize()
        }
        s = "ontouchstart" in window, p = window.jQuery, n = document, i = window, e.prototype.Defaults = {
            layout: "rd-navbar-static",
            deviceLayout: "rd-navbar-fixed",
            focusOnHover: !0,
            focusOnHoverTimeout: 800,
            linkedElements: ["html"],
            domAppend: !0,
            stickUp: !0,
            stickUpClone: !0,
            stickUpOffset: "100%",
            anchorNav: !0,
            anchorNavSpeed: 400,
            anchorNavOffset: 0,
            anchorNavEasing: "swing",
            autoHeight: !0,
            responsive: {
                0: {
                    layout: "rd-navbar-fixed",
                    deviceLayout: "rd-navbar-fixed",
                    focusOnHover: !1,
                    stickUp: !1
                },
                992: {
                    layout: "rd-navbar-static",
                    deviceLayout: "rd-navbar-static",
                    focusOnHover: !0,
                    stickUp: !0
                }
            },
            callbacks: {
                onToggleSwitch: !1,
                onToggleClose: !1,
                onDomAppend: !1,
                onDropdownOver: !1,
                onDropdownOut: !1,
                onDropdownToggle: !1,
                onDropdownClose: !1,
                onStuck: !1,
                onUnstuck: !1,
                onAnchorChange: !1
            }
        }, e.prototype.initialize = function() {
            var e;
            return (e = this).$element.addClass("rd-navbar").addClass(e.options.layout), s && e.$element.addClass("rd-navbar--is-touch"), e.options.domAppend && e.createNav(e), e.options.stickUpClone && e.createClone(e), e.$element.addClass("rd-navbar-original"), e.addAdditionalClassToToggles(".rd-navbar-original", "toggle-original", "toggle-original-elements"), e.applyHandlers(e), e.offset = e.$element.offset().top, e.height = e.$element.outerHeight(), e.loaded = !0, e
        }, e.prototype.resize = function(n, e) {
            var t, i;
            return i = s ? n.getOption("deviceLayout") : n.getOption("layout"), t = n.$element.add(n.$clone), i === n.currentLayout && n.loaded || (n.switchClass(t, n.currentLayout, i), null != n.options.linkedElements && p.grep(n.options.linkedElements, function(e, t) {
                return n.switchClass(e, n.currentLayout + "-linked", i + "-linked")
            }), n.currentLayout = i), n.focusOnHover = n.getOption("focusOnHover"), n
        }, e.prototype.stickUp = function(e, t) {
            function n() {
                "resize" === t.type ? e.switchClass(o, "", "rd-navbar--is-stuck") : o.addClass("rd-navbar--is-stuck"), e.isStuck = !0
            }
            var i, s, r, o, a;
            return s = e.getOption("stickUp"), (p("html").hasClass("ios") || e.$element.hasClass("rd-navbar-fixed")) && (s = !1), i = e.$doc.scrollTop(), o = null != e.$clone ? e.$clone : e.$element, a = "string" == typeof(r = e.getOption("stickUpOffset")) ? 0 < r.indexOf("%") ? parseFloat(r) * e.height / 100 : parseFloat(r) : r, s ? (a <= i && !e.isStuck || i < a && e.isStuck) && (e.$element.add(e.$clone).find("[data-rd-navbar-toggle]").each(function() {
                p.proxy(e.closeToggle, this)(e, !1)
            }).end().find(".rd-navbar-submenu").removeClass("opened").removeClass("focus"), a <= i && !e.isStuck && !e.$element.hasClass("rd-navbar-fixed") ? (e.options.callbacks.onStuck && e.options.callbacks.onStuck.call(e), navigator.platform.match(/(Mac)/i) ? setTimeout(n, 10) : n()) : ("resize" === t.type ? e.switchClass(o, "rd-navbar--is-stuck", "") : o.removeClass("rd-navbar--is-stuck").one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", p.proxy(e.resizeWrap, e, t)), e.isStuck = !1, e.options.callbacks.onUnstuck && e.options.callbacks.onUnstuck.call(e))) : (e.$element.find(".rd-navbar-submenu").removeClass("opened").removeClass("focus"), e.isStuck && (e.switchClass(o, "rd-navbar--is-stuck", ""), e.isStuck = !1, e.resizeWrap(t))), e
        }, e.prototype.resizeWrap = function(e) {
            var t;
            if (null == this.$clone && !this.isStuck) return t = this.$element.parent(), this.getOption("autoHeight") ? (this.height = this.$element.outerHeight(), "resize" === e.type ? (t.addClass("rd-navbar--no-transition").css("height", this.height), t[0].offsetHeight, t.removeClass("rd-navbar--no-transition")) : t.css("height", this.height)) : void t.css("height", "auto")
        }, e.prototype.createNav = function(e) {
            return e.$element.find(".rd-navbar-dropdown, .rd-navbar-megamenu").each(function() {
                var e;
                return e = p(this), this.getBoundingClientRect(), e.hasClass("rd-navbar-megamenu") ? e.parent().addClass("rd-navbar--has-megamenu") : e.parent().addClass("rd-navbar--has-dropdown")
            }).parents("li").addClass("rd-navbar-submenu"), p('<span class="rd-navbar-submenu-toggle"></span>').insertAfter(".rd-navbar-nav li.rd-navbar-submenu > a"), e.options.callbacks.onDomAppend && e.options.callbacks.onDomAppend.call(this), e
        }, e.prototype.createClone = function(e) {
            return e.$clone = e.$element.clone().insertAfter(e.$element).addClass("rd-navbar--is-clone"), e.addAdditionalClassToToggles(".rd-navbar--is-clone", "toggle-cloned", "toggle-cloned-elements"), e
        }, e.prototype.closeToggle = function(e, t) {
            var n, i, s, r, o, a, l;
            return i = p(t.target), o = !1, a = this.getAttribute("data-rd-navbar-toggle"), l = e.options.stickUpClone && e.isStuck ? (r = ".toggle-cloned", s = ".toggle-cloned-elements", !i.hasClass("toggle-cloned")) : (r = ".toggle-original", s = ".toggle-original-elements", !i.hasClass("toggle-original")), t.target !== this && !i.parents(r + "[data-rd-navbar-toggle]").length && !i.parents(s).length && a && l && ((n = p(this).parents("body").find(a).add(p(this).parents(".rd-navbar")[0])).each(function() {
                if (!o) return o = !0 === (t.target === this || p.contains(this, t.target))
            }), o || (n.add(this).removeClass("active"), e.options.callbacks.onToggleClose && e.options.callbacks.onToggleClose.call(this, e))), this
        }, e.prototype.switchToggle = function(e, t) {
            var n, i, s;
            return t.preventDefault(), n = p(this).hasClass("toggle-cloned") ? (s = ".rd-navbar--is-clone", ".toggle-cloned-elements") : (s = ".rd-navbar-original", ".toggle-original-elements"), (i = this.getAttribute("data-rd-navbar-toggle")) && (p(s + " [data-rd-navbar-toggle]").not(this).each(function() {
                var e;
                if (e = this.getAttribute("data-rd-navbar-toggle")) return p(this).parents("body").find(s + " " + e + n).add(this).add(-1 < p.inArray(".rd-navbar", e.split(/\s*,\s*/i)) && p(this).parents("body")[0]).removeClass("active")
            }), p(this).parents("body").find(s + " " + i + n).add(this).add(-1 < p.inArray(".rd-navbar", i.split(/\s*,\s*/i)) && p(this).parents(".rd-navbar")[0]).toggleClass("active")), e.options.callbacks.onToggleSwitch && e.options.callbacks.onToggleSwitch.call(this, e), this
        }, e.prototype.dropdownOver = function(e, t) {
            var n;
            if (e.focusOnHover) {
                if (n = p(this), clearTimeout(t), e.options.callbacks.onDropdownOver && !e.options.callbacks.onDropdownOver.call(this, e)) return this;
                n.addClass("focus").siblings().removeClass("opened").each(e.dropdownUnfocus)
            }
            return this
        }, e.prototype.dropdownTouch = function(e, t) {
            var n, i;
            if (n = p(this), clearTimeout(t), e.focusOnHover) {
                if (i = !1, n.hasClass("focus") && (i = !0), !i) return n.addClass("focus").siblings().removeClass("opened").each(e.dropdownUnfocus), !1;
                e.options.callbacks.onDropdownOver && e.options.callbacks.onDropdownOver.call(this, e)
            }
            return this
        }, e.prototype.dropdownOut = function(e, t) {
            return e.focusOnHover && (p(this).one("mouseenter.navbar", function() {
                return clearTimeout(t)
            }), e.options.callbacks.onDropdownOut && e.options.callbacks.onDropdownOut.call(this, e), clearTimeout(t), t = setTimeout(p.proxy(e.dropdownUnfocus, this, e), e.options.focusOnHoverTimeout)), this
        }, e.prototype.dropdownUnfocus = function(e) {
            return p(this).find("li.focus").add(this).removeClass("focus"), this
        }, e.prototype.dropdownClose = function(e, t) {
            return t.target === this || p(t.target).parents(".rd-navbar-submenu").length || (p(this).find("li.focus").add(this).removeClass("focus").removeClass("opened"), e.options.callbacks.onDropdownClose && e.options.callbacks.onDropdownClose.call(this, e)), this
        }, e.prototype.dropdownToggle = function(e) {
            return p(this).toggleClass("opened").siblings().removeClass("opened"), e.options.callbacks.onDropdownToggle && e.options.callbacks.onDropdownToggle.call(this, e), this
        }, e.prototype.goToAnchor = function(e, t) {
            var n, i;
            return i = this.hash, n = p(i), !!e.getOption("anchorNav") && (n.length && (t.preventDefault(), p("html, body").stop().animate({
                scrollTop: n.offset().top + e.getOption("anchorNavOffset") + 1
            }, e.getOption("anchorNavSpeed"), e.getOption("anchorNavEasing"), function() {
                return e.changeAnchor(i)
            })), this)
        }, e.prototype.activateAnchor = function(e) {
            var t, n, i, s, r, o, a, l, c, d, u, h;
            if (u = (s = this).$doc.scrollTop(), h = s.$win.height(), r = s.$doc.height(), d = s.getOption("anchorNavOffset"), !s.options.anchorNav) return !1;
            if (r - 50 < u + h) return (t = p('[data-type="anchor"]').last()).length && t.offset().top >= u && (o = "#" + t.attr("id"), (n = p('.rd-navbar-nav a[href^="' + o + '"]').parent()).hasClass("active") || (n.addClass("active").siblings().removeClass("active"), s.options.callbacks.onAnchorChange && s.options.callbacks.onAnchorChange.call(t[0], s))), t;
            for (a in c = p('.rd-navbar-nav a[href^="#"]').get()) l = c[a], o = (i = p(l)).attr("href"), (t = p(o)).length && t.offset().top + d <= u && t.offset().top + t.outerHeight() > u && (i.parent().addClass("active").siblings().removeClass("active"), s.options.callbacks.onAnchorChange && s.options.callbacks.onAnchorChange.call(t[0], s));
            return null
        }, e.prototype.getAnchor = function() {
            return history && history.state ? history.state.id : null
        }, e.prototype.changeAnchor = function(e) {
            return history && (history.state && history.state.id !== e ? history.replaceState({
                anchorId: e
            }, null, e) : history.pushState({
                anchorId: e
            }, null, e)), this
        }, e.prototype.applyHandlers = function(n) {
            return null != n.options.responsive && n.$win.on("resize.navbar", p.proxy(n.resize, n.$win[0], n)).on("resize.navbar", p.proxy(n.resizeWrap, n)).on("resize.navbar", p.proxy(n.stickUp, null != n.$clone ? n.$clone : n.$element, n)).on("orientationchange.navbar", p.proxy(n.resize, n.$win[0], n)).trigger("resize.navbar"), n.$doc.on("scroll.navbar", p.proxy(n.stickUp, null != n.$clone ? n.$clone : n.$element, n)).on("scroll.navbar", p.proxy(n.activateAnchor, n)), n.$element.add(n.$clone).find("[data-rd-navbar-toggle]").each(function() {
                var e;
                return (e = p(this)).on("click", p.proxy(n.switchToggle, this, n)), e.parents("body").on("click", p.proxy(n.closeToggle, this, n))
            }), n.$element.add(n.$clone).find(".rd-navbar-submenu").each(function() {
                var e, t;
                return t = (e = p(this)).parents(".rd-navbar--is-clone").length ? n.cloneTimer : n.focusTimer, e.on("mouseleave.navbar", p.proxy(n.dropdownOut, this, n, t)), e.find("> a").on("mouseenter.navbar", p.proxy(n.dropdownOver, this, n, t)), e.find("> a").on("touchstart.navbar", p.proxy(n.dropdownTouch, this, n, t)), e.find("> .rd-navbar-submenu-toggle").on("click", p.proxy(n.dropdownToggle, this, n)), e.parents("body").on("click", p.proxy(n.dropdownClose, this, n))
            }), n.$element.add(n.$clone).find('.rd-navbar-nav a[href^="#"]').each(function() {
                return p(this).on("click", p.proxy(n.goToAnchor, this, n))
            }), n.$element.find(".rd-navbar-dropdown, .rd-navbar-megamenu").each(function() {
                var e, t;
                e = p(this), (t = this.getBoundingClientRect()).left + e.outerWidth() >= i.innerWidth - 10 ? this.className += " rd-navbar-open-left" : t.left - e.outerWidth() <= 10 && (this.className += " rd-navbar-open-right")
            }), n
        }, e.prototype.switchClass = function(e, t, n) {
            var i;
            return (i = e instanceof jQuery ? e : p(e)).addClass("rd-navbar--no-transition").removeClass(t).addClass(n), i[0].offsetHeight, i.removeClass("rd-navbar--no-transition")
        }, e.prototype.getOption = function(e) {
            var t, n;
            for (t in this.options.responsive) t <= i.innerWidth && (n = t);
            return null != this.options.responsive && null != this.options.responsive[n][e] ? this.options.responsive[n][e] : this.options[e]
        }, e.prototype.addAdditionalClassToToggles = function(t, n, i) {
            return p(t).find("[data-rd-navbar-toggle]").each(function() {
                var e;
                return p(this).addClass(n), e = this.getAttribute("data-rd-navbar-toggle"), p(this).parents("body").find(t).find(e).addClass(i)
            })
        }, r = e, p.fn.extend({
            RDNavbar: function(e) {
                var t;
                if (!(t = p(this)).data("RDNavbar")) return t.data("RDNavbar", new r(this, e))
            }
        }), i.RDNavbar = r, "undefined" != typeof module && null !== module ? module.exports = window.RDNavbar : "function" == typeof define && define.amd && define(["jquery"], function() {
            "use strict";
            return window.RDNavbar
        })
    }.call(this), function(i) {
        i.fn.UItoTop = function(e) {
            var t = i.extend({
                    text: "",
                    min: 500,
                    scrollSpeed: 800,
                    containerID: "ui-to-top",
                    containerClass: "ui-to-top fa fa-angle-up",
                    easingType: "easeIn"
                }, e),
                n = "#" + t.containerID;
            i("body").append('<a href="#" id="' + t.containerID + '" class="' + t.containerClass + '" >' + t.text + "</a>"), i(n).click(function() {
                return i("html, body").stop().animate({
                    scrollTop: 0
                }, t.scrollSpeed, t.easingType), !1
            }), i(window).scroll(function() {
                var e = i(window).scrollTop();
                void 0 === document.body.style.maxHeight && i(n).css({
                    position: "absolute",
                    top: i(window).scrollTop() + i(window).height() - 50
                }), e > t.min ? i(n).stop(!0, !0).addClass("active") : i(n).removeClass("active")
            })
        }
    }(jQuery), function(h) {
        h.fn.parallax = function() {
            var u = h(window).width();
            return this.each(function(e) {
                function t(e) {
                    var t;
                    t = u < 601 ? 0 < d.height() ? d.height() : d.children("img").height() : 0 < d.height() ? d.height() : 500;
                    var n = d.children("img").first(),
                        i = n.height() - t,
                        s = d.offset().top + t,
                        r = d.offset().top,
                        o = h(window).scrollTop(),
                        a = window.innerHeight,
                        l = (o + a - r) / (t + a),
                        c = Math.round(i * l);
                    e && n.css("display", "block"), o < s && r < o + a && n.css("transform", "translate3D(-50%," + c + "px, 0)")
                }
                var d = h(this).prepend('<div class="material-parallax parallax"><img src="images/_blank.png" alt=""></div>').find(".material-parallax");
                d.children("img").first().attr("src", d.parents("[data-parallax-img]").data("parallax-img")), d.children("img").one("load", function() {
                    t(!0)
                }).each(function() {
                    this.complete && h(this).trigger("load")
                }), h(window).scroll(function() {
                    u = h(window).width(), t(!1)
                }), h(window).resize(function() {
                    u = h(window).width(), t(!1)
                })
            })
        }
    }(jQuery), function(e, t) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : (e = e || self).Swiper = t()
    }(this, function() {
        "use strict";
        var m = "undefined" == typeof document ? {
                body: {},
                addEventListener: function() {},
                removeEventListener: function() {},
                activeElement: {
                    blur: function() {},
                    nodeName: ""
                },
                querySelector: function() {
                    return null
                },
                querySelectorAll: function() {
                    return []
                },
                getElementById: function() {
                    return null
                },
                createEvent: function() {
                    return {
                        initEvent: function() {}
                    }
                },
                createElement: function() {
                    return {
                        children: [],
                        childNodes: [],
                        style: {},
                        setAttribute: function() {},
                        getElementsByTagName: function() {
                            return []
                        }
                    }
                },
                location: {
                    hash: ""
                }
            } : document,
            J = "undefined" == typeof window ? {
                document: m,
                navigator: {
                    userAgent: ""
                },
                location: {},
                history: {},
                CustomEvent: function() {
                    return this
                },
                addEventListener: function() {},
                removeEventListener: function() {},
                getComputedStyle: function() {
                    return {
                        getPropertyValue: function() {
                            return ""
                        }
                    }
                },
                Image: function() {},
                Date: function() {},
                screen: {},
                setTimeout: function() {},
                clearTimeout: function() {}
            } : window,
            l = function(e) {
                for (var t = 0; t < e.length; t += 1) this[t] = e[t];
                return this.length = e.length, this
            };

        function A(e, t) {
            var n = [],
                i = 0;
            if (e && !t && e instanceof l) return e;
            if (e)
                if ("string" == typeof e) {
                    var s, r, o = e.trim();
                    if (0 <= o.indexOf("<") && 0 <= o.indexOf(">")) {
                        var a = "div";
                        for (0 === o.indexOf("<li") && (a = "ul"), 0 === o.indexOf("<tr") && (a = "tbody"), 0 !== o.indexOf("<td") && 0 !== o.indexOf("<th") || (a = "tr"), 0 === o.indexOf("<tbody") && (a = "table"), 0 === o.indexOf("<option") && (a = "select"), (r = m.createElement(a)).innerHTML = o, i = 0; i < r.childNodes.length; i += 1) n.push(r.childNodes[i])
                    } else
                        for (s = t || "#" !== e[0] || e.match(/[ .<>:~]/) ? (t || m).querySelectorAll(e.trim()) : [m.getElementById(e.trim().split("#")[1])], i = 0; i < s.length; i += 1) s[i] && n.push(s[i])
                } else if (e.nodeType || e === J || e === m) n.push(e);
            else if (0 < e.length && e[0].nodeType)
                for (i = 0; i < e.length; i += 1) n.push(e[i]);
            return new l(n)
        }

        function r(e) {
            for (var t = [], n = 0; n < e.length; n += 1) - 1 === t.indexOf(e[n]) && t.push(e[n]);
            return t
        }
        A.fn = l.prototype, A.Class = l, A.Dom7 = l;
        var t = {
            addClass: function(e) {
                if (void 0 === e) return this;
                for (var t = e.split(" "), n = 0; n < t.length; n += 1)
                    for (var i = 0; i < this.length; i += 1) void 0 !== this[i] && void 0 !== this[i].classList && this[i].classList.add(t[n]);
                return this
            },
            removeClass: function(e) {
                for (var t = e.split(" "), n = 0; n < t.length; n += 1)
                    for (var i = 0; i < this.length; i += 1) void 0 !== this[i] && void 0 !== this[i].classList && this[i].classList.remove(t[n]);
                return this
            },
            hasClass: function(e) {
                return !!this[0] && this[0].classList.contains(e)
            },
            toggleClass: function(e) {
                for (var t = e.split(" "), n = 0; n < t.length; n += 1)
                    for (var i = 0; i < this.length; i += 1) void 0 !== this[i] && void 0 !== this[i].classList && this[i].classList.toggle(t[n]);
                return this
            },
            attr: function(e, t) {
                var n = arguments;
                if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;
                for (var i = 0; i < this.length; i += 1)
                    if (2 === n.length) this[i].setAttribute(e, t);
                    else
                        for (var s in e) this[i][s] = e[s], this[i].setAttribute(s, e[s]);
                return this
            },
            removeAttr: function(e) {
                for (var t = 0; t < this.length; t += 1) this[t].removeAttribute(e);
                return this
            },
            data: function(e, t) {
                var n;
                if (void 0 !== t) {
                    for (var i = 0; i < this.length; i += 1)(n = this[i]).dom7ElementDataStorage || (n.dom7ElementDataStorage = {}), n.dom7ElementDataStorage[e] = t;
                    return this
                }
                if (n = this[0]) return n.dom7ElementDataStorage && e in n.dom7ElementDataStorage ? n.dom7ElementDataStorage[e] : n.getAttribute("data-" + e) || void 0
            },
            transform: function(e) {
                for (var t = 0; t < this.length; t += 1) {
                    var n = this[t].style;
                    n.webkitTransform = e, n.transform = e
                }
                return this
            },
            transition: function(e) {
                "string" != typeof e && (e += "ms");
                for (var t = 0; t < this.length; t += 1) {
                    var n = this[t].style;
                    n.webkitTransitionDuration = e, n.transitionDuration = e
                }
                return this
            },
            on: function() {
                for (var e, t = [], n = arguments.length; n--;) t[n] = arguments[n];
                var i = t[0],
                    r = t[1],
                    o = t[2],
                    s = t[3];

                function a(e) {
                    var t = e.target;
                    if (t) {
                        var n = e.target.dom7EventData || [];
                        if (n.indexOf(e) < 0 && n.unshift(e), A(t).is(r)) o.apply(t, n);
                        else
                            for (var i = A(t).parents(), s = 0; s < i.length; s += 1) A(i[s]).is(r) && o.apply(i[s], n)
                    }
                }

                function l(e) {
                    var t = e && e.target && e.target.dom7EventData || [];
                    t.indexOf(e) < 0 && t.unshift(e), o.apply(this, t)
                }
                "function" == typeof t[1] && (i = (e = t)[0], o = e[1], s = e[2], r = void 0), s = s || !1;
                for (var c, d = i.split(" "), u = 0; u < this.length; u += 1) {
                    var h = this[u];
                    if (r)
                        for (c = 0; c < d.length; c += 1) {
                            var p = d[c];
                            h.dom7LiveListeners || (h.dom7LiveListeners = {}), h.dom7LiveListeners[p] || (h.dom7LiveListeners[p] = []), h.dom7LiveListeners[p].push({
                                listener: o,
                                proxyListener: a
                            }), h.addEventListener(p, a, s)
                        } else
                            for (c = 0; c < d.length; c += 1) {
                                var f = d[c];
                                h.dom7Listeners || (h.dom7Listeners = {}), h.dom7Listeners[f] || (h.dom7Listeners[f] = []), h.dom7Listeners[f].push({
                                    listener: o,
                                    proxyListener: l
                                }), h.addEventListener(f, l, s)
                            }
                }
                return this
            },
            off: function() {
                for (var e, t = [], n = arguments.length; n--;) t[n] = arguments[n];
                var i = t[0],
                    s = t[1],
                    r = t[2],
                    o = t[3];
                "function" == typeof t[1] && (i = (e = t)[0], r = e[1], o = e[2], s = void 0), o = o || !1;
                for (var a = i.split(" "), l = 0; l < a.length; l += 1)
                    for (var c = a[l], d = 0; d < this.length; d += 1) {
                        var u = this[d],
                            h = void 0;
                        if (!s && u.dom7Listeners ? h = u.dom7Listeners[c] : s && u.dom7LiveListeners && (h = u.dom7LiveListeners[c]), h && h.length)
                            for (var p = h.length - 1; 0 <= p; p -= 1) {
                                var f = h[p];
                                r && f.listener === r ? (u.removeEventListener(c, f.proxyListener, o), h.splice(p, 1)) : r && f.listener && f.listener.dom7proxy && f.listener.dom7proxy === r ? (u.removeEventListener(c, f.proxyListener, o), h.splice(p, 1)) : r || (u.removeEventListener(c, f.proxyListener, o), h.splice(p, 1))
                            }
                    }
                return this
            },
            trigger: function() {
                for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                for (var n = e[0].split(" "), i = e[1], s = 0; s < n.length; s += 1)
                    for (var r = n[s], o = 0; o < this.length; o += 1) {
                        var a = this[o],
                            l = void 0;
                        try {
                            l = new J.CustomEvent(r, {
                                detail: i,
                                bubbles: !0,
                                cancelable: !0
                            })
                        } catch (e) {
                            (l = m.createEvent("Event")).initEvent(r, !0, !0), l.detail = i
                        }
                        a.dom7EventData = e.filter(function(e, t) {
                            return 0 < t
                        }), a.dispatchEvent(l), a.dom7EventData = [], delete a.dom7EventData
                    }
                return this
            },
            transitionEnd: function(t) {
                var n, i = ["webkitTransitionEnd", "transitionend"],
                    s = this;

                function r(e) {
                    if (e.target === this)
                        for (t.call(this, e), n = 0; n < i.length; n += 1) s.off(i[n], r)
                }
                if (t)
                    for (n = 0; n < i.length; n += 1) s.on(i[n], r);
                return this
            },
            outerWidth: function(e) {
                if (0 < this.length) {
                    if (e) {
                        var t = this.styles();
                        return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"))
                    }
                    return this[0].offsetWidth
                }
                return null
            },
            outerHeight: function(e) {
                if (0 < this.length) {
                    if (e) {
                        var t = this.styles();
                        return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"))
                    }
                    return this[0].offsetHeight
                }
                return null
            },
            offset: function() {
                if (0 < this.length) {
                    var e = this[0],
                        t = e.getBoundingClientRect(),
                        n = m.body,
                        i = e.clientTop || n.clientTop || 0,
                        s = e.clientLeft || n.clientLeft || 0,
                        r = e === J ? J.scrollY : e.scrollTop,
                        o = e === J ? J.scrollX : e.scrollLeft;
                    return {
                        top: t.top + r - i,
                        left: t.left + o - s
                    }
                }
                return null
            },
            css: function(e, t) {
                var n;
                if (1 === arguments.length) {
                    if ("string" != typeof e) {
                        for (n = 0; n < this.length; n += 1)
                            for (var i in e) this[n].style[i] = e[i];
                        return this
                    }
                    if (this[0]) return J.getComputedStyle(this[0], null).getPropertyValue(e)
                }
                if (2 !== arguments.length || "string" != typeof e) return this;
                for (n = 0; n < this.length; n += 1) this[n].style[e] = t;
                return this
            },
            each: function(e) {
                if (!e) return this;
                for (var t = 0; t < this.length; t += 1)
                    if (!1 === e.call(this[t], t, this[t])) return this;
                return this
            },
            html: function(e) {
                if (void 0 === e) return this[0] ? this[0].innerHTML : void 0;
                for (var t = 0; t < this.length; t += 1) this[t].innerHTML = e;
                return this
            },
            text: function(e) {
                if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;
                for (var t = 0; t < this.length; t += 1) this[t].textContent = e;
                return this
            },
            is: function(e) {
                var t, n, i = this[0];
                if (!i || void 0 === e) return !1;
                if ("string" == typeof e) {
                    if (i.matches) return i.matches(e);
                    if (i.webkitMatchesSelector) return i.webkitMatchesSelector(e);
                    if (i.msMatchesSelector) return i.msMatchesSelector(e);
                    for (t = A(e), n = 0; n < t.length; n += 1)
                        if (t[n] === i) return !0;
                    return !1
                }
                if (e === m) return i === m;
                if (e === J) return i === J;
                if (e.nodeType || e instanceof l) {
                    for (t = e.nodeType ? [e] : e, n = 0; n < t.length; n += 1)
                        if (t[n] === i) return !0;
                    return !1
                }
                return !1
            },
            index: function() {
                var e, t = this[0];
                if (t) {
                    for (e = 0; null !== (t = t.previousSibling);) 1 === t.nodeType && (e += 1);
                    return e
                }
            },
            eq: function(e) {
                if (void 0 === e) return this;
                var t, n = this.length;
                return new l(n - 1 < e ? [] : e < 0 ? (t = n + e) < 0 ? [] : [this[t]] : [this[e]])
            },
            append: function() {
                for (var e, t = [], n = arguments.length; n--;) t[n] = arguments[n];
                for (var i = 0; i < t.length; i += 1) {
                    e = t[i];
                    for (var s = 0; s < this.length; s += 1)
                        if ("string" == typeof e) {
                            var r = m.createElement("div");
                            for (r.innerHTML = e; r.firstChild;) this[s].appendChild(r.firstChild)
                        } else if (e instanceof l)
                        for (var o = 0; o < e.length; o += 1) this[s].appendChild(e[o]);
                    else this[s].appendChild(e)
                }
                return this
            },
            prepend: function(e) {
                var t, n;
                for (t = 0; t < this.length; t += 1)
                    if ("string" == typeof e) {
                        var i = m.createElement("div");
                        for (i.innerHTML = e, n = i.childNodes.length - 1; 0 <= n; n -= 1) this[t].insertBefore(i.childNodes[n], this[t].childNodes[0])
                    } else if (e instanceof l)
                    for (n = 0; n < e.length; n += 1) this[t].insertBefore(e[n], this[t].childNodes[0]);
                else this[t].insertBefore(e, this[t].childNodes[0]);
                return this
            },
            next: function(e) {
                return 0 < this.length ? e ? this[0].nextElementSibling && A(this[0].nextElementSibling).is(e) ? new l([this[0].nextElementSibling]) : new l([]) : this[0].nextElementSibling ? new l([this[0].nextElementSibling]) : new l([]) : new l([])
            },
            nextAll: function(e) {
                var t = [],
                    n = this[0];
                if (!n) return new l([]);
                for (; n.nextElementSibling;) {
                    var i = n.nextElementSibling;
                    e ? A(i).is(e) && t.push(i) : t.push(i), n = i
                }
                return new l(t)
            },
            prev: function(e) {
                if (0 < this.length) {
                    var t = this[0];
                    return e ? t.previousElementSibling && A(t.previousElementSibling).is(e) ? new l([t.previousElementSibling]) : new l([]) : t.previousElementSibling ? new l([t.previousElementSibling]) : new l([])
                }
                return new l([])
            },
            prevAll: function(e) {
                var t = [],
                    n = this[0];
                if (!n) return new l([]);
                for (; n.previousElementSibling;) {
                    var i = n.previousElementSibling;
                    e ? A(i).is(e) && t.push(i) : t.push(i), n = i
                }
                return new l(t)
            },
            parent: function(e) {
                for (var t = [], n = 0; n < this.length; n += 1) null !== this[n].parentNode && (e ? A(this[n].parentNode).is(e) && t.push(this[n].parentNode) : t.push(this[n].parentNode));
                return A(r(t))
            },
            parents: function(e) {
                for (var t = [], n = 0; n < this.length; n += 1)
                    for (var i = this[n].parentNode; i;) e ? A(i).is(e) && t.push(i) : t.push(i), i = i.parentNode;
                return A(r(t))
            },
            closest: function(e) {
                var t = this;
                return void 0 === e ? new l([]) : (t.is(e) || (t = t.parents(e).eq(0)), t)
            },
            find: function(e) {
                for (var t = [], n = 0; n < this.length; n += 1)
                    for (var i = this[n].querySelectorAll(e), s = 0; s < i.length; s += 1) t.push(i[s]);
                return new l(t)
            },
            children: function(e) {
                for (var t = [], n = 0; n < this.length; n += 1)
                    for (var i = this[n].childNodes, s = 0; s < i.length; s += 1) e ? 1 === i[s].nodeType && A(i[s]).is(e) && t.push(i[s]) : 1 === i[s].nodeType && t.push(i[s]);
                return new l(r(t))
            },
            remove: function() {
                for (var e = 0; e < this.length; e += 1) this[e].parentNode && this[e].parentNode.removeChild(this[e]);
                return this
            },
            add: function() {
                for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                var n, i;
                for (n = 0; n < e.length; n += 1) {
                    var s = A(e[n]);
                    for (i = 0; i < s.length; i += 1) this[this.length] = s[i], this.length += 1
                }
                return this
            },
            styles: function() {
                return this[0] ? J.getComputedStyle(this[0], null) : {}
            }
        };
        Object.keys(t).forEach(function(e) {
            A.fn[e] = t[e]
        });

        function e(e) {
            void 0 === e && (e = {});
            var t = this;
            t.params = e, t.eventsListeners = {}, t.params && t.params.on && Object.keys(t.params.on).forEach(function(e) {
                t.on(e, t.params.on[e])
            })
        }
        var n, i, s, o, ee = {
                deleteProps: function(e) {
                    var t = e;
                    Object.keys(t).forEach(function(e) {
                        try {
                            t[e] = null
                        } catch (e) {}
                        try {
                            delete t[e]
                        } catch (e) {}
                    })
                },
                nextTick: function(e, t) {
                    return void 0 === t && (t = 0), setTimeout(e, t)
                },
                now: function() {
                    return Date.now()
                },
                getTranslate: function(e, t) {
                    var n, i, s;
                    void 0 === t && (t = "x");
                    var r = J.getComputedStyle(e, null);
                    return J.WebKitCSSMatrix ? (6 < (i = r.transform || r.webkitTransform).split(",").length && (i = i.split(", ").map(function(e) {
                        return e.replace(",", ".")
                    }).join(", ")), s = new J.WebKitCSSMatrix("none" === i ? "" : i)) : n = (s = r.MozTransform || r.OTransform || r.MsTransform || r.msTransform || r.transform || r.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === t && (i = J.WebKitCSSMatrix ? s.m41 : 16 === n.length ? parseFloat(n[12]) : parseFloat(n[4])), "y" === t && (i = J.WebKitCSSMatrix ? s.m42 : 16 === n.length ? parseFloat(n[13]) : parseFloat(n[5])), i || 0
                },
                parseUrlQuery: function(e) {
                    var t, n, i, s, r = {},
                        o = e || J.location.href;
                    if ("string" == typeof o && o.length)
                        for (s = (n = (o = -1 < o.indexOf("?") ? o.replace(/\S*\?/, "") : "").split("&").filter(function(e) {
                                return "" !== e
                            })).length, t = 0; t < s; t += 1) i = n[t].replace(/#\S+/g, "").split("="), r[decodeURIComponent(i[0])] = void 0 === i[1] ? void 0 : decodeURIComponent(i[1]) || "";
                    return r
                },
                isObject: function(e) {
                    return "object" == typeof e && null !== e && e.constructor && e.constructor === Object
                },
                extend: function() {
                    for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                    for (var n = Object(e[0]), i = 1; i < e.length; i += 1) {
                        var s = e[i];
                        if (null != s)
                            for (var r = Object.keys(Object(s)), o = 0, a = r.length; o < a; o += 1) {
                                var l = r[o],
                                    c = Object.getOwnPropertyDescriptor(s, l);
                                void 0 !== c && c.enumerable && (ee.isObject(n[l]) && ee.isObject(s[l]) ? ee.extend(n[l], s[l]) : !ee.isObject(n[l]) && ee.isObject(s[l]) ? (n[l] = {}, ee.extend(n[l], s[l])) : n[l] = s[l])
                            }
                    }
                    return n
                }
            },
            te = (s = m.createElement("div"), {
                touch: J.Modernizr && !0 === J.Modernizr.touch || !!(0 < J.navigator.maxTouchPoints || "ontouchstart" in J || J.DocumentTouch && m instanceof J.DocumentTouch),
                pointerEvents: !!(J.navigator.pointerEnabled || J.PointerEvent || "maxTouchPoints" in J.navigator && 0 < J.navigator.maxTouchPoints),
                prefixedPointerEvents: !!J.navigator.msPointerEnabled,
                transition: (i = s.style, "transition" in i || "webkitTransition" in i || "MozTransition" in i),
                transforms3d: J.Modernizr && !0 === J.Modernizr.csstransforms3d || (n = s.style, "webkitPerspective" in n || "MozPerspective" in n || "OPerspective" in n || "MsPerspective" in n || "perspective" in n),
                flexbox: function() {
                    for (var e = s.style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), n = 0; n < t.length; n += 1)
                        if (t[n] in e) return !0;
                    return !1
                }(),
                observer: "MutationObserver" in J || "WebkitMutationObserver" in J,
                passiveListener: function() {
                    var e = !1;
                    try {
                        var t = Object.defineProperty({}, "passive", {
                            get: function() {
                                e = !0
                            }
                        });
                        J.addEventListener("testPassiveListener", null, t)
                    } catch (e) {}
                    return e
                }(),
                gestures: "ongesturestart" in J
            }),
            O = {
                isIE: !!J.navigator.userAgent.match(/Trident/g) || !!J.navigator.userAgent.match(/MSIE/g),
                isEdge: !!J.navigator.userAgent.match(/Edge/g),
                isSafari: (o = J.navigator.userAgent.toLowerCase(), 0 <= o.indexOf("safari") && o.indexOf("chrome") < 0 && o.indexOf("android") < 0),
                isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(J.navigator.userAgent)
            },
            a = {
                components: {
                    configurable: !0
                }
            };
        e.prototype.on = function(e, t, n) {
            var i = this;
            if ("function" != typeof t) return i;
            var s = n ? "unshift" : "push";
            return e.split(" ").forEach(function(e) {
                i.eventsListeners[e] || (i.eventsListeners[e] = []), i.eventsListeners[e][s](t)
            }), i
        }, e.prototype.once = function(n, i, e) {
            var s = this;
            if ("function" != typeof i) return s;

            function r() {
                for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                i.apply(s, e), s.off(n, r), r.f7proxy && delete r.f7proxy
            }
            return r.f7proxy = i, s.on(n, r, e)
        }, e.prototype.off = function(e, i) {
            var s = this;
            return s.eventsListeners && e.split(" ").forEach(function(n) {
                void 0 === i ? s.eventsListeners[n] = [] : s.eventsListeners[n] && s.eventsListeners[n].length && s.eventsListeners[n].forEach(function(e, t) {
                    (e === i || e.f7proxy && e.f7proxy === i) && s.eventsListeners[n].splice(t, 1)
                })
            }), s
        }, e.prototype.emit = function() {
            for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
            var n, i, s, r = this;
            return r.eventsListeners && (s = "string" == typeof e[0] || Array.isArray(e[0]) ? (n = e[0], i = e.slice(1, e.length), r) : (n = e[0].events, i = e[0].data, e[0].context || r), (Array.isArray(n) ? n : n.split(" ")).forEach(function(e) {
                if (r.eventsListeners && r.eventsListeners[e]) {
                    var t = [];
                    r.eventsListeners[e].forEach(function(e) {
                        t.push(e)
                    }), t.forEach(function(e) {
                        e.apply(s, i)
                    })
                }
            })), r
        }, e.prototype.useModulesParams = function(n) {
            var i = this;
            i.modules && Object.keys(i.modules).forEach(function(e) {
                var t = i.modules[e];
                t.params && ee.extend(n, t.params)
            })
        }, e.prototype.useModules = function(i) {
            void 0 === i && (i = {});
            var s = this;
            s.modules && Object.keys(s.modules).forEach(function(e) {
                var n = s.modules[e],
                    t = i[e] || {};
                n.instance && Object.keys(n.instance).forEach(function(e) {
                    var t = n.instance[e];
                    s[e] = "function" == typeof t ? t.bind(s) : t
                }), n.on && s.on && Object.keys(n.on).forEach(function(e) {
                    s.on(e, n.on[e])
                }), n.create && n.create.bind(s)(t)
            })
        }, a.components.set = function(e) {
            this.use && this.use(e)
        }, e.installModule = function(t) {
            for (var e = [], n = arguments.length - 1; 0 < n--;) e[n] = arguments[n + 1];
            var i = this;
            i.prototype.modules || (i.prototype.modules = {});
            var s = t.name || Object.keys(i.prototype.modules).length + "_" + ee.now();
            return (i.prototype.modules[s] = t).proto && Object.keys(t.proto).forEach(function(e) {
                i.prototype[e] = t.proto[e]
            }), t.static && Object.keys(t.static).forEach(function(e) {
                i[e] = t.static[e]
            }), t.install && t.install.apply(i, e), i
        }, e.use = function(e) {
            for (var t = [], n = arguments.length - 1; 0 < n--;) t[n] = arguments[n + 1];
            var i = this;
            return Array.isArray(e) ? (e.forEach(function(e) {
                return i.installModule(e)
            }), i) : i.installModule.apply(i, [e].concat(t))
        }, Object.defineProperties(e, a);
        var c = {
                updateSize: function() {
                    var e, t, n = this.$el;
                    e = void 0 !== this.params.width ? this.params.width : n[0].clientWidth, t = void 0 !== this.params.height ? this.params.height : n[0].clientHeight, 0 === e && this.isHorizontal() || 0 === t && this.isVertical() || (e = e - parseInt(n.css("padding-left"), 10) - parseInt(n.css("padding-right"), 10), t = t - parseInt(n.css("padding-top"), 10) - parseInt(n.css("padding-bottom"), 10), ee.extend(this, {
                        width: e,
                        height: t,
                        size: this.isHorizontal() ? e : t
                    }))
                },
                updateSlides: function() {
                    var e = this,
                        t = e.params,
                        n = e.$wrapperEl,
                        i = e.size,
                        s = e.rtlTranslate,
                        r = e.wrongRTL,
                        o = e.virtual && t.virtual.enabled,
                        a = o ? e.virtual.slides.length : e.slides.length,
                        l = n.children("." + e.params.slideClass),
                        c = o ? e.virtual.slides.length : l.length,
                        d = [],
                        u = [],
                        h = [],
                        p = t.slidesOffsetBefore;
                    "function" == typeof p && (p = t.slidesOffsetBefore.call(e));
                    var f = t.slidesOffsetAfter;
                    "function" == typeof f && (f = t.slidesOffsetAfter.call(e));
                    var m = e.snapGrid.length,
                        g = e.snapGrid.length,
                        v = t.spaceBetween,
                        y = -p,
                        b = 0,
                        w = 0;
                    if (void 0 !== i) {
                        var x, T;
                        "string" == typeof v && 0 <= v.indexOf("%") && (v = parseFloat(v.replace("%", "")) / 100 * i), e.virtualSize = -v, s ? l.css({
                            marginLeft: "",
                            marginTop: ""
                        }) : l.css({
                            marginRight: "",
                            marginBottom: ""
                        }), 1 < t.slidesPerColumn && (x = Math.floor(c / t.slidesPerColumn) === c / e.params.slidesPerColumn ? c : Math.ceil(c / t.slidesPerColumn) * t.slidesPerColumn, "auto" !== t.slidesPerView && "row" === t.slidesPerColumnFill && (x = Math.max(x, t.slidesPerView * t.slidesPerColumn)));
                        for (var C, S = t.slidesPerColumn, E = x / S, _ = Math.floor(c / t.slidesPerColumn), k = 0; k < c; k += 1) {
                            T = 0;
                            var D = l.eq(k);
                            if (1 < t.slidesPerColumn) {
                                var $ = void 0,
                                    M = void 0,
                                    A = void 0;
                                "column" === t.slidesPerColumnFill ? (A = k - (M = Math.floor(k / S)) * S, (_ < M || M === _ && A === S - 1) && S <= (A += 1) && (A = 0, M += 1), $ = M + A * x / S, D.css({
                                    "-webkit-box-ordinal-group": $,
                                    "-moz-box-ordinal-group": $,
                                    "-ms-flex-order": $,
                                    "-webkit-order": $,
                                    order: $
                                })) : M = k - (A = Math.floor(k / E)) * E, D.css("margin-" + (e.isHorizontal() ? "top" : "left"), 0 !== A && t.spaceBetween && t.spaceBetween + "px").attr("data-swiper-column", M).attr("data-swiper-row", A)
                            }
                            if ("none" !== D.css("display")) {
                                if ("auto" === t.slidesPerView) {
                                    var O = J.getComputedStyle(D[0], null),
                                        I = D[0].style.transform,
                                        P = D[0].style.webkitTransform;
                                    if (I && (D[0].style.transform = "none"), P && (D[0].style.webkitTransform = "none"), t.roundLengths) T = e.isHorizontal() ? D.outerWidth(!0) : D.outerHeight(!0);
                                    else if (e.isHorizontal()) {
                                        var L = parseFloat(O.getPropertyValue("width")),
                                            N = parseFloat(O.getPropertyValue("padding-left")),
                                            j = parseFloat(O.getPropertyValue("padding-right")),
                                            H = parseFloat(O.getPropertyValue("margin-left")),
                                            z = parseFloat(O.getPropertyValue("margin-right")),
                                            q = O.getPropertyValue("box-sizing");
                                        T = q && "border-box" === q ? L + H + z : L + N + j + H + z
                                    } else {
                                        var F = parseFloat(O.getPropertyValue("height")),
                                            R = parseFloat(O.getPropertyValue("padding-top")),
                                            Y = parseFloat(O.getPropertyValue("padding-bottom")),
                                            W = parseFloat(O.getPropertyValue("margin-top")),
                                            B = parseFloat(O.getPropertyValue("margin-bottom")),
                                            U = O.getPropertyValue("box-sizing");
                                        T = U && "border-box" === U ? F + W + B : F + R + Y + W + B
                                    }
                                    I && (D[0].style.transform = I), P && (D[0].style.webkitTransform = P), t.roundLengths && (T = Math.floor(T))
                                } else T = (i - (t.slidesPerView - 1) * v) / t.slidesPerView, t.roundLengths && (T = Math.floor(T)), l[k] && (e.isHorizontal() ? l[k].style.width = T + "px" : l[k].style.height = T + "px");
                                l[k] && (l[k].swiperSlideSize = T), h.push(T), t.centeredSlides ? (y = y + T / 2 + b / 2 + v, 0 === b && 0 !== k && (y = y - i / 2 - v), 0 === k && (y = y - i / 2 - v), Math.abs(y) < .001 && (y = 0), t.roundLengths && (y = Math.floor(y)), w % t.slidesPerGroup == 0 && d.push(y), u.push(y)) : (t.roundLengths && (y = Math.floor(y)), w % t.slidesPerGroup == 0 && d.push(y), u.push(y), y = y + T + v), e.virtualSize += T + v, b = T, w += 1
                            }
                        }
                        if (e.virtualSize = Math.max(e.virtualSize, i) + f, s && r && ("slide" === t.effect || "coverflow" === t.effect) && n.css({
                                width: e.virtualSize + t.spaceBetween + "px"
                            }), te.flexbox && !t.setWrapperSize || (e.isHorizontal() ? n.css({
                                width: e.virtualSize + t.spaceBetween + "px"
                            }) : n.css({
                                height: e.virtualSize + t.spaceBetween + "px"
                            })), 1 < t.slidesPerColumn && (e.virtualSize = (T + t.spaceBetween) * x, e.virtualSize = Math.ceil(e.virtualSize / t.slidesPerColumn) - t.spaceBetween, e.isHorizontal() ? n.css({
                                width: e.virtualSize + t.spaceBetween + "px"
                            }) : n.css({
                                height: e.virtualSize + t.spaceBetween + "px"
                            }), t.centeredSlides)) {
                            C = [];
                            for (var V = 0; V < d.length; V += 1) {
                                var G = d[V];
                                t.roundLengths && (G = Math.floor(G)), d[V] < e.virtualSize + d[0] && C.push(G)
                            }
                            d = C
                        }
                        if (!t.centeredSlides) {
                            C = [];
                            for (var X = 0; X < d.length; X += 1) {
                                var Q = d[X];
                                t.roundLengths && (Q = Math.floor(Q)), d[X] <= e.virtualSize - i && C.push(Q)
                            }
                            d = C, 1 < Math.floor(e.virtualSize - i) - Math.floor(d[d.length - 1]) && d.push(e.virtualSize - i)
                        }
                        if (0 === d.length && (d = [0]), 0 !== t.spaceBetween && (e.isHorizontal() ? s ? l.css({
                                marginLeft: v + "px"
                            }) : l.css({
                                marginRight: v + "px"
                            }) : l.css({
                                marginBottom: v + "px"
                            })), t.centerInsufficientSlides) {
                            var K = 0;
                            if (h.forEach(function(e) {
                                    K += e + (t.spaceBetween ? t.spaceBetween : 0)
                                }), (K -= t.spaceBetween) < i) {
                                var Z = (i - K) / 2;
                                d.forEach(function(e, t) {
                                    d[t] = e - Z
                                }), u.forEach(function(e, t) {
                                    u[t] = e + Z
                                })
                            }
                        }
                        ee.extend(e, {
                            slides: l,
                            snapGrid: d,
                            slidesGrid: u,
                            slidesSizesGrid: h
                        }), c !== a && e.emit("slidesLengthChange"), d.length !== m && (e.params.watchOverflow && e.checkOverflow(), e.emit("snapGridLengthChange")), u.length !== g && e.emit("slidesGridLengthChange"), (t.watchSlidesProgress || t.watchSlidesVisibility) && e.updateSlidesOffset()
                    }
                },
                updateAutoHeight: function(e) {
                    var t, n = this,
                        i = [],
                        s = 0;
                    if ("number" == typeof e ? n.setTransition(e) : !0 === e && n.setTransition(n.params.speed), "auto" !== n.params.slidesPerView && 1 < n.params.slidesPerView)
                        for (t = 0; t < Math.ceil(n.params.slidesPerView); t += 1) {
                            var r = n.activeIndex + t;
                            if (r > n.slides.length) break;
                            i.push(n.slides.eq(r)[0])
                        } else i.push(n.slides.eq(n.activeIndex)[0]);
                    for (t = 0; t < i.length; t += 1)
                        if (void 0 !== i[t]) {
                            var o = i[t].offsetHeight;
                            s = s < o ? o : s
                        } s && n.$wrapperEl.css("height", s + "px")
                },
                updateSlidesOffset: function() {
                    for (var e = this.slides, t = 0; t < e.length; t += 1) e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop
                },
                updateSlidesProgress: function(e) {
                    void 0 === e && (e = this && this.translate || 0);
                    var t = this,
                        n = t.params,
                        i = t.slides,
                        s = t.rtlTranslate;
                    if (0 !== i.length) {
                        void 0 === i[0].swiperSlideOffset && t.updateSlidesOffset();
                        var r = -e;
                        s && (r = e), i.removeClass(n.slideVisibleClass), t.visibleSlidesIndexes = [], t.visibleSlides = [];
                        for (var o = 0; o < i.length; o += 1) {
                            var a = i[o],
                                l = (r + (n.centeredSlides ? t.minTranslate() : 0) - a.swiperSlideOffset) / (a.swiperSlideSize + n.spaceBetween);
                            if (n.watchSlidesVisibility) {
                                var c = -(r - a.swiperSlideOffset),
                                    d = c + t.slidesSizesGrid[o];
                                (0 <= c && c < t.size || 0 < d && d <= t.size || c <= 0 && d >= t.size) && (t.visibleSlides.push(a), t.visibleSlidesIndexes.push(o), i.eq(o).addClass(n.slideVisibleClass))
                            }
                            a.progress = s ? -l : l
                        }
                        t.visibleSlides = A(t.visibleSlides)
                    }
                },
                updateProgress: function(e) {
                    void 0 === e && (e = this && this.translate || 0);
                    var t = this,
                        n = t.params,
                        i = t.maxTranslate() - t.minTranslate(),
                        s = t.progress,
                        r = t.isBeginning,
                        o = t.isEnd,
                        a = r,
                        l = o;
                    o = 0 == i ? r = !(s = 0) : (r = (s = (e - t.minTranslate()) / i) <= 0, 1 <= s), ee.extend(t, {
                        progress: s,
                        isBeginning: r,
                        isEnd: o
                    }), (n.watchSlidesProgress || n.watchSlidesVisibility) && t.updateSlidesProgress(e), r && !a && t.emit("reachBeginning toEdge"), o && !l && t.emit("reachEnd toEdge"), (a && !r || l && !o) && t.emit("fromEdge"), t.emit("progress", s)
                },
                updateSlidesClasses: function() {
                    var e, t = this.slides,
                        n = this.params,
                        i = this.$wrapperEl,
                        s = this.activeIndex,
                        r = this.realIndex,
                        o = this.virtual && n.virtual.enabled;
                    t.removeClass(n.slideActiveClass + " " + n.slideNextClass + " " + n.slidePrevClass + " " + n.slideDuplicateActiveClass + " " + n.slideDuplicateNextClass + " " + n.slideDuplicatePrevClass), (e = o ? this.$wrapperEl.find("." + n.slideClass + '[data-swiper-slide-index="' + s + '"]') : t.eq(s)).addClass(n.slideActiveClass), n.loop && (e.hasClass(n.slideDuplicateClass) ? i.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + r + '"]').addClass(n.slideDuplicateActiveClass) : i.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + r + '"]').addClass(n.slideDuplicateActiveClass));
                    var a = e.nextAll("." + n.slideClass).eq(0).addClass(n.slideNextClass);
                    n.loop && 0 === a.length && (a = t.eq(0)).addClass(n.slideNextClass);
                    var l = e.prevAll("." + n.slideClass).eq(0).addClass(n.slidePrevClass);
                    n.loop && 0 === l.length && (l = t.eq(-1)).addClass(n.slidePrevClass), n.loop && (a.hasClass(n.slideDuplicateClass) ? i.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + a.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass) : i.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + a.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicateNextClass), l.hasClass(n.slideDuplicateClass) ? i.children("." + n.slideClass + ":not(." + n.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass) : i.children("." + n.slideClass + "." + n.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(n.slideDuplicatePrevClass))
                },
                updateActiveIndex: function(e) {
                    var t, n = this,
                        i = n.rtlTranslate ? n.translate : -n.translate,
                        s = n.slidesGrid,
                        r = n.snapGrid,
                        o = n.params,
                        a = n.activeIndex,
                        l = n.realIndex,
                        c = n.snapIndex,
                        d = e;
                    if (void 0 === d) {
                        for (var u = 0; u < s.length; u += 1) void 0 !== s[u + 1] ? i >= s[u] && i < s[u + 1] - (s[u + 1] - s[u]) / 2 ? d = u : i >= s[u] && i < s[u + 1] && (d = u + 1) : i >= s[u] && (d = u);
                        o.normalizeSlideIndex && (d < 0 || void 0 === d) && (d = 0)
                    }
                    if ((t = 0 <= r.indexOf(i) ? r.indexOf(i) : Math.floor(d / o.slidesPerGroup)) >= r.length && (t = r.length - 1), d !== a) {
                        var h = parseInt(n.slides.eq(d).attr("data-swiper-slide-index") || d, 10);
                        ee.extend(n, {
                            snapIndex: t,
                            realIndex: h,
                            previousIndex: a,
                            activeIndex: d
                        }), n.emit("activeIndexChange"), n.emit("snapIndexChange"), l !== h && n.emit("realIndexChange"), n.emit("slideChange")
                    } else t !== c && (n.snapIndex = t, n.emit("snapIndexChange"))
                },
                updateClickedSlide: function(e) {
                    var t = this,
                        n = t.params,
                        i = A(e.target).closest("." + n.slideClass)[0],
                        s = !1;
                    if (i)
                        for (var r = 0; r < t.slides.length; r += 1) t.slides[r] === i && (s = !0);
                    if (!i || !s) return t.clickedSlide = void 0, void(t.clickedIndex = void 0);
                    t.clickedSlide = i, t.virtual && t.params.virtual.enabled ? t.clickedIndex = parseInt(A(i).attr("data-swiper-slide-index"), 10) : t.clickedIndex = A(i).index(), n.slideToClickedSlide && void 0 !== t.clickedIndex && t.clickedIndex !== t.activeIndex && t.slideToClickedSlide()
                }
            },
            d = {
                getTranslate: function(e) {
                    void 0 === e && (e = this.isHorizontal() ? "x" : "y");
                    var t = this.params,
                        n = this.rtlTranslate,
                        i = this.translate,
                        s = this.$wrapperEl;
                    if (t.virtualTranslate) return n ? -i : i;
                    var r = ee.getTranslate(s[0], e);
                    return n && (r = -r), r || 0
                },
                setTranslate: function(e, t) {
                    var n = this,
                        i = n.rtlTranslate,
                        s = n.params,
                        r = n.$wrapperEl,
                        o = n.progress,
                        a = 0,
                        l = 0;
                    n.isHorizontal() ? a = i ? -e : e : l = e, s.roundLengths && (a = Math.floor(a), l = Math.floor(l)), s.virtualTranslate || (te.transforms3d ? r.transform("translate3d(" + a + "px, " + l + "px, 0px)") : r.transform("translate(" + a + "px, " + l + "px)")), n.previousTranslate = n.translate, n.translate = n.isHorizontal() ? a : l;
                    var c = n.maxTranslate() - n.minTranslate();
                    (0 == c ? 0 : (e - n.minTranslate()) / c) !== o && n.updateProgress(e), n.emit("setTranslate", n.translate, t)
                },
                minTranslate: function() {
                    return -this.snapGrid[0]
                },
                maxTranslate: function() {
                    return -this.snapGrid[this.snapGrid.length - 1]
                }
            },
            u = {
                slideTo: function(e, t, n, i) {
                    void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === n && (n = !0);
                    var s = this,
                        r = e;
                    r < 0 && (r = 0);
                    var o = s.params,
                        a = s.snapGrid,
                        l = s.slidesGrid,
                        c = s.previousIndex,
                        d = s.activeIndex,
                        u = s.rtlTranslate;
                    if (s.animating && o.preventInteractionOnTransition) return !1;
                    var h = Math.floor(r / o.slidesPerGroup);
                    h >= a.length && (h = a.length - 1), (d || o.initialSlide || 0) === (c || 0) && n && s.emit("beforeSlideChangeStart");
                    var p, f = -a[h];
                    if (s.updateProgress(f), o.normalizeSlideIndex)
                        for (var m = 0; m < l.length; m += 1) - Math.floor(100 * f) >= Math.floor(100 * l[m]) && (r = m);
                    if (s.initialized && r !== d) {
                        if (!s.allowSlideNext && f < s.translate && f < s.minTranslate()) return !1;
                        if (!s.allowSlidePrev && f > s.translate && f > s.maxTranslate() && (d || 0) !== r) return !1
                    }
                    return p = d < r ? "next" : r < d ? "prev" : "reset", u && -f === s.translate || !u && f === s.translate ? (s.updateActiveIndex(r), o.autoHeight && s.updateAutoHeight(), s.updateSlidesClasses(), "slide" !== o.effect && s.setTranslate(f), "reset" != p && (s.transitionStart(n, p), s.transitionEnd(n, p)), !1) : (0 !== t && te.transition ? (s.setTransition(t), s.setTranslate(f), s.updateActiveIndex(r), s.updateSlidesClasses(), s.emit("beforeTransitionStart", t, i), s.transitionStart(n, p), s.animating || (s.animating = !0, s.onSlideToWrapperTransitionEnd || (s.onSlideToWrapperTransitionEnd = function(e) {
                        s && !s.destroyed && e.target === this && (s.$wrapperEl[0].removeEventListener("transitionend", s.onSlideToWrapperTransitionEnd), s.$wrapperEl[0].removeEventListener("webkitTransitionEnd", s.onSlideToWrapperTransitionEnd), s.onSlideToWrapperTransitionEnd = null, delete s.onSlideToWrapperTransitionEnd, s.transitionEnd(n, p))
                    }), s.$wrapperEl[0].addEventListener("transitionend", s.onSlideToWrapperTransitionEnd), s.$wrapperEl[0].addEventListener("webkitTransitionEnd", s.onSlideToWrapperTransitionEnd))) : (s.setTransition(0), s.setTranslate(f), s.updateActiveIndex(r), s.updateSlidesClasses(), s.emit("beforeTransitionStart", t, i), s.transitionStart(n, p), s.transitionEnd(n, p)), !0)
                },
                slideToLoop: function(e, t, n, i) {
                    void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === n && (n = !0);
                    var s = e;
                    return this.params.loop && (s += this.loopedSlides), this.slideTo(s, t, n, i)
                },
                slideNext: function(e, t, n) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var i = this.params,
                        s = this.animating;
                    return i.loop ? !s && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex + i.slidesPerGroup, e, t, n)) : this.slideTo(this.activeIndex + i.slidesPerGroup, e, t, n)
                },
                slidePrev: function(e, t, n) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var i = this,
                        s = i.params,
                        r = i.animating,
                        o = i.snapGrid,
                        a = i.slidesGrid,
                        l = i.rtlTranslate;
                    if (s.loop) {
                        if (r) return !1;
                        i.loopFix(), i._clientLeft = i.$wrapperEl[0].clientLeft
                    }

                    function c(e) {
                        return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e)
                    }
                    var d, u = c(l ? i.translate : -i.translate),
                        h = o.map(function(e) {
                            return c(e)
                        }),
                        p = (a.map(function(e) {
                            return c(e)
                        }), o[h.indexOf(u)], o[h.indexOf(u) - 1]);
                    return void 0 !== p && (d = a.indexOf(p)) < 0 && (d = i.activeIndex - 1), i.slideTo(d, e, t, n)
                },
                slideReset: function(e, t, n) {
                    return void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), this.slideTo(this.activeIndex, e, t, n)
                },
                slideToClosest: function(e, t, n) {
                    void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                    var i = this,
                        s = i.activeIndex,
                        r = Math.floor(s / i.params.slidesPerGroup);
                    if (r < i.snapGrid.length - 1) {
                        var o = i.rtlTranslate ? i.translate : -i.translate,
                            a = i.snapGrid[r];
                        (i.snapGrid[r + 1] - a) / 2 < o - a && (s = i.params.slidesPerGroup)
                    }
                    return i.slideTo(s, e, t, n)
                },
                slideToClickedSlide: function() {
                    var e, t = this,
                        n = t.params,
                        i = t.$wrapperEl,
                        s = "auto" === n.slidesPerView ? t.slidesPerViewDynamic() : n.slidesPerView,
                        r = t.clickedIndex;
                    if (n.loop) {
                        if (t.animating) return;
                        e = parseInt(A(t.clickedSlide).attr("data-swiper-slide-index"), 10), n.centeredSlides ? r < t.loopedSlides - s / 2 || r > t.slides.length - t.loopedSlides + s / 2 ? (t.loopFix(), r = i.children("." + n.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + n.slideDuplicateClass + ")").eq(0).index(), ee.nextTick(function() {
                            t.slideTo(r)
                        })) : t.slideTo(r) : r > t.slides.length - s ? (t.loopFix(), r = i.children("." + n.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + n.slideDuplicateClass + ")").eq(0).index(), ee.nextTick(function() {
                            t.slideTo(r)
                        })) : t.slideTo(r)
                    } else t.slideTo(r)
                }
            },
            h = {
                loopCreate: function() {
                    var i = this,
                        e = i.params,
                        t = i.$wrapperEl;
                    t.children("." + e.slideClass + "." + e.slideDuplicateClass).remove();
                    var s = t.children("." + e.slideClass);
                    if (e.loopFillGroupWithBlank) {
                        var n = e.slidesPerGroup - s.length % e.slidesPerGroup;
                        if (n !== e.slidesPerGroup) {
                            for (var r = 0; r < n; r += 1) {
                                var o = A(m.createElement("div")).addClass(e.slideClass + " " + e.slideBlankClass);
                                t.append(o)
                            }
                            s = t.children("." + e.slideClass)
                        }
                    }
                    "auto" !== e.slidesPerView || e.loopedSlides || (e.loopedSlides = s.length), i.loopedSlides = parseInt(e.loopedSlides || e.slidesPerView, 10), i.loopedSlides += e.loopAdditionalSlides, i.loopedSlides > s.length && (i.loopedSlides = s.length);
                    var a = [],
                        l = [];
                    s.each(function(e, t) {
                        var n = A(t);
                        e < i.loopedSlides && l.push(t), e < s.length && e >= s.length - i.loopedSlides && a.push(t), n.attr("data-swiper-slide-index", e)
                    });
                    for (var c = 0; c < l.length; c += 1) t.append(A(l[c].cloneNode(!0)).addClass(e.slideDuplicateClass));
                    for (var d = a.length - 1; 0 <= d; d -= 1) t.prepend(A(a[d].cloneNode(!0)).addClass(e.slideDuplicateClass))
                },
                loopFix: function() {
                    var e, t = this,
                        n = t.params,
                        i = t.activeIndex,
                        s = t.slides,
                        r = t.loopedSlides,
                        o = t.allowSlidePrev,
                        a = t.allowSlideNext,
                        l = t.snapGrid,
                        c = t.rtlTranslate;
                    t.allowSlidePrev = !0, t.allowSlideNext = !0;
                    var d = -l[i] - t.getTranslate();
                    i < r ? (e = s.length - 3 * r + i, e += r, t.slideTo(e, 0, !1, !0) && 0 != d && t.setTranslate((c ? -t.translate : t.translate) - d)) : ("auto" === n.slidesPerView && 2 * r <= i || i >= s.length - r) && (e = -s.length + i + r, e += r, t.slideTo(e, 0, !1, !0) && 0 != d && t.setTranslate((c ? -t.translate : t.translate) - d)), t.allowSlidePrev = o, t.allowSlideNext = a
                },
                loopDestroy: function() {
                    var e = this.$wrapperEl,
                        t = this.params,
                        n = this.slides;
                    e.children("." + t.slideClass + "." + t.slideDuplicateClass + ",." + t.slideClass + "." + t.slideBlankClass).remove(), n.removeAttr("data-swiper-slide-index")
                }
            },
            p = {
                setGrabCursor: function(e) {
                    if (!(te.touch || !this.params.simulateTouch || this.params.watchOverflow && this.isLocked)) {
                        var t = this.el;
                        t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab"
                    }
                },
                unsetGrabCursor: function() {
                    te.touch || this.params.watchOverflow && this.isLocked || (this.el.style.cursor = "")
                }
            },
            f = {
                appendSlide: function(e) {
                    var t = this.$wrapperEl,
                        n = this.params;
                    if (n.loop && this.loopDestroy(), "object" == typeof e && "length" in e)
                        for (var i = 0; i < e.length; i += 1) e[i] && t.append(e[i]);
                    else t.append(e);
                    n.loop && this.loopCreate(), n.observer && te.observer || this.update()
                },
                prependSlide: function(e) {
                    var t = this.params,
                        n = this.$wrapperEl,
                        i = this.activeIndex;
                    t.loop && this.loopDestroy();
                    var s = i + 1;
                    if ("object" == typeof e && "length" in e) {
                        for (var r = 0; r < e.length; r += 1) e[r] && n.prepend(e[r]);
                        s = i + e.length
                    } else n.prepend(e);
                    t.loop && this.loopCreate(), t.observer && te.observer || this.update(), this.slideTo(s, 0, !1)
                },
                addSlide: function(e, t) {
                    var n = this,
                        i = n.$wrapperEl,
                        s = n.params,
                        r = n.activeIndex;
                    s.loop && (r -= n.loopedSlides, n.loopDestroy(), n.slides = i.children("." + s.slideClass));
                    var o = n.slides.length;
                    if (e <= 0) n.prependSlide(t);
                    else if (o <= e) n.appendSlide(t);
                    else {
                        for (var a = e < r ? r + 1 : r, l = [], c = o - 1; e <= c; c -= 1) {
                            var d = n.slides.eq(c);
                            d.remove(), l.unshift(d)
                        }
                        if ("object" == typeof t && "length" in t) {
                            for (var u = 0; u < t.length; u += 1) t[u] && i.append(t[u]);
                            a = e < r ? r + t.length : r
                        } else i.append(t);
                        for (var h = 0; h < l.length; h += 1) i.append(l[h]);
                        s.loop && n.loopCreate(), s.observer && te.observer || n.update(), s.loop ? n.slideTo(a + n.loopedSlides, 0, !1) : n.slideTo(a, 0, !1)
                    }
                },
                removeSlide: function(e) {
                    var t = this,
                        n = t.params,
                        i = t.$wrapperEl,
                        s = t.activeIndex;
                    n.loop && (s -= t.loopedSlides, t.loopDestroy(), t.slides = i.children("." + n.slideClass));
                    var r, o = s;
                    if ("object" == typeof e && "length" in e) {
                        for (var a = 0; a < e.length; a += 1) r = e[a], t.slides[r] && t.slides.eq(r).remove(), r < o && (o -= 1);
                        o = Math.max(o, 0)
                    } else r = e, t.slides[r] && t.slides.eq(r).remove(), r < o && (o -= 1), o = Math.max(o, 0);
                    n.loop && t.loopCreate(), n.observer && te.observer || t.update(), n.loop ? t.slideTo(o + t.loopedSlides, 0, !1) : t.slideTo(o, 0, !1)
                },
                removeAllSlides: function() {
                    for (var e = [], t = 0; t < this.slides.length; t += 1) e.push(t);
                    this.removeSlide(e)
                }
            },
            g = function() {
                var e = J.navigator.userAgent,
                    t = {
                        ios: !1,
                        android: !1,
                        androidChrome: !1,
                        desktop: !1,
                        windows: !1,
                        iphone: !1,
                        ipod: !1,
                        ipad: !1,
                        cordova: J.cordova || J.phonegap,
                        phonegap: J.cordova || J.phonegap
                    },
                    n = e.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
                    i = e.match(/(Android);?[\s\/]+([\d.]+)?/),
                    s = e.match(/(iPad).*OS\s([\d_]+)/),
                    r = e.match(/(iPod)(.*OS\s([\d_]+))?/),
                    o = !s && e.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
                if (n && (t.os = "windows", t.osVersion = n[2], t.windows = !0), i && !n && (t.os = "android", t.osVersion = i[2], t.android = !0, t.androidChrome = 0 <= e.toLowerCase().indexOf("chrome")), (s || o || r) && (t.os = "ios", t.ios = !0), o && !r && (t.osVersion = o[2].replace(/_/g, "."), t.iphone = !0), s && (t.osVersion = s[2].replace(/_/g, "."), t.ipad = !0), r && (t.osVersion = r[3] ? r[3].replace(/_/g, ".") : null, t.iphone = !0), t.ios && t.osVersion && 0 <= e.indexOf("Version/") && "10" === t.osVersion.split(".")[0] && (t.osVersion = e.toLowerCase().split("version/")[1].split(" ")[0]), t.desktop = !(t.os || t.android || t.webView), t.webView = (o || s || r) && e.match(/.*AppleWebKit(?!.*Safari)/i), t.os && "ios" === t.os) {
                    var a = t.osVersion.split("."),
                        l = m.querySelector('meta[name="viewport"]');
                    t.minimalUi = !t.webView && (r || o) && (1 * a[0] == 7 ? 1 <= 1 * a[1] : 7 < 1 * a[0]) && l && 0 <= l.getAttribute("content").indexOf("minimal-ui")
                }
                return t.pixelRatio = J.devicePixelRatio || 1, t
            }();

        function v() {
            var e = this,
                t = e.params,
                n = e.el;
            if (!n || 0 !== n.offsetWidth) {
                t.breakpoints && e.setBreakpoint();
                var i = e.allowSlideNext,
                    s = e.allowSlidePrev,
                    r = e.snapGrid;
                if (e.allowSlideNext = !0, e.allowSlidePrev = !0, e.updateSize(), e.updateSlides(), t.freeMode) {
                    var o = Math.min(Math.max(e.translate, e.maxTranslate()), e.minTranslate());
                    e.setTranslate(o), e.updateActiveIndex(), e.updateSlidesClasses(), t.autoHeight && e.updateAutoHeight()
                } else e.updateSlidesClasses(), ("auto" === t.slidesPerView || 1 < t.slidesPerView) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0);
                e.allowSlidePrev = s, e.allowSlideNext = i, e.params.watchOverflow && r !== e.snapGrid && e.checkOverflow()
            }
        }
        var y = {
                init: !0,
                direction: "horizontal",
                touchEventsTarget: "container",
                initialSlide: 0,
                speed: 300,
                preventInteractionOnTransition: !1,
                edgeSwipeDetection: !1,
                edgeSwipeThreshold: 20,
                freeMode: !1,
                freeModeMomentum: !0,
                freeModeMomentumRatio: 1,
                freeModeMomentumBounce: !0,
                freeModeMomentumBounceRatio: 1,
                freeModeMomentumVelocityRatio: 1,
                freeModeSticky: !1,
                freeModeMinimumVelocity: .02,
                autoHeight: !1,
                setWrapperSize: !1,
                virtualTranslate: !1,
                effect: "slide",
                breakpoints: void 0,
                breakpointsInverse: !1,
                spaceBetween: 0,
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerColumnFill: "column",
                slidesPerGroup: 1,
                centeredSlides: !1,
                slidesOffsetBefore: 0,
                slidesOffsetAfter: 0,
                normalizeSlideIndex: !0,
                centerInsufficientSlides: !1,
                watchOverflow: !1,
                roundLengths: !1,
                touchRatio: 1,
                touchAngle: 45,
                simulateTouch: !0,
                shortSwipes: !0,
                longSwipes: !0,
                longSwipesRatio: .5,
                longSwipesMs: 300,
                followFinger: !0,
                allowTouchMove: !0,
                threshold: 0,
                touchMoveStopPropagation: !0,
                touchStartPreventDefault: !0,
                touchStartForcePreventDefault: !1,
                touchReleaseOnEdges: !1,
                uniqueNavElements: !0,
                resistance: !0,
                resistanceRatio: .85,
                watchSlidesProgress: !1,
                watchSlidesVisibility: !1,
                grabCursor: !1,
                preventClicks: !0,
                preventClicksPropagation: !0,
                slideToClickedSlide: !1,
                preloadImages: !0,
                updateOnImagesReady: !0,
                loop: !1,
                loopAdditionalSlides: 0,
                loopedSlides: null,
                loopFillGroupWithBlank: !1,
                allowSlidePrev: !0,
                allowSlideNext: !0,
                swipeHandler: null,
                noSwiping: !0,
                noSwipingClass: "swiper-no-swiping",
                noSwipingSelector: null,
                passiveListeners: !0,
                containerModifierClass: "swiper-container-",
                slideClass: "swiper-slide",
                slideBlankClass: "swiper-slide-invisible-blank",
                slideActiveClass: "swiper-slide-active",
                slideDuplicateActiveClass: "swiper-slide-duplicate-active",
                slideVisibleClass: "swiper-slide-visible",
                slideDuplicateClass: "swiper-slide-duplicate",
                slideNextClass: "swiper-slide-next",
                slideDuplicateNextClass: "swiper-slide-duplicate-next",
                slidePrevClass: "swiper-slide-prev",
                slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
                wrapperClass: "swiper-wrapper",
                runCallbacksOnInit: !0
            },
            b = {
                update: c,
                translate: d,
                transition: {
                    setTransition: function(e, t) {
                        this.$wrapperEl.transition(e), this.emit("setTransition", e, t)
                    },
                    transitionStart: function(e, t) {
                        void 0 === e && (e = !0);
                        var n = this.activeIndex,
                            i = this.params,
                            s = this.previousIndex;
                        i.autoHeight && this.updateAutoHeight();
                        var r = t;
                        if (r = r || (s < n ? "next" : n < s ? "prev" : "reset"), this.emit("transitionStart"), e && n !== s) {
                            if ("reset" === r) return void this.emit("slideResetTransitionStart");
                            this.emit("slideChangeTransitionStart"), "next" === r ? this.emit("slideNextTransitionStart") : this.emit("slidePrevTransitionStart")
                        }
                    },
                    transitionEnd: function(e, t) {
                        void 0 === e && (e = !0);
                        var n = this.activeIndex,
                            i = this.previousIndex;
                        this.animating = !1, this.setTransition(0);
                        var s = t;
                        if (s = s || (i < n ? "next" : n < i ? "prev" : "reset"), this.emit("transitionEnd"), e && n !== i) {
                            if ("reset" === s) return void this.emit("slideResetTransitionEnd");
                            this.emit("slideChangeTransitionEnd"), "next" === s ? this.emit("slideNextTransitionEnd") : this.emit("slidePrevTransitionEnd")
                        }
                    }
                },
                slide: u,
                loop: h,
                grabCursor: p,
                manipulation: f,
                events: {
                    attachEvents: function() {
                        var e = this,
                            t = e.params,
                            n = e.touchEvents,
                            i = e.el,
                            s = e.wrapperEl;
                        e.onTouchStart = function(e) {
                            var t = this,
                                n = t.touchEventsData,
                                i = t.params,
                                s = t.touches;
                            if (!t.animating || !i.preventInteractionOnTransition) {
                                var r = e;
                                if (r.originalEvent && (r = r.originalEvent), n.isTouchEvent = "touchstart" === r.type, (n.isTouchEvent || !("which" in r) || 3 !== r.which) && !(!n.isTouchEvent && "button" in r && 0 < r.button || n.isTouched && n.isMoved))
                                    if (i.noSwiping && A(r.target).closest(i.noSwipingSelector ? i.noSwipingSelector : "." + i.noSwipingClass)[0]) t.allowClick = !0;
                                    else if (!i.swipeHandler || A(r).closest(i.swipeHandler)[0]) {
                                    s.currentX = "touchstart" === r.type ? r.targetTouches[0].pageX : r.pageX, s.currentY = "touchstart" === r.type ? r.targetTouches[0].pageY : r.pageY;
                                    var o = s.currentX,
                                        a = s.currentY,
                                        l = i.edgeSwipeDetection || i.iOSEdgeSwipeDetection,
                                        c = i.edgeSwipeThreshold || i.iOSEdgeSwipeThreshold;
                                    if (!l || !(o <= c || o >= J.screen.width - c)) {
                                        if (ee.extend(n, {
                                                isTouched: !0,
                                                isMoved: !1,
                                                allowTouchCallbacks: !0,
                                                isScrolling: void 0,
                                                startMoving: void 0
                                            }), s.startX = o, s.startY = a, n.touchStartTime = ee.now(), t.allowClick = !0, t.updateSize(), t.swipeDirection = void 0, 0 < i.threshold && (n.allowThresholdMove = !1), "touchstart" !== r.type) {
                                            var d = !0;
                                            A(r.target).is(n.formElements) && (d = !1), m.activeElement && A(m.activeElement).is(n.formElements) && m.activeElement !== r.target && m.activeElement.blur();
                                            var u = d && t.allowTouchMove && i.touchStartPreventDefault;
                                            (i.touchStartForcePreventDefault || u) && r.preventDefault()
                                        }
                                        t.emit("touchStart", r)
                                    }
                                }
                            }
                        }.bind(e), e.onTouchMove = function(e) {
                            var t = this,
                                n = t.touchEventsData,
                                i = t.params,
                                s = t.touches,
                                r = t.rtlTranslate,
                                o = e;
                            if (o.originalEvent && (o = o.originalEvent), n.isTouched) {
                                if (!n.isTouchEvent || "mousemove" !== o.type) {
                                    var a = "touchmove" === o.type ? o.targetTouches[0].pageX : o.pageX,
                                        l = "touchmove" === o.type ? o.targetTouches[0].pageY : o.pageY;
                                    if (o.preventedByNestedSwiper) return s.startX = a, void(s.startY = l);
                                    if (!t.allowTouchMove) return t.allowClick = !1, void(n.isTouched && (ee.extend(s, {
                                        startX: a,
                                        startY: l,
                                        currentX: a,
                                        currentY: l
                                    }), n.touchStartTime = ee.now()));
                                    if (n.isTouchEvent && i.touchReleaseOnEdges && !i.loop)
                                        if (t.isVertical()) {
                                            if (l < s.startY && t.translate <= t.maxTranslate() || l > s.startY && t.translate >= t.minTranslate()) return n.isTouched = !1, void(n.isMoved = !1)
                                        } else if (a < s.startX && t.translate <= t.maxTranslate() || a > s.startX && t.translate >= t.minTranslate()) return;
                                    if (n.isTouchEvent && m.activeElement && o.target === m.activeElement && A(o.target).is(n.formElements)) return n.isMoved = !0, void(t.allowClick = !1);
                                    if (n.allowTouchCallbacks && t.emit("touchMove", o), !(o.targetTouches && 1 < o.targetTouches.length)) {
                                        s.currentX = a, s.currentY = l;
                                        var c, d = s.currentX - s.startX,
                                            u = s.currentY - s.startY;
                                        if (!(t.params.threshold && Math.sqrt(Math.pow(d, 2) + Math.pow(u, 2)) < t.params.threshold))
                                            if (void 0 === n.isScrolling && (t.isHorizontal() && s.currentY === s.startY || t.isVertical() && s.currentX === s.startX ? n.isScrolling = !1 : 25 <= d * d + u * u && (c = 180 * Math.atan2(Math.abs(u), Math.abs(d)) / Math.PI, n.isScrolling = t.isHorizontal() ? c > i.touchAngle : 90 - c > i.touchAngle)), n.isScrolling && t.emit("touchMoveOpposite", o), void 0 === n.startMoving && (s.currentX === s.startX && s.currentY === s.startY || (n.startMoving = !0)), n.isScrolling) n.isTouched = !1;
                                            else if (n.startMoving) {
                                            t.allowClick = !1, o.preventDefault(), i.touchMoveStopPropagation && !i.nested && o.stopPropagation(), n.isMoved || (i.loop && t.loopFix(), n.startTranslate = t.getTranslate(), t.setTransition(0), t.animating && t.$wrapperEl.trigger("webkitTransitionEnd transitionend"), n.allowMomentumBounce = !1, !i.grabCursor || !0 !== t.allowSlideNext && !0 !== t.allowSlidePrev || t.setGrabCursor(!0), t.emit("sliderFirstMove", o)), t.emit("sliderMove", o), n.isMoved = !0;
                                            var h = t.isHorizontal() ? d : u;
                                            s.diff = h, h *= i.touchRatio, r && (h = -h), t.swipeDirection = 0 < h ? "prev" : "next", n.currentTranslate = h + n.startTranslate;
                                            var p = !0,
                                                f = i.resistanceRatio;
                                            if (i.touchReleaseOnEdges && (f = 0), 0 < h && n.currentTranslate > t.minTranslate() ? (p = !1, i.resistance && (n.currentTranslate = t.minTranslate() - 1 + Math.pow(-t.minTranslate() + n.startTranslate + h, f))) : h < 0 && n.currentTranslate < t.maxTranslate() && (p = !1, i.resistance && (n.currentTranslate = t.maxTranslate() + 1 - Math.pow(t.maxTranslate() - n.startTranslate - h, f))), p && (o.preventedByNestedSwiper = !0), !t.allowSlideNext && "next" === t.swipeDirection && n.currentTranslate < n.startTranslate && (n.currentTranslate = n.startTranslate), !t.allowSlidePrev && "prev" === t.swipeDirection && n.currentTranslate > n.startTranslate && (n.currentTranslate = n.startTranslate), 0 < i.threshold) {
                                                if (!(Math.abs(h) > i.threshold || n.allowThresholdMove)) return void(n.currentTranslate = n.startTranslate);
                                                if (!n.allowThresholdMove) return n.allowThresholdMove = !0, s.startX = s.currentX, s.startY = s.currentY, n.currentTranslate = n.startTranslate, void(s.diff = t.isHorizontal() ? s.currentX - s.startX : s.currentY - s.startY)
                                            }
                                            i.followFinger && ((i.freeMode || i.watchSlidesProgress || i.watchSlidesVisibility) && (t.updateActiveIndex(), t.updateSlidesClasses()), i.freeMode && (0 === n.velocities.length && n.velocities.push({
                                                position: s[t.isHorizontal() ? "startX" : "startY"],
                                                time: n.touchStartTime
                                            }), n.velocities.push({
                                                position: s[t.isHorizontal() ? "currentX" : "currentY"],
                                                time: ee.now()
                                            })), t.updateProgress(n.currentTranslate), t.setTranslate(n.currentTranslate))
                                        }
                                    }
                                }
                            } else n.startMoving && n.isScrolling && t.emit("touchMoveOpposite", o)
                        }.bind(e), e.onTouchEnd = function(e) {
                            var t = this,
                                n = t.touchEventsData,
                                i = t.params,
                                s = t.touches,
                                r = t.rtlTranslate,
                                o = t.$wrapperEl,
                                a = t.slidesGrid,
                                l = t.snapGrid,
                                c = e;
                            if (c.originalEvent && (c = c.originalEvent), n.allowTouchCallbacks && t.emit("touchEnd", c), n.allowTouchCallbacks = !1, !n.isTouched) return n.isMoved && i.grabCursor && t.setGrabCursor(!1), n.isMoved = !1, void(n.startMoving = !1);
                            i.grabCursor && n.isMoved && n.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
                            var d, u = ee.now(),
                                h = u - n.touchStartTime;
                            if (t.allowClick && (t.updateClickedSlide(c), t.emit("tap", c), h < 300 && 300 < u - n.lastClickTime && (n.clickTimeout && clearTimeout(n.clickTimeout), n.clickTimeout = ee.nextTick(function() {
                                    t && !t.destroyed && t.emit("click", c)
                                }, 300)), h < 300 && u - n.lastClickTime < 300 && (n.clickTimeout && clearTimeout(n.clickTimeout), t.emit("doubleTap", c))), n.lastClickTime = ee.now(), ee.nextTick(function() {
                                    t.destroyed || (t.allowClick = !0)
                                }), !n.isTouched || !n.isMoved || !t.swipeDirection || 0 === s.diff || n.currentTranslate === n.startTranslate) return n.isTouched = !1, n.isMoved = !1, void(n.startMoving = !1);
                            if (n.isTouched = !1, n.isMoved = !1, n.startMoving = !1, d = i.followFinger ? r ? t.translate : -t.translate : -n.currentTranslate, i.freeMode) {
                                if (d < -t.minTranslate()) return void t.slideTo(t.activeIndex);
                                if (d > -t.maxTranslate()) return void(t.slides.length < l.length ? t.slideTo(l.length - 1) : t.slideTo(t.slides.length - 1));
                                if (i.freeModeMomentum) {
                                    if (1 < n.velocities.length) {
                                        var p = n.velocities.pop(),
                                            f = n.velocities.pop(),
                                            m = p.position - f.position,
                                            g = p.time - f.time;
                                        t.velocity = m / g, t.velocity /= 2, Math.abs(t.velocity) < i.freeModeMinimumVelocity && (t.velocity = 0), (150 < g || 300 < ee.now() - p.time) && (t.velocity = 0)
                                    } else t.velocity = 0;
                                    t.velocity *= i.freeModeMomentumVelocityRatio, n.velocities.length = 0;
                                    var v = 1e3 * i.freeModeMomentumRatio,
                                        y = t.velocity * v,
                                        b = t.translate + y;
                                    r && (b = -b);
                                    var w, x, T = !1,
                                        C = 20 * Math.abs(t.velocity) * i.freeModeMomentumBounceRatio;
                                    if (b < t.maxTranslate()) i.freeModeMomentumBounce ? (b + t.maxTranslate() < -C && (b = t.maxTranslate() - C), w = t.maxTranslate(), T = !0, n.allowMomentumBounce = !0) : b = t.maxTranslate(), i.loop && i.centeredSlides && (x = !0);
                                    else if (b > t.minTranslate()) i.freeModeMomentumBounce ? (b - t.minTranslate() > C && (b = t.minTranslate() + C), w = t.minTranslate(), T = !0, n.allowMomentumBounce = !0) : b = t.minTranslate(), i.loop && i.centeredSlides && (x = !0);
                                    else if (i.freeModeSticky) {
                                        for (var S, E = 0; E < l.length; E += 1)
                                            if (l[E] > -b) {
                                                S = E;
                                                break
                                            } b = -(b = Math.abs(l[S] - b) < Math.abs(l[S - 1] - b) || "next" === t.swipeDirection ? l[S] : l[S - 1])
                                    }
                                    if (x && t.once("transitionEnd", function() {
                                            t.loopFix()
                                        }), 0 !== t.velocity) v = r ? Math.abs((-b - t.translate) / t.velocity) : Math.abs((b - t.translate) / t.velocity);
                                    else if (i.freeModeSticky) return void t.slideToClosest();
                                    i.freeModeMomentumBounce && T ? (t.updateProgress(w), t.setTransition(v), t.setTranslate(b), t.transitionStart(!0, t.swipeDirection), t.animating = !0, o.transitionEnd(function() {
                                        t && !t.destroyed && n.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(i.speed), t.setTranslate(w), o.transitionEnd(function() {
                                            t && !t.destroyed && t.transitionEnd()
                                        }))
                                    })) : t.velocity ? (t.updateProgress(b), t.setTransition(v), t.setTranslate(b), t.transitionStart(!0, t.swipeDirection), t.animating || (t.animating = !0, o.transitionEnd(function() {
                                        t && !t.destroyed && t.transitionEnd()
                                    }))) : t.updateProgress(b), t.updateActiveIndex(), t.updateSlidesClasses()
                                } else if (i.freeModeSticky) return void t.slideToClosest();
                                (!i.freeModeMomentum || h >= i.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses())
                            } else {
                                for (var _ = 0, k = t.slidesSizesGrid[0], D = 0; D < a.length; D += i.slidesPerGroup) void 0 !== a[D + i.slidesPerGroup] ? d >= a[D] && d < a[D + i.slidesPerGroup] && (k = a[(_ = D) + i.slidesPerGroup] - a[D]) : d >= a[D] && (_ = D, k = a[a.length - 1] - a[a.length - 2]);
                                var $ = (d - a[_]) / k;
                                if (h > i.longSwipesMs) {
                                    if (!i.longSwipes) return void t.slideTo(t.activeIndex);
                                    "next" === t.swipeDirection && ($ >= i.longSwipesRatio ? t.slideTo(_ + i.slidesPerGroup) : t.slideTo(_)), "prev" === t.swipeDirection && ($ > 1 - i.longSwipesRatio ? t.slideTo(_ + i.slidesPerGroup) : t.slideTo(_))
                                } else {
                                    if (!i.shortSwipes) return void t.slideTo(t.activeIndex);
                                    "next" === t.swipeDirection && t.slideTo(_ + i.slidesPerGroup), "prev" === t.swipeDirection && t.slideTo(_)
                                }
                            }
                        }.bind(e), e.onClick = function(e) {
                            this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
                        }.bind(e);
                        var r = "container" === t.touchEventsTarget ? i : s,
                            o = !!t.nested;
                        if (te.touch || !te.pointerEvents && !te.prefixedPointerEvents) {
                            if (te.touch) {
                                var a = !("touchstart" !== n.start || !te.passiveListener || !t.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                r.addEventListener(n.start, e.onTouchStart, a), r.addEventListener(n.move, e.onTouchMove, te.passiveListener ? {
                                    passive: !1,
                                    capture: o
                                } : o), r.addEventListener(n.end, e.onTouchEnd, a)
                            }(t.simulateTouch && !g.ios && !g.android || t.simulateTouch && !te.touch && g.ios) && (r.addEventListener("mousedown", e.onTouchStart, !1), m.addEventListener("mousemove", e.onTouchMove, o), m.addEventListener("mouseup", e.onTouchEnd, !1))
                        } else r.addEventListener(n.start, e.onTouchStart, !1), m.addEventListener(n.move, e.onTouchMove, o), m.addEventListener(n.end, e.onTouchEnd, !1);
                        (t.preventClicks || t.preventClicksPropagation) && r.addEventListener("click", e.onClick, !0), e.on(g.ios || g.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", v, !0)
                    },
                    detachEvents: function() {
                        var e = this,
                            t = e.params,
                            n = e.touchEvents,
                            i = e.el,
                            s = e.wrapperEl,
                            r = "container" === t.touchEventsTarget ? i : s,
                            o = !!t.nested;
                        if (te.touch || !te.pointerEvents && !te.prefixedPointerEvents) {
                            if (te.touch) {
                                var a = !("onTouchStart" !== n.start || !te.passiveListener || !t.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                r.removeEventListener(n.start, e.onTouchStart, a), r.removeEventListener(n.move, e.onTouchMove, o), r.removeEventListener(n.end, e.onTouchEnd, a)
                            }(t.simulateTouch && !g.ios && !g.android || t.simulateTouch && !te.touch && g.ios) && (r.removeEventListener("mousedown", e.onTouchStart, !1), m.removeEventListener("mousemove", e.onTouchMove, o), m.removeEventListener("mouseup", e.onTouchEnd, !1))
                        } else r.removeEventListener(n.start, e.onTouchStart, !1), m.removeEventListener(n.move, e.onTouchMove, o), m.removeEventListener(n.end, e.onTouchEnd, !1);
                        (t.preventClicks || t.preventClicksPropagation) && r.removeEventListener("click", e.onClick, !0), e.off(g.ios || g.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", v)
                    }
                },
                breakpoints: {
                    setBreakpoint: function() {
                        var e = this,
                            t = e.activeIndex,
                            n = e.initialized,
                            i = e.loopedSlides;
                        void 0 === i && (i = 0);
                        var s = e.params,
                            r = s.breakpoints;
                        if (r && (!r || 0 !== Object.keys(r).length)) {
                            var o = e.getBreakpoint(r);
                            if (o && e.currentBreakpoint !== o) {
                                var a = o in r ? r[o] : void 0;
                                a && ["slidesPerView", "spaceBetween", "slidesPerGroup"].forEach(function(e) {
                                    var t = a[e];
                                    void 0 !== t && (a[e] = "slidesPerView" !== e || "AUTO" !== t && "auto" !== t ? "slidesPerView" === e ? parseFloat(t) : parseInt(t, 10) : "auto")
                                });
                                var l = a || e.originalParams,
                                    c = l.direction && l.direction !== s.direction,
                                    d = s.loop && (l.slidesPerView !== s.slidesPerView || c);
                                c && n && e.changeDirection(), ee.extend(e.params, l), ee.extend(e, {
                                    allowTouchMove: e.params.allowTouchMove,
                                    allowSlideNext: e.params.allowSlideNext,
                                    allowSlidePrev: e.params.allowSlidePrev
                                }), e.currentBreakpoint = o, d && n && (e.loopDestroy(), e.loopCreate(), e.updateSlides(), e.slideTo(t - i + e.loopedSlides, 0, !1)), e.emit("breakpoint", l)
                            }
                        }
                    },
                    getBreakpoint: function(e) {
                        if (e) {
                            var t = !1,
                                n = [];
                            Object.keys(e).forEach(function(e) {
                                n.push(e)
                            }), n.sort(function(e, t) {
                                return parseInt(e, 10) - parseInt(t, 10)
                            });
                            for (var i = 0; i < n.length; i += 1) {
                                var s = n[i];
                                this.params.breakpointsInverse ? s <= J.innerWidth && (t = s) : s >= J.innerWidth && !t && (t = s)
                            }
                            return t || "max"
                        }
                    }
                },
                checkOverflow: {
                    checkOverflow: function() {
                        var e = this,
                            t = e.isLocked;
                        e.isLocked = 1 === e.snapGrid.length, e.allowSlideNext = !e.isLocked, e.allowSlidePrev = !e.isLocked, t !== e.isLocked && e.emit(e.isLocked ? "lock" : "unlock"), t && t !== e.isLocked && (e.isEnd = !1, e.navigation.update())
                    }
                },
                classes: {
                    addClasses: function() {
                        var t = this.classNames,
                            n = this.params,
                            e = this.rtl,
                            i = this.$el,
                            s = [];
                        s.push("initialized"), s.push(n.direction), n.freeMode && s.push("free-mode"), te.flexbox || s.push("no-flexbox"), n.autoHeight && s.push("autoheight"), e && s.push("rtl"), 1 < n.slidesPerColumn && s.push("multirow"), g.android && s.push("android"), g.ios && s.push("ios"), (O.isIE || O.isEdge) && (te.pointerEvents || te.prefixedPointerEvents) && s.push("wp8-" + n.direction), s.forEach(function(e) {
                            t.push(n.containerModifierClass + e)
                        }), i.addClass(t.join(" "))
                    },
                    removeClasses: function() {
                        var e = this.$el,
                            t = this.classNames;
                        e.removeClass(t.join(" "))
                    }
                },
                images: {
                    loadImage: function(e, t, n, i, s, r) {
                        var o;

                        function a() {
                            r && r()
                        }
                        e.complete && s ? a() : t ? ((o = new J.Image).onload = a, o.onerror = a, i && (o.sizes = i), n && (o.srcset = n), t && (o.src = t)) : a()
                    },
                    preloadImages: function() {
                        var e = this;

                        function t() {
                            null != e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady")))
                        }
                        e.imagesToLoad = e.$el.find("img");
                        for (var n = 0; n < e.imagesToLoad.length; n += 1) {
                            var i = e.imagesToLoad[n];
                            e.loadImage(i, i.currentSrc || i.getAttribute("src"), i.srcset || i.getAttribute("srcset"), i.sizes || i.getAttribute("sizes"), !0, t)
                        }
                    }
                }
            },
            w = {},
            x = function(h) {
                function p() {
                    for (var e, t, s, n = [], i = arguments.length; i--;) n[i] = arguments[i];
                    s = (s = 1 === n.length && n[0].constructor && n[0].constructor === Object ? n[0] : (t = (e = n)[0], e[1])) || {}, s = ee.extend({}, s), t && !s.el && (s.el = t), h.call(this, s), Object.keys(b).forEach(function(t) {
                        Object.keys(b[t]).forEach(function(e) {
                            p.prototype[e] || (p.prototype[e] = b[t][e])
                        })
                    });
                    var r = this;
                    void 0 === r.modules && (r.modules = {}), Object.keys(r.modules).forEach(function(e) {
                        var t = r.modules[e];
                        if (t.params) {
                            var n = Object.keys(t.params)[0],
                                i = t.params[n];
                            if ("object" != typeof i || null === i) return;
                            if (!(n in s && "enabled" in i)) return;
                            !0 === s[n] && (s[n] = {
                                enabled: !0
                            }), "object" != typeof s[n] || "enabled" in s[n] || (s[n].enabled = !0), s[n] || (s[n] = {
                                enabled: !1
                            })
                        }
                    });
                    var o = ee.extend({}, y);
                    r.useModulesParams(o), r.params = ee.extend({}, o, w, s), r.originalParams = ee.extend({}, r.params), r.passedParams = ee.extend({}, s);
                    var a = (r.$ = A)(r.params.el);
                    if (t = a[0]) {
                        if (1 < a.length) {
                            var l = [];
                            return a.each(function(e, t) {
                                var n = ee.extend({}, s, {
                                    el: t
                                });
                                l.push(new p(n))
                            }), l
                        }
                        t.swiper = r, a.data("swiper", r);
                        var c, d, u = a.children("." + r.params.wrapperClass);
                        return ee.extend(r, {
                            $el: a,
                            el: t,
                            $wrapperEl: u,
                            wrapperEl: u[0],
                            classNames: [],
                            slides: A(),
                            slidesGrid: [],
                            snapGrid: [],
                            slidesSizesGrid: [],
                            isHorizontal: function() {
                                return "horizontal" === r.params.direction
                            },
                            isVertical: function() {
                                return "vertical" === r.params.direction
                            },
                            rtl: "rtl" === t.dir.toLowerCase() || "rtl" === a.css("direction"),
                            rtlTranslate: "horizontal" === r.params.direction && ("rtl" === t.dir.toLowerCase() || "rtl" === a.css("direction")),
                            wrongRTL: "-webkit-box" === u.css("display"),
                            activeIndex: 0,
                            realIndex: 0,
                            isBeginning: !0,
                            isEnd: !1,
                            translate: 0,
                            previousTranslate: 0,
                            progress: 0,
                            velocity: 0,
                            animating: !1,
                            allowSlideNext: r.params.allowSlideNext,
                            allowSlidePrev: r.params.allowSlidePrev,
                            touchEvents: (c = ["touchstart", "touchmove", "touchend"], d = ["mousedown", "mousemove", "mouseup"], te.pointerEvents ? d = ["pointerdown", "pointermove", "pointerup"] : te.prefixedPointerEvents && (d = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), r.touchEventsTouch = {
                                start: c[0],
                                move: c[1],
                                end: c[2]
                            }, r.touchEventsDesktop = {
                                start: d[0],
                                move: d[1],
                                end: d[2]
                            }, te.touch || !r.params.simulateTouch ? r.touchEventsTouch : r.touchEventsDesktop),
                            touchEventsData: {
                                isTouched: void 0,
                                isMoved: void 0,
                                allowTouchCallbacks: void 0,
                                touchStartTime: void 0,
                                isScrolling: void 0,
                                currentTranslate: void 0,
                                startTranslate: void 0,
                                allowThresholdMove: void 0,
                                formElements: "input, select, option, textarea, button, video",
                                lastClickTime: ee.now(),
                                clickTimeout: void 0,
                                velocities: [],
                                allowMomentumBounce: void 0,
                                isTouchEvent: void 0,
                                startMoving: void 0
                            },
                            allowClick: !0,
                            allowTouchMove: r.params.allowTouchMove,
                            touches: {
                                startX: 0,
                                startY: 0,
                                currentX: 0,
                                currentY: 0,
                                diff: 0
                            },
                            imagesToLoad: [],
                            imagesLoaded: 0
                        }), r.useModules(), r.params.init && r.init(), r
                    }
                }
                h && (p.__proto__ = h);
                var e = {
                    extendedDefaults: {
                        configurable: !0
                    },
                    defaults: {
                        configurable: !0
                    },
                    Class: {
                        configurable: !0
                    },
                    $: {
                        configurable: !0
                    }
                };
                return ((p.prototype = Object.create(h && h.prototype)).constructor = p).prototype.slidesPerViewDynamic = function() {
                    var e = this.params,
                        t = this.slides,
                        n = this.slidesGrid,
                        i = this.size,
                        s = this.activeIndex,
                        r = 1;
                    if (e.centeredSlides) {
                        for (var o, a = t[s].swiperSlideSize, l = s + 1; l < t.length; l += 1) t[l] && !o && (r += 1, i < (a += t[l].swiperSlideSize) && (o = !0));
                        for (var c = s - 1; 0 <= c; c -= 1) t[c] && !o && (r += 1, i < (a += t[c].swiperSlideSize) && (o = !0))
                    } else
                        for (var d = s + 1; d < t.length; d += 1) n[d] - n[s] < i && (r += 1);
                    return r
                }, p.prototype.update = function() {
                    var n = this;
                    if (n && !n.destroyed) {
                        var e = n.snapGrid,
                            t = n.params;
                        t.breakpoints && n.setBreakpoint(), n.updateSize(), n.updateSlides(), n.updateProgress(), n.updateSlidesClasses(), n.params.freeMode ? (i(), n.params.autoHeight && n.updateAutoHeight()) : (("auto" === n.params.slidesPerView || 1 < n.params.slidesPerView) && n.isEnd && !n.params.centeredSlides ? n.slideTo(n.slides.length - 1, 0, !1, !0) : n.slideTo(n.activeIndex, 0, !1, !0)) || i(), t.watchOverflow && e !== n.snapGrid && n.checkOverflow(), n.emit("update")
                    }

                    function i() {
                        var e = n.rtlTranslate ? -1 * n.translate : n.translate,
                            t = Math.min(Math.max(e, n.maxTranslate()), n.minTranslate());
                        n.setTranslate(t), n.updateActiveIndex(), n.updateSlidesClasses()
                    }
                }, p.prototype.changeDirection = function(n, e) {
                    void 0 === e && (e = !0);
                    var t = this,
                        i = t.params.direction;
                    return (n = n || ("horizontal" === i ? "vertical" : "horizontal")) === i || "horizontal" !== n && "vertical" !== n || ("vertical" === i && (t.$el.removeClass(t.params.containerModifierClass + "vertical wp8-vertical").addClass("" + t.params.containerModifierClass + n), (O.isIE || O.isEdge) && (te.pointerEvents || te.prefixedPointerEvents) && t.$el.addClass(t.params.containerModifierClass + "wp8-" + n)), "horizontal" === i && (t.$el.removeClass(t.params.containerModifierClass + "horizontal wp8-horizontal").addClass("" + t.params.containerModifierClass + n), (O.isIE || O.isEdge) && (te.pointerEvents || te.prefixedPointerEvents) && t.$el.addClass(t.params.containerModifierClass + "wp8-" + n)), t.params.direction = n, t.slides.each(function(e, t) {
                        "vertical" === n ? t.style.width = "" : t.style.height = ""
                    }), t.emit("changeDirection"), e && t.update()), t
                }, p.prototype.init = function() {
                    var e = this;
                    e.initialized || (e.emit("beforeInit"), e.params.breakpoints && e.setBreakpoint(), e.addClasses(), e.params.loop && e.loopCreate(), e.updateSize(), e.updateSlides(), e.params.watchOverflow && e.checkOverflow(), e.params.grabCursor && e.setGrabCursor(), e.params.preloadImages && e.preloadImages(), e.params.loop ? e.slideTo(e.params.initialSlide + e.loopedSlides, 0, e.params.runCallbacksOnInit) : e.slideTo(e.params.initialSlide, 0, e.params.runCallbacksOnInit), e.attachEvents(), e.initialized = !0, e.emit("init"))
                }, p.prototype.destroy = function(e, t) {
                    void 0 === e && (e = !0), void 0 === t && (t = !0);
                    var n = this,
                        i = n.params,
                        s = n.$el,
                        r = n.$wrapperEl,
                        o = n.slides;
                    return void 0 === n.params || n.destroyed || (n.emit("beforeDestroy"), n.initialized = !1, n.detachEvents(), i.loop && n.loopDestroy(), t && (n.removeClasses(), s.removeAttr("style"), r.removeAttr("style"), o && o.length && o.removeClass([i.slideVisibleClass, i.slideActiveClass, i.slideNextClass, i.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), n.emit("destroy"), Object.keys(n.eventsListeners).forEach(function(e) {
                        n.off(e)
                    }), !1 !== e && (n.$el[0].swiper = null, n.$el.data("swiper", null), ee.deleteProps(n)), n.destroyed = !0), null
                }, p.extendDefaults = function(e) {
                    ee.extend(w, e)
                }, e.extendedDefaults.get = function() {
                    return w
                }, e.defaults.get = function() {
                    return y
                }, e.Class.get = function() {
                    return h
                }, e.$.get = function() {
                    return A
                }, Object.defineProperties(p, e), p
            }(e),
            T = {
                name: "device",
                proto: {
                    device: g
                },
                static: {
                    device: g
                }
            },
            C = {
                name: "support",
                proto: {
                    support: te
                },
                static: {
                    support: te
                }
            },
            S = {
                name: "browser",
                proto: {
                    browser: O
                },
                static: {
                    browser: O
                }
            },
            E = {
                name: "resize",
                create: function() {
                    var e = this;
                    ee.extend(e, {
                        resize: {
                            resizeHandler: function() {
                                e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize"))
                            },
                            orientationChangeHandler: function() {
                                e && !e.destroyed && e.initialized && e.emit("orientationchange")
                            }
                        }
                    })
                },
                on: {
                    init: function() {
                        J.addEventListener("resize", this.resize.resizeHandler), J.addEventListener("orientationchange", this.resize.orientationChangeHandler)
                    },
                    destroy: function() {
                        J.removeEventListener("resize", this.resize.resizeHandler), J.removeEventListener("orientationchange", this.resize.orientationChangeHandler)
                    }
                }
            },
            _ = {
                func: J.MutationObserver || J.WebkitMutationObserver,
                attach: function(e, t) {
                    void 0 === t && (t = {});
                    var n = this,
                        i = new _.func(function(e) {
                            if (1 !== e.length) {
                                var t = function() {
                                    n.emit("observerUpdate", e[0])
                                };
                                J.requestAnimationFrame ? J.requestAnimationFrame(t) : J.setTimeout(t, 0)
                            } else n.emit("observerUpdate", e[0])
                        });
                    i.observe(e, {
                        attributes: void 0 === t.attributes || t.attributes,
                        childList: void 0 === t.childList || t.childList,
                        characterData: void 0 === t.characterData || t.characterData
                    }), n.observer.observers.push(i)
                },
                init: function() {
                    if (te.observer && this.params.observer) {
                        if (this.params.observeParents)
                            for (var e = this.$el.parents(), t = 0; t < e.length; t += 1) this.observer.attach(e[t]);
                        this.observer.attach(this.$el[0], {
                            childList: this.params.observeSlideChildren
                        }), this.observer.attach(this.$wrapperEl[0], {
                            attributes: !1
                        })
                    }
                },
                destroy: function() {
                    this.observer.observers.forEach(function(e) {
                        e.disconnect()
                    }), this.observer.observers = []
                }
            },
            k = {
                name: "observer",
                params: {
                    observer: !1,
                    observeParents: !1,
                    observeSlideChildren: !1
                },
                create: function() {
                    ee.extend(this, {
                        observer: {
                            init: _.init.bind(this),
                            attach: _.attach.bind(this),
                            destroy: _.destroy.bind(this),
                            observers: []
                        }
                    })
                },
                on: {
                    init: function() {
                        this.observer.init()
                    },
                    destroy: function() {
                        this.observer.destroy()
                    }
                }
            },
            D = {
                update: function(e) {
                    var t = this,
                        n = t.params,
                        i = n.slidesPerView,
                        s = n.slidesPerGroup,
                        r = n.centeredSlides,
                        o = t.params.virtual,
                        a = o.addSlidesBefore,
                        l = o.addSlidesAfter,
                        c = t.virtual,
                        d = c.from,
                        u = c.to,
                        h = c.slides,
                        p = c.slidesGrid,
                        f = c.renderSlide,
                        m = c.offset;
                    t.updateActiveIndex();
                    var g, v, y, b = t.activeIndex || 0;
                    g = t.rtlTranslate ? "right" : t.isHorizontal() ? "left" : "top", y = r ? (v = Math.floor(i / 2) + s + a, Math.floor(i / 2) + s + l) : (v = i + (s - 1) + a, s + l);
                    var w = Math.max((b || 0) - y, 0),
                        x = Math.min((b || 0) + v, h.length - 1),
                        T = (t.slidesGrid[w] || 0) - (t.slidesGrid[0] || 0);

                    function C() {
                        t.updateSlides(), t.updateProgress(), t.updateSlidesClasses(), t.lazy && t.params.lazy.enabled && t.lazy.load()
                    }
                    if (ee.extend(t.virtual, {
                            from: w,
                            to: x,
                            offset: T,
                            slidesGrid: t.slidesGrid
                        }), d === w && u === x && !e) return t.slidesGrid !== p && T !== m && t.slides.css(g, T + "px"), void t.updateProgress();
                    if (t.params.virtual.renderExternal) return t.params.virtual.renderExternal.call(t, {
                        offset: T,
                        from: w,
                        to: x,
                        slides: function() {
                            for (var e = [], t = w; t <= x; t += 1) e.push(h[t]);
                            return e
                        }()
                    }), void C();
                    var S = [],
                        E = [];
                    if (e) t.$wrapperEl.find("." + t.params.slideClass).remove();
                    else
                        for (var _ = d; _ <= u; _ += 1)(_ < w || x < _) && t.$wrapperEl.find("." + t.params.slideClass + '[data-swiper-slide-index="' + _ + '"]').remove();
                    for (var k = 0; k < h.length; k += 1) w <= k && k <= x && (void 0 === u || e ? E.push(k) : (u < k && E.push(k), k < d && S.push(k)));
                    E.forEach(function(e) {
                        t.$wrapperEl.append(f(h[e], e))
                    }), S.sort(function(e, t) {
                        return t - e
                    }).forEach(function(e) {
                        t.$wrapperEl.prepend(f(h[e], e))
                    }), t.$wrapperEl.children(".swiper-slide").css(g, T + "px"), C()
                },
                renderSlide: function(e, t) {
                    var n = this.params.virtual;
                    if (n.cache && this.virtual.cache[t]) return this.virtual.cache[t];
                    var i = n.renderSlide ? A(n.renderSlide.call(this, e, t)) : A('<div class="' + this.params.slideClass + '" data-swiper-slide-index="' + t + '">' + e + "</div>");
                    return i.attr("data-swiper-slide-index") || i.attr("data-swiper-slide-index", t), n.cache && (this.virtual.cache[t] = i), i
                },
                appendSlide: function(e) {
                    if ("object" == typeof e && "length" in e)
                        for (var t = 0; t < e.length; t += 1) e[t] && this.virtual.slides.push(e[t]);
                    else this.virtual.slides.push(e);
                    this.virtual.update(!0)
                },
                prependSlide: function(e) {
                    var t = this.activeIndex,
                        n = t + 1,
                        i = 1;
                    if (Array.isArray(e)) {
                        for (var s = 0; s < e.length; s += 1) e[s] && this.virtual.slides.unshift(e[s]);
                        n = t + e.length, i = e.length
                    } else this.virtual.slides.unshift(e);
                    if (this.params.virtual.cache) {
                        var r = this.virtual.cache,
                            o = {};
                        Object.keys(r).forEach(function(e) {
                            o[parseInt(e, 10) + i] = r[e]
                        }), this.virtual.cache = o
                    }
                    this.virtual.update(!0), this.slideTo(n, 0)
                },
                removeSlide: function(e) {
                    if (null != e) {
                        var t = this.activeIndex;
                        if (Array.isArray(e))
                            for (var n = e.length - 1; 0 <= n; n -= 1) this.virtual.slides.splice(e[n], 1), this.params.virtual.cache && delete this.virtual.cache[e[n]], e[n] < t && (t -= 1), t = Math.max(t, 0);
                        else this.virtual.slides.splice(e, 1), this.params.virtual.cache && delete this.virtual.cache[e], e < t && (t -= 1), t = Math.max(t, 0);
                        this.virtual.update(!0), this.slideTo(t, 0)
                    }
                },
                removeAllSlides: function() {
                    this.virtual.slides = [], this.params.virtual.cache && (this.virtual.cache = {}), this.virtual.update(!0), this.slideTo(0, 0)
                }
            },
            $ = {
                name: "virtual",
                params: {
                    virtual: {
                        enabled: !1,
                        slides: [],
                        cache: !0,
                        renderSlide: null,
                        renderExternal: null,
                        addSlidesBefore: 0,
                        addSlidesAfter: 0
                    }
                },
                create: function() {
                    ee.extend(this, {
                        virtual: {
                            update: D.update.bind(this),
                            appendSlide: D.appendSlide.bind(this),
                            prependSlide: D.prependSlide.bind(this),
                            removeSlide: D.removeSlide.bind(this),
                            removeAllSlides: D.removeAllSlides.bind(this),
                            renderSlide: D.renderSlide.bind(this),
                            slides: this.params.virtual.slides,
                            cache: {}
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        if (this.params.virtual.enabled) {
                            this.classNames.push(this.params.containerModifierClass + "virtual");
                            var e = {
                                watchSlidesProgress: !0
                            };
                            ee.extend(this.params, e), ee.extend(this.originalParams, e), this.params.initialSlide || this.virtual.update()
                        }
                    },
                    setTranslate: function() {
                        this.params.virtual.enabled && this.virtual.update()
                    }
                }
            },
            M = {
                handle: function(e) {
                    var t = this,
                        n = t.rtlTranslate,
                        i = e;
                    i.originalEvent && (i = i.originalEvent);
                    var s = i.keyCode || i.charCode;
                    if (!t.allowSlideNext && (t.isHorizontal() && 39 === s || t.isVertical() && 40 === s)) return !1;
                    if (!t.allowSlidePrev && (t.isHorizontal() && 37 === s || t.isVertical() && 38 === s)) return !1;
                    if (!(i.shiftKey || i.altKey || i.ctrlKey || i.metaKey || m.activeElement && m.activeElement.nodeName && ("input" === m.activeElement.nodeName.toLowerCase() || "textarea" === m.activeElement.nodeName.toLowerCase()))) {
                        if (t.params.keyboard.onlyInViewport && (37 === s || 39 === s || 38 === s || 40 === s)) {
                            var r = !1;
                            if (0 < t.$el.parents("." + t.params.slideClass).length && 0 === t.$el.parents("." + t.params.slideActiveClass).length) return;
                            var o = J.innerWidth,
                                a = J.innerHeight,
                                l = t.$el.offset();
                            n && (l.left -= t.$el[0].scrollLeft);
                            for (var c = [
                                    [l.left, l.top],
                                    [l.left + t.width, l.top],
                                    [l.left, l.top + t.height],
                                    [l.left + t.width, l.top + t.height]
                                ], d = 0; d < c.length; d += 1) {
                                var u = c[d];
                                0 <= u[0] && u[0] <= o && 0 <= u[1] && u[1] <= a && (r = !0)
                            }
                            if (!r) return
                        }
                        t.isHorizontal() ? (37 !== s && 39 !== s || (i.preventDefault ? i.preventDefault() : i.returnValue = !1), (39 === s && !n || 37 === s && n) && t.slideNext(), (37 === s && !n || 39 === s && n) && t.slidePrev()) : (38 !== s && 40 !== s || (i.preventDefault ? i.preventDefault() : i.returnValue = !1), 40 === s && t.slideNext(), 38 === s && t.slidePrev()), t.emit("keyPress", s)
                    }
                },
                enable: function() {
                    this.keyboard.enabled || (A(m).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0)
                },
                disable: function() {
                    this.keyboard.enabled && (A(m).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1)
                }
            },
            I = {
                name: "keyboard",
                params: {
                    keyboard: {
                        enabled: !1,
                        onlyInViewport: !0
                    }
                },
                create: function() {
                    ee.extend(this, {
                        keyboard: {
                            enabled: !1,
                            enable: M.enable.bind(this),
                            disable: M.disable.bind(this),
                            handle: M.handle.bind(this)
                        }
                    })
                },
                on: {
                    init: function() {
                        this.params.keyboard.enabled && this.keyboard.enable()
                    },
                    destroy: function() {
                        this.keyboard.enabled && this.keyboard.disable()
                    }
                }
            },
            P = {
                lastScrollTime: ee.now(),
                event: -1 < J.navigator.userAgent.indexOf("firefox") ? "DOMMouseScroll" : function() {
                    var e = "onwheel",
                        t = e in m;
                    if (!t) {
                        var n = m.createElement("div");
                        n.setAttribute(e, "return;"), t = "function" == typeof n[e]
                    }
                    return !t && m.implementation && m.implementation.hasFeature && !0 !== m.implementation.hasFeature("", "") && (t = m.implementation.hasFeature("Events.wheel", "3.0")), t
                }() ? "wheel" : "mousewheel",
                normalize: function(e) {
                    var t = 0,
                        n = 0,
                        i = 0,
                        s = 0;
                    return "detail" in e && (n = e.detail), "wheelDelta" in e && (n = -e.wheelDelta / 120), "wheelDeltaY" in e && (n = -e.wheelDeltaY / 120), "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120), "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = n, n = 0), i = 10 * t, s = 10 * n, "deltaY" in e && (s = e.deltaY), "deltaX" in e && (i = e.deltaX), (i || s) && e.deltaMode && (1 === e.deltaMode ? (i *= 40, s *= 40) : (i *= 800, s *= 800)), i && !t && (t = i < 1 ? -1 : 1), s && !n && (n = s < 1 ? -1 : 1), {
                        spinX: t,
                        spinY: n,
                        pixelX: i,
                        pixelY: s
                    }
                },
                handleMouseEnter: function() {
                    this.mouseEntered = !0
                },
                handleMouseLeave: function() {
                    this.mouseEntered = !1
                },
                handle: function(e) {
                    var t = e,
                        n = this,
                        i = n.params.mousewheel;
                    if (!n.mouseEntered && !i.releaseOnEdges) return !0;
                    t.originalEvent && (t = t.originalEvent);
                    var s = 0,
                        r = n.rtlTranslate ? -1 : 1,
                        o = P.normalize(t);
                    if (i.forceToAxis)
                        if (n.isHorizontal()) {
                            if (!(Math.abs(o.pixelX) > Math.abs(o.pixelY))) return !0;
                            s = o.pixelX * r
                        } else {
                            if (!(Math.abs(o.pixelY) > Math.abs(o.pixelX))) return !0;
                            s = o.pixelY
                        }
                    else s = Math.abs(o.pixelX) > Math.abs(o.pixelY) ? -o.pixelX * r : -o.pixelY;
                    if (0 === s) return !0;
                    if (i.invert && (s = -s), n.params.freeMode) {
                        n.params.loop && n.loopFix();
                        var a = n.getTranslate() + s * i.sensitivity,
                            l = n.isBeginning,
                            c = n.isEnd;
                        if (a >= n.minTranslate() && (a = n.minTranslate()), a <= n.maxTranslate() && (a = n.maxTranslate()), n.setTransition(0), n.setTranslate(a), n.updateProgress(), n.updateActiveIndex(), n.updateSlidesClasses(), (!l && n.isBeginning || !c && n.isEnd) && n.updateSlidesClasses(), n.params.freeModeSticky && (clearTimeout(n.mousewheel.timeout), n.mousewheel.timeout = ee.nextTick(function() {
                                n.slideToClosest()
                            }, 300)), n.emit("scroll", t), n.params.autoplay && n.params.autoplayDisableOnInteraction && n.autoplay.stop(), a === n.minTranslate() || a === n.maxTranslate()) return !0
                    } else {
                        if (60 < ee.now() - n.mousewheel.lastScrollTime)
                            if (s < 0)
                                if (n.isEnd && !n.params.loop || n.animating) {
                                    if (i.releaseOnEdges) return !0
                                } else n.slideNext(), n.emit("scroll", t);
                        else if (n.isBeginning && !n.params.loop || n.animating) {
                            if (i.releaseOnEdges) return !0
                        } else n.slidePrev(), n.emit("scroll", t);
                        n.mousewheel.lastScrollTime = (new J.Date).getTime()
                    }
                    return t.preventDefault ? t.preventDefault() : t.returnValue = !1, !1
                },
                enable: function() {
                    if (!P.event) return !1;
                    if (this.mousewheel.enabled) return !1;
                    var e = this.$el;
                    return "container" !== this.params.mousewheel.eventsTarged && (e = A(this.params.mousewheel.eventsTarged)), e.on("mouseenter", this.mousewheel.handleMouseEnter), e.on("mouseleave", this.mousewheel.handleMouseLeave), e.on(P.event, this.mousewheel.handle), this.mousewheel.enabled = !0
                },
                disable: function() {
                    if (!P.event) return !1;
                    if (!this.mousewheel.enabled) return !1;
                    var e = this.$el;
                    return "container" !== this.params.mousewheel.eventsTarged && (e = A(this.params.mousewheel.eventsTarged)), e.off(P.event, this.mousewheel.handle), !(this.mousewheel.enabled = !1)
                }
            },
            L = {
                update: function() {
                    var e = this.params.navigation;
                    if (!this.params.loop) {
                        var t = this.navigation,
                            n = t.$nextEl,
                            i = t.$prevEl;
                        i && 0 < i.length && (this.isBeginning ? i.addClass(e.disabledClass) : i.removeClass(e.disabledClass), i[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass)), n && 0 < n.length && (this.isEnd ? n.addClass(e.disabledClass) : n.removeClass(e.disabledClass), n[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass))
                    }
                },
                onPrevClick: function(e) {
                    e.preventDefault(), this.isBeginning && !this.params.loop || this.slidePrev()
                },
                onNextClick: function(e) {
                    e.preventDefault(), this.isEnd && !this.params.loop || this.slideNext()
                },
                init: function() {
                    var e, t, n = this,
                        i = n.params.navigation;
                    (i.nextEl || i.prevEl) && (i.nextEl && (e = A(i.nextEl), n.params.uniqueNavElements && "string" == typeof i.nextEl && 1 < e.length && 1 === n.$el.find(i.nextEl).length && (e = n.$el.find(i.nextEl))), i.prevEl && (t = A(i.prevEl), n.params.uniqueNavElements && "string" == typeof i.prevEl && 1 < t.length && 1 === n.$el.find(i.prevEl).length && (t = n.$el.find(i.prevEl))), e && 0 < e.length && e.on("click", n.navigation.onNextClick), t && 0 < t.length && t.on("click", n.navigation.onPrevClick), ee.extend(n.navigation, {
                        $nextEl: e,
                        nextEl: e && e[0],
                        $prevEl: t,
                        prevEl: t && t[0]
                    }))
                },
                destroy: function() {
                    var e = this.navigation,
                        t = e.$nextEl,
                        n = e.$prevEl;
                    t && t.length && (t.off("click", this.navigation.onNextClick), t.removeClass(this.params.navigation.disabledClass)), n && n.length && (n.off("click", this.navigation.onPrevClick), n.removeClass(this.params.navigation.disabledClass))
                }
            },
            N = {
                update: function() {
                    var e = this,
                        t = e.rtl,
                        s = e.params.pagination;
                    if (s.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                        var r, n = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
                            i = e.pagination.$el,
                            o = e.params.loop ? Math.ceil((n - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length;
                        if (e.params.loop ? ((r = Math.ceil((e.activeIndex - e.loopedSlides) / e.params.slidesPerGroup)) > n - 1 - 2 * e.loopedSlides && (r -= n - 2 * e.loopedSlides), o - 1 < r && (r -= o), r < 0 && "bullets" !== e.params.paginationType && (r = o + r)) : r = void 0 !== e.snapIndex ? e.snapIndex : e.activeIndex || 0, "bullets" === s.type && e.pagination.bullets && 0 < e.pagination.bullets.length) {
                            var a, l, c, d = e.pagination.bullets;
                            if (s.dynamicBullets && (e.pagination.bulletSize = d.eq(0)[e.isHorizontal() ? "outerWidth" : "outerHeight"](!0), i.css(e.isHorizontal() ? "width" : "height", e.pagination.bulletSize * (s.dynamicMainBullets + 4) + "px"), 1 < s.dynamicMainBullets && void 0 !== e.previousIndex && (e.pagination.dynamicBulletIndex += r - e.previousIndex, e.pagination.dynamicBulletIndex > s.dynamicMainBullets - 1 ? e.pagination.dynamicBulletIndex = s.dynamicMainBullets - 1 : e.pagination.dynamicBulletIndex < 0 && (e.pagination.dynamicBulletIndex = 0)), a = r - e.pagination.dynamicBulletIndex, c = ((l = a + (Math.min(d.length, s.dynamicMainBullets) - 1)) + a) / 2), d.removeClass(s.bulletActiveClass + " " + s.bulletActiveClass + "-next " + s.bulletActiveClass + "-next-next " + s.bulletActiveClass + "-prev " + s.bulletActiveClass + "-prev-prev " + s.bulletActiveClass + "-main"), 1 < i.length) d.each(function(e, t) {
                                var n = A(t),
                                    i = n.index();
                                i === r && n.addClass(s.bulletActiveClass), s.dynamicBullets && (a <= i && i <= l && n.addClass(s.bulletActiveClass + "-main"), i === a && n.prev().addClass(s.bulletActiveClass + "-prev").prev().addClass(s.bulletActiveClass + "-prev-prev"), i === l && n.next().addClass(s.bulletActiveClass + "-next").next().addClass(s.bulletActiveClass + "-next-next"))
                            });
                            else if (d.eq(r).addClass(s.bulletActiveClass), s.dynamicBullets) {
                                for (var u = d.eq(a), h = d.eq(l), p = a; p <= l; p += 1) d.eq(p).addClass(s.bulletActiveClass + "-main");
                                u.prev().addClass(s.bulletActiveClass + "-prev").prev().addClass(s.bulletActiveClass + "-prev-prev"), h.next().addClass(s.bulletActiveClass + "-next").next().addClass(s.bulletActiveClass + "-next-next")
                            }
                            if (s.dynamicBullets) {
                                var f = Math.min(d.length, s.dynamicMainBullets + 4),
                                    m = (e.pagination.bulletSize * f - e.pagination.bulletSize) / 2 - c * e.pagination.bulletSize,
                                    g = t ? "right" : "left";
                                d.css(e.isHorizontal() ? g : "top", m + "px")
                            }
                        }
                        if ("fraction" === s.type && (i.find("." + s.currentClass).text(s.formatFractionCurrent(r + 1)), i.find("." + s.totalClass).text(s.formatFractionTotal(o))), "progressbar" === s.type) {
                            var v;
                            v = s.progressbarOpposite ? e.isHorizontal() ? "vertical" : "horizontal" : e.isHorizontal() ? "horizontal" : "vertical";
                            var y = (r + 1) / o,
                                b = 1,
                                w = 1;
                            "horizontal" == v ? b = y : w = y, i.find("." + s.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + b + ") scaleY(" + w + ")").transition(e.params.speed)
                        }
                        "custom" === s.type && s.renderCustom ? (i.html(s.renderCustom(e, r + 1, o)), e.emit("paginationRender", e, i[0])) : e.emit("paginationUpdate", e, i[0]), i[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](s.lockClass)
                    }
                },
                render: function() {
                    var e = this,
                        t = e.params.pagination;
                    if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
                        var n = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
                            i = e.pagination.$el,
                            s = "";
                        if ("bullets" === t.type) {
                            for (var r = e.params.loop ? Math.ceil((n - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length, o = 0; o < r; o += 1) t.renderBullet ? s += t.renderBullet.call(e, o, t.bulletClass) : s += "<" + t.bulletElement + ' class="' + t.bulletClass + '"></' + t.bulletElement + ">";
                            i.html(s), e.pagination.bullets = i.find("." + t.bulletClass)
                        }
                        "fraction" === t.type && (s = t.renderFraction ? t.renderFraction.call(e, t.currentClass, t.totalClass) : '<span class="' + t.currentClass + '"></span> / <span class="' + t.totalClass + '"></span>', i.html(s)), "progressbar" === t.type && (s = t.renderProgressbar ? t.renderProgressbar.call(e, t.progressbarFillClass) : '<span class="' + t.progressbarFillClass + '"></span>', i.html(s)), "custom" !== t.type && e.emit("paginationRender", e.pagination.$el[0])
                    }
                },
                init: function() {
                    var n = this,
                        e = n.params.pagination;
                    if (e.el) {
                        var t = A(e.el);
                        0 !== t.length && (n.params.uniqueNavElements && "string" == typeof e.el && 1 < t.length && 1 === n.$el.find(e.el).length && (t = n.$el.find(e.el)), "bullets" === e.type && e.clickable && t.addClass(e.clickableClass), t.addClass(e.modifierClass + e.type), "bullets" === e.type && e.dynamicBullets && (t.addClass("" + e.modifierClass + e.type + "-dynamic"), n.pagination.dynamicBulletIndex = 0, e.dynamicMainBullets < 1 && (e.dynamicMainBullets = 1)), "progressbar" === e.type && e.progressbarOpposite && t.addClass(e.progressbarOppositeClass), e.clickable && t.on("click", "." + e.bulletClass, function(e) {
                            e.preventDefault();
                            var t = A(this).index() * n.params.slidesPerGroup;
                            n.params.loop && (t += n.loopedSlides), n.slideTo(t)
                        }), ee.extend(n.pagination, {
                            $el: t,
                            el: t[0]
                        }))
                    }
                },
                destroy: function() {
                    var e = this.params.pagination;
                    if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                        var t = this.pagination.$el;
                        t.removeClass(e.hiddenClass), t.removeClass(e.modifierClass + e.type), this.pagination.bullets && this.pagination.bullets.removeClass(e.bulletActiveClass), e.clickable && t.off("click", "." + e.bulletClass)
                    }
                }
            },
            j = {
                setTranslate: function() {
                    if (this.params.scrollbar.el && this.scrollbar.el) {
                        var e = this.scrollbar,
                            t = this.rtlTranslate,
                            n = this.progress,
                            i = e.dragSize,
                            s = e.trackSize,
                            r = e.$dragEl,
                            o = e.$el,
                            a = this.params.scrollbar,
                            l = i,
                            c = (s - i) * n;
                        t ? 0 < (c = -c) ? (l = i - c, c = 0) : s < -c + i && (l = s + c) : c < 0 ? (l = i + c, c = 0) : s < c + i && (l = s - c), this.isHorizontal() ? (te.transforms3d ? r.transform("translate3d(" + c + "px, 0, 0)") : r.transform("translateX(" + c + "px)"), r[0].style.width = l + "px") : (te.transforms3d ? r.transform("translate3d(0px, " + c + "px, 0)") : r.transform("translateY(" + c + "px)"), r[0].style.height = l + "px"), a.hide && (clearTimeout(this.scrollbar.timeout), o[0].style.opacity = 1, this.scrollbar.timeout = setTimeout(function() {
                            o[0].style.opacity = 0, o.transition(400)
                        }, 1e3))
                    }
                },
                setTransition: function(e) {
                    this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e)
                },
                updateSize: function() {
                    var e = this;
                    if (e.params.scrollbar.el && e.scrollbar.el) {
                        var t = e.scrollbar,
                            n = t.$dragEl,
                            i = t.$el;
                        n[0].style.width = "", n[0].style.height = "";
                        var s, r = e.isHorizontal() ? i[0].offsetWidth : i[0].offsetHeight,
                            o = e.size / e.virtualSize,
                            a = o * (r / e.size);
                        s = "auto" === e.params.scrollbar.dragSize ? r * o : parseInt(e.params.scrollbar.dragSize, 10), e.isHorizontal() ? n[0].style.width = s + "px" : n[0].style.height = s + "px", i[0].style.display = 1 <= o ? "none" : "", e.params.scrollbar.hide && (i[0].style.opacity = 0), ee.extend(t, {
                            trackSize: r,
                            divider: o,
                            moveDivider: a,
                            dragSize: s
                        }), t.$el[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](e.params.scrollbar.lockClass)
                    }
                },
                setDragPosition: function(e) {
                    var t, n = this,
                        i = n.scrollbar,
                        s = n.rtlTranslate,
                        r = i.$el,
                        o = i.dragSize,
                        a = i.trackSize;
                    t = ((n.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY) - r.offset()[n.isHorizontal() ? "left" : "top"] - o / 2) / (a - o), t = Math.max(Math.min(t, 1), 0), s && (t = 1 - t);
                    var l = n.minTranslate() + (n.maxTranslate() - n.minTranslate()) * t;
                    n.updateProgress(l), n.setTranslate(l), n.updateActiveIndex(), n.updateSlidesClasses()
                },
                onDragStart: function(e) {
                    var t = this.params.scrollbar,
                        n = this.scrollbar,
                        i = this.$wrapperEl,
                        s = n.$el,
                        r = n.$dragEl;
                    this.scrollbar.isTouched = !0, e.preventDefault(), e.stopPropagation(), i.transition(100), r.transition(100), n.setDragPosition(e), clearTimeout(this.scrollbar.dragTimeout), s.transition(0), t.hide && s.css("opacity", 1), this.emit("scrollbarDragStart", e)
                },
                onDragMove: function(e) {
                    var t = this.scrollbar,
                        n = this.$wrapperEl,
                        i = t.$el,
                        s = t.$dragEl;
                    this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), n.transition(0), i.transition(0), s.transition(0), this.emit("scrollbarDragMove", e))
                },
                onDragEnd: function(e) {
                    var t = this.params.scrollbar,
                        n = this.scrollbar.$el;
                    this.scrollbar.isTouched && (this.scrollbar.isTouched = !1, t.hide && (clearTimeout(this.scrollbar.dragTimeout), this.scrollbar.dragTimeout = ee.nextTick(function() {
                        n.css("opacity", 0), n.transition(400)
                    }, 1e3)), this.emit("scrollbarDragEnd", e), t.snapOnRelease && this.slideToClosest())
                },
                enableDraggable: function() {
                    var e = this;
                    if (e.params.scrollbar.el) {
                        var t = e.scrollbar,
                            n = e.touchEventsTouch,
                            i = e.touchEventsDesktop,
                            s = e.params,
                            r = t.$el[0],
                            o = !(!te.passiveListener || !s.passiveListeners) && {
                                passive: !1,
                                capture: !1
                            },
                            a = !(!te.passiveListener || !s.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        te.touch ? (r.addEventListener(n.start, e.scrollbar.onDragStart, o), r.addEventListener(n.move, e.scrollbar.onDragMove, o), r.addEventListener(n.end, e.scrollbar.onDragEnd, a)) : (r.addEventListener(i.start, e.scrollbar.onDragStart, o), m.addEventListener(i.move, e.scrollbar.onDragMove, o), m.addEventListener(i.end, e.scrollbar.onDragEnd, a))
                    }
                },
                disableDraggable: function() {
                    var e = this;
                    if (e.params.scrollbar.el) {
                        var t = e.scrollbar,
                            n = e.touchEventsTouch,
                            i = e.touchEventsDesktop,
                            s = e.params,
                            r = t.$el[0],
                            o = !(!te.passiveListener || !s.passiveListeners) && {
                                passive: !1,
                                capture: !1
                            },
                            a = !(!te.passiveListener || !s.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        te.touch ? (r.removeEventListener(n.start, e.scrollbar.onDragStart, o), r.removeEventListener(n.move, e.scrollbar.onDragMove, o), r.removeEventListener(n.end, e.scrollbar.onDragEnd, a)) : (r.removeEventListener(i.start, e.scrollbar.onDragStart, o), m.removeEventListener(i.move, e.scrollbar.onDragMove, o), m.removeEventListener(i.end, e.scrollbar.onDragEnd, a))
                    }
                },
                init: function() {
                    if (this.params.scrollbar.el) {
                        var e = this.scrollbar,
                            t = this.$el,
                            n = this.params.scrollbar,
                            i = A(n.el);
                        this.params.uniqueNavElements && "string" == typeof n.el && 1 < i.length && 1 === t.find(n.el).length && (i = t.find(n.el));
                        var s = i.find("." + this.params.scrollbar.dragClass);
                        0 === s.length && (s = A('<div class="' + this.params.scrollbar.dragClass + '"></div>'), i.append(s)), ee.extend(e, {
                            $el: i,
                            el: i[0],
                            $dragEl: s,
                            dragEl: s[0]
                        }), n.draggable && e.enableDraggable()
                    }
                },
                destroy: function() {
                    this.scrollbar.disableDraggable()
                }
            },
            H = {
                setTransform: function(e, t) {
                    var n = this.rtl,
                        i = A(e),
                        s = n ? -1 : 1,
                        r = i.attr("data-swiper-parallax") || "0",
                        o = i.attr("data-swiper-parallax-x"),
                        a = i.attr("data-swiper-parallax-y"),
                        l = i.attr("data-swiper-parallax-scale"),
                        c = i.attr("data-swiper-parallax-opacity");
                    if (o || a ? (o = o || "0", a = a || "0") : this.isHorizontal() ? (o = r, a = "0") : (a = r, o = "0"), o = 0 <= o.indexOf("%") ? parseInt(o, 10) * t * s + "%" : o * t * s + "px", a = 0 <= a.indexOf("%") ? parseInt(a, 10) * t + "%" : a * t + "px", null != c) {
                        var d = c - (c - 1) * (1 - Math.abs(t));
                        i[0].style.opacity = d
                    }
                    if (null == l) i.transform("translate3d(" + o + ", " + a + ", 0px)");
                    else {
                        var u = l - (l - 1) * (1 - Math.abs(t));
                        i.transform("translate3d(" + o + ", " + a + ", 0px) scale(" + u + ")")
                    }
                },
                setTranslate: function() {
                    var i = this,
                        e = i.$el,
                        t = i.slides,
                        s = i.progress,
                        r = i.snapGrid;
                    e.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(e, t) {
                        i.parallax.setTransform(t, s)
                    }), t.each(function(e, t) {
                        var n = t.progress;
                        1 < i.params.slidesPerGroup && "auto" !== i.params.slidesPerView && (n += Math.ceil(e / 2) - s * (r.length - 1)), n = Math.min(Math.max(n, -1), 1), A(t).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(e, t) {
                            i.parallax.setTransform(t, n)
                        })
                    })
                },
                setTransition: function(s) {
                    void 0 === s && (s = this.params.speed), this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(e, t) {
                        var n = A(t),
                            i = parseInt(n.attr("data-swiper-parallax-duration"), 10) || s;
                        0 === s && (i = 0), n.transition(i)
                    })
                }
            },
            z = {
                getDistanceBetweenTouches: function(e) {
                    if (e.targetTouches.length < 2) return 1;
                    var t = e.targetTouches[0].pageX,
                        n = e.targetTouches[0].pageY,
                        i = e.targetTouches[1].pageX,
                        s = e.targetTouches[1].pageY;
                    return Math.sqrt(Math.pow(i - t, 2) + Math.pow(s - n, 2))
                },
                onGestureStart: function(e) {
                    var t = this.params.zoom,
                        n = this.zoom,
                        i = n.gesture;
                    if (n.fakeGestureTouched = !1, n.fakeGestureMoved = !1, !te.gestures) {
                        if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2) return;
                        n.fakeGestureTouched = !0, i.scaleStart = z.getDistanceBetweenTouches(e)
                    }
                    i.$slideEl && i.$slideEl.length || (i.$slideEl = A(e.target).closest(".swiper-slide"), 0 === i.$slideEl.length && (i.$slideEl = this.slides.eq(this.activeIndex)), i.$imageEl = i.$slideEl.find("img, svg, canvas"), i.$imageWrapEl = i.$imageEl.parent("." + t.containerClass), i.maxRatio = i.$imageWrapEl.attr("data-swiper-zoom") || t.maxRatio, 0 !== i.$imageWrapEl.length) ? (i.$imageEl.transition(0), this.zoom.isScaling = !0) : i.$imageEl = void 0
                },
                onGestureChange: function(e) {
                    var t = this.params.zoom,
                        n = this.zoom,
                        i = n.gesture;
                    if (!te.gestures) {
                        if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2) return;
                        n.fakeGestureMoved = !0, i.scaleMove = z.getDistanceBetweenTouches(e)
                    }
                    i.$imageEl && 0 !== i.$imageEl.length && (n.scale = te.gestures ? e.scale * n.currentScale : i.scaleMove / i.scaleStart * n.currentScale, n.scale > i.maxRatio && (n.scale = i.maxRatio - 1 + Math.pow(n.scale - i.maxRatio + 1, .5)), n.scale < t.minRatio && (n.scale = t.minRatio + 1 - Math.pow(t.minRatio - n.scale + 1, .5)), i.$imageEl.transform("translate3d(0,0,0) scale(" + n.scale + ")"))
                },
                onGestureEnd: function(e) {
                    var t = this.params.zoom,
                        n = this.zoom,
                        i = n.gesture;
                    if (!te.gestures) {
                        if (!n.fakeGestureTouched || !n.fakeGestureMoved) return;
                        if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !g.android) return;
                        n.fakeGestureTouched = !1, n.fakeGestureMoved = !1
                    }
                    i.$imageEl && 0 !== i.$imageEl.length && (n.scale = Math.max(Math.min(n.scale, i.maxRatio), t.minRatio), i.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + n.scale + ")"), n.currentScale = n.scale, n.isScaling = !1, 1 === n.scale && (i.$slideEl = void 0))
                },
                onTouchStart: function(e) {
                    var t = this.zoom,
                        n = t.gesture,
                        i = t.image;
                    n.$imageEl && 0 !== n.$imageEl.length && (i.isTouched || (g.android && e.preventDefault(), i.isTouched = !0, i.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, i.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY))
                },
                onTouchMove: function(e) {
                    var t = this.zoom,
                        n = t.gesture,
                        i = t.image,
                        s = t.velocity;
                    if (n.$imageEl && 0 !== n.$imageEl.length && (this.allowClick = !1, i.isTouched && n.$slideEl)) {
                        i.isMoved || (i.width = n.$imageEl[0].offsetWidth, i.height = n.$imageEl[0].offsetHeight, i.startX = ee.getTranslate(n.$imageWrapEl[0], "x") || 0, i.startY = ee.getTranslate(n.$imageWrapEl[0], "y") || 0, n.slideWidth = n.$slideEl[0].offsetWidth, n.slideHeight = n.$slideEl[0].offsetHeight, n.$imageWrapEl.transition(0), this.rtl && (i.startX = -i.startX, i.startY = -i.startY));
                        var r = i.width * t.scale,
                            o = i.height * t.scale;
                        if (!(r < n.slideWidth && o < n.slideHeight)) {
                            if (i.minX = Math.min(n.slideWidth / 2 - r / 2, 0), i.maxX = -i.minX, i.minY = Math.min(n.slideHeight / 2 - o / 2, 0), i.maxY = -i.minY, i.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, i.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !i.isMoved && !t.isScaling) {
                                if (this.isHorizontal() && (Math.floor(i.minX) === Math.floor(i.startX) && i.touchesCurrent.x < i.touchesStart.x || Math.floor(i.maxX) === Math.floor(i.startX) && i.touchesCurrent.x > i.touchesStart.x)) return void(i.isTouched = !1);
                                if (!this.isHorizontal() && (Math.floor(i.minY) === Math.floor(i.startY) && i.touchesCurrent.y < i.touchesStart.y || Math.floor(i.maxY) === Math.floor(i.startY) && i.touchesCurrent.y > i.touchesStart.y)) return void(i.isTouched = !1)
                            }
                            e.preventDefault(), e.stopPropagation(), i.isMoved = !0, i.currentX = i.touchesCurrent.x - i.touchesStart.x + i.startX, i.currentY = i.touchesCurrent.y - i.touchesStart.y + i.startY, i.currentX < i.minX && (i.currentX = i.minX + 1 - Math.pow(i.minX - i.currentX + 1, .8)), i.currentX > i.maxX && (i.currentX = i.maxX - 1 + Math.pow(i.currentX - i.maxX + 1, .8)), i.currentY < i.minY && (i.currentY = i.minY + 1 - Math.pow(i.minY - i.currentY + 1, .8)), i.currentY > i.maxY && (i.currentY = i.maxY - 1 + Math.pow(i.currentY - i.maxY + 1, .8)), s.prevPositionX || (s.prevPositionX = i.touchesCurrent.x), s.prevPositionY || (s.prevPositionY = i.touchesCurrent.y), s.prevTime || (s.prevTime = Date.now()), s.x = (i.touchesCurrent.x - s.prevPositionX) / (Date.now() - s.prevTime) / 2, s.y = (i.touchesCurrent.y - s.prevPositionY) / (Date.now() - s.prevTime) / 2, Math.abs(i.touchesCurrent.x - s.prevPositionX) < 2 && (s.x = 0), Math.abs(i.touchesCurrent.y - s.prevPositionY) < 2 && (s.y = 0), s.prevPositionX = i.touchesCurrent.x, s.prevPositionY = i.touchesCurrent.y, s.prevTime = Date.now(), n.$imageWrapEl.transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
                        }
                    }
                },
                onTouchEnd: function() {
                    var e = this.zoom,
                        t = e.gesture,
                        n = e.image,
                        i = e.velocity;
                    if (t.$imageEl && 0 !== t.$imageEl.length) {
                        if (!n.isTouched || !n.isMoved) return n.isTouched = !1, void(n.isMoved = !1);
                        n.isTouched = !1, n.isMoved = !1;
                        var s = 300,
                            r = 300,
                            o = i.x * s,
                            a = n.currentX + o,
                            l = i.y * r,
                            c = n.currentY + l;
                        0 !== i.x && (s = Math.abs((a - n.currentX) / i.x)), 0 !== i.y && (r = Math.abs((c - n.currentY) / i.y));
                        var d = Math.max(s, r);
                        n.currentX = a, n.currentY = c;
                        var u = n.width * e.scale,
                            h = n.height * e.scale;
                        n.minX = Math.min(t.slideWidth / 2 - u / 2, 0), n.maxX = -n.minX, n.minY = Math.min(t.slideHeight / 2 - h / 2, 0), n.maxY = -n.minY, n.currentX = Math.max(Math.min(n.currentX, n.maxX), n.minX), n.currentY = Math.max(Math.min(n.currentY, n.maxY), n.minY), t.$imageWrapEl.transition(d).transform("translate3d(" + n.currentX + "px, " + n.currentY + "px,0)")
                    }
                },
                onTransitionEnd: function() {
                    var e = this.zoom,
                        t = e.gesture;
                    t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"), t.$imageWrapEl.transform("translate3d(0,0,0)"), e.scale = 1, e.currentScale = 1, t.$slideEl = void 0, t.$imageEl = void 0, t.$imageWrapEl = void 0)
                },
                toggle: function(e) {
                    var t = this.zoom;
                    t.scale && 1 !== t.scale ? t.out() : t.in(e)
                },
                in: function(e) {
                    var t, n, i, s, r, o, a, l, c, d, u, h, p, f, m, g, v = this.zoom,
                        y = this.params.zoom,
                        b = v.gesture,
                        w = v.image;
                    b.$slideEl || (b.$slideEl = this.clickedSlide ? A(this.clickedSlide) : this.slides.eq(this.activeIndex), b.$imageEl = b.$slideEl.find("img, svg, canvas"), b.$imageWrapEl = b.$imageEl.parent("." + y.containerClass)), b.$imageEl && 0 !== b.$imageEl.length && (b.$slideEl.addClass("" + y.zoomedSlideClass), n = void 0 === w.touchesStart.x && e ? (t = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX, "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (t = w.touchesStart.x, w.touchesStart.y), v.scale = b.$imageWrapEl.attr("data-swiper-zoom") || y.maxRatio, v.currentScale = b.$imageWrapEl.attr("data-swiper-zoom") || y.maxRatio, e ? (m = b.$slideEl[0].offsetWidth, g = b.$slideEl[0].offsetHeight, i = b.$slideEl.offset().left + m / 2 - t, s = b.$slideEl.offset().top + g / 2 - n, a = b.$imageEl[0].offsetWidth, l = b.$imageEl[0].offsetHeight, c = a * v.scale, d = l * v.scale, p = -(u = Math.min(m / 2 - c / 2, 0)), f = -(h = Math.min(g / 2 - d / 2, 0)), (r = i * v.scale) < u && (r = u), p < r && (r = p), (o = s * v.scale) < h && (o = h), f < o && (o = f)) : o = r = 0, b.$imageWrapEl.transition(300).transform("translate3d(" + r + "px, " + o + "px,0)"), b.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + v.scale + ")"))
                },
                out: function() {
                    var e = this.zoom,
                        t = this.params.zoom,
                        n = e.gesture;
                    n.$slideEl || (n.$slideEl = this.clickedSlide ? A(this.clickedSlide) : this.slides.eq(this.activeIndex), n.$imageEl = n.$slideEl.find("img, svg, canvas"), n.$imageWrapEl = n.$imageEl.parent("." + t.containerClass)), n.$imageEl && 0 !== n.$imageEl.length && (e.scale = 1, e.currentScale = 1, n.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), n.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), n.$slideEl.removeClass("" + t.zoomedSlideClass), n.$slideEl = void 0)
                },
                enable: function() {
                    var e = this,
                        t = e.zoom;
                    if (!t.enabled) {
                        t.enabled = !0;
                        var n = !("touchstart" !== e.touchEvents.start || !te.passiveListener || !e.params.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        te.gestures ? (e.$wrapperEl.on("gesturestart", ".swiper-slide", t.onGestureStart, n), e.$wrapperEl.on("gesturechange", ".swiper-slide", t.onGestureChange, n), e.$wrapperEl.on("gestureend", ".swiper-slide", t.onGestureEnd, n)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.on(e.touchEvents.start, ".swiper-slide", t.onGestureStart, n), e.$wrapperEl.on(e.touchEvents.move, ".swiper-slide", t.onGestureChange, n), e.$wrapperEl.on(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, n)), e.$wrapperEl.on(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                    }
                },
                disable: function() {
                    var e = this,
                        t = e.zoom;
                    if (t.enabled) {
                        e.zoom.enabled = !1;
                        var n = !("touchstart" !== e.touchEvents.start || !te.passiveListener || !e.params.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        };
                        te.gestures ? (e.$wrapperEl.off("gesturestart", ".swiper-slide", t.onGestureStart, n), e.$wrapperEl.off("gesturechange", ".swiper-slide", t.onGestureChange, n), e.$wrapperEl.off("gestureend", ".swiper-slide", t.onGestureEnd, n)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.off(e.touchEvents.start, ".swiper-slide", t.onGestureStart, n), e.$wrapperEl.off(e.touchEvents.move, ".swiper-slide", t.onGestureChange, n), e.$wrapperEl.off(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, n)), e.$wrapperEl.off(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove)
                    }
                }
            },
            q = {
                loadInSlide: function(e, l) {
                    void 0 === l && (l = !0);
                    var c = this,
                        d = c.params.lazy;
                    if (void 0 !== e && 0 !== c.slides.length) {
                        var u = c.virtual && c.params.virtual.enabled ? c.$wrapperEl.children("." + c.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : c.slides.eq(e),
                            t = u.find("." + d.elementClass + ":not(." + d.loadedClass + "):not(." + d.loadingClass + ")");
                        !u.hasClass(d.elementClass) || u.hasClass(d.loadedClass) || u.hasClass(d.loadingClass) || (t = t.add(u[0])), 0 !== t.length && t.each(function(e, t) {
                            var i = A(t);
                            i.addClass(d.loadingClass);
                            var s = i.attr("data-background"),
                                r = i.attr("data-src"),
                                o = i.attr("data-srcset"),
                                a = i.attr("data-sizes");
                            c.loadImage(i[0], r || s, o, a, !1, function() {
                                if (null != c && c && (!c || c.params) && !c.destroyed) {
                                    if (s ? (i.css("background-image", 'url("' + s + '")'), i.removeAttr("data-background")) : (o && (i.attr("srcset", o), i.removeAttr("data-srcset")), a && (i.attr("sizes", a), i.removeAttr("data-sizes")), r && (i.attr("src", r), i.removeAttr("data-src"))), i.addClass(d.loadedClass).removeClass(d.loadingClass), u.find("." + d.preloaderClass).remove(), c.params.loop && l) {
                                        var e = u.attr("data-swiper-slide-index");
                                        if (u.hasClass(c.params.slideDuplicateClass)) {
                                            var t = c.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + c.params.slideDuplicateClass + ")");
                                            c.lazy.loadInSlide(t.index(), !1)
                                        } else {
                                            var n = c.$wrapperEl.children("." + c.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                                            c.lazy.loadInSlide(n.index(), !1)
                                        }
                                    }
                                    c.emit("lazyImageReady", u[0], i[0])
                                }
                            }), c.emit("lazyImageLoad", u[0], i[0])
                        })
                    }
                },
                load: function() {
                    var i = this,
                        t = i.$wrapperEl,
                        n = i.params,
                        s = i.slides,
                        e = i.activeIndex,
                        r = i.virtual && n.virtual.enabled,
                        o = n.lazy,
                        a = n.slidesPerView;

                    function l(e) {
                        if (r) {
                            if (t.children("." + n.slideClass + '[data-swiper-slide-index="' + e + '"]').length) return !0
                        } else if (s[e]) return !0;
                        return !1
                    }

                    function c(e) {
                        return r ? A(e).attr("data-swiper-slide-index") : A(e).index()
                    }
                    if ("auto" === a && (a = 0), i.lazy.initialImageLoaded || (i.lazy.initialImageLoaded = !0), i.params.watchSlidesVisibility) t.children("." + n.slideVisibleClass).each(function(e, t) {
                        var n = r ? A(t).attr("data-swiper-slide-index") : A(t).index();
                        i.lazy.loadInSlide(n)
                    });
                    else if (1 < a)
                        for (var d = e; d < e + a; d += 1) l(d) && i.lazy.loadInSlide(d);
                    else i.lazy.loadInSlide(e);
                    if (o.loadPrevNext)
                        if (1 < a || o.loadPrevNextAmount && 1 < o.loadPrevNextAmount) {
                            for (var u = o.loadPrevNextAmount, h = a, p = Math.min(e + h + Math.max(u, h), s.length), f = Math.max(e - Math.max(h, u), 0), m = e + a; m < p; m += 1) l(m) && i.lazy.loadInSlide(m);
                            for (var g = f; g < e; g += 1) l(g) && i.lazy.loadInSlide(g)
                        } else {
                            var v = t.children("." + n.slideNextClass);
                            0 < v.length && i.lazy.loadInSlide(c(v));
                            var y = t.children("." + n.slidePrevClass);
                            0 < y.length && i.lazy.loadInSlide(c(y))
                        }
                }
            },
            F = {
                LinearSpline: function(e, t) {
                    var n, i, s, r, o;
                    return this.x = e, this.y = t, this.lastIndex = e.length - 1, this.interpolate = function(e) {
                        return e ? (o = function(e, t) {
                            for (i = -1, n = e.length; 1 < n - i;) e[s = n + i >> 1] <= t ? i = s : n = s;
                            return n
                        }(this.x, e), r = o - 1, (e - this.x[r]) * (this.y[o] - this.y[r]) / (this.x[o] - this.x[r]) + this.y[r]) : 0
                    }, this
                },
                getInterpolateFunction: function(e) {
                    this.controller.spline || (this.controller.spline = this.params.loop ? new F.LinearSpline(this.slidesGrid, e.slidesGrid) : new F.LinearSpline(this.snapGrid, e.snapGrid))
                },
                setTranslate: function(e, t) {
                    var n, i, s = this,
                        r = s.controller.control;

                    function o(e) {
                        var t = s.rtlTranslate ? -s.translate : s.translate;
                        "slide" === s.params.controller.by && (s.controller.getInterpolateFunction(e), i = -s.controller.spline.interpolate(-t)), i && "container" !== s.params.controller.by || (n = (e.maxTranslate() - e.minTranslate()) / (s.maxTranslate() - s.minTranslate()), i = (t - s.minTranslate()) * n + e.minTranslate()), s.params.controller.inverse && (i = e.maxTranslate() - i), e.updateProgress(i), e.setTranslate(i, s), e.updateActiveIndex(), e.updateSlidesClasses()
                    }
                    if (Array.isArray(r))
                        for (var a = 0; a < r.length; a += 1) r[a] !== t && r[a] instanceof x && o(r[a]);
                    else r instanceof x && t !== r && o(r)
                },
                setTransition: function(t, e) {
                    var n, i = this,
                        s = i.controller.control;

                    function r(e) {
                        e.setTransition(t, i), 0 !== t && (e.transitionStart(), e.params.autoHeight && ee.nextTick(function() {
                            e.updateAutoHeight()
                        }), e.$wrapperEl.transitionEnd(function() {
                            s && (e.params.loop && "slide" === i.params.controller.by && e.loopFix(), e.transitionEnd())
                        }))
                    }
                    if (Array.isArray(s))
                        for (n = 0; n < s.length; n += 1) s[n] !== e && s[n] instanceof x && r(s[n]);
                    else s instanceof x && e !== s && r(s)
                }
            },
            R = {
                makeElFocusable: function(e) {
                    return e.attr("tabIndex", "0"), e
                },
                addElRole: function(e, t) {
                    return e.attr("role", t), e
                },
                addElLabel: function(e, t) {
                    return e.attr("aria-label", t), e
                },
                disableEl: function(e) {
                    return e.attr("aria-disabled", !0), e
                },
                enableEl: function(e) {
                    return e.attr("aria-disabled", !1), e
                },
                onEnterKey: function(e) {
                    var t = this,
                        n = t.params.a11y;
                    if (13 === e.keyCode) {
                        var i = A(e.target);
                        t.navigation && t.navigation.$nextEl && i.is(t.navigation.$nextEl) && (t.isEnd && !t.params.loop || t.slideNext(), t.isEnd ? t.a11y.notify(n.lastSlideMessage) : t.a11y.notify(n.nextSlideMessage)), t.navigation && t.navigation.$prevEl && i.is(t.navigation.$prevEl) && (t.isBeginning && !t.params.loop || t.slidePrev(), t.isBeginning ? t.a11y.notify(n.firstSlideMessage) : t.a11y.notify(n.prevSlideMessage)), t.pagination && i.is("." + t.params.pagination.bulletClass) && i[0].click()
                    }
                },
                notify: function(e) {
                    var t = this.a11y.liveRegion;
                    0 !== t.length && (t.html(""), t.html(e))
                },
                updateNavigation: function() {
                    if (!this.params.loop) {
                        var e = this.navigation,
                            t = e.$nextEl,
                            n = e.$prevEl;
                        n && 0 < n.length && (this.isBeginning ? this.a11y.disableEl(n) : this.a11y.enableEl(n)), t && 0 < t.length && (this.isEnd ? this.a11y.disableEl(t) : this.a11y.enableEl(t))
                    }
                },
                updatePagination: function() {
                    var i = this,
                        s = i.params.a11y;
                    i.pagination && i.params.pagination.clickable && i.pagination.bullets && i.pagination.bullets.length && i.pagination.bullets.each(function(e, t) {
                        var n = A(t);
                        i.a11y.makeElFocusable(n), i.a11y.addElRole(n, "button"), i.a11y.addElLabel(n, s.paginationBulletMessage.replace(/{{index}}/, n.index() + 1))
                    })
                },
                init: function() {
                    var e = this;
                    e.$el.append(e.a11y.liveRegion);
                    var t, n, i = e.params.a11y;
                    e.navigation && e.navigation.$nextEl && (t = e.navigation.$nextEl), e.navigation && e.navigation.$prevEl && (n = e.navigation.$prevEl), t && (e.a11y.makeElFocusable(t), e.a11y.addElRole(t, "button"), e.a11y.addElLabel(t, i.nextSlideMessage), t.on("keydown", e.a11y.onEnterKey)), n && (e.a11y.makeElFocusable(n), e.a11y.addElRole(n, "button"), e.a11y.addElLabel(n, i.prevSlideMessage), n.on("keydown", e.a11y.onEnterKey)), e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.$el.on("keydown", "." + e.params.pagination.bulletClass, e.a11y.onEnterKey)
                },
                destroy: function() {
                    var e, t, n = this;
                    n.a11y.liveRegion && 0 < n.a11y.liveRegion.length && n.a11y.liveRegion.remove(), n.navigation && n.navigation.$nextEl && (e = n.navigation.$nextEl), n.navigation && n.navigation.$prevEl && (t = n.navigation.$prevEl), e && e.off("keydown", n.a11y.onEnterKey), t && t.off("keydown", n.a11y.onEnterKey), n.pagination && n.params.pagination.clickable && n.pagination.bullets && n.pagination.bullets.length && n.pagination.$el.off("keydown", "." + n.params.pagination.bulletClass, n.a11y.onEnterKey)
                }
            },
            Y = {
                init: function() {
                    if (this.params.history) {
                        if (!J.history || !J.history.pushState) return this.params.history.enabled = !1, void(this.params.hashNavigation.enabled = !0);
                        var e = this.history;
                        e.initialized = !0, e.paths = Y.getPathValues(), (e.paths.key || e.paths.value) && (e.scrollToSlide(0, e.paths.value, this.params.runCallbacksOnInit), this.params.history.replaceState || J.addEventListener("popstate", this.history.setHistoryPopState))
                    }
                },
                destroy: function() {
                    this.params.history.replaceState || J.removeEventListener("popstate", this.history.setHistoryPopState)
                },
                setHistoryPopState: function() {
                    this.history.paths = Y.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1)
                },
                getPathValues: function() {
                    var e = J.location.pathname.slice(1).split("/").filter(function(e) {
                            return "" !== e
                        }),
                        t = e.length;
                    return {
                        key: e[t - 2],
                        value: e[t - 1]
                    }
                },
                setHistory: function(e, t) {
                    if (this.history.initialized && this.params.history.enabled) {
                        var n = this.slides.eq(t),
                            i = Y.slugify(n.attr("data-history"));
                        J.location.pathname.includes(e) || (i = e + "/" + i);
                        var s = J.history.state;
                        s && s.value === i || (this.params.history.replaceState ? J.history.replaceState({
                            value: i
                        }, null, i) : J.history.pushState({
                            value: i
                        }, null, i))
                    }
                },
                slugify: function(e) {
                    return e.toString().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
                },
                scrollToSlide: function(e, t, n) {
                    if (t)
                        for (var i = 0, s = this.slides.length; i < s; i += 1) {
                            var r = this.slides.eq(i);
                            if (Y.slugify(r.attr("data-history")) === t && !r.hasClass(this.params.slideDuplicateClass)) {
                                var o = r.index();
                                this.slideTo(o, e, n)
                            }
                        } else this.slideTo(0, e, n)
                }
            },
            W = {
                onHashCange: function() {
                    var e = m.location.hash.replace("#", "");
                    if (e !== this.slides.eq(this.activeIndex).attr("data-hash")) {
                        var t = this.$wrapperEl.children("." + this.params.slideClass + '[data-hash="' + e + '"]').index();
                        if (void 0 === t) return;
                        this.slideTo(t)
                    }
                },
                setHash: function() {
                    if (this.hashNavigation.initialized && this.params.hashNavigation.enabled)
                        if (this.params.hashNavigation.replaceState && J.history && J.history.replaceState) J.history.replaceState(null, null, "#" + this.slides.eq(this.activeIndex).attr("data-hash") || "");
                        else {
                            var e = this.slides.eq(this.activeIndex),
                                t = e.attr("data-hash") || e.attr("data-history");
                            m.location.hash = t || ""
                        }
                },
                init: function() {
                    var e = this;
                    if (!(!e.params.hashNavigation.enabled || e.params.history && e.params.history.enabled)) {
                        e.hashNavigation.initialized = !0;
                        var t = m.location.hash.replace("#", "");
                        if (t)
                            for (var n = 0, i = e.slides.length; n < i; n += 1) {
                                var s = e.slides.eq(n);
                                if ((s.attr("data-hash") || s.attr("data-history")) === t && !s.hasClass(e.params.slideDuplicateClass)) {
                                    var r = s.index();
                                    e.slideTo(r, 0, e.params.runCallbacksOnInit, !0)
                                }
                            }
                        e.params.hashNavigation.watchState && A(J).on("hashchange", e.hashNavigation.onHashCange)
                    }
                },
                destroy: function() {
                    this.params.hashNavigation.watchState && A(J).off("hashchange", this.hashNavigation.onHashCange)
                }
            },
            B = {
                run: function() {
                    var e = this,
                        t = e.slides.eq(e.activeIndex),
                        n = e.params.autoplay.delay;
                    t.attr("data-swiper-autoplay") && (n = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), e.autoplay.timeout = ee.nextTick(function() {
                        e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay"))
                    }, n)
                },
                start: function() {
                    return void 0 === this.autoplay.timeout && !this.autoplay.running && (this.autoplay.running = !0, this.emit("autoplayStart"), this.autoplay.run(), !0)
                },
                stop: function() {
                    return !!this.autoplay.running && void 0 !== this.autoplay.timeout && (this.autoplay.timeout && (clearTimeout(this.autoplay.timeout), this.autoplay.timeout = void 0), this.autoplay.running = !1, this.emit("autoplayStop"), !0)
                },
                pause: function(e) {
                    var t = this;
                    t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout), t.autoplay.paused = !0, 0 !== e && t.params.autoplay.waitForTransition ? (t.$wrapperEl[0].addEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].addEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd)) : (t.autoplay.paused = !1, t.autoplay.run())))
                }
            },
            U = {
                setTranslate: function() {
                    for (var e = this.slides, t = 0; t < e.length; t += 1) {
                        var n = this.slides.eq(t),
                            i = -n[0].swiperSlideOffset;
                        this.params.virtualTranslate || (i -= this.translate);
                        var s = 0;
                        this.isHorizontal() || (s = i, i = 0);
                        var r = this.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(n[0].progress), 0) : 1 + Math.min(Math.max(n[0].progress, -1), 0);
                        n.css({
                            opacity: r
                        }).transform("translate3d(" + i + "px, " + s + "px, 0px)")
                    }
                },
                setTransition: function(e) {
                    var n = this,
                        t = n.slides,
                        i = n.$wrapperEl;
                    if (t.transition(e), n.params.virtualTranslate && 0 !== e) {
                        var s = !1;
                        t.transitionEnd(function() {
                            if (!s && n && !n.destroyed) {
                                s = !0, n.animating = !1;
                                for (var e = ["webkitTransitionEnd", "transitionend"], t = 0; t < e.length; t += 1) i.trigger(e[t])
                            }
                        })
                    }
                }
            },
            V = {
                setTranslate: function() {
                    var e, t = this,
                        n = t.$el,
                        i = t.$wrapperEl,
                        s = t.slides,
                        r = t.width,
                        o = t.height,
                        a = t.rtlTranslate,
                        l = t.size,
                        c = t.params.cubeEffect,
                        d = t.isHorizontal(),
                        u = t.virtual && t.params.virtual.enabled,
                        h = 0;
                    c.shadow && (d ? (0 === (e = i.find(".swiper-cube-shadow")).length && (e = A('<div class="swiper-cube-shadow"></div>'), i.append(e)), e.css({
                        height: r + "px"
                    })) : 0 === (e = n.find(".swiper-cube-shadow")).length && (e = A('<div class="swiper-cube-shadow"></div>'), n.append(e)));
                    for (var p = 0; p < s.length; p += 1) {
                        var f = s.eq(p),
                            m = p;
                        u && (m = parseInt(f.attr("data-swiper-slide-index"), 10));
                        var g = 90 * m,
                            v = Math.floor(g / 360);
                        a && (g = -g, v = Math.floor(-g / 360));
                        var y = Math.max(Math.min(f[0].progress, 1), -1),
                            b = 0,
                            w = 0,
                            x = 0;
                        m % 4 == 0 ? (b = 4 * -v * l, x = 0) : (m - 1) % 4 == 0 ? (b = 0, x = 4 * -v * l) : (m - 2) % 4 == 0 ? (b = l + 4 * v * l, x = l) : (m - 3) % 4 == 0 && (b = -l, x = 3 * l + 4 * l * v), a && (b = -b), d || (w = b, b = 0);
                        var T = "rotateX(" + (d ? 0 : -g) + "deg) rotateY(" + (d ? g : 0) + "deg) translate3d(" + b + "px, " + w + "px, " + x + "px)";
                        if (y <= 1 && -1 < y && (h = 90 * m + 90 * y, a && (h = 90 * -m - 90 * y)), f.transform(T), c.slideShadows) {
                            var C = d ? f.find(".swiper-slide-shadow-left") : f.find(".swiper-slide-shadow-top"),
                                S = d ? f.find(".swiper-slide-shadow-right") : f.find(".swiper-slide-shadow-bottom");
                            0 === C.length && (C = A('<div class="swiper-slide-shadow-' + (d ? "left" : "top") + '"></div>'), f.append(C)), 0 === S.length && (S = A('<div class="swiper-slide-shadow-' + (d ? "right" : "bottom") + '"></div>'), f.append(S)), C.length && (C[0].style.opacity = Math.max(-y, 0)), S.length && (S[0].style.opacity = Math.max(y, 0))
                        }
                    }
                    if (i.css({
                            "-webkit-transform-origin": "50% 50% -" + l / 2 + "px",
                            "-moz-transform-origin": "50% 50% -" + l / 2 + "px",
                            "-ms-transform-origin": "50% 50% -" + l / 2 + "px",
                            "transform-origin": "50% 50% -" + l / 2 + "px"
                        }), c.shadow)
                        if (d) e.transform("translate3d(0px, " + (r / 2 + c.shadowOffset) + "px, " + -r / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + c.shadowScale + ")");
                        else {
                            var E = Math.abs(h) - 90 * Math.floor(Math.abs(h) / 90),
                                _ = 1.5 - (Math.sin(2 * E * Math.PI / 360) / 2 + Math.cos(2 * E * Math.PI / 360) / 2),
                                k = c.shadowScale,
                                D = c.shadowScale / _,
                                $ = c.shadowOffset;
                            e.transform("scale3d(" + k + ", 1, " + D + ") translate3d(0px, " + (o / 2 + $) + "px, " + -o / 2 / D + "px) rotateX(-90deg)")
                        } var M = O.isSafari || O.isUiWebView ? -l / 2 : 0;
                    i.transform("translate3d(0px,0," + M + "px) rotateX(" + (t.isHorizontal() ? 0 : h) + "deg) rotateY(" + (t.isHorizontal() ? -h : 0) + "deg)")
                },
                setTransition: function(e) {
                    var t = this.$el;
                    this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e)
                }
            },
            G = {
                setTranslate: function() {
                    for (var e = this.slides, t = this.rtlTranslate, n = 0; n < e.length; n += 1) {
                        var i = e.eq(n),
                            s = i[0].progress;
                        this.params.flipEffect.limitRotation && (s = Math.max(Math.min(i[0].progress, 1), -1));
                        var r = -180 * s,
                            o = 0,
                            a = -i[0].swiperSlideOffset,
                            l = 0;
                        if (this.isHorizontal() ? t && (r = -r) : (l = a, o = -r, r = a = 0), i[0].style.zIndex = -Math.abs(Math.round(s)) + e.length, this.params.flipEffect.slideShadows) {
                            var c = this.isHorizontal() ? i.find(".swiper-slide-shadow-left") : i.find(".swiper-slide-shadow-top"),
                                d = this.isHorizontal() ? i.find(".swiper-slide-shadow-right") : i.find(".swiper-slide-shadow-bottom");
                            0 === c.length && (c = A('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "left" : "top") + '"></div>'), i.append(c)), 0 === d.length && (d = A('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "right" : "bottom") + '"></div>'), i.append(d)), c.length && (c[0].style.opacity = Math.max(-s, 0)), d.length && (d[0].style.opacity = Math.max(s, 0))
                        }
                        i.transform("translate3d(" + a + "px, " + l + "px, 0px) rotateX(" + o + "deg) rotateY(" + r + "deg)")
                    }
                },
                setTransition: function(e) {
                    var n = this,
                        t = n.slides,
                        i = n.activeIndex,
                        s = n.$wrapperEl;
                    if (t.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), n.params.virtualTranslate && 0 !== e) {
                        var r = !1;
                        t.eq(i).transitionEnd(function() {
                            if (!r && n && !n.destroyed) {
                                r = !0, n.animating = !1;
                                for (var e = ["webkitTransitionEnd", "transitionend"], t = 0; t < e.length; t += 1) s.trigger(e[t])
                            }
                        })
                    }
                }
            },
            X = {
                setTranslate: function() {
                    for (var e = this.width, t = this.height, n = this.slides, i = this.$wrapperEl, s = this.slidesSizesGrid, r = this.params.coverflowEffect, o = this.isHorizontal(), a = this.translate, l = o ? e / 2 - a : t / 2 - a, c = o ? r.rotate : -r.rotate, d = r.depth, u = 0, h = n.length; u < h; u += 1) {
                        var p = n.eq(u),
                            f = s[u],
                            m = (l - p[0].swiperSlideOffset - f / 2) / f * r.modifier,
                            g = o ? c * m : 0,
                            v = o ? 0 : c * m,
                            y = -d * Math.abs(m),
                            b = o ? 0 : r.stretch * m,
                            w = o ? r.stretch * m : 0;
                        Math.abs(w) < .001 && (w = 0), Math.abs(b) < .001 && (b = 0), Math.abs(y) < .001 && (y = 0), Math.abs(g) < .001 && (g = 0), Math.abs(v) < .001 && (v = 0);
                        var x = "translate3d(" + w + "px," + b + "px," + y + "px)  rotateX(" + v + "deg) rotateY(" + g + "deg)";
                        if (p.transform(x), p[0].style.zIndex = 1 - Math.abs(Math.round(m)), r.slideShadows) {
                            var T = o ? p.find(".swiper-slide-shadow-left") : p.find(".swiper-slide-shadow-top"),
                                C = o ? p.find(".swiper-slide-shadow-right") : p.find(".swiper-slide-shadow-bottom");
                            0 === T.length && (T = A('<div class="swiper-slide-shadow-' + (o ? "left" : "top") + '"></div>'), p.append(T)), 0 === C.length && (C = A('<div class="swiper-slide-shadow-' + (o ? "right" : "bottom") + '"></div>'), p.append(C)), T.length && (T[0].style.opacity = 0 < m ? m : 0), C.length && (C[0].style.opacity = 0 < -m ? -m : 0)
                        }
                    }(te.pointerEvents || te.prefixedPointerEvents) && (i[0].style.perspectiveOrigin = l + "px 50%")
                },
                setTransition: function(e) {
                    this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
                }
            },
            Q = {
                init: function() {
                    var e = this,
                        t = e.params.thumbs,
                        n = e.constructor;
                    t.swiper instanceof n ? (e.thumbs.swiper = t.swiper, ee.extend(e.thumbs.swiper.originalParams, {
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    }), ee.extend(e.thumbs.swiper.params, {
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    })) : ee.isObject(t.swiper) && (e.thumbs.swiper = new n(ee.extend({}, t.swiper, {
                        watchSlidesVisibility: !0,
                        watchSlidesProgress: !0,
                        slideToClickedSlide: !1
                    })), e.thumbs.swiperCreated = !0), e.thumbs.swiper.$el.addClass(e.params.thumbs.thumbsContainerClass), e.thumbs.swiper.on("tap", e.thumbs.onThumbClick)
                },
                onThumbClick: function() {
                    var e = this,
                        t = e.thumbs.swiper;
                    if (t) {
                        var n = t.clickedIndex,
                            i = t.clickedSlide;
                        if (!(i && A(i).hasClass(e.params.thumbs.slideThumbActiveClass) || null == n)) {
                            var s;
                            if (s = t.params.loop ? parseInt(A(t.clickedSlide).attr("data-swiper-slide-index"), 10) : n, e.params.loop) {
                                var r = e.activeIndex;
                                e.slides.eq(r).hasClass(e.params.slideDuplicateClass) && (e.loopFix(), e._clientLeft = e.$wrapperEl[0].clientLeft, r = e.activeIndex);
                                var o = e.slides.eq(r).prevAll('[data-swiper-slide-index="' + s + '"]').eq(0).index(),
                                    a = e.slides.eq(r).nextAll('[data-swiper-slide-index="' + s + '"]').eq(0).index();
                                s = void 0 === o ? a : void 0 === a ? o : a - r < r - o ? a : o
                            }
                            e.slideTo(s)
                        }
                    }
                },
                update: function(e) {
                    var t = this,
                        n = t.thumbs.swiper;
                    if (n) {
                        var i = "auto" === n.params.slidesPerView ? n.slidesPerViewDynamic() : n.params.slidesPerView;
                        if (t.realIndex !== n.realIndex) {
                            var s, r = n.activeIndex;
                            if (n.params.loop) {
                                n.slides.eq(r).hasClass(n.params.slideDuplicateClass) && (n.loopFix(), n._clientLeft = n.$wrapperEl[0].clientLeft, r = n.activeIndex);
                                var o = n.slides.eq(r).prevAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index(),
                                    a = n.slides.eq(r).nextAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index();
                                s = void 0 === o ? a : void 0 === a ? o : a - r == r - o ? r : a - r < r - o ? a : o
                            } else s = t.realIndex;
                            n.visibleSlidesIndexes.indexOf(s) < 0 && (n.params.centeredSlides ? s = r < s ? s - Math.floor(i / 2) + 1 : s + Math.floor(i / 2) - 1 : r < s && (s = s - i + 1), n.slideTo(s, e ? 0 : void 0))
                        }
                        var l = 1,
                            c = t.params.thumbs.slideThumbActiveClass;
                        if (1 < t.params.slidesPerView && !t.params.centeredSlides && (l = t.params.slidesPerView), n.slides.removeClass(c), n.params.loop)
                            for (var d = 0; d < l; d += 1) n.$wrapperEl.children('[data-swiper-slide-index="' + (t.realIndex + d) + '"]').addClass(c);
                        else
                            for (var u = 0; u < l; u += 1) n.slides.eq(t.realIndex + u).addClass(c)
                    }
                }
            },
            K = [T, C, S, E, k, $, I, {
                name: "mousewheel",
                params: {
                    mousewheel: {
                        enabled: !1,
                        releaseOnEdges: !1,
                        invert: !1,
                        forceToAxis: !1,
                        sensitivity: 1,
                        eventsTarged: "container"
                    }
                },
                create: function() {
                    ee.extend(this, {
                        mousewheel: {
                            enabled: !1,
                            enable: P.enable.bind(this),
                            disable: P.disable.bind(this),
                            handle: P.handle.bind(this),
                            handleMouseEnter: P.handleMouseEnter.bind(this),
                            handleMouseLeave: P.handleMouseLeave.bind(this),
                            lastScrollTime: ee.now()
                        }
                    })
                },
                on: {
                    init: function() {
                        this.params.mousewheel.enabled && this.mousewheel.enable()
                    },
                    destroy: function() {
                        this.mousewheel.enabled && this.mousewheel.disable()
                    }
                }
            }, {
                name: "navigation",
                params: {
                    navigation: {
                        nextEl: null,
                        prevEl: null,
                        hideOnClick: !1,
                        disabledClass: "swiper-button-disabled",
                        hiddenClass: "swiper-button-hidden",
                        lockClass: "swiper-button-lock"
                    }
                },
                create: function() {
                    ee.extend(this, {
                        navigation: {
                            init: L.init.bind(this),
                            update: L.update.bind(this),
                            destroy: L.destroy.bind(this),
                            onNextClick: L.onNextClick.bind(this),
                            onPrevClick: L.onPrevClick.bind(this)
                        }
                    })
                },
                on: {
                    init: function() {
                        this.navigation.init(), this.navigation.update()
                    },
                    toEdge: function() {
                        this.navigation.update()
                    },
                    fromEdge: function() {
                        this.navigation.update()
                    },
                    destroy: function() {
                        this.navigation.destroy()
                    },
                    click: function(e) {
                        var t, n = this,
                            i = n.navigation,
                            s = i.$nextEl,
                            r = i.$prevEl;
                        !n.params.navigation.hideOnClick || A(e.target).is(r) || A(e.target).is(s) || (s ? t = s.hasClass(n.params.navigation.hiddenClass) : r && (t = r.hasClass(n.params.navigation.hiddenClass)), !0 === t ? n.emit("navigationShow", n) : n.emit("navigationHide", n), s && s.toggleClass(n.params.navigation.hiddenClass), r && r.toggleClass(n.params.navigation.hiddenClass))
                    }
                }
            }, {
                name: "pagination",
                params: {
                    pagination: {
                        el: null,
                        bulletElement: "span",
                        clickable: !1,
                        hideOnClick: !1,
                        renderBullet: null,
                        renderProgressbar: null,
                        renderFraction: null,
                        renderCustom: null,
                        progressbarOpposite: !1,
                        type: "bullets",
                        dynamicBullets: !1,
                        dynamicMainBullets: 1,
                        formatFractionCurrent: function(e) {
                            return e
                        },
                        formatFractionTotal: function(e) {
                            return e
                        },
                        bulletClass: "swiper-pagination-bullet",
                        bulletActiveClass: "swiper-pagination-bullet-active",
                        modifierClass: "swiper-pagination-",
                        currentClass: "swiper-pagination-current",
                        totalClass: "swiper-pagination-total",
                        hiddenClass: "swiper-pagination-hidden",
                        progressbarFillClass: "swiper-pagination-progressbar-fill",
                        progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
                        clickableClass: "swiper-pagination-clickable",
                        lockClass: "swiper-pagination-lock"
                    }
                },
                create: function() {
                    ee.extend(this, {
                        pagination: {
                            init: N.init.bind(this),
                            render: N.render.bind(this),
                            update: N.update.bind(this),
                            destroy: N.destroy.bind(this),
                            dynamicBulletIndex: 0
                        }
                    })
                },
                on: {
                    init: function() {
                        this.pagination.init(), this.pagination.render(), this.pagination.update()
                    },
                    activeIndexChange: function() {
                        this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update()
                    },
                    snapIndexChange: function() {
                        this.params.loop || this.pagination.update()
                    },
                    slidesLengthChange: function() {
                        this.params.loop && (this.pagination.render(), this.pagination.update())
                    },
                    snapGridLengthChange: function() {
                        this.params.loop || (this.pagination.render(), this.pagination.update())
                    },
                    destroy: function() {
                        this.pagination.destroy()
                    },
                    click: function(e) {
                        var t = this;
                        t.params.pagination.el && t.params.pagination.hideOnClick && 0 < t.pagination.$el.length && !A(e.target).hasClass(t.params.pagination.bulletClass) && (!0 === t.pagination.$el.hasClass(t.params.pagination.hiddenClass) ? t.emit("paginationShow", t) : t.emit("paginationHide", t), t.pagination.$el.toggleClass(t.params.pagination.hiddenClass))
                    }
                }
            }, {
                name: "scrollbar",
                params: {
                    scrollbar: {
                        el: null,
                        dragSize: "auto",
                        hide: !1,
                        draggable: !1,
                        snapOnRelease: !0,
                        lockClass: "swiper-scrollbar-lock",
                        dragClass: "swiper-scrollbar-drag"
                    }
                },
                create: function() {
                    var e = this;
                    ee.extend(e, {
                        scrollbar: {
                            init: j.init.bind(e),
                            destroy: j.destroy.bind(e),
                            updateSize: j.updateSize.bind(e),
                            setTranslate: j.setTranslate.bind(e),
                            setTransition: j.setTransition.bind(e),
                            enableDraggable: j.enableDraggable.bind(e),
                            disableDraggable: j.disableDraggable.bind(e),
                            setDragPosition: j.setDragPosition.bind(e),
                            onDragStart: j.onDragStart.bind(e),
                            onDragMove: j.onDragMove.bind(e),
                            onDragEnd: j.onDragEnd.bind(e),
                            isTouched: !1,
                            timeout: null,
                            dragTimeout: null
                        }
                    })
                },
                on: {
                    init: function() {
                        this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate()
                    },
                    update: function() {
                        this.scrollbar.updateSize()
                    },
                    resize: function() {
                        this.scrollbar.updateSize()
                    },
                    observerUpdate: function() {
                        this.scrollbar.updateSize()
                    },
                    setTranslate: function() {
                        this.scrollbar.setTranslate()
                    },
                    setTransition: function(e) {
                        this.scrollbar.setTransition(e)
                    },
                    destroy: function() {
                        this.scrollbar.destroy()
                    }
                }
            }, {
                name: "parallax",
                params: {
                    parallax: {
                        enabled: !1
                    }
                },
                create: function() {
                    ee.extend(this, {
                        parallax: {
                            setTransform: H.setTransform.bind(this),
                            setTranslate: H.setTranslate.bind(this),
                            setTransition: H.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        this.params.parallax.enabled && (this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0)
                    },
                    init: function() {
                        this.params.parallax.enabled && this.parallax.setTranslate()
                    },
                    setTranslate: function() {
                        this.params.parallax.enabled && this.parallax.setTranslate()
                    },
                    setTransition: function(e) {
                        this.params.parallax.enabled && this.parallax.setTransition(e)
                    }
                }
            }, {
                name: "zoom",
                params: {
                    zoom: {
                        enabled: !1,
                        maxRatio: 3,
                        minRatio: 1,
                        toggle: !0,
                        containerClass: "swiper-zoom-container",
                        zoomedSlideClass: "swiper-slide-zoomed"
                    }
                },
                create: function() {
                    var i = this,
                        t = {
                            enabled: !1,
                            scale: 1,
                            currentScale: 1,
                            isScaling: !1,
                            gesture: {
                                $slideEl: void 0,
                                slideWidth: void 0,
                                slideHeight: void 0,
                                $imageEl: void 0,
                                $imageWrapEl: void 0,
                                maxRatio: 3
                            },
                            image: {
                                isTouched: void 0,
                                isMoved: void 0,
                                currentX: void 0,
                                currentY: void 0,
                                minX: void 0,
                                minY: void 0,
                                maxX: void 0,
                                maxY: void 0,
                                width: void 0,
                                height: void 0,
                                startX: void 0,
                                startY: void 0,
                                touchesStart: {},
                                touchesCurrent: {}
                            },
                            velocity: {
                                x: void 0,
                                y: void 0,
                                prevPositionX: void 0,
                                prevPositionY: void 0,
                                prevTime: void 0
                            }
                        };
                    "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function(e) {
                        t[e] = z[e].bind(i)
                    }), ee.extend(i, {
                        zoom: t
                    });
                    var s = 1;
                    Object.defineProperty(i.zoom, "scale", {
                        get: function() {
                            return s
                        },
                        set: function(e) {
                            if (s !== e) {
                                var t = i.zoom.gesture.$imageEl ? i.zoom.gesture.$imageEl[0] : void 0,
                                    n = i.zoom.gesture.$slideEl ? i.zoom.gesture.$slideEl[0] : void 0;
                                i.emit("zoomChange", e, t, n)
                            }
                            s = e
                        }
                    })
                },
                on: {
                    init: function() {
                        this.params.zoom.enabled && this.zoom.enable()
                    },
                    destroy: function() {
                        this.zoom.disable()
                    },
                    touchStart: function(e) {
                        this.zoom.enabled && this.zoom.onTouchStart(e)
                    },
                    touchEnd: function(e) {
                        this.zoom.enabled && this.zoom.onTouchEnd(e)
                    },
                    doubleTap: function(e) {
                        this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e)
                    },
                    transitionEnd: function() {
                        this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd()
                    }
                }
            }, {
                name: "lazy",
                params: {
                    lazy: {
                        enabled: !1,
                        loadPrevNext: !1,
                        loadPrevNextAmount: 1,
                        loadOnTransitionStart: !1,
                        elementClass: "swiper-lazy",
                        loadingClass: "swiper-lazy-loading",
                        loadedClass: "swiper-lazy-loaded",
                        preloaderClass: "swiper-lazy-preloader"
                    }
                },
                create: function() {
                    ee.extend(this, {
                        lazy: {
                            initialImageLoaded: !1,
                            load: q.load.bind(this),
                            loadInSlide: q.loadInSlide.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1)
                    },
                    init: function() {
                        this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load()
                    },
                    scroll: function() {
                        this.params.freeMode && !this.params.freeModeSticky && this.lazy.load()
                    },
                    resize: function() {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    scrollbarDragMove: function() {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    transitionStart: function() {
                        this.params.lazy.enabled && (this.params.lazy.loadOnTransitionStart || !this.params.lazy.loadOnTransitionStart && !this.lazy.initialImageLoaded) && this.lazy.load()
                    },
                    transitionEnd: function() {
                        this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load()
                    }
                }
            }, {
                name: "controller",
                params: {
                    controller: {
                        control: void 0,
                        inverse: !1,
                        by: "slide"
                    }
                },
                create: function() {
                    ee.extend(this, {
                        controller: {
                            control: this.params.controller.control,
                            getInterpolateFunction: F.getInterpolateFunction.bind(this),
                            setTranslate: F.setTranslate.bind(this),
                            setTransition: F.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    update: function() {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    resize: function() {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    observerUpdate: function() {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    setTranslate: function(e, t) {
                        this.controller.control && this.controller.setTranslate(e, t)
                    },
                    setTransition: function(e, t) {
                        this.controller.control && this.controller.setTransition(e, t)
                    }
                }
            }, {
                name: "a11y",
                params: {
                    a11y: {
                        enabled: !0,
                        notificationClass: "swiper-notification",
                        prevSlideMessage: "Previous slide",
                        nextSlideMessage: "Next slide",
                        firstSlideMessage: "This is the first slide",
                        lastSlideMessage: "This is the last slide",
                        paginationBulletMessage: "Go to slide {{index}}"
                    }
                },
                create: function() {
                    var t = this;
                    ee.extend(t, {
                        a11y: {
                            liveRegion: A('<span class="' + t.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')
                        }
                    }), Object.keys(R).forEach(function(e) {
                        t.a11y[e] = R[e].bind(t)
                    })
                },
                on: {
                    init: function() {
                        this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation())
                    },
                    toEdge: function() {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    fromEdge: function() {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    paginationUpdate: function() {
                        this.params.a11y.enabled && this.a11y.updatePagination()
                    },
                    destroy: function() {
                        this.params.a11y.enabled && this.a11y.destroy()
                    }
                }
            }, {
                name: "history",
                params: {
                    history: {
                        enabled: !1,
                        replaceState: !1,
                        key: "slides"
                    }
                },
                create: function() {
                    ee.extend(this, {
                        history: {
                            init: Y.init.bind(this),
                            setHistory: Y.setHistory.bind(this),
                            setHistoryPopState: Y.setHistoryPopState.bind(this),
                            scrollToSlide: Y.scrollToSlide.bind(this),
                            destroy: Y.destroy.bind(this)
                        }
                    })
                },
                on: {
                    init: function() {
                        this.params.history.enabled && this.history.init()
                    },
                    destroy: function() {
                        this.params.history.enabled && this.history.destroy()
                    },
                    transitionEnd: function() {
                        this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex)
                    }
                }
            }, {
                name: "hash-navigation",
                params: {
                    hashNavigation: {
                        enabled: !1,
                        replaceState: !1,
                        watchState: !1
                    }
                },
                create: function() {
                    ee.extend(this, {
                        hashNavigation: {
                            initialized: !1,
                            init: W.init.bind(this),
                            destroy: W.destroy.bind(this),
                            setHash: W.setHash.bind(this),
                            onHashCange: W.onHashCange.bind(this)
                        }
                    })
                },
                on: {
                    init: function() {
                        this.params.hashNavigation.enabled && this.hashNavigation.init()
                    },
                    destroy: function() {
                        this.params.hashNavigation.enabled && this.hashNavigation.destroy()
                    },
                    transitionEnd: function() {
                        this.hashNavigation.initialized && this.hashNavigation.setHash()
                    }
                }
            }, {
                name: "autoplay",
                params: {
                    autoplay: {
                        enabled: !1,
                        delay: 3e3,
                        waitForTransition: !0,
                        disableOnInteraction: !0,
                        stopOnLastSlide: !1,
                        reverseDirection: !1
                    }
                },
                create: function() {
                    var t = this;
                    ee.extend(t, {
                        autoplay: {
                            running: !1,
                            paused: !1,
                            run: B.run.bind(t),
                            start: B.start.bind(t),
                            stop: B.stop.bind(t),
                            pause: B.pause.bind(t),
                            onTransitionEnd: function(e) {
                                t && !t.destroyed && t.$wrapperEl && e.target === this && (t.$wrapperEl[0].removeEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].removeEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd), t.autoplay.paused = !1, t.autoplay.running ? t.autoplay.run() : t.autoplay.stop())
                            }
                        }
                    })
                },
                on: {
                    init: function() {
                        this.params.autoplay.enabled && this.autoplay.start()
                    },
                    beforeTransitionStart: function(e, t) {
                        this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop())
                    },
                    sliderFirstMove: function() {
                        this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause())
                    },
                    destroy: function() {
                        this.autoplay.running && this.autoplay.stop()
                    }
                }
            }, {
                name: "effect-fade",
                params: {
                    fadeEffect: {
                        crossFade: !1
                    }
                },
                create: function() {
                    ee.extend(this, {
                        fadeEffect: {
                            setTranslate: U.setTranslate.bind(this),
                            setTransition: U.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        if ("fade" === this.params.effect) {
                            this.classNames.push(this.params.containerModifierClass + "fade");
                            var e = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            ee.extend(this.params, e), ee.extend(this.originalParams, e)
                        }
                    },
                    setTranslate: function() {
                        "fade" === this.params.effect && this.fadeEffect.setTranslate()
                    },
                    setTransition: function(e) {
                        "fade" === this.params.effect && this.fadeEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-cube",
                params: {
                    cubeEffect: {
                        slideShadows: !0,
                        shadow: !0,
                        shadowOffset: 20,
                        shadowScale: .94
                    }
                },
                create: function() {
                    ee.extend(this, {
                        cubeEffect: {
                            setTranslate: V.setTranslate.bind(this),
                            setTransition: V.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        if ("cube" === this.params.effect) {
                            this.classNames.push(this.params.containerModifierClass + "cube"), this.classNames.push(this.params.containerModifierClass + "3d");
                            var e = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                resistanceRatio: 0,
                                spaceBetween: 0,
                                centeredSlides: !1,
                                virtualTranslate: !0
                            };
                            ee.extend(this.params, e), ee.extend(this.originalParams, e)
                        }
                    },
                    setTranslate: function() {
                        "cube" === this.params.effect && this.cubeEffect.setTranslate()
                    },
                    setTransition: function(e) {
                        "cube" === this.params.effect && this.cubeEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-flip",
                params: {
                    flipEffect: {
                        slideShadows: !0,
                        limitRotation: !0
                    }
                },
                create: function() {
                    ee.extend(this, {
                        flipEffect: {
                            setTranslate: G.setTranslate.bind(this),
                            setTransition: G.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        if ("flip" === this.params.effect) {
                            this.classNames.push(this.params.containerModifierClass + "flip"), this.classNames.push(this.params.containerModifierClass + "3d");
                            var e = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            ee.extend(this.params, e), ee.extend(this.originalParams, e)
                        }
                    },
                    setTranslate: function() {
                        "flip" === this.params.effect && this.flipEffect.setTranslate()
                    },
                    setTransition: function(e) {
                        "flip" === this.params.effect && this.flipEffect.setTransition(e)
                    }
                }
            }, {
                name: "effect-coverflow",
                params: {
                    coverflowEffect: {
                        rotate: 50,
                        stretch: 0,
                        depth: 100,
                        modifier: 1,
                        slideShadows: !0
                    }
                },
                create: function() {
                    ee.extend(this, {
                        coverflowEffect: {
                            setTranslate: X.setTranslate.bind(this),
                            setTransition: X.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        "coverflow" === this.params.effect && (this.classNames.push(this.params.containerModifierClass + "coverflow"), this.classNames.push(this.params.containerModifierClass + "3d"), this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0)
                    },
                    setTranslate: function() {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTranslate()
                    },
                    setTransition: function(e) {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e)
                    }
                }
            }, {
                name: "thumbs",
                params: {
                    thumbs: {
                        swiper: null,
                        slideThumbActiveClass: "swiper-slide-thumb-active",
                        thumbsContainerClass: "swiper-container-thumbs"
                    }
                },
                create: function() {
                    ee.extend(this, {
                        thumbs: {
                            swiper: null,
                            init: Q.init.bind(this),
                            update: Q.update.bind(this),
                            onThumbClick: Q.onThumbClick.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function() {
                        var e = this.params.thumbs;
                        e && e.swiper && (this.thumbs.init(), this.thumbs.update(!0))
                    },
                    slideChange: function() {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    update: function() {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    resize: function() {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    observerUpdate: function() {
                        this.thumbs.swiper && this.thumbs.update()
                    },
                    setTransition: function(e) {
                        var t = this.thumbs.swiper;
                        t && t.setTransition(e)
                    },
                    beforeDestroy: function() {
                        var e = this.thumbs.swiper;
                        e && this.thumbs.swiperCreated && e && e.destroy()
                    }
                }
            }];
        return void 0 === x.use && (x.use = x.Class.use, x.installModule = x.Class.installModule), x.use(K), x
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(e.jQuery)
    }(this, function(m) {
        ! function() {
            "use strict";

            function t(e, t) {
                if (this.el = e, this.$el = m(e), this.s = m.extend({}, n, t), this.s.dynamic && "undefined" !== this.s.dynamicEl && this.s.dynamicEl.constructor === Array && !this.s.dynamicEl.length) throw "When using dynamic mode, you must also define dynamicEl as an Array.";
                return this.modules = {}, this.lGalleryOn = !1, this.lgBusy = !1, this.hideBartimeout = !1, this.isTouch = "ontouchstart" in document.documentElement, this.s.slideEndAnimatoin && (this.s.hideControlOnEnd = !1), this.s.dynamic ? this.$items = this.s.dynamicEl : "this" === this.s.selector ? this.$items = this.$el : "" !== this.s.selector ? this.s.selectWithin ? this.$items = m(this.s.selectWithin).find(this.s.selector) : this.$items = this.$el.find(m(this.s.selector)) : this.$items = this.$el.children(), this.$slide = "", this.$outer = "", this.init(), this
            }
            var n = {
                mode: "lg-slide",
                cssEasing: "ease",
                easing: "linear",
                speed: 600,
                height: "100%",
                width: "100%",
                addClass: "",
                startClass: "lg-start-zoom",
                backdropDuration: 150,
                hideBarsDelay: 6e3,
                useLeft: !1,
                closable: !0,
                loop: !0,
                escKey: !0,
                keyPress: !0,
                controls: !0,
                slideEndAnimatoin: !0,
                hideControlOnEnd: !1,
                mousewheel: !0,
                getCaptionFromTitleOrAlt: !0,
                appendSubHtmlTo: ".lg-sub-html",
                subHtmlSelectorRelative: !1,
                preload: 1,
                showAfterLoad: !0,
                selector: "",
                selectWithin: "",
                nextHtml: "",
                prevHtml: "",
                index: !1,
                iframeMaxWidth: "100%",
                download: !0,
                counter: !0,
                appendCounterTo: ".lg-toolbar",
                swipeThreshold: 50,
                enableSwipe: !0,
                enableDrag: !0,
                dynamic: !1,
                dynamicEl: [],
                galleryId: 1
            };
            t.prototype.init = function() {
                var e = this;
                e.s.preload > e.$items.length && (e.s.preload = e.$items.length);
                var t = window.location.hash;
                0 < t.indexOf("lg=" + this.s.galleryId) && (e.index = parseInt(t.split("&slide=")[1], 10), m("body").addClass("lg-from-hash"), m("body").hasClass("lg-on") || (setTimeout(function() {
                    e.build(e.index)
                }), m("body").addClass("lg-on"))), e.s.dynamic ? (e.$el.trigger("onBeforeOpen.lg"), e.index = e.s.index || 0, m("body").hasClass("lg-on") || setTimeout(function() {
                    e.build(e.index), m("body").addClass("lg-on")
                })) : e.$items.on("click.lgcustom", function(t) {
                    try {
                        t.preventDefault(), t.preventDefault()
                    } catch (e) {
                        t.returnValue = !1
                    }
                    e.$el.trigger("onBeforeOpen.lg"), e.index = e.s.index || e.$items.index(this), m("body").hasClass("lg-on") || (e.build(e.index), m("body").addClass("lg-on"))
                })
            }, t.prototype.build = function(e) {
                var t = this;
                t.structure(), m.each(m.fn.lightGallery.modules, function(e) {
                    t.modules[e] = new m.fn.lightGallery.modules[e](t.el)
                }), t.slide(e, !1, !1, !1), t.s.keyPress && t.keyPress(), 1 < t.$items.length ? (t.arrow(), setTimeout(function() {
                    t.enableDrag(), t.enableSwipe()
                }, 50), t.s.mousewheel && t.mousewheel()) : t.$slide.on("click.lg", function() {
                    t.$el.trigger("onSlideClick.lg")
                }), t.counter(), t.closeGallery(), t.$el.trigger("onAfterOpen.lg"), t.$outer.on("mousemove.lg click.lg touchstart.lg", function() {
                    t.$outer.removeClass("lg-hide-items"), clearTimeout(t.hideBartimeout), t.hideBartimeout = setTimeout(function() {
                        t.$outer.addClass("lg-hide-items")
                    }, t.s.hideBarsDelay)
                }), t.$outer.trigger("mousemove.lg")
            }, t.prototype.structure = function() {
                var e, t = "",
                    n = "",
                    i = 0,
                    s = "",
                    r = this;
                for (m("body").append('<div class="lg-backdrop"></div>'), m(".lg-backdrop").css("transition-duration", this.s.backdropDuration + "ms"), i = 0; i < this.$items.length; i++) t += '<div class="lg-item"></div>';
                if (this.s.controls && 1 < this.$items.length && (n = '<div class="lg-actions"><button class="lg-prev lg-icon">' + this.s.prevHtml + '</button><button class="lg-next lg-icon">' + this.s.nextHtml + "</button></div>"), ".lg-sub-html" === this.s.appendSubHtmlTo && (s = '<div class="lg-sub-html"></div>'), e = '<div class="lg-outer ' + this.s.addClass + " " + this.s.startClass + '"><div class="lg" style="width:' + this.s.width + "; height:" + this.s.height + '"><div class="lg-inner">' + t + '</div><div class="lg-toolbar lg-group"><span class="lg-close lg-icon"></span></div>' + n + s + "</div></div>", m("body").append(e), this.$outer = m(".lg-outer"), this.$slide = this.$outer.find(".lg-item"), this.s.useLeft ? (this.$outer.addClass("lg-use-left"), this.s.mode = "lg-slide") : this.$outer.addClass("lg-use-css3"), r.setTop(), m(window).on("resize.lg orientationchange.lg", function() {
                        setTimeout(function() {
                            r.setTop()
                        }, 100)
                    }), this.$slide.eq(this.index).addClass("lg-current"), this.doCss() ? this.$outer.addClass("lg-css3") : (this.$outer.addClass("lg-css"), this.s.speed = 0), this.$outer.addClass(this.s.mode), this.s.enableDrag && 1 < this.$items.length && this.$outer.addClass("lg-grab"), this.s.showAfterLoad && this.$outer.addClass("lg-show-after-load"), this.doCss()) {
                    var o = this.$outer.find(".lg-inner");
                    o.css("transition-timing-function", this.s.cssEasing), o.css("transition-duration", this.s.speed + "ms")
                }
                setTimeout(function() {
                    m(".lg-backdrop").addClass("in")
                }), setTimeout(function() {
                    r.$outer.addClass("lg-visible")
                }, this.s.backdropDuration), this.s.download && this.$outer.find(".lg-toolbar").append('<a id="lg-download" target="_blank" download class="lg-download lg-icon"></a>'), this.prevScrollTop = m(window).scrollTop()
            }, t.prototype.setTop = function() {
                if ("100%" !== this.s.height) {
                    var e = m(window).height(),
                        t = (e - parseInt(this.s.height, 10)) / 2,
                        n = this.$outer.find(".lg");
                    e >= parseInt(this.s.height, 10) ? n.css("top", t + "px") : n.css("top", "0px")
                }
            }, t.prototype.doCss = function() {
                return !! function() {
                    var e = ["transition", "MozTransition", "WebkitTransition", "OTransition", "msTransition", "KhtmlTransition"],
                        t = document.documentElement,
                        n = 0;
                    for (n = 0; n < e.length; n++)
                        if (e[n] in t.style) return !0
                }()
            }, t.prototype.isVideo = function(e, t) {
                var n;
                if (n = this.s.dynamic ? this.s.dynamicEl[t].html : this.$items.eq(t).attr("data-html"), !e) return n ? {
                    html5: !0
                } : (console.error("lightGallery :- data-src is not pvovided on slide item " + (t + 1) + ". Please make sure the selector property is properly configured. More info - http://sachinchoolur.github.io/lightGallery/demos/html-markup.html"), !1);
                var i = e.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9\-\_\%]+)/i),
                    s = e.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i),
                    r = e.match(/\/\/(?:www\.)?dai.ly\/([0-9a-z\-_]+)/i),
                    o = e.match(/\/\/(?:www\.)?(?:vk\.com|vkontakte\.ru)\/(?:video_ext\.php\?)(.*)/i);
                return i ? {
                    youtube: i
                } : s ? {
                    vimeo: s
                } : r ? {
                    dailymotion: r
                } : o ? {
                    vk: o
                } : void 0
            }, t.prototype.counter = function() {
                this.s.counter && m(this.s.appendCounterTo).append('<div id="lg-counter"><span id="lg-counter-current">' + (parseInt(this.index, 10) + 1) + '</span> / <span id="lg-counter-all">' + this.$items.length + "</span></div>")
            }, t.prototype.addHtml = function(e) {
                var t, n, i = null;
                if (this.s.dynamic ? this.s.dynamicEl[e].subHtmlUrl ? t = this.s.dynamicEl[e].subHtmlUrl : i = this.s.dynamicEl[e].subHtml : (n = this.$items.eq(e)).attr("data-sub-html-url") ? t = n.attr("data-sub-html-url") : (i = n.attr("data-sub-html"), this.s.getCaptionFromTitleOrAlt && !i && (i = n.attr("title") || n.find("img").first().attr("alt"))), !t)
                    if (null != i) {
                        var s = i.substring(0, 1);
                        "." !== s && "#" !== s || (i = this.s.subHtmlSelectorRelative && !this.s.dynamic ? n.find(i).html() : m(i).html())
                    } else i = "";
                ".lg-sub-html" === this.s.appendSubHtmlTo ? t ? this.$outer.find(this.s.appendSubHtmlTo).load(t) : this.$outer.find(this.s.appendSubHtmlTo).html(i) : t ? this.$slide.eq(e).load(t) : this.$slide.eq(e).append(i), null != i && ("" === i ? this.$outer.find(this.s.appendSubHtmlTo).addClass("lg-empty-html") : this.$outer.find(this.s.appendSubHtmlTo).removeClass("lg-empty-html")), this.$el.trigger("onAfterAppendSubHtml.lg", [e])
            }, t.prototype.preload = function(e) {
                var t = 1,
                    n = 1;
                for (t = 1; t <= this.s.preload && !(t >= this.$items.length - e); t++) this.loadContent(e + t, !1, 0);
                for (n = 1; n <= this.s.preload && !(e - n < 0); n++) this.loadContent(e - n, !1, 0)
            }, t.prototype.loadContent = function(t, e, n) {
                function i(e) {
                    for (var t = [], n = [], i = 0; i < e.length; i++) {
                        var s = e[i].split(" ");
                        "" === s[0] && s.splice(0, 1), n.push(s[0]), t.push(s[1])
                    }
                    for (var r = m(window).width(), o = 0; o < t.length; o++)
                        if (parseInt(t[o], 10) > r) {
                            a = n[o];
                            break
                        }
                }
                var s, a, r, o, l, c, d = this,
                    u = !1;
                if (d.s.dynamic) {
                    if (d.s.dynamicEl[t].poster && (u = !0, r = d.s.dynamicEl[t].poster), c = d.s.dynamicEl[t].html, a = d.s.dynamicEl[t].src, d.s.dynamicEl[t].responsive) i(d.s.dynamicEl[t].responsive.split(","));
                    o = d.s.dynamicEl[t].srcset, l = d.s.dynamicEl[t].sizes
                } else {
                    if (d.$items.eq(t).attr("data-poster") && (u = !0, r = d.$items.eq(t).attr("data-poster")), c = d.$items.eq(t).attr("data-html"), a = d.$items.eq(t).attr("href") || d.$items.eq(t).attr("data-src"), d.$items.eq(t).attr("data-responsive")) i(d.$items.eq(t).attr("data-responsive").split(","));
                    o = d.$items.eq(t).attr("data-srcset"), l = d.$items.eq(t).attr("data-sizes")
                }
                var h = !1;
                d.s.dynamic ? d.s.dynamicEl[t].iframe && (h = !0) : "true" === d.$items.eq(t).attr("data-iframe") && (h = !0);
                var p = d.isVideo(a, t);
                if (!d.$slide.eq(t).hasClass("lg-loaded")) {
                    if (h) d.$slide.eq(t).prepend('<div class="lg-video-cont lg-has-iframe" style="max-width:' + d.s.iframeMaxWidth + '"><div class="lg-video"><iframe class="lg-object" frameborder="0" src="' + a + '"  allowfullscreen="true"></iframe></div></div>');
                    else if (u) {
                        var f;
                        f = p && p.youtube ? "lg-has-youtube" : p && p.vimeo ? "lg-has-vimeo" : "lg-has-html5", d.$slide.eq(t).prepend('<div class="lg-video-cont ' + f + ' "><div class="lg-video"><span class="lg-video-play"></span><img class="lg-object lg-has-poster" src="' + r + '" /></div></div>')
                    } else p ? (d.$slide.eq(t).prepend('<div class="lg-video-cont "><div class="lg-video"></div></div>'), d.$el.trigger("hasVideo.lg", [t, a, c])) : d.$slide.eq(t).prepend('<div class="lg-img-wrap"><img class="lg-object lg-image" src="' + a + '" /></div>');
                    if (d.$el.trigger("onAferAppendSlide.lg", [t]), s = d.$slide.eq(t).find(".lg-object"), l && s.attr("sizes", l), o) {
                        s.attr("srcset", o);
                        try {
                            picturefill({
                                elements: [s[0]]
                            })
                        } catch (e) {
                            console.warn("lightGallery :- If you want srcset to be supported for older browser please include picturefil version 2 javascript library in your document.")
                        }
                    }
                    ".lg-sub-html" !== this.s.appendSubHtmlTo && d.addHtml(t), d.$slide.eq(t).addClass("lg-loaded")
                }
                d.$slide.eq(t).find(".lg-object").on("load.lg error.lg", function() {
                    var e = 0;
                    n && !m("body").hasClass("lg-from-hash") && (e = n), setTimeout(function() {
                        d.$slide.eq(t).addClass("lg-complete"), d.$el.trigger("onSlideItemLoad.lg", [t, n || 0])
                    }, e)
                }), p && p.html5 && !u && d.$slide.eq(t).addClass("lg-complete"), !0 === e && (d.$slide.eq(t).hasClass("lg-complete") ? d.preload(t) : d.$slide.eq(t).find(".lg-object").on("load.lg error.lg", function() {
                    d.preload(t)
                }))
            }, t.prototype.slide = function(e, t, n, i) {
                var s = this.$outer.find(".lg-current").index(),
                    r = this;
                if (!r.lGalleryOn || s !== e) {
                    var o = this.$slide.length,
                        a = r.lGalleryOn ? this.s.speed : 0;
                    if (!r.lgBusy) {
                        var l, c, d;
                        if (this.s.download)(l = r.s.dynamic ? !1 !== r.s.dynamicEl[e].downloadUrl && (r.s.dynamicEl[e].downloadUrl || r.s.dynamicEl[e].src) : "false" !== r.$items.eq(e).attr("data-download-url") && (r.$items.eq(e).attr("data-download-url") || r.$items.eq(e).attr("href") || r.$items.eq(e).attr("data-src"))) ? (m("#lg-download").attr("href", l), r.$outer.removeClass("lg-hide-download")) : r.$outer.addClass("lg-hide-download");
                        if (this.$el.trigger("onBeforeSlide.lg", [s, e, t, n]), r.lgBusy = !0, clearTimeout(r.hideBartimeout), ".lg-sub-html" === this.s.appendSubHtmlTo && setTimeout(function() {
                                r.addHtml(e)
                            }, a), this.arrowDisable(e), i || (e < s ? i = "prev" : s < e && (i = "next")), t) this.$slide.removeClass("lg-prev-slide lg-current lg-next-slide"), 2 < o ? (c = e - 1, d = e + 1, 0 === e && s === o - 1 ? (d = 0, c = o - 1) : e === o - 1 && 0 === s && (d = 0, c = o - 1)) : (c = 0, d = 1), "prev" === i ? r.$slide.eq(d).addClass("lg-next-slide") : r.$slide.eq(c).addClass("lg-prev-slide"), r.$slide.eq(e).addClass("lg-current");
                        else r.$outer.addClass("lg-no-trans"), this.$slide.removeClass("lg-prev-slide lg-next-slide"), "prev" === i ? (this.$slide.eq(e).addClass("lg-prev-slide"), this.$slide.eq(s).addClass("lg-next-slide")) : (this.$slide.eq(e).addClass("lg-next-slide"), this.$slide.eq(s).addClass("lg-prev-slide")), setTimeout(function() {
                            r.$slide.removeClass("lg-current"), r.$slide.eq(e).addClass("lg-current"), r.$outer.removeClass("lg-no-trans")
                        }, 50);
                        r.lGalleryOn ? (setTimeout(function() {
                            r.loadContent(e, !0, 0)
                        }, this.s.speed + 50), setTimeout(function() {
                            r.lgBusy = !1, r.$el.trigger("onAfterSlide.lg", [s, e, t, n])
                        }, this.s.speed)) : (r.loadContent(e, !0, r.s.backdropDuration), r.lgBusy = !1, r.$el.trigger("onAfterSlide.lg", [s, e, t, n])), r.lGalleryOn = !0, this.s.counter && m("#lg-counter-current").text(e + 1)
                    }
                    r.index = e
                }
            }, t.prototype.goToNextSlide = function(e) {
                var t = this,
                    n = t.s.loop;
                e && t.$slide.length < 3 && (n = !1), t.lgBusy || (t.index + 1 < t.$slide.length ? (t.index++, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1, "next")) : n ? (t.index = 0, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1, "next")) : t.s.slideEndAnimatoin && !e && (t.$outer.addClass("lg-right-end"), setTimeout(function() {
                    t.$outer.removeClass("lg-right-end")
                }, 400)))
            }, t.prototype.goToPrevSlide = function(e) {
                var t = this,
                    n = t.s.loop;
                e && t.$slide.length < 3 && (n = !1), t.lgBusy || (0 < t.index ? (t.index--, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1, "prev")) : n ? (t.index = t.$items.length - 1, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1, "prev")) : t.s.slideEndAnimatoin && !e && (t.$outer.addClass("lg-left-end"), setTimeout(function() {
                    t.$outer.removeClass("lg-left-end")
                }, 400)))
            }, t.prototype.keyPress = function() {
                var t = this;
                1 < this.$items.length && m(window).on("keyup.lg", function(e) {
                    1 < t.$items.length && (37 === e.keyCode && (e.preventDefault(), t.goToPrevSlide()), 39 === e.keyCode && (e.preventDefault(), t.goToNextSlide()))
                }), m(window).on("keydown.lg", function(e) {
                    !0 === t.s.escKey && 27 === e.keyCode && (e.preventDefault(), t.$outer.hasClass("lg-thumb-open") ? t.$outer.removeClass("lg-thumb-open") : t.destroy())
                })
            }, t.prototype.arrow = function() {
                var e = this;
                this.$outer.find(".lg-prev").on("click.lg", function() {
                    e.goToPrevSlide()
                }), this.$outer.find(".lg-next").on("click.lg", function() {
                    e.goToNextSlide()
                })
            }, t.prototype.arrowDisable = function(e) {
                !this.s.loop && this.s.hideControlOnEnd && (e + 1 < this.$slide.length ? this.$outer.find(".lg-next").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-next").attr("disabled", "disabled").addClass("disabled"), 0 < e ? this.$outer.find(".lg-prev").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-prev").attr("disabled", "disabled").addClass("disabled"))
            }, t.prototype.setTranslate = function(e, t, n) {
                this.s.useLeft ? e.css("left", t) : e.css({
                    transform: "translate3d(" + t + "px, " + n + "px, 0px)"
                })
            }, t.prototype.touchMove = function(e, t) {
                var n = t - e;
                15 < Math.abs(n) && (this.$outer.addClass("lg-dragging"), this.setTranslate(this.$slide.eq(this.index), n, 0), this.setTranslate(m(".lg-prev-slide"), -this.$slide.eq(this.index).width() + n, 0), this.setTranslate(m(".lg-next-slide"), this.$slide.eq(this.index).width() + n, 0))
            }, t.prototype.touchEnd = function(e) {
                var t = this;
                "lg-slide" !== t.s.mode && t.$outer.addClass("lg-slide"), this.$slide.not(".lg-current, .lg-prev-slide, .lg-next-slide").css("opacity", "0"), setTimeout(function() {
                    t.$outer.removeClass("lg-dragging"), e < 0 && Math.abs(e) > t.s.swipeThreshold ? t.goToNextSlide(!0) : 0 < e && Math.abs(e) > t.s.swipeThreshold ? t.goToPrevSlide(!0) : Math.abs(e) < 5 && t.$el.trigger("onSlideClick.lg"), t.$slide.removeAttr("style")
                }), setTimeout(function() {
                    t.$outer.hasClass("lg-dragging") || "lg-slide" === t.s.mode || t.$outer.removeClass("lg-slide")
                }, t.s.speed + 100)
            }, t.prototype.enableSwipe = function() {
                var t = this,
                    n = 0,
                    i = 0,
                    s = !1;
                t.s.enableSwipe && t.doCss() && (t.$slide.on("touchstart.lg", function(e) {
                    t.$outer.hasClass("lg-zoomed") || t.lgBusy || (e.preventDefault(), t.manageSwipeClass(), n = e.originalEvent.targetTouches[0].pageX)
                }), t.$slide.on("touchmove.lg", function(e) {
                    t.$outer.hasClass("lg-zoomed") || (e.preventDefault(), i = e.originalEvent.targetTouches[0].pageX, t.touchMove(n, i), s = !0)
                }), t.$slide.on("touchend.lg", function() {
                    t.$outer.hasClass("lg-zoomed") || (s ? (s = !1, t.touchEnd(i - n)) : t.$el.trigger("onSlideClick.lg"))
                }))
            }, t.prototype.enableDrag = function() {
                var t = this,
                    n = 0,
                    i = 0,
                    s = !1,
                    r = !1;
                t.s.enableDrag && t.doCss() && (t.$slide.on("mousedown.lg", function(e) {
                    t.$outer.hasClass("lg-zoomed") || (m(e.target).hasClass("lg-object") || m(e.target).hasClass("lg-video-play")) && (e.preventDefault(), t.lgBusy || (t.manageSwipeClass(), n = e.pageX, s = !0, t.$outer.scrollLeft += 1, t.$outer.scrollLeft -= 1, t.$outer.removeClass("lg-grab").addClass("lg-grabbing"), t.$el.trigger("onDragstart.lg")))
                }), m(window).on("mousemove.lg", function(e) {
                    s && (r = !0, i = e.pageX, t.touchMove(n, i), t.$el.trigger("onDragmove.lg"))
                }), m(window).on("mouseup.lg", function(e) {
                    r ? (r = !1, t.touchEnd(i - n), t.$el.trigger("onDragend.lg")) : (m(e.target).hasClass("lg-object") || m(e.target).hasClass("lg-video-play")) && t.$el.trigger("onSlideClick.lg"), s && (s = !1, t.$outer.removeClass("lg-grabbing").addClass("lg-grab"))
                }))
            }, t.prototype.manageSwipeClass = function() {
                var e = this.index + 1,
                    t = this.index - 1;
                this.s.loop && 2 < this.$slide.length && (0 === this.index ? t = this.$slide.length - 1 : this.index === this.$slide.length - 1 && (e = 0)), this.$slide.removeClass("lg-next-slide lg-prev-slide"), -1 < t && this.$slide.eq(t).addClass("lg-prev-slide"), this.$slide.eq(e).addClass("lg-next-slide")
            }, t.prototype.mousewheel = function() {
                var t = this;
                t.$outer.on("mousewheel.lg", function(e) {
                    e.deltaY && (0 < e.deltaY ? t.goToPrevSlide() : t.goToNextSlide(), e.preventDefault())
                })
            }, t.prototype.closeGallery = function() {
                var t = this,
                    n = !1;
                this.$outer.find(".lg-close").on("click.lg", function() {
                    t.destroy()
                }), t.s.closable && (t.$outer.on("mousedown.lg", function(e) {
                    n = !!(m(e.target).is(".lg-outer") || m(e.target).is(".lg-item ") || m(e.target).is(".lg-img-wrap"))
                }), t.$outer.on("mouseup.lg", function(e) {
                    (m(e.target).is(".lg-outer") || m(e.target).is(".lg-item ") || m(e.target).is(".lg-img-wrap") && n) && (t.$outer.hasClass("lg-dragging") || t.destroy())
                }))
            }, t.prototype.destroy = function(e) {
                var t = this;
                e || (t.$el.trigger("onBeforeClose.lg"), m(window).scrollTop(t.prevScrollTop)), e && (t.s.dynamic || this.$items.off("click.lg click.lgcustom"), m.removeData(t.el, "lightGallery")), this.$el.off(".lg.tm"), m.each(m.fn.lightGallery.modules, function(e) {
                    t.modules[e] && t.modules[e].destroy()
                }), this.lGalleryOn = !1, clearTimeout(t.hideBartimeout), this.hideBartimeout = !1, m(window).off(".lg"), m("body").removeClass("lg-on lg-from-hash"), t.$outer && t.$outer.removeClass("lg-visible"), m(".lg-backdrop").removeClass("in"), setTimeout(function() {
                    t.$outer && t.$outer.remove(), m(".lg-backdrop").remove(), e || t.$el.trigger("onCloseAfter.lg")
                }, t.s.backdropDuration + 50)
            }, m.fn.lightGallery = function(e) {
                return this.each(function() {
                    if (m.data(this, "lightGallery")) try {
                        m(this).data("lightGallery").init()
                    } catch (e) {
                        console.error("lightGallery has not initiated properly")
                    } else m.data(this, "lightGallery", new t(this, e))
                })
            }, m.fn.lightGallery.modules = {}
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(n) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = n(e).data("lightGallery"), this.$el = n(e), !(this.core.$items.length < 2) && (this.core.s = n.extend({}, t, this.core.s), this.interval = !1, this.fromAuto = !0, this.canceledOnTouch = !1, this.fourceAutoplayTemp = this.core.s.fourceAutoplay, this.core.doCss() || (this.core.s.progressBar = !1), this.init(), this)
            }
            var t = {
                autoplay: !1,
                pause: 5e3,
                progressBar: !0,
                fourceAutoplay: !1,
                autoplayControls: !0,
                appendAutoplayControlsTo: ".lg-toolbar"
            };
            e.prototype.init = function() {
                var e = this;
                e.core.s.autoplayControls && e.controls(), e.core.s.progressBar && e.core.$outer.find(".lg").append('<div class="lg-progress-bar"><div class="lg-progress"></div></div>'), e.progress(), e.core.s.autoplay && e.$el.one("onSlideItemLoad.lg.tm", function() {
                    e.startlAuto()
                }), e.$el.on("onDragstart.lg.tm touchstart.lg.tm", function() {
                    e.interval && (e.cancelAuto(), e.canceledOnTouch = !0)
                }), e.$el.on("onDragend.lg.tm touchend.lg.tm onSlideClick.lg.tm", function() {
                    !e.interval && e.canceledOnTouch && (e.startlAuto(), e.canceledOnTouch = !1)
                })
            }, e.prototype.progress = function() {
                var e, t, n = this;
                n.$el.on("onBeforeSlide.lg.tm", function() {
                    n.core.s.progressBar && n.fromAuto && (e = n.core.$outer.find(".lg-progress-bar"), t = n.core.$outer.find(".lg-progress"), n.interval && (t.removeAttr("style"), e.removeClass("lg-start"), setTimeout(function() {
                        t.css("transition", "width " + (n.core.s.speed + n.core.s.pause) + "ms ease 0s"), e.addClass("lg-start")
                    }, 20))), n.fromAuto || n.core.s.fourceAutoplay || n.cancelAuto(), n.fromAuto = !1
                })
            }, e.prototype.controls = function() {
                var e = this;
                n(this.core.s.appendAutoplayControlsTo).append('<span class="lg-autoplay-button lg-icon"></span>'), e.core.$outer.find(".lg-autoplay-button").on("click.lg", function() {
                    n(e.core.$outer).hasClass("lg-show-autoplay") ? (e.cancelAuto(), e.core.s.fourceAutoplay = !1) : e.interval || (e.startlAuto(), e.core.s.fourceAutoplay = e.fourceAutoplayTemp)
                })
            }, e.prototype.startlAuto = function() {
                var e = this;
                e.core.$outer.find(".lg-progress").css("transition", "width " + (e.core.s.speed + e.core.s.pause) + "ms ease 0s"), e.core.$outer.addClass("lg-show-autoplay"), e.core.$outer.find(".lg-progress-bar").addClass("lg-start"), e.interval = setInterval(function() {
                    e.core.index + 1 < e.core.$items.length ? e.core.index++ : e.core.index = 0, e.fromAuto = !0, e.core.slide(e.core.index, !1, !1, "next")
                }, e.core.s.speed + e.core.s.pause)
            }, e.prototype.cancelAuto = function() {
                clearInterval(this.interval), this.interval = !1, this.core.$outer.find(".lg-progress").removeAttr("style"), this.core.$outer.removeClass("lg-show-autoplay"), this.core.$outer.find(".lg-progress-bar").removeClass("lg-start")
            }, e.prototype.destroy = function() {
                this.cancelAuto(), this.core.$outer.find(".lg-progress-bar").remove()
            }, n.fn.lightGallery.modules.autoplay = e
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(n) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = n(e).data("lightGallery"), this.$el = n(e), this.core.s = n.extend({}, t, this.core.s), this.init(), this
            }
            var t = {
                fullScreen: !0
            };
            e.prototype.init = function() {
                var e = "";
                if (this.core.s.fullScreen) {
                    if (!(document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled)) return;
                    e = '<span class="lg-fullscreen lg-icon"></span>', this.core.$outer.find(".lg-toolbar").append(e), this.fullScreen()
                }
            }, e.prototype.requestFullscreen = function() {
                var e = document.documentElement;
                e.requestFullscreen ? e.requestFullscreen() : e.msRequestFullscreen ? e.msRequestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.webkitRequestFullscreen && e.webkitRequestFullscreen()
            }, e.prototype.exitFullscreen = function() {
                window.fullScreen && (document.exitFullscreen ? document.exitFullscreen() : document.msExitFullscreen ? document.msExitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen && document.webkitExitFullscreen())
            }, e.prototype.fullScreen = function() {
                var e = this;
                n(document).on("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg", function() {
                    e.core.$outer.toggleClass("lg-fullscreen-on")
                }), this.core.$outer.find(".lg-fullscreen").on("click.lg", function() {
                    document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement ? e.exitFullscreen() : e.requestFullscreen()
                })
            }, e.prototype.destroy = function() {
                this.exitFullscreen(), n(document).off("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg")
            }, n.fn.lightGallery.modules.fullscreen = e
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(o) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = o(e).data("lightGallery"), this.$el = o(e), this.core.s = o.extend({}, t, this.core.s), this.core.s.pager && 1 < this.core.$items.length && this.init(), this
            }
            var t = {
                pager: !1
            };
            e.prototype.init = function() {
                var i, e, t, n = this,
                    s = "";
                if (n.core.$outer.find(".lg").append('<div class="lg-pager-outer"></div>'), n.core.s.dynamic)
                    for (var r = 0; r < n.core.s.dynamicEl.length; r++) s += '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + n.core.s.dynamicEl[r].thumb + '" /></div></span>';
                else n.core.$items.each(function() {
                    s += n.core.s.exThumbImage ? '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + o(this).attr(n.core.s.exThumbImage) + '" /></div></span>' : '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + o(this).find("img").attr("src") + '" /></div></span>'
                });
                (e = n.core.$outer.find(".lg-pager-outer")).html(s), (i = n.core.$outer.find(".lg-pager-cont")).on("click.lg touchend.lg", function() {
                    var e = o(this);
                    n.core.index = e.index(), n.core.slide(n.core.index, !1, !0, !1)
                }), e.on("mouseover.lg", function() {
                    clearTimeout(t), e.addClass("lg-pager-hover")
                }), e.on("mouseout.lg", function() {
                    t = setTimeout(function() {
                        e.removeClass("lg-pager-hover")
                    })
                }), n.core.$el.on("onBeforeSlide.lg.tm", function(e, t, n) {
                    i.removeClass("lg-pager-active"), i.eq(n).addClass("lg-pager-active")
                })
            }, e.prototype.destroy = function() {}, o.fn.lightGallery.modules.pager = e
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(c) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = c(e).data("lightGallery"), this.core.s = c.extend({}, t, this.core.s), this.$el = c(e), this.$thumbOuter = null, this.thumbOuterWidth = 0, this.thumbTotalWidth = this.core.$items.length * (this.core.s.thumbWidth + this.core.s.thumbMargin), this.thumbIndex = this.core.index, this.core.s.animateThumb && (this.core.s.thumbHeight = "100%"), this.left = 0, this.init(), this
            }
            var t = {
                thumbnail: !0,
                animateThumb: !0,
                currentPagerPosition: "middle",
                thumbWidth: 100,
                thumbHeight: "80px",
                thumbContHeight: 100,
                thumbMargin: 5,
                exThumbImage: !1,
                showThumbByDefault: !0,
                toogleThumb: !0,
                pullCaptionUp: !0,
                enableThumbDrag: !0,
                enableThumbSwipe: !0,
                swipeThreshold: 50,
                loadYoutubeThumbnail: !0,
                youtubeThumbSize: 1,
                loadVimeoThumbnail: !0,
                vimeoThumbSize: "thumbnail_small",
                loadDailymotionThumbnail: !0
            };
            e.prototype.init = function() {
                var e = this;
                this.core.s.thumbnail && 1 < this.core.$items.length && (this.core.s.showThumbByDefault && setTimeout(function() {
                    e.core.$outer.addClass("lg-thumb-open")
                }, 700), this.core.s.pullCaptionUp && this.core.$outer.addClass("lg-pull-caption-up"), this.build(), this.core.s.animateThumb && this.core.doCss() ? (this.core.s.enableThumbDrag && this.enableThumbDrag(), this.core.s.enableThumbSwipe && this.enableThumbSwipe(), this.thumbClickable = !1) : this.thumbClickable = !0, this.toogle(), this.thumbkeyPress())
            }, e.prototype.build = function() {
                function t(e, t, n) {
                    var i, s = o.core.isVideo(e, n) || {},
                        r = "";
                    s.youtube || s.vimeo || s.dailymotion ? s.youtube ? i = o.core.s.loadYoutubeThumbnail ? "//img.youtube.com/vi/" + s.youtube[1] + "/" + o.core.s.youtubeThumbSize + ".jpg" : t : s.vimeo ? o.core.s.loadVimeoThumbnail ? (i = "//i.vimeocdn.com/video/error_" + l + ".jpg", r = s.vimeo[1]) : i = t : s.dailymotion && (i = o.core.s.loadDailymotionThumbnail ? "//www.dailymotion.com/thumbnail/video/" + s.dailymotion[1] : t) : i = t, a += '<div data-vimeo-id="' + r + '" class="lg-thumb-item" style="width:' + o.core.s.thumbWidth + "px; height: " + o.core.s.thumbHeight + "; margin-right: " + o.core.s.thumbMargin + 'px"><img src="' + i + '" /></div>', r = ""
                }
                var e, o = this,
                    a = "",
                    l = "";
                switch (this.core.s.vimeoThumbSize) {
                    case "thumbnail_large":
                        l = "640";
                        break;
                    case "thumbnail_medium":
                        l = "200x150";
                        break;
                    case "thumbnail_small":
                        l = "100x75"
                }
                if (o.core.$outer.addClass("lg-has-thumb"), o.core.$outer.find(".lg").append('<div class="lg-thumb-outer"><div class="lg-thumb lg-group"></div></div>'), o.$thumbOuter = o.core.$outer.find(".lg-thumb-outer"), o.thumbOuterWidth = o.$thumbOuter.width(), o.core.s.animateThumb && o.core.$outer.find(".lg-thumb").css({
                        width: o.thumbTotalWidth + "px",
                        position: "relative"
                    }), this.core.s.animateThumb && o.$thumbOuter.css("height", o.core.s.thumbContHeight + "px"), o.core.s.dynamic)
                    for (var n = 0; n < o.core.s.dynamicEl.length; n++) t(o.core.s.dynamicEl[n].src, o.core.s.dynamicEl[n].thumb, n);
                else o.core.$items.each(function(e) {
                    o.core.s.exThumbImage ? t(c(this).attr("href") || c(this).attr("data-src"), c(this).attr(o.core.s.exThumbImage), e) : t(c(this).attr("href") || c(this).attr("data-src"), c(this).find("img").attr("src"), e)
                });
                o.core.$outer.find(".lg-thumb").html(a), (e = o.core.$outer.find(".lg-thumb-item")).each(function() {
                    var t = c(this),
                        e = t.attr("data-vimeo-id");
                    e && c.getJSON("//www.vimeo.com/api/v2/video/" + e + ".json?callback=?", {
                        format: "json"
                    }, function(e) {
                        t.find("img").attr("src", e[0][o.core.s.vimeoThumbSize])
                    })
                }), e.eq(o.core.index).addClass("active"), o.core.$el.on("onBeforeSlide.lg.tm", function() {
                    e.removeClass("active"), e.eq(o.core.index).addClass("active")
                }), e.on("click.lg touchend.lg", function() {
                    var e = c(this);
                    setTimeout(function() {
                        (!o.thumbClickable || o.core.lgBusy) && o.core.doCss() || (o.core.index = e.index(), o.core.slide(o.core.index, !1, !0, !1))
                    }, 50)
                }), o.core.$el.on("onBeforeSlide.lg.tm", function() {
                    o.animateThumb(o.core.index)
                }), c(window).on("resize.lg.thumb orientationchange.lg.thumb", function() {
                    setTimeout(function() {
                        o.animateThumb(o.core.index), o.thumbOuterWidth = o.$thumbOuter.width()
                    }, 200)
                })
            }, e.prototype.setTranslate = function(e) {
                this.core.$outer.find(".lg-thumb").css({
                    transform: "translate3d(-" + e + "px, 0px, 0px)"
                })
            }, e.prototype.animateThumb = function(e) {
                var t = this.core.$outer.find(".lg-thumb");
                if (this.core.s.animateThumb) {
                    var n;
                    switch (this.core.s.currentPagerPosition) {
                        case "left":
                            n = 0;
                            break;
                        case "middle":
                            n = this.thumbOuterWidth / 2 - this.core.s.thumbWidth / 2;
                            break;
                        case "right":
                            n = this.thumbOuterWidth - this.core.s.thumbWidth
                    }
                    this.left = (this.core.s.thumbWidth + this.core.s.thumbMargin) * e - 1 - n, this.left > this.thumbTotalWidth - this.thumbOuterWidth && (this.left = this.thumbTotalWidth - this.thumbOuterWidth), this.left < 0 && (this.left = 0), this.core.lGalleryOn ? (t.hasClass("on") || this.core.$outer.find(".lg-thumb").css("transition-duration", this.core.s.speed + "ms"), this.core.doCss() || t.animate({
                        left: -this.left + "px"
                    }, this.core.s.speed)) : this.core.doCss() || t.css("left", -this.left + "px"), this.setTranslate(this.left)
                }
            }, e.prototype.enableThumbDrag = function() {
                var t = this,
                    n = 0,
                    i = 0,
                    s = !1,
                    r = !1,
                    o = 0;
                t.$thumbOuter.addClass("lg-grab"), t.core.$outer.find(".lg-thumb").on("mousedown.lg.thumb", function(e) {
                    t.thumbTotalWidth > t.thumbOuterWidth && (e.preventDefault(), n = e.pageX, s = !0, t.core.$outer.scrollLeft += 1, t.core.$outer.scrollLeft -= 1, t.thumbClickable = !1, t.$thumbOuter.removeClass("lg-grab").addClass("lg-grabbing"))
                }), c(window).on("mousemove.lg.thumb", function(e) {
                    s && (o = t.left, r = !0, i = e.pageX, t.$thumbOuter.addClass("lg-dragging"), (o -= i - n) > t.thumbTotalWidth - t.thumbOuterWidth && (o = t.thumbTotalWidth - t.thumbOuterWidth), o < 0 && (o = 0), t.setTranslate(o))
                }), c(window).on("mouseup.lg.thumb", function() {
                    r ? (r = !1, t.$thumbOuter.removeClass("lg-dragging"), t.left = o, Math.abs(i - n) < t.core.s.swipeThreshold && (t.thumbClickable = !0)) : t.thumbClickable = !0, s && (s = !1, t.$thumbOuter.removeClass("lg-grabbing").addClass("lg-grab"))
                })
            }, e.prototype.enableThumbSwipe = function() {
                var t = this,
                    n = 0,
                    i = 0,
                    s = !1,
                    r = 0;
                t.core.$outer.find(".lg-thumb").on("touchstart.lg", function(e) {
                    t.thumbTotalWidth > t.thumbOuterWidth && (e.preventDefault(), n = e.originalEvent.targetTouches[0].pageX, t.thumbClickable = !1)
                }), t.core.$outer.find(".lg-thumb").on("touchmove.lg", function(e) {
                    t.thumbTotalWidth > t.thumbOuterWidth && (e.preventDefault(), i = e.originalEvent.targetTouches[0].pageX, s = !0, t.$thumbOuter.addClass("lg-dragging"), r = t.left, (r -= i - n) > t.thumbTotalWidth - t.thumbOuterWidth && (r = t.thumbTotalWidth - t.thumbOuterWidth), r < 0 && (r = 0), t.setTranslate(r))
                }), t.core.$outer.find(".lg-thumb").on("touchend.lg", function() {
                    t.thumbTotalWidth > t.thumbOuterWidth && s ? (s = !1, t.$thumbOuter.removeClass("lg-dragging"), Math.abs(i - n) < t.core.s.swipeThreshold && (t.thumbClickable = !0), t.left = r) : t.thumbClickable = !0
                })
            }, e.prototype.toogle = function() {
                var e = this;
                e.core.s.toogleThumb && (e.core.$outer.addClass("lg-can-toggle"), e.$thumbOuter.append('<span class="lg-toogle-thumb lg-icon"></span>'), e.core.$outer.find(".lg-toogle-thumb").on("click.lg", function() {
                    e.core.$outer.toggleClass("lg-thumb-open")
                }))
            }, e.prototype.thumbkeyPress = function() {
                var t = this;
                c(window).on("keydown.lg.thumb", function(e) {
                    38 === e.keyCode ? (e.preventDefault(), t.core.$outer.addClass("lg-thumb-open")) : 40 === e.keyCode && (e.preventDefault(), t.core.$outer.removeClass("lg-thumb-open"))
                })
            }, e.prototype.destroy = function() {
                this.core.s.thumbnail && 1 < this.core.$items.length && (c(window).off("resize.lg.thumb orientationchange.lg.thumb keydown.lg.thumb"), this.$thumbOuter.remove(), this.core.$outer.removeClass("lg-has-thumb"))
            }, c.fn.lightGallery.modules.Thumbnail = e
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(h) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = h(e).data("lightGallery"), this.$el = h(e), this.core.s = h.extend({}, t, this.core.s), this.videoLoaded = !1, this.init(), this
            }
            var t = {
                videoMaxWidth: "855px",
                youtubePlayerParams: !1,
                vimeoPlayerParams: !1,
                dailymotionPlayerParams: !1,
                vkPlayerParams: !1,
                videojs: !1,
                videojsOptions: {}
            };
            e.prototype.init = function() {
                var u = this;
                u.core.$el.on("hasVideo.lg.tm", function(e, t, n, i) {
                    if (u.core.$slide.eq(t).find(".lg-video").append(u.loadVideo(n, "lg-object", !0, t, i)), i)
                        if (u.core.s.videojs) try {
                            videojs(u.core.$slide.eq(t).find(".lg-html5").get(0), u.core.s.videojsOptions, function() {
                                u.videoLoaded || this.play()
                            })
                        } catch (e) {
                            console.error("Make sure you have included videojs")
                        } else u.videoLoaded || u.core.$slide.eq(t).find(".lg-html5").get(0).play()
                }), u.core.$el.on("onAferAppendSlide.lg.tm", function(e, t) {
                    var n = u.core.$slide.eq(t).find(".lg-video-cont");
                    n.hasClass("lg-has-iframe") || (n.css("max-width", u.core.s.videoMaxWidth), u.videoLoaded = !0)
                });

                function t(n) {
                    if (n.find(".lg-object").hasClass("lg-has-poster") && n.find(".lg-object").is(":visible"))
                        if (n.hasClass("lg-has-video")) {
                            var e = n.find(".lg-youtube").get(0),
                                t = n.find(".lg-vimeo").get(0),
                                i = n.find(".lg-dailymotion").get(0),
                                s = n.find(".lg-html5").get(0);
                            if (e) e.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', "*");
                            else if (t) try {
                                    $f(t).api("play")
                                } catch (n) {
                                    console.error("Make sure you have included froogaloop2 js")
                                } else if (i) i.contentWindow.postMessage("play", "*");
                                else if (s)
                                if (u.core.s.videojs) try {
                                    videojs(s).play()
                                } catch (n) {
                                    console.error("Make sure you have included videojs")
                                } else s.play();
                            n.addClass("lg-video-playing")
                        } else {
                            n.addClass("lg-video-playing lg-has-video");
                            var r, o, a = function(e, t) {
                                if (n.find(".lg-video").append(u.loadVideo(e, "", !1, u.core.index, t)), t)
                                    if (u.core.s.videojs) try {
                                        videojs(u.core.$slide.eq(u.core.index).find(".lg-html5").get(0), u.core.s.videojsOptions, function() {
                                            this.play()
                                        })
                                    } catch (e) {
                                        console.error("Make sure you have included videojs")
                                    } else u.core.$slide.eq(u.core.index).find(".lg-html5").get(0).play()
                            };
                            o = u.core.s.dynamic ? (r = u.core.s.dynamicEl[u.core.index].src, u.core.s.dynamicEl[u.core.index].html) : (r = u.core.$items.eq(u.core.index).attr("href") || u.core.$items.eq(u.core.index).attr("data-src"), u.core.$items.eq(u.core.index).attr("data-html")), a(r, o);
                            var l = n.find(".lg-object");
                            n.find(".lg-video").append(l), n.find(".lg-video-object").hasClass("lg-html5") || (n.removeClass("lg-complete"), n.find(".lg-video-object").on("load.lg error.lg", function() {
                                n.addClass("lg-complete")
                            }))
                        }
                }
                u.core.doCss() && 1 < u.core.$items.length && (u.core.s.enableSwipe || u.core.s.enableDrag) ? u.core.$el.on("onSlideClick.lg.tm", function() {
                    var e = u.core.$slide.eq(u.core.index);
                    t(e)
                }) : u.core.$slide.on("click.lg", function() {
                    t(h(this))
                }), u.core.$el.on("onBeforeSlide.lg.tm", function(e, t, n) {
                    var i, s = u.core.$slide.eq(t),
                        r = s.find(".lg-youtube").get(0),
                        o = s.find(".lg-vimeo").get(0),
                        a = s.find(".lg-dailymotion").get(0),
                        l = s.find(".lg-vk").get(0),
                        c = s.find(".lg-html5").get(0);
                    if (r) r.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*");
                    else if (o) try {
                            $f(o).api("pause")
                        } catch (e) {
                            console.error("Make sure you have included froogaloop2 js")
                        } else if (a) a.contentWindow.postMessage("pause", "*");
                        else if (c)
                        if (u.core.s.videojs) try {
                            videojs(c).pause()
                        } catch (e) {
                            console.error("Make sure you have included videojs")
                        } else c.pause();
                    l && h(l).attr("src", h(l).attr("src").replace("&autoplay", "&noplay")), i = u.core.s.dynamic ? u.core.s.dynamicEl[n].src : u.core.$items.eq(n).attr("href") || u.core.$items.eq(n).attr("data-src");
                    var d = u.core.isVideo(i, n) || {};
                    (d.youtube || d.vimeo || d.dailymotion || d.vk) && u.core.$outer.addClass("lg-hide-download")
                }), u.core.$el.on("onAfterSlide.lg.tm", function(e, t) {
                    u.core.$slide.eq(t).removeClass("lg-video-playing")
                })
            }, e.prototype.loadVideo = function(e, t, n, i, s) {
                var r = "",
                    o = 1,
                    a = "",
                    l = this.core.isVideo(e, i) || {};
                if (n && (o = this.videoLoaded ? 0 : 1), l.youtube) a = "?wmode=opaque&autoplay=" + o + "&enablejsapi=1", this.core.s.youtubePlayerParams && (a = a + "&" + h.param(this.core.s.youtubePlayerParams)), r = '<iframe class="lg-video-object lg-youtube ' + t + '" width="560" height="315" src="//www.youtube.com/embed/' + l.youtube[1] + a + '" frameborder="0" allowfullscreen></iframe>';
                else if (l.vimeo) a = "?autoplay=" + o + "&api=1", this.core.s.vimeoPlayerParams && (a = a + "&" + h.param(this.core.s.vimeoPlayerParams)), r = '<iframe class="lg-video-object lg-vimeo ' + t + '" width="560" height="315"  src="//player.vimeo.com/video/' + l.vimeo[1] + a + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
                else if (l.dailymotion) a = "?wmode=opaque&autoplay=" + o + "&api=postMessage", this.core.s.dailymotionPlayerParams && (a = a + "&" + h.param(this.core.s.dailymotionPlayerParams)), r = '<iframe class="lg-video-object lg-dailymotion ' + t + '" width="560" height="315" src="//www.dailymotion.com/embed/video/' + l.dailymotion[1] + a + '" frameborder="0" allowfullscreen></iframe>';
                else if (l.html5) {
                    var c = s.substring(0, 1);
                    "." !== c && "#" !== c || (s = h(s).html()), r = s
                } else l.vk && (a = "&autoplay=" + o, this.core.s.vkPlayerParams && (a = a + "&" + h.param(this.core.s.vkPlayerParams)), r = '<iframe class="lg-video-object lg-vk ' + t + '" width="560" height="315" src="http://vk.com/video_ext.php?' + l.vk[1] + a + '" frameborder="0" allowfullscreen></iframe>');
                return r
            }, e.prototype.destroy = function() {
                this.videoLoaded = !1
            }, h.fn.lightGallery.modules.video = e
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(u) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = u(e).data("lightGallery"), this.core.s = u.extend({}, i, this.core.s), this.core.s.zoom && this.core.doCss() && (this.init(), this.zoomabletimeout = !1, this.pageX = u(window).width() / 2, this.pageY = u(window).height() / 2 + u(window).scrollTop()), this
            }
            var t, n, i = {
                scale: 1,
                zoom: !0,
                actualSize: !0,
                enableZoomAfter: 300,
                useLeftForZoom: (t = !1, n = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./), n && parseInt(n[2], 10) < 54 && (t = !0), t)
            };
            e.prototype.init = function() {
                var o = this,
                    e = '<span id="lg-zoom-in" class="lg-icon"></span><span id="lg-zoom-out" class="lg-icon"></span>';
                o.core.s.actualSize && (e += '<span id="lg-actual-size" class="lg-icon"></span>'), o.core.s.useLeftForZoom ? o.core.$outer.addClass("lg-use-left-for-zoom") : o.core.$outer.addClass("lg-use-transition-for-zoom"), this.core.$outer.find(".lg-toolbar").append(e), o.core.$el.on("onSlideItemLoad.lg.tm.zoom", function(e, t, n) {
                    var i = o.core.s.enableZoomAfter + n;
                    u("body").hasClass("lg-from-hash") && n ? i = 0 : u("body").removeClass("lg-from-hash"), o.zoomabletimeout = setTimeout(function() {
                        o.core.$slide.eq(t).addClass("lg-zoomable")
                    }, i + 30)
                });

                function t(e) {
                    var t = o.core.$outer.find(".lg-current .lg-image"),
                        n = (u(window).width() - t.prop("offsetWidth")) / 2,
                        i = (u(window).height() - t.prop("offsetHeight")) / 2 + u(window).scrollTop(),
                        s = (e - 1) * (o.pageX - n),
                        r = (e - 1) * (o.pageY - i);
                    t.css("transform", "scale3d(" + e + ", " + e + ", 1)").attr("data-scale", e), o.core.s.useLeftForZoom ? t.parent().css({
                        left: -s + "px",
                        top: -r + "px"
                    }).attr("data-x", s).attr("data-y", r) : t.parent().css("transform", "translate3d(-" + s + "px, -" + r + "px, 0)").attr("data-x", s).attr("data-y", r)
                }

                function a() {
                    1 < l ? o.core.$outer.addClass("lg-zoomed") : o.resetZoom(), l < 1 && (l = 1), t(l)
                }

                function i(e, t, n, i) {
                    var s, r = t.prop("offsetWidth");
                    s = o.core.s.dynamic ? o.core.s.dynamicEl[n].width || t[0].naturalWidth || r : o.core.$items.eq(n).attr("data-width") || t[0].naturalWidth || r, o.core.$outer.hasClass("lg-zoomed") ? l = 1 : r < s && (l = s / r || 2), i ? (o.pageX = u(window).width() / 2, o.pageY = u(window).height() / 2 + u(window).scrollTop()) : (o.pageX = e.pageX || e.originalEvent.targetTouches[0].pageX, o.pageY = e.pageY || e.originalEvent.targetTouches[0].pageY), a(), setTimeout(function() {
                        o.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")
                    }, 10)
                }
                var l = 1,
                    s = !1;
                o.core.$el.on("onAferAppendSlide.lg.tm.zoom", function(e, t) {
                    var n = o.core.$slide.eq(t).find(".lg-image");
                    n.on("dblclick", function(e) {
                        i(e, n, t)
                    }), n.on("touchstart", function(e) {
                        s ? (clearTimeout(s), s = null, i(e, n, t)) : s = setTimeout(function() {
                            s = null
                        }, 300), e.preventDefault()
                    })
                }), u(window).on("resize.lg.zoom scroll.lg.zoom orientationchange.lg.zoom", function() {
                    o.pageX = u(window).width() / 2, o.pageY = u(window).height() / 2 + u(window).scrollTop(), t(l)
                }), u("#lg-zoom-out").on("click.lg", function() {
                    o.core.$outer.find(".lg-current .lg-image").length && (l -= o.core.s.scale, a())
                }), u("#lg-zoom-in").on("click.lg", function() {
                    o.core.$outer.find(".lg-current .lg-image").length && (l += o.core.s.scale, a())
                }), u("#lg-actual-size").on("click.lg", function(e) {
                    i(e, o.core.$slide.eq(o.core.index).find(".lg-image"), o.core.index, !0)
                }), o.core.$el.on("onBeforeSlide.lg.tm", function() {
                    l = 1, o.resetZoom()
                }), o.zoomDrag(), o.zoomSwipe()
            }, e.prototype.resetZoom = function() {
                this.core.$outer.removeClass("lg-zoomed"), this.core.$slide.find(".lg-img-wrap").removeAttr("style data-x data-y"), this.core.$slide.find(".lg-image").removeAttr("style data-scale"), this.pageX = u(window).width() / 2, this.pageY = u(window).height() / 2 + u(window).scrollTop()
            }, e.prototype.zoomSwipe = function() {
                var s = this,
                    r = {},
                    o = {},
                    a = !1,
                    l = !1,
                    c = !1;
                s.core.$slide.on("touchstart.lg", function(e) {
                    if (s.core.$outer.hasClass("lg-zoomed")) {
                        var t = s.core.$slide.eq(s.core.index).find(".lg-object");
                        c = t.prop("offsetHeight") * t.attr("data-scale") > s.core.$outer.find(".lg").height(), ((l = t.prop("offsetWidth") * t.attr("data-scale") > s.core.$outer.find(".lg").width()) || c) && (e.preventDefault(), r = {
                            x: e.originalEvent.targetTouches[0].pageX,
                            y: e.originalEvent.targetTouches[0].pageY
                        })
                    }
                }), s.core.$slide.on("touchmove.lg", function(e) {
                    if (s.core.$outer.hasClass("lg-zoomed")) {
                        var t, n, i = s.core.$slide.eq(s.core.index).find(".lg-img-wrap");
                        e.preventDefault(), a = !0, o = {
                            x: e.originalEvent.targetTouches[0].pageX,
                            y: e.originalEvent.targetTouches[0].pageY
                        }, s.core.$outer.addClass("lg-zoom-dragging"), n = c ? -Math.abs(i.attr("data-y")) + (o.y - r.y) : -Math.abs(i.attr("data-y")), t = l ? -Math.abs(i.attr("data-x")) + (o.x - r.x) : -Math.abs(i.attr("data-x")), (15 < Math.abs(o.x - r.x) || 15 < Math.abs(o.y - r.y)) && (s.core.s.useLeftForZoom ? i.css({
                            left: t + "px",
                            top: n + "px"
                        }) : i.css("transform", "translate3d(" + t + "px, " + n + "px, 0)"))
                    }
                }), s.core.$slide.on("touchend.lg", function() {
                    s.core.$outer.hasClass("lg-zoomed") && a && (a = !1, s.core.$outer.removeClass("lg-zoom-dragging"), s.touchendZoom(r, o, l, c))
                })
            }, e.prototype.zoomDrag = function() {
                var s = this,
                    r = {},
                    o = {},
                    a = !1,
                    l = !1,
                    c = !1,
                    d = !1;
                s.core.$slide.on("mousedown.lg.zoom", function(e) {
                    var t = s.core.$slide.eq(s.core.index).find(".lg-object");
                    d = t.prop("offsetHeight") * t.attr("data-scale") > s.core.$outer.find(".lg").height(), c = t.prop("offsetWidth") * t.attr("data-scale") > s.core.$outer.find(".lg").width(), s.core.$outer.hasClass("lg-zoomed") && u(e.target).hasClass("lg-object") && (c || d) && (e.preventDefault(), r = {
                        x: e.pageX,
                        y: e.pageY
                    }, a = !0, s.core.$outer.scrollLeft += 1, s.core.$outer.scrollLeft -= 1, s.core.$outer.removeClass("lg-grab").addClass("lg-grabbing"))
                }), u(window).on("mousemove.lg.zoom", function(e) {
                    if (a) {
                        var t, n, i = s.core.$slide.eq(s.core.index).find(".lg-img-wrap");
                        l = !0, o = {
                            x: e.pageX,
                            y: e.pageY
                        }, s.core.$outer.addClass("lg-zoom-dragging"), n = d ? -Math.abs(i.attr("data-y")) + (o.y - r.y) : -Math.abs(i.attr("data-y")), t = c ? -Math.abs(i.attr("data-x")) + (o.x - r.x) : -Math.abs(i.attr("data-x")), s.core.s.useLeftForZoom ? i.css({
                            left: t + "px",
                            top: n + "px"
                        }) : i.css("transform", "translate3d(" + t + "px, " + n + "px, 0)")
                    }
                }), u(window).on("mouseup.lg.zoom", function(e) {
                    a && (a = !1, s.core.$outer.removeClass("lg-zoom-dragging"), !l || r.x === o.x && r.y === o.y || (o = {
                        x: e.pageX,
                        y: e.pageY
                    }, s.touchendZoom(r, o, c, d)), l = !1), s.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")
                })
            }, e.prototype.touchendZoom = function(e, t, n, i) {
                var s = this.core.$slide.eq(this.core.index).find(".lg-img-wrap"),
                    r = this.core.$slide.eq(this.core.index).find(".lg-object"),
                    o = -Math.abs(s.attr("data-x")) + (t.x - e.x),
                    a = -Math.abs(s.attr("data-y")) + (t.y - e.y),
                    l = (this.core.$outer.find(".lg").height() - r.prop("offsetHeight")) / 2,
                    c = Math.abs(r.prop("offsetHeight") * Math.abs(r.attr("data-scale")) - this.core.$outer.find(".lg").height() + l),
                    d = (this.core.$outer.find(".lg").width() - r.prop("offsetWidth")) / 2,
                    u = Math.abs(r.prop("offsetWidth") * Math.abs(r.attr("data-scale")) - this.core.$outer.find(".lg").width() + d);
                (15 < Math.abs(t.x - e.x) || 15 < Math.abs(t.y - e.y)) && (i && (a <= -c ? a = -c : -l <= a && (a = -l)), n && (o <= -u ? o = -u : -d <= o && (o = -d)), i ? s.attr("data-y", Math.abs(a)) : a = -Math.abs(s.attr("data-y")), n ? s.attr("data-x", Math.abs(o)) : o = -Math.abs(s.attr("data-x")), this.core.s.useLeftForZoom ? s.css({
                    left: o + "px",
                    top: a + "px"
                }) : s.css("transform", "translate3d(" + o + "px, " + a + "px, 0)"))
            }, e.prototype.destroy = function() {
                this.core.$el.off(".lg.zoom"), u(window).off(".lg.zoom"), this.core.$slide.off(".lg.zoom"), this.core.$el.off(".lg.tm.zoom"), this.resetZoom(), clearTimeout(this.zoomabletimeout), this.zoomabletimeout = !1
            }, u.fn.lightGallery.modules.zoom = e
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(n) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = n(e).data("lightGallery"), this.core.s = n.extend({}, t, this.core.s), this.core.s.hash && (this.oldHash = window.location.hash, this.init()), this
            }
            var t = {
                hash: !0
            };
            e.prototype.init = function() {
                var t, i = this;
                i.core.$el.on("onAfterSlide.lg.tm", function(e, t, n) {
                    history.replaceState ? history.replaceState(null, null, "#lg=" + i.core.s.galleryId + "&slide=" + n) : window.location.hash = "lg=" + i.core.s.galleryId + "&slide=" + n
                }), n(window).on("hashchange.lg.hash", function() {
                    t = window.location.hash;
                    var e = parseInt(t.split("&slide=")[1], 10); - 1 < t.indexOf("lg=" + i.core.s.galleryId) ? i.core.slide(e, !1, !1) : i.core.lGalleryOn && i.core.destroy()
                })
            }, e.prototype.destroy = function() {
                this.core.s.hash && (this.oldHash && this.oldHash.indexOf("lg=" + this.core.s.galleryId) < 0 ? history.replaceState ? history.replaceState(null, null, this.oldHash) : window.location.hash = this.oldHash : history.replaceState ? history.replaceState(null, document.title, window.location.pathname + window.location.search) : window.location.hash = "", this.core.$el.off(".lg.hash"))
            }, n.fn.lightGallery.modules.hash = e
        }()
    }), function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(s) {
        ! function() {
            "use strict";

            function e(e) {
                return this.core = s(e).data("lightGallery"), this.core.s = s.extend({}, t, this.core.s), this.core.s.share && this.init(), this
            }
            var t = {
                share: !0,
                facebook: !0,
                facebookDropdownText: "Facebook",
                twitter: !0,
                twitterDropdownText: "Twitter",
                googlePlus: !0,
                googlePlusDropdownText: "GooglePlus",
                pinterest: !0,
                pinterestDropdownText: "Pinterest"
            };
            e.prototype.init = function() {
                var i = this,
                    e = '<span id="lg-share" class="lg-icon"><ul class="lg-dropdown" style="position: absolute;">';
                e += i.core.s.facebook ? '<li><a id="lg-share-facebook" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.facebookDropdownText + "</span></a></li>" : "", e += i.core.s.twitter ? '<li><a id="lg-share-twitter" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.twitterDropdownText + "</span></a></li>" : "", e += i.core.s.googlePlus ? '<li><a id="lg-share-googleplus" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.googlePlusDropdownText + "</span></a></li>" : "", e += i.core.s.pinterest ? '<li><a id="lg-share-pinterest" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.pinterestDropdownText + "</span></a></li>" : "", e += "</ul></span>", this.core.$outer.find(".lg-toolbar").append(e), this.core.$outer.find(".lg").append('<div id="lg-dropdown-overlay"></div>'), s("#lg-share").on("click.lg", function() {
                    i.core.$outer.toggleClass("lg-dropdown-active")
                }), s("#lg-dropdown-overlay").on("click.lg", function() {
                    i.core.$outer.removeClass("lg-dropdown-active")
                }), i.core.$el.on("onAfterSlide.lg.tm", function(e, t, n) {
                    setTimeout(function() {
                        s("#lg-share-facebook").attr("href", "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(i.getSahreProps(n, "facebookShareUrl") || window.location.href)), s("#lg-share-twitter").attr("href", "https://twitter.com/intent/tweet?text=" + i.getSahreProps(n, "tweetText") + "&url=" + encodeURIComponent(i.getSahreProps(n, "twitterShareUrl") || window.location.href)), s("#lg-share-googleplus").attr("href", "https://plus.google.com/share?url=" + encodeURIComponent(i.getSahreProps(n, "googleplusShareUrl") || window.location.href)), s("#lg-share-pinterest").attr("href", "http://www.pinterest.com/pin/create/button/?url=" + encodeURIComponent(i.getSahreProps(n, "pinterestShareUrl") || window.location.href) + "&media=" + encodeURIComponent(i.getSahreProps(n, "src")) + "&description=" + i.getSahreProps(n, "pinterestText"))
                    }, 100)
                })
            }, e.prototype.getSahreProps = function(e, t) {
                var n = "";
                if (this.core.s.dynamic) n = this.core.s.dynamicEl[e][t];
                else {
                    var i = this.core.$items.eq(e).attr("href"),
                        s = this.core.$items.eq(e).data(t);
                    n = "src" === t && i || s
                }
                return n
            }, e.prototype.destroy = function() {}, s.fn.lightGallery.modules.share = e
        }()
    }), function() {
        function t(e, t) {
            return function() {
                return e.apply(t, arguments)
            }
        }
        var i, e, n, l, s, o = [].indexOf || function(e) {
            for (var t = 0, n = this.length; t < n; t++)
                if (t in this && this[t] === e) return t;
            return -1
        };

        function r(e) {
            null == e && (e = {}), this.scrollCallback = t(this.scrollCallback, this), this.scrollHandler = t(this.scrollHandler, this), this.resetAnimation = t(this.resetAnimation, this), this.start = t(this.start, this), this.scrolled = !0, this.config = this.util().extend(e, this.defaults), null != e.scrollContainer && (this.config.scrollContainer = document.querySelector(e.scrollContainer)), this.animationNameCache = new n, this.wowEvent = this.util().createEvent(this.config.boxClass)
        }

        function a() {
            "undefined" != typeof console && null !== console && console.warn("MutationObserver is not supported by your browser."), "undefined" != typeof console && null !== console && console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")
        }

        function c() {
            this.keys = [], this.values = []
        }

        function d() {}
        d.prototype.extend = function(e, t) {
            var n, i;
            for (n in t) i = t[n], null == e[n] && (e[n] = i);
            return e
        }, d.prototype.isMobile = function(e) {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(e)
        }, d.prototype.createEvent = function(e, t, n, i) {
            var s;
            return null == t && (t = !1), null == n && (n = !1), null == i && (i = null), null != document.createEvent ? (s = document.createEvent("CustomEvent")).initCustomEvent(e, t, n, i) : null != document.createEventObject ? (s = document.createEventObject()).eventType = e : s.eventName = e, s
        }, d.prototype.emitEvent = function(e, t) {
            return null != e.dispatchEvent ? e.dispatchEvent(t) : t in (null != e) ? e[t]() : "on" + t in (null != e) ? e["on" + t]() : void 0
        }, d.prototype.addEvent = function(e, t, n) {
            return null != e.addEventListener ? e.addEventListener(t, n, !1) : null != e.attachEvent ? e.attachEvent("on" + t, n) : e[t] = n
        }, d.prototype.removeEvent = function(e, t, n) {
            return null != e.removeEventListener ? e.removeEventListener(t, n, !1) : null != e.detachEvent ? e.detachEvent("on" + t, n) : delete e[t]
        }, d.prototype.innerHeight = function() {
            return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight
        }, e = d, n = this.WeakMap || this.MozWeakMap || (c.prototype.get = function(e) {
            var t, n, i, s;
            for (t = n = 0, i = (s = this.keys).length; n < i; t = ++n)
                if (s[t] === e) return this.values[t]
        }, c.prototype.set = function(e, t) {
            var n, i, s, r;
            for (n = i = 0, s = (r = this.keys).length; i < s; n = ++i)
                if (r[n] === e) return void(this.values[n] = t);
            return this.keys.push(e), this.values.push(t)
        }, n = c), i = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (a.notSupported = !0, a.prototype.observe = function() {}, i = a), l = this.getComputedStyle || function(n, e) {
            return this.getPropertyValue = function(e) {
                var t;
                return "float" === e && (e = "styleFloat"), s.test(e) && e.replace(s, function(e, t) {
                    return t.toUpperCase()
                }), (null != (t = n.currentStyle) ? t[e] : void 0) || null
            }, this
        }, s = /(\-([a-z]){1})/g, this.WOW = (r.prototype.defaults = {
            boxClass: "wow",
            animateClass: "animated",
            offset: 0,
            mobile: !0,
            live: !0,
            callback: null,
            scrollContainer: null
        }, r.prototype.init = function() {
            var e;
            return this.element = window.document.documentElement, "interactive" === (e = document.readyState) || "complete" === e ? this.start() : this.util().addEvent(document, "DOMContentLoaded", this.start), this.finished = []
        }, r.prototype.start = function() {
            var s, e, t, n, o;
            if (this.stopped = !1, this.boxes = function() {
                    var e, t, n, i;
                    for (i = [], e = 0, t = (n = this.element.querySelectorAll("." + this.config.boxClass)).length; e < t; e++) s = n[e], i.push(s);
                    return i
                }.call(this), this.all = function() {
                    var e, t, n, i;
                    for (i = [], e = 0, t = (n = this.boxes).length; e < t; e++) s = n[e], i.push(s);
                    return i
                }.call(this), this.boxes.length)
                if (this.disabled()) this.resetStyle();
                else
                    for (e = 0, t = (n = this.boxes).length; e < t; e++) s = n[e], this.applyStyle(s, !0);
            return this.disabled() || (this.util().addEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().addEvent(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live ? new i((o = this, function(e) {
                var t, n, s, r, i;
                for (i = [], t = 0, n = e.length; t < n; t++) r = e[t], i.push(function() {
                    var e, t, n, i;
                    for (i = [], e = 0, t = (n = r.addedNodes || []).length; e < t; e++) s = n[e], i.push(this.doSync(s));
                    return i
                }.call(o));
                return i
            })).observe(document.body, {
                childList: !0,
                subtree: !0
            }) : void 0
        }, r.prototype.stop = function() {
            return this.stopped = !0, this.util().removeEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().removeEvent(window, "resize", this.scrollHandler), null != this.interval ? clearInterval(this.interval) : void 0
        }, r.prototype.sync = function(e) {
            return i.notSupported ? this.doSync(this.element) : void 0
        }, r.prototype.doSync = function(e) {
            var t, n, i, s, r;
            if (null == e && (e = this.element), 1 === e.nodeType) {
                for (r = [], n = 0, i = (s = (e = e.parentNode || e).querySelectorAll("." + this.config.boxClass)).length; n < i; n++) t = s[n], o.call(this.all, t) < 0 ? (this.boxes.push(t), this.all.push(t), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(t, !0), r.push(this.scrolled = !0)) : r.push(void 0);
                return r
            }
        }, r.prototype.show = function(e) {
            return this.applyStyle(e), e.className = e.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(e), this.util().emitEvent(e, this.wowEvent), this.util().addEvent(e, "animationend", this.resetAnimation), this.util().addEvent(e, "oanimationend", this.resetAnimation), this.util().addEvent(e, "webkitAnimationEnd", this.resetAnimation), this.util().addEvent(e, "MSAnimationEnd", this.resetAnimation), e
        }, r.prototype.applyStyle = function(e, t) {
            var n, i, s, r;
            return i = e.getAttribute("data-wow-duration"), n = e.getAttribute("data-wow-delay"), s = e.getAttribute("data-wow-iteration"), this.animate((r = this, function() {
                return r.customStyle(e, t, i, n, s)
            }))
        }, r.prototype.animate = "requestAnimationFrame" in window ? function(e) {
            return window.requestAnimationFrame(e)
        } : function(e) {
            return e()
        }, r.prototype.resetStyle = function() {
            var e, t, n, i, s;
            for (s = [], t = 0, n = (i = this.boxes).length; t < n; t++) e = i[t], s.push(e.style.visibility = "visible");
            return s
        }, r.prototype.resetAnimation = function(e) {
            var t;
            return 0 <= e.type.toLowerCase().indexOf("animationend") ? (t = e.target || e.srcElement).className = t.className.replace(this.config.animateClass, "").trim() : void 0
        }, r.prototype.customStyle = function(e, t, n, i, s) {
            return t && this.cacheAnimationName(e), e.style.visibility = t ? "hidden" : "visible", n && this.vendorSet(e.style, {
                animationDuration: n
            }), i && this.vendorSet(e.style, {
                animationDelay: i
            }), s && this.vendorSet(e.style, {
                animationIterationCount: s
            }), this.vendorSet(e.style, {
                animationName: t ? "none" : this.cachedAnimationName(e)
            }), e
        }, r.prototype.vendors = ["moz", "webkit"], r.prototype.vendorSet = function(s, e) {
            var r, t, o, a;
            for (r in t = [], e) o = e[r], s["" + r] = o, t.push(function() {
                var e, t, n, i;
                for (i = [], e = 0, t = (n = this.vendors).length; e < t; e++) a = n[e], i.push(s["" + a + r.charAt(0).toUpperCase() + r.substr(1)] = o);
                return i
            }.call(this));
            return t
        }, r.prototype.vendorCSS = function(e, t) {
            var n, i, s, r, o, a;
            for (r = (o = l(e)).getPropertyCSSValue(t), n = 0, i = (s = this.vendors).length; n < i; n++) a = s[n], r = r || o.getPropertyCSSValue("-" + a + "-" + t);
            return r
        }, r.prototype.animationName = function(t) {
            var n;
            try {
                n = this.vendorCSS(t, "animation-name").cssText
            } catch (e) {
                n = l(t).getPropertyValue("animation-name")
            }
            return "none" === n ? "" : n
        }, r.prototype.cacheAnimationName = function(e) {
            return this.animationNameCache.set(e, this.animationName(e))
        }, r.prototype.cachedAnimationName = function(e) {
            return this.animationNameCache.get(e)
        }, r.prototype.scrollHandler = function() {
            return this.scrolled = !0
        }, r.prototype.scrollCallback = function() {
            var s;
            return !this.scrolled || (this.scrolled = !1, this.boxes = function() {
                var e, t, n, i;
                for (i = [], e = 0, t = (n = this.boxes).length; e < t; e++)(s = n[e]) && (this.isVisible(s) ? this.show(s) : i.push(s));
                return i
            }.call(this), this.boxes.length || this.config.live) ? void 0 : this.stop()
        }, r.prototype.offsetTop = function(e) {
            for (var t; void 0 === e.offsetTop;) e = e.parentNode;
            for (t = e.offsetTop; e = e.offsetParent;) t += e.offsetTop;
            return t
        }, r.prototype.isVisible = function(e) {
            var t, n, i, s, r;
            return n = e.getAttribute("data-wow-offset") || this.config.offset, s = (r = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset) + Math.min(this.element.clientHeight, this.util().innerHeight()) - n, t = (i = this.offsetTop(e)) + e.clientHeight, i <= s && r <= t
        }, r.prototype.util = function() {
            return null != this._util ? this._util : this._util = new e
        }, r.prototype.disabled = function() {
            return !this.config.mobile && this.util().isMobile(navigator.userAgent)
        }, r)
    }.call(this), function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
    }(function(h) {
        function t(e) {
            var t = e || window.event,
                n = v.call(arguments, 1),
                i = 0,
                s = 0,
                r = 0,
                o = 0,
                a = 0,
                l = 0;
            if ((e = h.event.fix(t)).type = "mousewheel", "detail" in t && (r = -1 * t.detail), "wheelDelta" in t && (r = t.wheelDelta), "wheelDeltaY" in t && (r = t.wheelDeltaY), "wheelDeltaX" in t && (s = -1 * t.wheelDeltaX), "axis" in t && t.axis === t.HORIZONTAL_AXIS && (s = -1 * r, r = 0), i = 0 === r ? s : r, "deltaY" in t && (i = r = -1 * t.deltaY), "deltaX" in t && (s = t.deltaX, 0 === r && (i = -1 * s)), 0 !== r || 0 !== s) {
                if (1 === t.deltaMode) {
                    var c = h.data(this, "mousewheel-line-height");
                    i *= c, r *= c, s *= c
                } else if (2 === t.deltaMode) {
                    var d = h.data(this, "mousewheel-page-height");
                    i *= d, r *= d, s *= d
                }
                if (o = Math.max(Math.abs(r), Math.abs(s)), (!g || o < g) && (f(t, g = o) && (g /= 40)), f(t, o) && (i /= 40, s /= 40, r /= 40), i = Math[1 <= i ? "floor" : "ceil"](i / g), s = Math[1 <= s ? "floor" : "ceil"](s / g), r = Math[1 <= r ? "floor" : "ceil"](r / g), y.settings.normalizeOffset && this.getBoundingClientRect) {
                    var u = this.getBoundingClientRect();
                    a = e.clientX - u.left, l = e.clientY - u.top
                }
                return e.deltaX = s, e.deltaY = r, e.deltaFactor = g, e.offsetX = a, e.offsetY = l, e.deltaMode = 0, n.unshift(e, i, s, r), m && clearTimeout(m), m = setTimeout(p, 200), (h.event.dispatch || h.event.handle).apply(this, n)
            }
        }

        function p() {
            g = null
        }

        function f(e, t) {
            return y.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0
        }
        var m, g, e = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            n = "onwheel" in document || 9 <= document.documentMode ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            v = Array.prototype.slice;
        if (h.event.fixHooks)
            for (var i = e.length; i;) h.event.fixHooks[e[--i]] = h.event.mouseHooks;
        var y = h.event.special.mousewheel = {
            version: "3.1.12",
            setup: function() {
                if (this.addEventListener)
                    for (var e = n.length; e;) this.addEventListener(n[--e], t, !1);
                else this.onmousewheel = t;
                h.data(this, "mousewheel-line-height", y.getLineHeight(this)), h.data(this, "mousewheel-page-height", y.getPageHeight(this))
            },
            teardown: function() {
                if (this.removeEventListener)
                    for (var e = n.length; e;) this.removeEventListener(n[--e], t, !1);
                else this.onmousewheel = null;
                h.removeData(this, "mousewheel-line-height"), h.removeData(this, "mousewheel-page-height")
            },
            getLineHeight: function(e) {
                var t = h(e),
                    n = t["offsetParent" in h.fn ? "offsetParent" : "parent"]();
                return n.length || (n = h("body")), parseInt(n.css("fontSize"), 10) || parseInt(t.css("fontSize"), 10) || 16
            },
            getPageHeight: function(e) {
                return h(e).height()
            },
            settings: {
                adjustOldDeltas: !0,
                normalizeOffset: !0
            }
        };
        h.fn.extend({
            mousewheel: function(e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function(e) {
                return this.unbind("mousewheel", e)
            }
        })
    }), function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
    }(function(h) {
        function t(e) {
            var t = e || window.event,
                n = v.call(arguments, 1),
                i = 0,
                s = 0,
                r = 0,
                o = 0,
                a = 0,
                l = 0;
            if ((e = h.event.fix(t)).type = "mousewheel", "detail" in t && (r = -1 * t.detail), "wheelDelta" in t && (r = t.wheelDelta), "wheelDeltaY" in t && (r = t.wheelDeltaY), "wheelDeltaX" in t && (s = -1 * t.wheelDeltaX), "axis" in t && t.axis === t.HORIZONTAL_AXIS && (s = -1 * r, r = 0), i = 0 === r ? s : r, "deltaY" in t && (i = r = -1 * t.deltaY), "deltaX" in t && (s = t.deltaX, 0 === r && (i = -1 * s)), 0 !== r || 0 !== s) {
                if (1 === t.deltaMode) {
                    var c = h.data(this, "mousewheel-line-height");
                    i *= c, r *= c, s *= c
                } else if (2 === t.deltaMode) {
                    var d = h.data(this, "mousewheel-page-height");
                    i *= d, r *= d, s *= d
                }
                if (o = Math.max(Math.abs(r), Math.abs(s)), (!g || o < g) && (f(t, g = o) && (g /= 40)), f(t, o) && (i /= 40, s /= 40, r /= 40), i = Math[1 <= i ? "floor" : "ceil"](i / g), s = Math[1 <= s ? "floor" : "ceil"](s / g), r = Math[1 <= r ? "floor" : "ceil"](r / g), y.settings.normalizeOffset && this.getBoundingClientRect) {
                    var u = this.getBoundingClientRect();
                    a = e.clientX - u.left, l = e.clientY - u.top
                }
                return e.deltaX = s, e.deltaY = r, e.deltaFactor = g, e.offsetX = a, e.offsetY = l, e.deltaMode = 0, n.unshift(e, i, s, r), m && clearTimeout(m), m = setTimeout(p, 200), (h.event.dispatch || h.event.handle).apply(this, n)
            }
        }

        function p() {
            g = null
        }

        function f(e, t) {
            return y.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0
        }
        var m, g, e = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            n = "onwheel" in document || 9 <= document.documentMode ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            v = Array.prototype.slice;
        if (h.event.fixHooks)
            for (var i = e.length; i;) h.event.fixHooks[e[--i]] = h.event.mouseHooks;
        var y = h.event.special.mousewheel = {
            version: "3.1.12",
            setup: function() {
                if (this.addEventListener)
                    for (var e = n.length; e;) this.addEventListener(n[--e], t, !1);
                else this.onmousewheel = t;
                h.data(this, "mousewheel-line-height", y.getLineHeight(this)), h.data(this, "mousewheel-page-height", y.getPageHeight(this))
            },
            teardown: function() {
                if (this.removeEventListener)
                    for (var e = n.length; e;) this.removeEventListener(n[--e], t, !1);
                else this.onmousewheel = null;
                h.removeData(this, "mousewheel-line-height"), h.removeData(this, "mousewheel-page-height")
            },
            getLineHeight: function(e) {
                var t = h(e),
                    n = t["offsetParent" in h.fn ? "offsetParent" : "parent"]();
                return n.length || (n = h("body")), parseInt(n.css("fontSize"), 10) || parseInt(t.css("fontSize"), 10) || 16
            },
            getPageHeight: function(e) {
                return h(e).height()
            },
            settings: {
                adjustOldDeltas: !0,
                normalizeOffset: !0
            }
        };
        h.fn.extend({
            mousewheel: function(e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function(e) {
                return this.unbind("mousewheel", e)
            }
        })
    }), Util.inViewport = function(e, t) {
        var n = e.getBoundingClientRect(),
            i = n.top < window.innerHeight && 0 < n.bottom;
        return t instanceof Function && i && t(), i
    }, Util.merge = function(e, t) {
        t = t || {};
        for (var n = {}, i = 0; i < e.length; i++) {
            var s = e[i];
            if (s)
                for (var r in s)
                    if (!t.except || t.except.indexOf(r))
                        if (!(s[r] instanceof Object) || s[r] instanceof Node || s[r] instanceof Function) {
                            if (t.skipNull && null === s[r]) continue;
                            n[r] = s[r]
                        } else n[r] = Util.merge([n[r], s[r]], t)
        }
        return n
    }, Util.uId = function(e) {
        for (var t = "", n = 0; n < e; n++) t += String.fromCharCode(97 + 25 * Math.random());
        return t
    }, function(e, t) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.moment = t()
    }(this, function() {
        "use strict";

        function h() {
            return Re.apply(null, arguments)
        }

        function s(e) {
            return e instanceof Array || "[object Array]" === Object.prototype.toString.call(e)
        }

        function r(e) {
            return e instanceof Date || "[object Date]" === Object.prototype.toString.call(e)
        }

        function i(e, t) {
            var n, i = [];
            for (n = 0; n < e.length; ++n) i.push(t(e[n], n));
            return i
        }

        function p(e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }

        function o(e, t) {
            for (var n in t) p(t, n) && (e[n] = t[n]);
            return p(t, "toString") && (e.toString = t.toString), p(t, "valueOf") && (e.valueOf = t.valueOf), e
        }

        function a(e, t, n, i) {
            return le(e, t, n, i, !0).utc()
        }

        function f(e) {
            return null == e._pf && (e._pf = {
                empty: !1,
                unusedTokens: [],
                unusedInput: [],
                overflow: -2,
                charsLeftOver: 0,
                nullInput: !1,
                invalidMonth: null,
                invalidFormat: !1,
                userInvalidated: !1,
                iso: !1
            }), e._pf
        }

        function l(e) {
            if (null == e._isValid) {
                var t = f(e);
                e._isValid = !(isNaN(e._d.getTime()) || !(t.overflow < 0) || t.empty || t.invalidMonth || t.invalidWeekday || t.nullInput || t.invalidFormat || t.userInvalidated), e._strict && (e._isValid = e._isValid && 0 === t.charsLeftOver && 0 === t.unusedTokens.length && void 0 === t.bigHour)
            }
            return e._isValid
        }

        function c(e) {
            var t = a(NaN);
            return null != e ? o(f(t), e) : f(t).userInvalidated = !0, t
        }

        function d(e) {
            return void 0 === e
        }

        function u(e, t) {
            var n, i, s;
            if (d(t._isAMomentObject) || (e._isAMomentObject = t._isAMomentObject), d(t._i) || (e._i = t._i), d(t._f) || (e._f = t._f), d(t._l) || (e._l = t._l), d(t._strict) || (e._strict = t._strict), d(t._tzm) || (e._tzm = t._tzm), d(t._isUTC) || (e._isUTC = t._isUTC), d(t._offset) || (e._offset = t._offset), d(t._pf) || (e._pf = f(t)), d(t._locale) || (e._locale = t._locale), 0 < Ye.length)
                for (n in Ye) d(s = t[i = Ye[n]]) || (e[i] = s);
            return e
        }

        function m(e) {
            u(this, e), this._d = new Date(null != e._d ? e._d.getTime() : NaN), !1 === We && (We = !0, h.updateOffset(this), We = !1)
        }

        function g(e) {
            return e instanceof m || null != e && null != e._isAMomentObject
        }

        function v(e) {
            return e < 0 ? Math.ceil(e) : Math.floor(e)
        }

        function y(e) {
            var t = +e,
                n = 0;
            return 0 != t && isFinite(t) && (n = v(t)), n
        }

        function b(e, t, n) {
            var i, s = Math.min(e.length, t.length),
                r = Math.abs(e.length - t.length),
                o = 0;
            for (i = 0; i < s; i++)(n && e[i] !== t[i] || !n && y(e[i]) !== y(t[i])) && o++;
            return o + r
        }

        function w(e) {
            !1 === h.suppressDeprecationWarnings && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + e)
        }

        function e(e, t) {
            var n = !0;
            return o(function() {
                return n && (w(e + "\nArguments: " + Array.prototype.slice.call(arguments).join(", ") + "\n" + (new Error).stack), n = !1), t.apply(this, arguments)
            }, t)
        }

        function x(e, t) {
            Be[e] || (w(t), Be[e] = !0)
        }

        function T(e) {
            return e instanceof Function || "[object Function]" === Object.prototype.toString.call(e)
        }

        function C(e) {
            return "[object Object]" === Object.prototype.toString.call(e)
        }

        function S(e, t) {
            var n, i = o({}, e);
            for (n in t) p(t, n) && (C(e[n]) && C(t[n]) ? (i[n] = {}, o(i[n], e[n]), o(i[n], t[n])) : null != t[n] ? i[n] = t[n] : delete i[n]);
            return i
        }

        function E(e) {
            null != e && this.set(e)
        }

        function _(e) {
            return e ? e.toLowerCase().replace("_", "-") : e
        }

        function k(e) {
            var t = null;
            if (!Ve[e] && "undefined" != typeof module && module && module.exports) try {
                t = Ue._abbr, require("./locale/" + e), D(t)
            } catch (e) {}
            return Ve[e]
        }

        function D(e, t) {
            var n;
            return e && ((n = d(t) ? M(e) : $(e, t)) && (Ue = n)), Ue._abbr
        }

        function $(e, t) {
            return null !== t ? (t.abbr = e, null != Ve[e] ? (x("defineLocaleOverride", "use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale"), t = S(Ve[e]._config, t)) : null != t.parentLocale && (null != Ve[t.parentLocale] ? t = S(Ve[t.parentLocale]._config, t) : x("parentLocaleUndefined", "specified parentLocale is not defined yet")), Ve[e] = new E(t), D(e), Ve[e]) : (delete Ve[e], null)
        }

        function M(e) {
            var t;
            if (e && e._locale && e._locale._abbr && (e = e._locale._abbr), !e) return Ue;
            if (!s(e)) {
                if (t = k(e)) return t;
                e = [e]
            }
            return function(e) {
                for (var t, n, i, s, r = 0; r < e.length;) {
                    for (t = (s = _(e[r]).split("-")).length, n = (n = _(e[r + 1])) ? n.split("-") : null; 0 < t;) {
                        if (i = k(s.slice(0, t).join("-"))) return i;
                        if (n && n.length >= t && b(s, n, !0) >= t - 1) break;
                        t--
                    }
                    r++
                }
                return null
            }(e)
        }

        function t(e, t) {
            var n = e.toLowerCase();
            Ge[n] = Ge[n + "s"] = Ge[t] = e
        }

        function A(e) {
            return "string" == typeof e ? Ge[e] || Ge[e.toLowerCase()] : void 0
        }

        function O(e) {
            var t, n, i = {};
            for (n in e) p(e, n) && ((t = A(n)) && (i[t] = e[n]));
            return i
        }

        function n(t, n) {
            return function(e) {
                return null != e ? (P(this, t, e), h.updateOffset(this, n), this) : I(this, t)
            }
        }

        function I(e, t) {
            return e.isValid() ? e._d["get" + (e._isUTC ? "UTC" : "") + t]() : NaN
        }

        function P(e, t, n) {
            e.isValid() && e._d["set" + (e._isUTC ? "UTC" : "") + t](n)
        }

        function L(e, t) {
            var n;
            if ("object" == typeof e)
                for (n in e) this.set(n, e[n]);
            else if (T(this[e = A(e)])) return this[e](t);
            return this
        }

        function N(e, t, n) {
            var i = "" + Math.abs(e),
                s = t - i.length;
            return (0 <= e ? n ? "+" : "" : "-") + Math.pow(10, Math.max(0, s)).toString().substr(1) + i
        }

        function j(e, t, n, i) {
            var s = i;
            "string" == typeof i && (s = function() {
                return this[i]()
            }), e && (Ze[e] = s), t && (Ze[t[0]] = function() {
                return N(s.apply(this, arguments), t[1], t[2])
            }), n && (Ze[n] = function() {
                return this.localeData().ordinal(s.apply(this, arguments), e)
            })
        }

        function H(e, t) {
            return e.isValid() ? (t = z(t, e.localeData()), Ke[t] = Ke[t] || function(n) {
                var i, s, e, r = n.match(Xe);
                for (i = 0, s = r.length; i < s; i++) Ze[r[i]] ? r[i] = Ze[r[i]] : r[i] = (e = r[i]).match(/\[[\s\S]/) ? e.replace(/^\[|\]$/g, "") : e.replace(/\\/g, "");
                return function(e) {
                    var t = "";
                    for (i = 0; i < s; i++) t += r[i] instanceof Function ? r[i].call(e, n) : r[i];
                    return t
                }
            }(t), Ke[t](e)) : e.localeData().invalidDate()
        }

        function z(e, t) {
            function n(e) {
                return t.longDateFormat(e) || e
            }
            var i = 5;
            for (Qe.lastIndex = 0; 0 <= i && Qe.test(e);) e = e.replace(Qe, n), Qe.lastIndex = 0, i -= 1;
            return e
        }

        function q(e, n, i) {
            mt[e] = T(n) ? n : function(e, t) {
                return e && i ? i : n
            }
        }

        function F(e, t) {
            return p(mt, e) ? mt[e](t._strict, t._locale) : new RegExp(function(e) {
                return R(e.replace("\\", "").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(e, t, n, i, s) {
                    return t || n || i || s
                }))
            }(e))
        }

        function R(e) {
            return e.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
        }

        function Y(e, n) {
            var t, i = n;
            for ("string" == typeof e && (e = [e]), "number" == typeof n && (i = function(e, t) {
                    t[n] = y(e)
                }), t = 0; t < e.length; t++) gt[e[t]] = i
        }

        function W(e, s) {
            Y(e, function(e, t, n, i) {
                n._w = n._w || {}, s(e, n._w, n, i)
            })
        }

        function B(e, t) {
            return new Date(Date.UTC(e, t + 1, 0)).getUTCDate()
        }

        function U(e, t) {
            var n;
            if (!e.isValid()) return e;
            if ("string" == typeof t)
                if (/^\d+$/.test(t)) t = y(t);
                else if ("number" != typeof(t = e.localeData().monthsParse(t))) return e;
            return n = Math.min(e.date(), B(e.year(), t)), e._d["set" + (e._isUTC ? "UTC" : "") + "Month"](t, n), e
        }

        function V(e) {
            return null != e ? (U(this, e), h.updateOffset(this, !0), this) : I(this, "Month")
        }

        function G() {
            function e(e, t) {
                return t.length - e.length
            }
            var t, n, i = [],
                s = [],
                r = [];
            for (t = 0; t < 12; t++) n = a([2e3, t]), i.push(this.monthsShort(n, "")), s.push(this.months(n, "")), r.push(this.months(n, "")), r.push(this.monthsShort(n, ""));
            for (i.sort(e), s.sort(e), r.sort(e), t = 0; t < 12; t++) i[t] = R(i[t]), s[t] = R(s[t]), r[t] = R(r[t]);
            this._monthsRegex = new RegExp("^(" + r.join("|") + ")", "i"), this._monthsShortRegex = this._monthsRegex, this._monthsStrictRegex = new RegExp("^(" + s.join("|") + ")$", "i"), this._monthsShortStrictRegex = new RegExp("^(" + i.join("|") + ")$", "i")
        }

        function X(e) {
            var t, n = e._a;
            return n && -2 === f(e).overflow && (t = n[yt] < 0 || 11 < n[yt] ? yt : n[bt] < 1 || n[bt] > B(n[vt], n[yt]) ? bt : n[wt] < 0 || 24 < n[wt] || 24 === n[wt] && (0 !== n[xt] || 0 !== n[Tt] || 0 !== n[Ct]) ? wt : n[xt] < 0 || 59 < n[xt] ? xt : n[Tt] < 0 || 59 < n[Tt] ? Tt : n[Ct] < 0 || 999 < n[Ct] ? Ct : -1, f(e)._overflowDayOfYear && (t < vt || bt < t) && (t = bt), f(e)._overflowWeeks && -1 === t && (t = St), f(e)._overflowWeekday && -1 === t && (t = Et), f(e).overflow = t), e
        }

        function Q(e) {
            var t, n, i, s, r, o, a = e._i,
                l = At.exec(a) || Ot.exec(a);
            if (l) {
                for (f(e).iso = !0, t = 0, n = Pt.length; t < n; t++)
                    if (Pt[t][1].exec(l[1])) {
                        s = Pt[t][0], i = !1 !== Pt[t][2];
                        break
                    } if (null == s) return void(e._isValid = !1);
                if (l[3]) {
                    for (t = 0, n = Lt.length; t < n; t++)
                        if (Lt[t][1].exec(l[3])) {
                            r = (l[2] || " ") + Lt[t][0];
                            break
                        } if (null == r) return void(e._isValid = !1)
                }
                if (!i && null != r) return void(e._isValid = !1);
                if (l[4]) {
                    if (!It.exec(l[4])) return void(e._isValid = !1);
                    o = "Z"
                }
                e._f = s + (r || "") + (o || ""), oe(e)
            } else e._isValid = !1
        }

        function K(e) {
            var t = new Date(Date.UTC.apply(null, arguments));
            return e < 100 && 0 <= e && isFinite(t.getUTCFullYear()) && t.setUTCFullYear(e), t
        }

        function Z(e) {
            return J(e) ? 366 : 365
        }

        function J(e) {
            return e % 4 == 0 && e % 100 != 0 || e % 400 == 0
        }

        function ee(e, t, n) {
            var i = 7 + t - n;
            return i - (7 + K(e, 0, i).getUTCDay() - t) % 7 - 1
        }

        function te(e, t, n, i, s) {
            var r, o, a = 1 + 7 * (t - 1) + (7 + n - i) % 7 + ee(e, i, s);
            return o = a <= 0 ? Z(r = e - 1) + a : a > Z(e) ? (r = e + 1, a - Z(e)) : (r = e, a), {
                year: r,
                dayOfYear: o
            }
        }

        function ne(e, t, n) {
            var i, s, r = ee(e.year(), t, n),
                o = Math.floor((e.dayOfYear() - r - 1) / 7) + 1;
            return o < 1 ? i = o + ie(s = e.year() - 1, t, n) : o > ie(e.year(), t, n) ? (i = o - ie(e.year(), t, n), s = e.year() + 1) : (s = e.year(), i = o), {
                week: i,
                year: s
            }
        }

        function ie(e, t, n) {
            var i = ee(e, t, n),
                s = ee(e + 1, t, n);
            return (Z(e) - i + s) / 7
        }

        function se(e, t, n) {
            return null != e ? e : null != t ? t : n
        }

        function re(e) {
            var t, n, i, s, r = [];
            if (!e._d) {
                for (i = function(e) {
                        var t = new Date(h.now());
                        return e._useUTC ? [t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate()] : [t.getFullYear(), t.getMonth(), t.getDate()]
                    }(e), e._w && null == e._a[bt] && null == e._a[yt] && function(e) {
                        var t, n, i, s, r, o, a, l;
                        null != (t = e._w).GG || null != t.W || null != t.E ? (r = 1, o = 4, n = se(t.GG, e._a[vt], ne(ce(), 1, 4).year), i = se(t.W, 1), ((s = se(t.E, 1)) < 1 || 7 < s) && (l = !0)) : (r = e._locale._week.dow, o = e._locale._week.doy, n = se(t.gg, e._a[vt], ne(ce(), r, o).year), i = se(t.w, 1), null != t.d ? ((s = t.d) < 0 || 6 < s) && (l = !0) : null != t.e ? (s = t.e + r, (t.e < 0 || 6 < t.e) && (l = !0)) : s = r), i < 1 || i > ie(n, r, o) ? f(e)._overflowWeeks = !0 : null != l ? f(e)._overflowWeekday = !0 : (a = te(n, i, s, r, o), e._a[vt] = a.year, e._dayOfYear = a.dayOfYear)
                    }(e), e._dayOfYear && (s = se(e._a[vt], i[vt]), e._dayOfYear > Z(s) && (f(e)._overflowDayOfYear = !0), n = K(s, 0, e._dayOfYear), e._a[yt] = n.getUTCMonth(), e._a[bt] = n.getUTCDate()), t = 0; t < 3 && null == e._a[t]; ++t) e._a[t] = r[t] = i[t];
                for (; t < 7; t++) e._a[t] = r[t] = null == e._a[t] ? 2 === t ? 1 : 0 : e._a[t];
                24 === e._a[wt] && 0 === e._a[xt] && 0 === e._a[Tt] && 0 === e._a[Ct] && (e._nextDay = !0, e._a[wt] = 0), e._d = (e._useUTC ? K : function(e, t, n, i, s, r, o) {
                    var a = new Date(e, t, n, i, s, r, o);
                    return e < 100 && 0 <= e && isFinite(a.getFullYear()) && a.setFullYear(e), a
                }).apply(null, r), null != e._tzm && e._d.setUTCMinutes(e._d.getUTCMinutes() - e._tzm), e._nextDay && (e._a[wt] = 24)
            }
        }

        function oe(e) {
            if (e._f !== h.ISO_8601) {
                e._a = [], f(e).empty = !0;
                var t, n, i, s, r, o, a, l, c = "" + e._i,
                    d = c.length,
                    u = 0;
                for (i = z(e._f, e._locale).match(Xe) || [], t = 0; t < i.length; t++) s = i[t], (n = (c.match(F(s, e)) || [])[0]) && (0 < (r = c.substr(0, c.indexOf(n))).length && f(e).unusedInput.push(r), c = c.slice(c.indexOf(n) + n.length), u += n.length), Ze[s] ? (n ? f(e).empty = !1 : f(e).unusedTokens.push(s), o = s, l = e, null != (a = n) && p(gt, o) && gt[o](a, l._a, l, o)) : e._strict && !n && f(e).unusedTokens.push(s);
                f(e).charsLeftOver = d - u, 0 < c.length && f(e).unusedInput.push(c), !0 === f(e).bigHour && e._a[wt] <= 12 && 0 < e._a[wt] && (f(e).bigHour = void 0), e._a[wt] = function(e, t, n) {
                    var i;
                    return null == n ? t : null != e.meridiemHour ? e.meridiemHour(t, n) : (null != e.isPM && ((i = e.isPM(n)) && t < 12 && (t += 12), i || 12 !== t || (t = 0)), t)
                }(e._locale, e._a[wt], e._meridiem), re(e), X(e)
            } else Q(e)
        }

        function ae(e) {
            var t = e._i,
                n = e._f;
            return e._locale = e._locale || M(e._l), null === t || void 0 === n && "" === t ? c({
                nullInput: !0
            }) : ("string" == typeof t && (e._i = t = e._locale.preparse(t)), g(t) ? new m(X(t)) : (s(n) ? function(e) {
                var t, n, i, s, r;
                if (0 === e._f.length) return f(e).invalidFormat = !0, e._d = new Date(NaN);
                for (s = 0; s < e._f.length; s++) r = 0, t = u({}, e), null != e._useUTC && (t._useUTC = e._useUTC), t._f = e._f[s], oe(t), l(t) && (r += f(t).charsLeftOver, r += 10 * f(t).unusedTokens.length, f(t).score = r, (null == i || r < i) && (i = r, n = t));
                o(e, n || t)
            }(e) : n ? oe(e) : r(t) ? e._d = t : function(e) {
                var t = e._i;
                void 0 === t ? e._d = new Date(h.now()) : r(t) ? e._d = new Date(+t) : "string" == typeof t ? function(e) {
                    var t = Nt.exec(e._i);
                    null !== t ? e._d = new Date(+t[1]) : (Q(e), !1 === e._isValid && (delete e._isValid, h.createFromInputFallback(e)))
                }(e) : s(t) ? (e._a = i(t.slice(0), function(e) {
                    return parseInt(e, 10)
                }), re(e)) : "object" == typeof t ? function(e) {
                    if (!e._d) {
                        var t = O(e._i);
                        e._a = i([t.year, t.month, t.day || t.date, t.hour, t.minute, t.second, t.millisecond], function(e) {
                            return e && parseInt(e, 10)
                        }), re(e)
                    }
                }(e) : "number" == typeof t ? e._d = new Date(t) : h.createFromInputFallback(e)
            }(e), l(e) || (e._d = null), e))
        }

        function le(e, t, n, i, s) {
            var r = {};
            return "boolean" == typeof n && (i = n, n = void 0), r._isAMomentObject = !0, r._useUTC = r._isUTC = s, r._l = n, r._i = e, r._f = t, r._strict = i,
                function(e) {
                    var t = new m(X(ae(e)));
                    return t._nextDay && (t.add(1, "d"), t._nextDay = void 0), t
                }(r)
        }

        function ce(e, t, n, i) {
            return le(e, t, n, i, !1)
        }

        function de(e, t) {
            var n, i;
            if (1 === t.length && s(t[0]) && (t = t[0]), !t.length) return ce();
            for (n = t[0], i = 1; i < t.length; ++i) t[i].isValid() && !t[i][e](n) || (n = t[i]);
            return n
        }

        function ue(e) {
            var t = O(e),
                n = t.year || 0,
                i = t.quarter || 0,
                s = t.month || 0,
                r = t.week || 0,
                o = t.day || 0,
                a = t.hour || 0,
                l = t.minute || 0,
                c = t.second || 0,
                d = t.millisecond || 0;
            this._milliseconds = +d + 1e3 * c + 6e4 * l + 36e5 * a, this._days = +o + 7 * r, this._months = +s + 3 * i + 12 * n, this._data = {}, this._locale = M(), this._bubble()
        }

        function he(e) {
            return e instanceof ue
        }

        function pe(e, n) {
            j(e, 0, 0, function() {
                var e = this.utcOffset(),
                    t = "+";
                return e < 0 && (e = -e, t = "-"), t + N(~~(e / 60), 2) + n + N(~~e % 60, 2)
            })
        }

        function fe(e, t) {
            var n = (t || "").match(e) || [],
                i = ((n[n.length - 1] || []) + "").match(qt) || ["-", 0, 0],
                s = 60 * i[1] + y(i[2]);
            return "+" === i[0] ? s : -s
        }

        function me(e, t) {
            var n, i;
            return t._isUTC ? (n = t.clone(), i = (g(e) || r(e) ? +e : +ce(e)) - +n, n._d.setTime(+n._d + i), h.updateOffset(n, !1), n) : ce(e).local()
        }

        function ge(e) {
            return 15 * -Math.round(e._d.getTimezoneOffset() / 15)
        }

        function ve() {
            return !!this.isValid() && (this._isUTC && 0 === this._offset)
        }

        function ye(e, t) {
            var n, i, s, r = e,
                o = null;
            return he(e) ? r = {
                ms: e._milliseconds,
                d: e._days,
                M: e._months
            } : "number" == typeof e ? (r = {}, t ? r[t] = e : r.milliseconds = e) : (o = Ft.exec(e)) ? (n = "-" === o[1] ? -1 : 1, r = {
                y: 0,
                d: y(o[bt]) * n,
                h: y(o[wt]) * n,
                m: y(o[xt]) * n,
                s: y(o[Tt]) * n,
                ms: y(o[Ct]) * n
            }) : (o = Rt.exec(e)) ? (n = "-" === o[1] ? -1 : 1, r = {
                y: be(o[2], n),
                M: be(o[3], n),
                w: be(o[4], n),
                d: be(o[5], n),
                h: be(o[6], n),
                m: be(o[7], n),
                s: be(o[8], n)
            }) : null == r ? r = {} : "object" == typeof r && ("from" in r || "to" in r) && (s = function(e, t) {
                var n;
                return e.isValid() && t.isValid() ? (t = me(t, e), e.isBefore(t) ? n = we(e, t) : ((n = we(t, e)).milliseconds = -n.milliseconds, n.months = -n.months), n) : {
                    milliseconds: 0,
                    months: 0
                }
            }(ce(r.from), ce(r.to)), (r = {}).ms = s.milliseconds, r.M = s.months), i = new ue(r), he(e) && p(e, "_locale") && (i._locale = e._locale), i
        }

        function be(e, t) {
            var n = e && parseFloat(e.replace(",", "."));
            return (isNaN(n) ? 0 : n) * t
        }

        function we(e, t) {
            var n = {
                milliseconds: 0,
                months: 0
            };
            return n.months = t.month() - e.month() + 12 * (t.year() - e.year()), e.clone().add(n.months, "M").isAfter(t) && --n.months, n.milliseconds = +t - +e.clone().add(n.months, "M"), n
        }

        function xe(e) {
            return e < 0 ? -1 * Math.round(-1 * e) : Math.round(e)
        }

        function Te(i, s) {
            return function(e, t) {
                var n;
                return null === t || isNaN(+t) || (x(s, "moment()." + s + "(period, number) is deprecated. Please use moment()." + s + "(number, period)."), n = e, e = t, t = n), Ce(this, ye(e = "string" == typeof e ? +e : e, t), i), this
            }
        }

        function Ce(e, t, n, i) {
            var s = t._milliseconds,
                r = xe(t._days),
                o = xe(t._months);
            e.isValid() && (i = null == i || i, s && e._d.setTime(+e._d + s * n), r && P(e, "Date", I(e, "Date") + r * n), o && U(e, I(e, "Month") + o * n), i && h.updateOffset(e, r || o))
        }

        function Se(e) {
            var t;
            return void 0 === e ? this._locale._abbr : (null != (t = M(e)) && (this._locale = t), this)
        }

        function Ee() {
            return this._locale
        }

        function _e(e, t) {
            j(0, [e, e.length], 0, t)
        }

        function ke(e, t, n, i, s) {
            var r;
            return null == e ? ne(this, i, s).year : ((r = ie(e, i, s)) < t && (t = r), function(e, t, n, i, s) {
                var r = te(e, t, n, i, s),
                    o = K(r.year, 0, r.dayOfYear);
                return this.year(o.getUTCFullYear()), this.month(o.getUTCMonth()), this.date(o.getUTCDate()), this
            }.call(this, e, t, n, i, s))
        }

        function De() {
            return this.hours() % 12 || 12
        }

        function $e(e, t) {
            j(e, 0, 0, function() {
                return this.localeData().meridiem(this.hours(), this.minutes(), t)
            })
        }

        function Me(e, t) {
            return t._meridiemParse
        }

        function Ae(e, t) {
            t[Ct] = y(1e3 * ("0." + e))
        }

        function Oe(e) {
            return e
        }

        function Ie(e, t, n, i) {
            var s = M(),
                r = a().set(i, t);
            return s[n](r, e)
        }

        function Pe(e, t, n, i, s) {
            if ("number" == typeof e && (t = e, e = void 0), e = e || "", null != t) return Ie(e, t, n, s);
            var r, o = [];
            for (r = 0; r < i; r++) o[r] = Ie(e, r, n, s);
            return o
        }

        function Le(e, t, n, i) {
            var s = ye(t, n);
            return e._milliseconds += i * s._milliseconds, e._days += i * s._days, e._months += i * s._months, e._bubble()
        }

        function Ne(e) {
            return e < 0 ? Math.floor(e) : Math.ceil(e)
        }

        function je(e) {
            return 4800 * e / 146097
        }

        function He(e) {
            return 146097 * e / 4800
        }

        function ze(e) {
            return function() {
                return this.as(e)
            }
        }

        function qe(e) {
            return function() {
                return this._data[e]
            }
        }

        function Fe() {
            var e, t, n = Cn(this._milliseconds) / 1e3,
                i = Cn(this._days),
                s = Cn(this._months);
            t = v((e = v(n / 60)) / 60), n %= 60, e %= 60;
            var r = v(s / 12),
                o = s %= 12,
                a = i,
                l = t,
                c = e,
                d = n,
                u = this.asSeconds();
            return u ? (u < 0 ? "-" : "") + "P" + (r ? r + "Y" : "") + (o ? o + "M" : "") + (a ? a + "D" : "") + (l || c || d ? "T" : "") + (l ? l + "H" : "") + (c ? c + "M" : "") + (d ? d + "S" : "") : "P0D"
        }
        var Re, Ye = h.momentProperties = [],
            We = !1,
            Be = {};
        h.suppressDeprecationWarnings = !1;
        var Ue, Ve = {},
            Ge = {},
            Xe = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,
            Qe = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,
            Ke = {},
            Ze = {},
            Je = /\d/,
            et = /\d\d/,
            tt = /\d{3}/,
            nt = /\d{4}/,
            it = /[+-]?\d{6}/,
            st = /\d\d?/,
            rt = /\d\d\d\d?/,
            ot = /\d\d\d\d\d\d?/,
            at = /\d{1,3}/,
            lt = /\d{1,4}/,
            ct = /[+-]?\d{1,6}/,
            dt = /\d+/,
            ut = /[+-]?\d+/,
            ht = /Z|[+-]\d\d:?\d\d/gi,
            pt = /Z|[+-]\d\d(?::?\d\d)?/gi,
            ft = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,
            mt = {},
            gt = {},
            vt = 0,
            yt = 1,
            bt = 2,
            wt = 3,
            xt = 4,
            Tt = 5,
            Ct = 6,
            St = 7,
            Et = 8;
        j("M", ["MM", 2], "Mo", function() {
            return this.month() + 1
        }), j("MMM", 0, 0, function(e) {
            return this.localeData().monthsShort(this, e)
        }), j("MMMM", 0, 0, function(e) {
            return this.localeData().months(this, e)
        }), t("month", "M"), q("M", st), q("MM", st, et), q("MMM", function(e, t) {
            return t.monthsShortRegex(e)
        }), q("MMMM", function(e, t) {
            return t.monthsRegex(e)
        }), Y(["M", "MM"], function(e, t) {
            t[yt] = y(e) - 1
        }), Y(["MMM", "MMMM"], function(e, t, n, i) {
            var s = n._locale.monthsParse(e, i, n._strict);
            null != s ? t[yt] = s : f(n).invalidMonth = e
        });
        var _t = /D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/,
            kt = "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
            Dt = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
            $t = ft,
            Mt = ft,
            At = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/,
            Ot = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/,
            It = /Z|[+-]\d\d(?::?\d\d)?/,
            Pt = [
                ["YYYYYY-MM-DD", /[+-]\d{6}-\d\d-\d\d/],
                ["YYYY-MM-DD", /\d{4}-\d\d-\d\d/],
                ["GGGG-[W]WW-E", /\d{4}-W\d\d-\d/],
                ["GGGG-[W]WW", /\d{4}-W\d\d/, !1],
                ["YYYY-DDD", /\d{4}-\d{3}/],
                ["YYYY-MM", /\d{4}-\d\d/, !1],
                ["YYYYYYMMDD", /[+-]\d{10}/],
                ["YYYYMMDD", /\d{8}/],
                ["GGGG[W]WWE", /\d{4}W\d{3}/],
                ["GGGG[W]WW", /\d{4}W\d{2}/, !1],
                ["YYYYDDD", /\d{7}/]
            ],
            Lt = [
                ["HH:mm:ss.SSSS", /\d\d:\d\d:\d\d\.\d+/],
                ["HH:mm:ss,SSSS", /\d\d:\d\d:\d\d,\d+/],
                ["HH:mm:ss", /\d\d:\d\d:\d\d/],
                ["HH:mm", /\d\d:\d\d/],
                ["HHmmss.SSSS", /\d\d\d\d\d\d\.\d+/],
                ["HHmmss,SSSS", /\d\d\d\d\d\d,\d+/],
                ["HHmmss", /\d\d\d\d\d\d/],
                ["HHmm", /\d\d\d\d/],
                ["HH", /\d\d/]
            ],
            Nt = /^\/?Date\((\-?\d+)/i;
        h.createFromInputFallback = e("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.", function(e) {
            e._d = new Date(e._i + (e._useUTC ? " UTC" : ""))
        }), j("Y", 0, 0, function() {
            var e = this.year();
            return e <= 9999 ? "" + e : "+" + e
        }), j(0, ["YY", 2], 0, function() {
            return this.year() % 100
        }), j(0, ["YYYY", 4], 0, "year"), j(0, ["YYYYY", 5], 0, "year"), j(0, ["YYYYYY", 6, !0], 0, "year"), t("year", "y"), q("Y", ut), q("YY", st, et), q("YYYY", lt, nt), q("YYYYY", ct, it), q("YYYYYY", ct, it), Y(["YYYYY", "YYYYYY"], vt), Y("YYYY", function(e, t) {
            t[vt] = 2 === e.length ? h.parseTwoDigitYear(e) : y(e)
        }), Y("YY", function(e, t) {
            t[vt] = h.parseTwoDigitYear(e)
        }), Y("Y", function(e, t) {
            t[vt] = parseInt(e, 10)
        });
        var jt = n("FullYear", !(h.parseTwoDigitYear = function(e) {
            return y(e) + (68 < y(e) ? 1900 : 2e3)
        }));
        h.ISO_8601 = function() {};
        var Ht = e("moment().min is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548", function() {
                var e = ce.apply(null, arguments);
                return this.isValid() && e.isValid() ? e < this ? this : e : c()
            }),
            zt = e("moment().max is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548", function() {
                var e = ce.apply(null, arguments);
                return this.isValid() && e.isValid() ? this < e ? this : e : c()
            });
        pe("Z", ":"), pe("ZZ", ""), q("Z", pt), q("ZZ", pt), Y(["Z", "ZZ"], function(e, t, n) {
            n._useUTC = !0, n._tzm = fe(pt, e)
        });
        var qt = /([\+\-]|\d\d)/gi;
        h.updateOffset = function() {};
        var Ft = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?\d*)?$/,
            Rt = /^(-)?P(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)W)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?$/;
        ye.fn = ue.prototype;
        var Yt = Te(1, "add"),
            Wt = Te(-1, "subtract");
        h.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ";
        var Bt = e("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.", function(e) {
            return void 0 === e ? this.localeData() : this.locale(e)
        });
        j(0, ["gg", 2], 0, function() {
            return this.weekYear() % 100
        }), j(0, ["GG", 2], 0, function() {
            return this.isoWeekYear() % 100
        }), _e("gggg", "weekYear"), _e("ggggg", "weekYear"), _e("GGGG", "isoWeekYear"), _e("GGGGG", "isoWeekYear"), t("weekYear", "gg"), t("isoWeekYear", "GG"), q("G", ut), q("g", ut), q("GG", st, et), q("gg", st, et), q("GGGG", lt, nt), q("gggg", lt, nt), q("GGGGG", ct, it), q("ggggg", ct, it), W(["gggg", "ggggg", "GGGG", "GGGGG"], function(e, t, n, i) {
            t[i.substr(0, 2)] = y(e)
        }), W(["gg", "GG"], function(e, t, n, i) {
            t[i] = h.parseTwoDigitYear(e)
        }), j("Q", 0, "Qo", "quarter"), t("quarter", "Q"), q("Q", Je), Y("Q", function(e, t) {
            t[yt] = 3 * (y(e) - 1)
        }), j("w", ["ww", 2], "wo", "week"), j("W", ["WW", 2], "Wo", "isoWeek"), t("week", "w"), t("isoWeek", "W"), q("w", st), q("ww", st, et), q("W", st), q("WW", st, et), W(["w", "ww", "W", "WW"], function(e, t, n, i) {
            t[i.substr(0, 1)] = y(e)
        });
        j("D", ["DD", 2], "Do", "date"), t("date", "D"), q("D", st), q("DD", st, et), q("Do", function(e, t) {
            return e ? t._ordinalParse : t._ordinalParseLenient
        }), Y(["D", "DD"], bt), Y("Do", function(e, t) {
            t[bt] = y(e.match(st)[0])
        });
        var Ut = n("Date", !0);
        j("d", 0, "do", "day"), j("dd", 0, 0, function(e) {
            return this.localeData().weekdaysMin(this, e)
        }), j("ddd", 0, 0, function(e) {
            return this.localeData().weekdaysShort(this, e)
        }), j("dddd", 0, 0, function(e) {
            return this.localeData().weekdays(this, e)
        }), j("e", 0, 0, "weekday"), j("E", 0, 0, "isoWeekday"), t("day", "d"), t("weekday", "e"), t("isoWeekday", "E"), q("d", st), q("e", st), q("E", st), q("dd", ft), q("ddd", ft), q("dddd", ft), W(["dd", "ddd", "dddd"], function(e, t, n, i) {
            var s = n._locale.weekdaysParse(e, i, n._strict);
            null != s ? t.d = s : f(n).invalidWeekday = e
        }), W(["d", "e", "E"], function(e, t, n, i) {
            t[i] = y(e)
        });
        var Vt = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
            Gt = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
            Xt = "Su_Mo_Tu_We_Th_Fr_Sa".split("_");
        j("DDD", ["DDDD", 3], "DDDo", "dayOfYear"), t("dayOfYear", "DDD"), q("DDD", at), q("DDDD", tt), Y(["DDD", "DDDD"], function(e, t, n) {
            n._dayOfYear = y(e)
        }), j("H", ["HH", 2], 0, "hour"), j("h", ["hh", 2], 0, De), j("hmm", 0, 0, function() {
            return "" + De.apply(this) + N(this.minutes(), 2)
        }), j("hmmss", 0, 0, function() {
            return "" + De.apply(this) + N(this.minutes(), 2) + N(this.seconds(), 2)
        }), j("Hmm", 0, 0, function() {
            return "" + this.hours() + N(this.minutes(), 2)
        }), j("Hmmss", 0, 0, function() {
            return "" + this.hours() + N(this.minutes(), 2) + N(this.seconds(), 2)
        }), $e("a", !0), $e("A", !1), t("hour", "h"), q("a", Me), q("A", Me), q("H", st), q("h", st), q("HH", st, et), q("hh", st, et), q("hmm", rt), q("hmmss", ot), q("Hmm", rt), q("Hmmss", ot), Y(["H", "HH"], wt), Y(["a", "A"], function(e, t, n) {
            n._isPm = n._locale.isPM(e), n._meridiem = e
        }), Y(["h", "hh"], function(e, t, n) {
            t[wt] = y(e), f(n).bigHour = !0
        }), Y("hmm", function(e, t, n) {
            var i = e.length - 2;
            t[wt] = y(e.substr(0, i)), t[xt] = y(e.substr(i)), f(n).bigHour = !0
        }), Y("hmmss", function(e, t, n) {
            var i = e.length - 4,
                s = e.length - 2;
            t[wt] = y(e.substr(0, i)), t[xt] = y(e.substr(i, 2)), t[Tt] = y(e.substr(s)), f(n).bigHour = !0
        }), Y("Hmm", function(e, t, n) {
            var i = e.length - 2;
            t[wt] = y(e.substr(0, i)), t[xt] = y(e.substr(i))
        }), Y("Hmmss", function(e, t, n) {
            var i = e.length - 4,
                s = e.length - 2;
            t[wt] = y(e.substr(0, i)), t[xt] = y(e.substr(i, 2)), t[Tt] = y(e.substr(s))
        });
        var Qt = n("Hours", !0);
        j("m", ["mm", 2], 0, "minute"), t("minute", "m"), q("m", st), q("mm", st, et), Y(["m", "mm"], xt);
        var Kt = n("Minutes", !1);
        j("s", ["ss", 2], 0, "second"), t("second", "s"), q("s", st), q("ss", st, et), Y(["s", "ss"], Tt);
        var Zt, Jt = n("Seconds", !1);
        for (j("S", 0, 0, function() {
                return ~~(this.millisecond() / 100)
            }), j(0, ["SS", 2], 0, function() {
                return ~~(this.millisecond() / 10)
            }), j(0, ["SSS", 3], 0, "millisecond"), j(0, ["SSSS", 4], 0, function() {
                return 10 * this.millisecond()
            }), j(0, ["SSSSS", 5], 0, function() {
                return 100 * this.millisecond()
            }), j(0, ["SSSSSS", 6], 0, function() {
                return 1e3 * this.millisecond()
            }), j(0, ["SSSSSSS", 7], 0, function() {
                return 1e4 * this.millisecond()
            }), j(0, ["SSSSSSSS", 8], 0, function() {
                return 1e5 * this.millisecond()
            }), j(0, ["SSSSSSSSS", 9], 0, function() {
                return 1e6 * this.millisecond()
            }), t("millisecond", "ms"), q("S", at, Je), q("SS", at, et), q("SSS", at, tt), Zt = "SSSS"; Zt.length <= 9; Zt += "S") q(Zt, dt);
        for (Zt = "S"; Zt.length <= 9; Zt += "S") Y(Zt, Ae);
        var en = n("Milliseconds", !1);
        j("z", 0, 0, "zoneAbbr"), j("zz", 0, 0, "zoneName");
        var tn = m.prototype;
        tn.add = Yt, tn.calendar = function(e, t) {
            var n = e || ce(),
                i = me(n, this).startOf("day"),
                s = this.diff(i, "days", !0),
                r = s < -6 ? "sameElse" : s < -1 ? "lastWeek" : s < 0 ? "lastDay" : s < 1 ? "sameDay" : s < 2 ? "nextDay" : s < 7 ? "nextWeek" : "sameElse",
                o = t && (T(t[r]) ? t[r]() : t[r]);
            return this.format(o || this.localeData().calendar(r, this, ce(n)))
        }, tn.clone = function() {
            return new m(this)
        }, tn.diff = function(e, t, n) {
            var i, s, r, o;
            return this.isValid() ? (i = me(e, this)).isValid() ? (s = 6e4 * (i.utcOffset() - this.utcOffset()), "year" === (t = A(t)) || "month" === t || "quarter" === t ? (o = function(e, t) {
                var n, i, s = 12 * (t.year() - e.year()) + (t.month() - e.month()),
                    r = e.clone().add(s, "months");
                return i = t - r < 0 ? (n = e.clone().add(s - 1, "months"), (t - r) / (r - n)) : (n = e.clone().add(1 + s, "months"), (t - r) / (n - r)), -(s + i)
            }(this, i), "quarter" === t ? o /= 3 : "year" === t && (o /= 12)) : (r = this - i, o = "second" === t ? r / 1e3 : "minute" === t ? r / 6e4 : "hour" === t ? r / 36e5 : "day" === t ? (r - s) / 864e5 : "week" === t ? (r - s) / 6048e5 : r), n ? o : v(o)) : NaN : NaN
        }, tn.endOf = function(e) {
            return void 0 === (e = A(e)) || "millisecond" === e ? this : this.startOf(e).add(1, "isoWeek" === e ? "week" : e).subtract(1, "ms")
        }, tn.format = function(e) {
            var t = H(this, e || h.defaultFormat);
            return this.localeData().postformat(t)
        }, tn.from = function(e, t) {
            return this.isValid() && (g(e) && e.isValid() || ce(e).isValid()) ? ye({
                to: this,
                from: e
            }).locale(this.locale()).humanize(!t) : this.localeData().invalidDate()
        }, tn.fromNow = function(e) {
            return this.from(ce(), e)
        }, tn.to = function(e, t) {
            return this.isValid() && (g(e) && e.isValid() || ce(e).isValid()) ? ye({
                from: this,
                to: e
            }).locale(this.locale()).humanize(!t) : this.localeData().invalidDate()
        }, tn.toNow = function(e) {
            return this.to(ce(), e)
        }, tn.get = L, tn.invalidAt = function() {
            return f(this).overflow
        }, tn.isAfter = function(e, t) {
            var n = g(e) ? e : ce(e);
            return !(!this.isValid() || !n.isValid()) && ("millisecond" === (t = A(d(t) ? "millisecond" : t)) ? +n < +this : +n < +this.clone().startOf(t))
        }, tn.isBefore = function(e, t) {
            var n = g(e) ? e : ce(e);
            return !(!this.isValid() || !n.isValid()) && ("millisecond" === (t = A(d(t) ? "millisecond" : t)) ? +this < +n : +this.clone().endOf(t) < +n)
        }, tn.isBetween = function(e, t, n) {
            return this.isAfter(e, n) && this.isBefore(t, n)
        }, tn.isSame = function(e, t) {
            var n, i = g(e) ? e : ce(e);
            return !(!this.isValid() || !i.isValid()) && ("millisecond" === (t = A(t || "millisecond")) ? +this == +i : (n = +i, +this.clone().startOf(t) <= n && n <= +this.clone().endOf(t)))
        }, tn.isSameOrAfter = function(e, t) {
            return this.isSame(e, t) || this.isAfter(e, t)
        }, tn.isSameOrBefore = function(e, t) {
            return this.isSame(e, t) || this.isBefore(e, t)
        }, tn.isValid = function() {
            return l(this)
        }, tn.lang = Bt, tn.locale = Se, tn.localeData = Ee, tn.max = zt, tn.min = Ht, tn.parsingFlags = function() {
            return o({}, f(this))
        }, tn.set = L, tn.startOf = function(e) {
            switch (e = A(e)) {
                case "year":
                    this.month(0);
                case "quarter":
                case "month":
                    this.date(1);
                case "week":
                case "isoWeek":
                case "day":
                    this.hours(0);
                case "hour":
                    this.minutes(0);
                case "minute":
                    this.seconds(0);
                case "second":
                    this.milliseconds(0)
            }
            return "week" === e && this.weekday(0), "isoWeek" === e && this.isoWeekday(1), "quarter" === e && this.month(3 * Math.floor(this.month() / 3)), this
        }, tn.subtract = Wt, tn.toArray = function() {
            return [this.year(), this.month(), this.date(), this.hour(), this.minute(), this.second(), this.millisecond()]
        }, tn.toObject = function() {
            return {
                years: this.year(),
                months: this.month(),
                date: this.date(),
                hours: this.hours(),
                minutes: this.minutes(),
                seconds: this.seconds(),
                milliseconds: this.milliseconds()
            }
        }, tn.toDate = function() {
            return this._offset ? new Date(+this) : this._d
        }, tn.toISOString = function() {
            var e = this.clone().utc();
            return 0 < e.year() && e.year() <= 9999 ? T(Date.prototype.toISOString) ? this.toDate().toISOString() : H(e, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : H(e, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
        }, tn.toJSON = function() {
            return this.isValid() ? this.toISOString() : null
        }, tn.toString = function() {
            return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
        }, tn.unix = function() {
            return Math.floor(+this / 1e3)
        }, tn.valueOf = function() {
            return +this._d - 6e4 * (this._offset || 0)
        }, tn.creationData = function() {
            return {
                input: this._i,
                format: this._f,
                locale: this._locale,
                isUTC: this._isUTC,
                strict: this._strict
            }
        }, tn.year = jt, tn.isLeapYear = function() {
            return J(this.year())
        }, tn.weekYear = function(e) {
            return ke.call(this, e, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy)
        }, tn.isoWeekYear = function(e) {
            return ke.call(this, e, this.isoWeek(), this.isoWeekday(), 1, 4)
        }, tn.quarter = tn.quarters = function(e) {
            return null == e ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (e - 1) + this.month() % 3)
        }, tn.month = V, tn.daysInMonth = function() {
            return B(this.year(), this.month())
        }, tn.week = tn.weeks = function(e) {
            var t = this.localeData().week(this);
            return null == e ? t : this.add(7 * (e - t), "d")
        }, tn.isoWeek = tn.isoWeeks = function(e) {
            var t = ne(this, 1, 4).week;
            return null == e ? t : this.add(7 * (e - t), "d")
        }, tn.weeksInYear = function() {
            var e = this.localeData()._week;
            return ie(this.year(), e.dow, e.doy)
        }, tn.isoWeeksInYear = function() {
            return ie(this.year(), 1, 4)
        }, tn.date = Ut, tn.day = tn.days = function(e) {
            if (!this.isValid()) return null != e ? this : NaN;
            var t = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
            return null != e ? (e = function(e, t) {
                return "string" != typeof e ? e : isNaN(e) ? "number" == typeof(e = t.weekdaysParse(e)) ? e : null : parseInt(e, 10)
            }(e, this.localeData()), this.add(e - t, "d")) : t
        }, tn.weekday = function(e) {
            if (!this.isValid()) return null != e ? this : NaN;
            var t = (this.day() + 7 - this.localeData()._week.dow) % 7;
            return null == e ? t : this.add(e - t, "d")
        }, tn.isoWeekday = function(e) {
            return this.isValid() ? null == e ? this.day() || 7 : this.day(this.day() % 7 ? e : e - 7) : null != e ? this : NaN
        }, tn.dayOfYear = function(e) {
            var t = Math.round((this.clone().startOf("day") - this.clone().startOf("year")) / 864e5) + 1;
            return null == e ? t : this.add(e - t, "d")
        }, tn.hour = tn.hours = Qt, tn.minute = tn.minutes = Kt, tn.second = tn.seconds = Jt, tn.millisecond = tn.milliseconds = en, tn.utcOffset = function(e, t) {
            var n, i = this._offset || 0;
            return this.isValid() ? null != e ? ("string" == typeof e ? e = fe(pt, e) : Math.abs(e) < 16 && (e *= 60), !this._isUTC && t && (n = ge(this)), this._offset = e, this._isUTC = !0, null != n && this.add(n, "m"), i !== e && (!t || this._changeInProgress ? Ce(this, ye(e - i, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, h.updateOffset(this, !0), this._changeInProgress = null)), this) : this._isUTC ? i : ge(this) : null != e ? this : NaN
        }, tn.utc = function(e) {
            return this.utcOffset(0, e)
        }, tn.local = function(e) {
            return this._isUTC && (this.utcOffset(0, e), this._isUTC = !1, e && this.subtract(ge(this), "m")), this
        }, tn.parseZone = function() {
            return this._tzm ? this.utcOffset(this._tzm) : "string" == typeof this._i && this.utcOffset(fe(ht, this._i)), this
        }, tn.hasAlignedHourOffset = function(e) {
            return !!this.isValid() && (e = e ? ce(e).utcOffset() : 0, (this.utcOffset() - e) % 60 == 0)
        }, tn.isDST = function() {
            return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset()
        }, tn.isDSTShifted = function() {
            if (!d(this._isDSTShifted)) return this._isDSTShifted;
            var e = {};
            if (u(e, this), (e = ae(e))._a) {
                var t = e._isUTC ? a(e._a) : ce(e._a);
                this._isDSTShifted = this.isValid() && 0 < b(e._a, t.toArray())
            } else this._isDSTShifted = !1;
            return this._isDSTShifted
        }, tn.isLocal = function() {
            return !!this.isValid() && !this._isUTC
        }, tn.isUtcOffset = function() {
            return !!this.isValid() && this._isUTC
        }, tn.isUtc = ve, tn.isUTC = ve, tn.zoneAbbr = function() {
            return this._isUTC ? "UTC" : ""
        }, tn.zoneName = function() {
            return this._isUTC ? "Coordinated Universal Time" : ""
        }, tn.dates = e("dates accessor is deprecated. Use date instead.", Ut), tn.months = e("months accessor is deprecated. Use month instead", V), tn.years = e("years accessor is deprecated. Use year instead", jt), tn.zone = e("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779", function(e, t) {
            return null != e ? ("string" != typeof e && (e = -e), this.utcOffset(e, t), this) : -this.utcOffset()
        });
        var nn = tn,
            sn = E.prototype;
        sn._calendar = {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        }, sn.calendar = function(e, t, n) {
            var i = this._calendar[e];
            return T(i) ? i.call(t, n) : i
        }, sn._longDateFormat = {
            LTS: "h:mm:ss A",
            LT: "h:mm A",
            L: "MM/DD/YYYY",
            LL: "MMMM D, YYYY",
            LLL: "MMMM D, YYYY h:mm A",
            LLLL: "dddd, MMMM D, YYYY h:mm A"
        }, sn.longDateFormat = function(e) {
            var t = this._longDateFormat[e],
                n = this._longDateFormat[e.toUpperCase()];
            return t || !n ? t : (this._longDateFormat[e] = n.replace(/MMMM|MM|DD|dddd/g, function(e) {
                return e.slice(1)
            }), this._longDateFormat[e])
        }, sn._invalidDate = "Invalid date", sn.invalidDate = function() {
            return this._invalidDate
        }, sn._ordinal = "%d", sn.ordinal = function(e) {
            return this._ordinal.replace("%d", e)
        }, sn._ordinalParse = /\d{1,2}/, sn.preparse = Oe, sn.postformat = Oe, sn._relativeTime = {
            future: "in %s",
            past: "%s ago",
            s: "a few seconds",
            m: "a minute",
            mm: "%d minutes",
            h: "an hour",
            hh: "%d hours",
            d: "a day",
            dd: "%d days",
            M: "a month",
            MM: "%d months",
            y: "a year",
            yy: "%d years"
        }, sn.relativeTime = function(e, t, n, i) {
            var s = this._relativeTime[n];
            return T(s) ? s(e, t, n, i) : s.replace(/%d/i, e)
        }, sn.pastFuture = function(e, t) {
            var n = this._relativeTime[0 < e ? "future" : "past"];
            return T(n) ? n(t) : n.replace(/%s/i, t)
        }, sn.set = function(e) {
            var t, n;
            for (n in e) T(t = e[n]) ? this[n] = t : this["_" + n] = t;
            this._config = e, this._ordinalParseLenient = new RegExp(this._ordinalParse.source + "|" + /\d{1,2}/.source)
        }, sn.months = function(e, t) {
            return s(this._months) ? this._months[e.month()] : this._months[_t.test(t) ? "format" : "standalone"][e.month()]
        }, sn._months = kt, sn.monthsShort = function(e, t) {
            return s(this._monthsShort) ? this._monthsShort[e.month()] : this._monthsShort[_t.test(t) ? "format" : "standalone"][e.month()]
        }, sn._monthsShort = Dt, sn.monthsParse = function(e, t, n) {
            var i, s, r;
            for (this._monthsParse || (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = []), i = 0; i < 12; i++) {
                if (s = a([2e3, i]), n && !this._longMonthsParse[i] && (this._longMonthsParse[i] = new RegExp("^" + this.months(s, "").replace(".", "") + "$", "i"), this._shortMonthsParse[i] = new RegExp("^" + this.monthsShort(s, "").replace(".", "") + "$", "i")), n || this._monthsParse[i] || (r = "^" + this.months(s, "") + "|^" + this.monthsShort(s, ""), this._monthsParse[i] = new RegExp(r.replace(".", ""), "i")), n && "MMMM" === t && this._longMonthsParse[i].test(e)) return i;
                if (n && "MMM" === t && this._shortMonthsParse[i].test(e)) return i;
                if (!n && this._monthsParse[i].test(e)) return i
            }
        }, sn._monthsRegex = Mt, sn.monthsRegex = function(e) {
            return this._monthsParseExact ? (p(this, "_monthsRegex") || G.call(this), e ? this._monthsStrictRegex : this._monthsRegex) : this._monthsStrictRegex && e ? this._monthsStrictRegex : this._monthsRegex
        }, sn._monthsShortRegex = $t, sn.monthsShortRegex = function(e) {
            return this._monthsParseExact ? (p(this, "_monthsRegex") || G.call(this), e ? this._monthsShortStrictRegex : this._monthsShortRegex) : this._monthsShortStrictRegex && e ? this._monthsShortStrictRegex : this._monthsShortRegex
        }, sn.week = function(e) {
            return ne(e, this._week.dow, this._week.doy).week
        }, sn._week = {
            dow: 0,
            doy: 6
        }, sn.firstDayOfYear = function() {
            return this._week.doy
        }, sn.firstDayOfWeek = function() {
            return this._week.dow
        }, sn.weekdays = function(e, t) {
            return s(this._weekdays) ? this._weekdays[e.day()] : this._weekdays[this._weekdays.isFormat.test(t) ? "format" : "standalone"][e.day()]
        }, sn._weekdays = Vt, sn.weekdaysMin = function(e) {
            return this._weekdaysMin[e.day()]
        }, sn._weekdaysMin = Xt, sn.weekdaysShort = function(e) {
            return this._weekdaysShort[e.day()]
        }, sn._weekdaysShort = Gt, sn.weekdaysParse = function(e, t, n) {
            var i, s, r;
            for (this._weekdaysParse || (this._weekdaysParse = [], this._minWeekdaysParse = [], this._shortWeekdaysParse = [], this._fullWeekdaysParse = []), i = 0; i < 7; i++) {
                if (s = ce([2e3, 1]).day(i), n && !this._fullWeekdaysParse[i] && (this._fullWeekdaysParse[i] = new RegExp("^" + this.weekdays(s, "").replace(".", ".?") + "$", "i"), this._shortWeekdaysParse[i] = new RegExp("^" + this.weekdaysShort(s, "").replace(".", ".?") + "$", "i"), this._minWeekdaysParse[i] = new RegExp("^" + this.weekdaysMin(s, "").replace(".", ".?") + "$", "i")), this._weekdaysParse[i] || (r = "^" + this.weekdays(s, "") + "|^" + this.weekdaysShort(s, "") + "|^" + this.weekdaysMin(s, ""), this._weekdaysParse[i] = new RegExp(r.replace(".", ""), "i")), n && "dddd" === t && this._fullWeekdaysParse[i].test(e)) return i;
                if (n && "ddd" === t && this._shortWeekdaysParse[i].test(e)) return i;
                if (n && "dd" === t && this._minWeekdaysParse[i].test(e)) return i;
                if (!n && this._weekdaysParse[i].test(e)) return i
            }
        }, sn.isPM = function(e) {
            return "p" === (e + "").toLowerCase().charAt(0)
        }, sn._meridiemParse = /[ap]\.?m?\.?/i, sn.meridiem = function(e, t, n) {
            return 11 < e ? n ? "pm" : "PM" : n ? "am" : "AM"
        }, D("en", {
            ordinalParse: /\d{1,2}(th|st|nd|rd)/,
            ordinal: function(e) {
                var t = e % 10;
                return e + (1 === y(e % 100 / 10) ? "th" : 1 == t ? "st" : 2 == t ? "nd" : 3 == t ? "rd" : "th")
            }
        }), h.lang = e("moment.lang is deprecated. Use moment.locale instead.", D), h.langData = e("moment.langData is deprecated. Use moment.localeData instead.", M);
        var rn = Math.abs,
            on = ze("ms"),
            an = ze("s"),
            ln = ze("m"),
            cn = ze("h"),
            dn = ze("d"),
            un = ze("w"),
            hn = ze("M"),
            pn = ze("y"),
            fn = qe("milliseconds"),
            mn = qe("seconds"),
            gn = qe("minutes"),
            vn = qe("hours"),
            yn = qe("days"),
            bn = qe("months"),
            wn = qe("years"),
            xn = Math.round,
            Tn = {
                s: 45,
                m: 45,
                h: 22,
                d: 26,
                M: 11
            },
            Cn = Math.abs,
            Sn = ue.prototype;
        return Sn.abs = function() {
            var e = this._data;
            return this._milliseconds = rn(this._milliseconds), this._days = rn(this._days), this._months = rn(this._months), e.milliseconds = rn(e.milliseconds), e.seconds = rn(e.seconds), e.minutes = rn(e.minutes), e.hours = rn(e.hours), e.months = rn(e.months), e.years = rn(e.years), this
        }, Sn.add = function(e, t) {
            return Le(this, e, t, 1)
        }, Sn.subtract = function(e, t) {
            return Le(this, e, t, -1)
        }, Sn.as = function(e) {
            var t, n, i = this._milliseconds;
            if ("month" === (e = A(e)) || "year" === e) return t = this._days + i / 864e5, n = this._months + je(t), "month" === e ? n : n / 12;
            switch (t = this._days + Math.round(He(this._months)), e) {
                case "week":
                    return t / 7 + i / 6048e5;
                case "day":
                    return t + i / 864e5;
                case "hour":
                    return 24 * t + i / 36e5;
                case "minute":
                    return 1440 * t + i / 6e4;
                case "second":
                    return 86400 * t + i / 1e3;
                case "millisecond":
                    return Math.floor(864e5 * t) + i;
                default:
                    throw new Error("Unknown unit " + e)
            }
        }, Sn.asMilliseconds = on, Sn.asSeconds = an, Sn.asMinutes = ln, Sn.asHours = cn, Sn.asDays = dn, Sn.asWeeks = un, Sn.asMonths = hn, Sn.asYears = pn, Sn.valueOf = function() {
            return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * y(this._months / 12)
        }, Sn._bubble = function() {
            var e, t, n, i, s, r = this._milliseconds,
                o = this._days,
                a = this._months,
                l = this._data;
            return 0 <= r && 0 <= o && 0 <= a || r <= 0 && o <= 0 && a <= 0 || (r += 864e5 * Ne(He(a) + o), a = o = 0), l.milliseconds = r % 1e3, e = v(r / 1e3), l.seconds = e % 60, t = v(e / 60), l.minutes = t % 60, n = v(t / 60), l.hours = n % 24, a += s = v(je(o += v(n / 24))), o -= Ne(He(s)), i = v(a / 12), a %= 12, l.days = o, l.months = a, l.years = i, this
        }, Sn.get = function(e) {
            return this[(e = A(e)) + "s"]()
        }, Sn.milliseconds = fn, Sn.seconds = mn, Sn.minutes = gn, Sn.hours = vn, Sn.days = yn, Sn.weeks = function() {
            return v(this.days() / 7)
        }, Sn.months = bn, Sn.years = wn, Sn.humanize = function(e) {
            var t = this.localeData(),
                n = function(e, t, n) {
                    var i = ye(e).abs(),
                        s = xn(i.as("s")),
                        r = xn(i.as("m")),
                        o = xn(i.as("h")),
                        a = xn(i.as("d")),
                        l = xn(i.as("M")),
                        c = xn(i.as("y")),
                        d = s < Tn.s && ["s", s] || r <= 1 && ["m"] || r < Tn.m && ["mm", r] || o <= 1 && ["h"] || o < Tn.h && ["hh", o] || a <= 1 && ["d"] || a < Tn.d && ["dd", a] || l <= 1 && ["M"] || l < Tn.M && ["MM", l] || c <= 1 && ["y"] || ["yy", c];
                    return d[2] = t, d[3] = 0 < +e, d[4] = n,
                        function(e, t, n, i, s) {
                            return s.relativeTime(t || 1, !!n, e, i)
                        }.apply(null, d)
                }(this, !e, t);
            return e && (n = t.pastFuture(+this, n)), t.postformat(n)
        }, Sn.toISOString = Fe, Sn.toString = Fe, Sn.toJSON = Fe, Sn.locale = Se, Sn.localeData = Ee, Sn.toIsoString = e("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", Fe), Sn.lang = Bt, j("X", 0, 0, "unix"), j("x", 0, 0, "valueOf"), q("x", ut), q("X", /[+-]?\d+(\.\d{1,3})?/), Y("X", function(e, t, n) {
            n._d = new Date(1e3 * parseFloat(e, 10))
        }), Y("x", function(e, t, n) {
            n._d = new Date(y(e))
        }), h.version = "2.12.0", Re = ce, h.fn = nn, h.min = function() {
            return de("isBefore", [].slice.call(arguments, 0))
        }, h.max = function() {
            return de("isAfter", [].slice.call(arguments, 0))
        }, h.now = function() {
            return Date.now ? Date.now() : +new Date
        }, h.utc = a, h.unix = function(e) {
            return ce(1e3 * e)
        }, h.months = function(e, t) {
            return Pe(e, t, "months", 12, "month")
        }, h.isDate = r, h.locale = D, h.invalid = c, h.duration = ye, h.isMoment = g, h.weekdays = function(e, t) {
            return Pe(e, t, "weekdays", 7, "day")
        }, h.parseZone = function() {
            return ce.apply(null, arguments).parseZone()
        }, h.localeData = M, h.isDuration = he, h.monthsShort = function(e, t) {
            return Pe(e, t, "monthsShort", 12, "month")
        }, h.weekdaysMin = function(e, t) {
            return Pe(e, t, "weekdaysMin", 7, "day")
        }, h.defineLocale = $, h.updateLocale = function(e, t) {
            var n;
            return null != t ? (null != Ve[e] && (t = S(Ve[e]._config, t)), (n = new E(t)).parentLocale = Ve[e], Ve[e] = n, D(e)) : null != Ve[e] && (null != Ve[e].parentLocale ? Ve[e] = Ve[e].parentLocale : null != Ve[e] && delete Ve[e]), Ve[e]
        }, h.locales = function() {
            return Object.keys(Ve)
        }, h.weekdaysShort = function(e, t) {
            return Pe(e, t, "weekdaysShort", 7, "day")
        }, h.normalizeUnits = A, h.relativeTimeThreshold = function(e, t) {
            return void 0 !== Tn[e] && (void 0 === t ? Tn[e] : (Tn[e] = t, !0))
        }, h.prototype = nn, h
    }), function(b, l) {
        function n(e, t) {
            this.currentView = 0, this.minDate, this.maxDate, this._attachedEvents = [], this.element = e, this.$element = b(e), this.params = {
                date: !0,
                time: !0,
                format: "YYYY-MM-DD",
                minDate: null,
                maxDate: null,
                currentDate: null,
                lang: "en",
                weekStart: 0,
                shortTime: !1,
                cancelText: "Cancel",
                okText: "OK"
            }, this.params = b.fn.extend(this.params, t), this.name = "dtp_" + this.setName(), this.$element.attr("data-dtp", this.name), this.init()
        }
        var e = "bootstrapMaterialDatePicker",
            i = "plugin_" + e;
        l.locale("en"), b.fn[e] = function(e, t) {
            return this.each(function() {
                b.data(this, i) ? ("function" == typeof b.data(this, i)[e] && b.data(this, i)[e](t), "destroy" === e && b.data(this, i)) : b.data(this, i, new n(this, e))
            }), this
        }, n.prototype = {
            init: function() {
                this.initDays(), this.initDates(), this.initTemplate(), this.initButtons(), this._attachEvent(b(window), "resize", this._centerBox(this)), this._attachEvent(this.$dtpElement.find(".dtp-content"), "click", this._onElementClick.bind(this)), this._attachEvent(this.$dtpElement, "click", this._onBackgroundClick.bind(this)), this._attachEvent(this.$dtpElement.find(".dtp-close > a"), "click", this._onCloseClick.bind(this)), this._attachEvent(this.$element, "click", this._onClick.bind(this))
            },
            initDays: function() {
                this.days = [];
                for (var e = this.params.weekStart; this.days.length < 7; e++) 6 < e && (e = 0), this.days.push(e.toString())
            },
            initDates: function() {
                if (0 < this.$element.val().length) void 0 !== this.params.format && null !== this.params.format ? this.currentDate = l(this.$element.val(), this.params.format).locale(this.params.lang) : this.currentDate = l(this.$element.val()).locale(this.params.lang);
                else if (void 0 !== this.$element.attr("value") && null !== this.$element.attr("value") && "" !== this.$element.attr("value")) "string" == typeof this.$element.attr("value") && (void 0 !== this.params.format && null !== this.params.format ? this.currentDate = l(this.$element.attr("value"), this.params.format).locale(this.params.lang) : this.currentDate = l(this.$element.attr("value")).locale(this.params.lang));
                else if (void 0 !== this.params.currentDate && null !== this.params.currentDate) {
                    if ("string" == typeof this.params.currentDate) void 0 !== this.params.format && null !== this.params.format ? this.currentDate = l(this.params.currentDate, this.params.format).locale(this.params.lang) : this.currentDate = l(this.params.currentDate).locale(this.params.lang);
                    else if (void 0 === this.params.currentDate.isValid || "function" != typeof this.params.currentDate.isValid) {
                        var e = this.params.currentDate.getTime();
                        this.currentDate = l(e, "x").locale(this.params.lang)
                    } else this.currentDate = this.params.currentDate;
                    this.$element.val(this.currentDate.format(this.params.format))
                } else this.currentDate = l();
                if (void 0 !== this.params.minDate && null !== this.params.minDate)
                    if ("string" == typeof this.params.minDate) void 0 !== this.params.format && null !== this.params.format ? this.minDate = l(this.params.minDate, this.params.format).locale(this.params.lang) : this.minDate = l(this.params.minDate).locale(this.params.lang);
                    else if (void 0 === this.params.minDate.isValid || "function" != typeof this.params.minDate.isValid) {
                    e = this.params.minDate.getTime();
                    this.minDate = l(e, "x").locale(this.params.lang)
                } else this.minDate = this.params.minDate;
                if (void 0 !== this.params.maxDate && null !== this.params.maxDate)
                    if ("string" == typeof this.params.maxDate) void 0 !== this.params.format && null !== this.params.format ? this.maxDate = l(this.params.maxDate, this.params.format).locale(this.params.lang) : this.maxDate = l(this.params.maxDate).locale(this.params.lang);
                    else if (void 0 === this.params.maxDate.isValid || "function" != typeof this.params.maxDate.isValid) {
                    e = this.params.maxDate.getTime();
                    this.maxDate = l(e, "x").locale(this.params.lang)
                } else this.maxDate = this.params.maxDate;
                this.isAfterMinDate(this.currentDate) || (this.currentDate = l(this.minDate)), this.isBeforeMaxDate(this.currentDate) || (this.currentDate = l(this.maxDate))
            },
            initTemplate: function() {
                this.template = '<div class="dtp hidden" id="' + this.name + '"><div class="dtp-content"><div class="dtp-date-view"><header class="dtp-header"><div class="dtp-actual-day">Lundi</div><div class="dtp-close"><a href="javascript:void(0);"><span class="mdi mdi-close"></span></</div></header><div class="dtp-date hidden"><div><div class="left center p10"><a href="javascript:void(0);" class="dtp-select-month-before"><span class="mdi mdi-chevron-left"></span></a></div><div class="dtp-actual-month p80">MAR</div><div class="right center p10"><a href="javascript:void(0);" class="dtp-select-month-after"><span class="mdi mdi-chevron-right"></span></a></div><div class="clearfix"></div></div><div class="dtp-actual-num">13</div><div><div class="left center p10"><a href="javascript:void(0);" class="dtp-select-year-before"><span class="mdi mdi-chevron-left"></span></a></div><div class="dtp-actual-year p80">2014</div><div class="right center p10"><a href="javascript:void(0);" class="dtp-select-year-after"><span class="mdi mdi-chevron-right"></span></a></div><div class="clearfix"></div></div></div><div class="dtp-time hidden"><div class="dtp-actual-maxtime">23:55</div></div><div class="dtp-picker"><div class="dtp-picker-calendar"></div><div class="dtp-picker-datetime hidden"><div class="dtp-actual-meridien"><div class="left p20"><a class="dtp-meridien-am" href="javascript:void(0);">AM</a></div><div class="dtp-actual-time p60"></div><div class="right p20"><a class="dtp-meridien-pm" href="javascript:void(0);">PM</a></div><div class="clearfix"></div></div><div class="dtp-picker-clock"></div></div></div></div><div class="dtp-buttons group"><button class="dtp-btn-cancel btn btn-sm btn-primary">' + this.params.cancelText + '</button><button class="dtp-btn-ok btn btn-sm btn-primary">' + this.params.okText + '</button><div class="clearfix"></div></div></div></div>', b("body").find("#" + this.name).length <= 0 && (b("body").append(this.template), this.dtpElement = b("body").find("#" + this.name), this.$dtpElement = b(this.dtpElement))
            },
            initButtons: function() {
                this._attachEvent(this.$dtpElement.find(".dtp-btn-cancel"), "click", this._onCancelClick.bind(this)), this._attachEvent(this.$dtpElement.find(".dtp-btn-ok"), "click", this._onOKClick.bind(this)), this._attachEvent(this.$dtpElement.find("a.dtp-select-month-before"), "click", this._onMonthBeforeClick.bind(this)), this._attachEvent(this.$dtpElement.find("a.dtp-select-month-after"), "click", this._onMonthAfterClick.bind(this)), this._attachEvent(this.$dtpElement.find("a.dtp-select-year-before"), "click", this._onYearBeforeClick.bind(this)), this._attachEvent(this.$dtpElement.find("a.dtp-select-year-after"), "click", this._onYearAfterClick.bind(this))
            },
            initMeridienButtons: function() {
                this.$dtpElement.find("a.dtp-meridien-am").off("click").on("click", this._onSelectAM.bind(this)), this.$dtpElement.find("a.dtp-meridien-pm").off("click").on("click", this._onSelectPM.bind(this))
            },
            initDate: function(e) {
                this.currentView = 0, this.$dtpElement.find(".dtp-picker-calendar").removeClass("hidden"), this.$dtpElement.find(".dtp-picker-datetime").addClass("hidden");
                var t = void 0 !== this.currentDate && null !== this.currentDate ? this.currentDate : null,
                    n = this.generateCalendar(this.currentDate);
                if (void 0 !== n.week && void 0 !== n.days) {
                    var i = this.constructHTMLCalendar(t, n);
                    this.$dtpElement.find("a.dtp-select-day").off("click"), this.$dtpElement.find(".dtp-picker-calendar").html(i), this.$dtpElement.find("a.dtp-select-day").on("click", this._onSelectDate.bind(this)), this.toggleButtons(t)
                }
                this._centerBox(), this.showDate(t)
            },
            initHours: function() {
                var y = this;
                setTimeout(function() {
                    if (y.currentView = 1, !y.params.date) {
                        var e = y.$dtpElement.find(".dtp-content").width(),
                            t = y.$dtpElement.find(".dtp-picker-clock").css("marginLeft").replace("px", ""),
                            n = y.$dtpElement.find(".dtp-picker-clock").css("marginRight").replace("px", ""),
                            i = y.$dtpElement.find(".dtp-picker").css("paddingLeft").replace("px", ""),
                            s = y.$dtpElement.find(".dtp-picker").css("paddingRight").replace("px", "");
                        y.$dtpElement.find(".dtp-picker-clock").innerWidth(e - (parseInt(t) + parseInt(n) + parseInt(i) + parseInt(s)))
                    }
                    y.showTime(y.currentDate), y.initMeridienButtons(), y.$dtpElement.find(".dtp-picker-datetime").removeClass("hidden"), y.$dtpElement.find(".dtp-picker-calendar").addClass("hidden"), y.currentDate.hour() < 12 ? y.$dtpElement.find("a.dtp-meridien-am").click() : y.$dtpElement.find("a.dtp-meridien-pm").click();
                    for (var r = y.$dtpElement.find(".dtp-picker-clock").parent().parent().css("paddingLeft").replace("px", ""), o = y.$dtpElement.find(".dtp-picker-clock").parent().parent().css("paddingTop").replace("px", ""), a = y.$dtpElement.find(".dtp-picker-clock").css("marginLeft").replace("px", ""), l = y.$dtpElement.find(".dtp-picker-clock").css("marginTop").replace("px", ""), c = y.$dtpElement.find(".dtp-picker-clock").innerWidth() / 2, d = c / 1.2, u = [], h = 0; h < 12; ++h) {
                        var p = d * Math.sin(2 * Math.PI * (h / 12)),
                            f = d * Math.cos(2 * Math.PI * (h / 12)),
                            m = b("<div>", {
                                class: "dtp-picker-time"
                            }).css({
                                marginLeft: c + p + parseInt(r) / 2 - (parseInt(r) + parseInt(a)) + "px",
                                marginTop: c - f - parseInt(l) / 2 - (parseInt(o) + parseInt(l)) + "px"
                            }),
                            g = 12 == y.currentDate.format("h") ? 0 : y.currentDate.format("h"),
                            v = b("<a>", {
                                href: "javascript:void(0);",
                                class: "dtp-select-hour"
                            }).data("hour", h).text(0 == h ? 12 : h);
                        h == parseInt(g) && v.addClass("selected"), m.append(v), u.push(m)
                    }
                    y.$dtpElement.find("a.dtp-select-hour").off("click"), y.$dtpElement.find(".dtp-picker-clock").html(u), y.toggleTime(!0), y.$dtpElement.find(".dtp-picker-clock").css("height", y.$dtpElement.find(".dtp-picker-clock").width() + (parseInt(o) + parseInt(l)) + "px"), y.initHands(!0)
                }, 300)
            },
            initMinutes: function() {
                this.currentView = 2, this.showTime(this.currentDate), this.initMeridienButtons(), this.currentDate.hour() < 12 ? this.$dtpElement.find("a.dtp-meridien-am").click() : this.$dtpElement.find("a.dtp-meridien-pm").click(), this.$dtpElement.find(".dtp-picker-calendar").addClass("hidden"), this.$dtpElement.find(".dtp-picker-datetime").removeClass("hidden");
                for (var e = this.$dtpElement.find(".dtp-picker-clock").parent().parent().css("paddingLeft").replace("px", ""), t = this.$dtpElement.find(".dtp-picker-clock").parent().parent().css("paddingTop").replace("px", ""), n = this.$dtpElement.find(".dtp-picker-clock").css("marginLeft").replace("px", ""), i = this.$dtpElement.find(".dtp-picker-clock").css("marginTop").replace("px", ""), s = this.$dtpElement.find(".dtp-picker-clock").innerWidth() / 2, r = s / 1.2, o = [], a = 0; a < 60; a += 5) {
                    var l = r * Math.sin(2 * Math.PI * (a / 60)),
                        c = r * Math.cos(2 * Math.PI * (a / 60)),
                        d = b("<div>", {
                            class: "dtp-picker-time"
                        }).css({
                            marginLeft: s + l + parseInt(e) / 2 - (parseInt(e) + parseInt(n)) + "px",
                            marginTop: s - c - parseInt(i) / 2 - (parseInt(t) + parseInt(i)) + "px"
                        }),
                        u = b("<a>", {
                            href: "javascript:void(0);",
                            class: "dtp-select-minute"
                        }).data("minute", a).text(2 == a.toString().length ? a : "0" + a);
                    a == 5 * Math.round(this.currentDate.minute() / 5) && (u.addClass("selected"), this.currentDate.minute(a)), d.append(u), o.push(d)
                }
                this.$dtpElement.find("a.dtp-select-minute").off("click"), this.$dtpElement.find(".dtp-picker-clock").html(o), this.toggleTime(!1), this.$dtpElement.find(".dtp-picker-clock").css("height", this.$dtpElement.find(".dtp-picker-clock").width() + (parseInt(t) + parseInt(i)) + "px"), this.initHands(!1), this._centerBox()
            },
            initHands: function(e) {
                this.$dtpElement.find(".dtp-picker-clock").append('<div class="dtp-hand dtp-hour-hand"></div><div class="dtp-hand dtp-minute-hand"></div><div class="dtp-clock-center"></div>');
                var t = this.$dtpElement.find(".dtp-picker-clock").parent().parent().css("paddingLeft").replace("px", ""),
                    n = (this.$dtpElement.find(".dtp-picker-clock").parent().parent().css("paddingTop").replace("px", ""), this.$dtpElement.find(".dtp-picker-clock").css("marginLeft").replace("px", "")),
                    i = (this.$dtpElement.find(".dtp-picker-clock").css("marginTop").replace("px", ""), this.$dtpElement.find(".dtp-clock-center").width() / 2),
                    s = this.$dtpElement.find(".dtp-clock-center").height() / 2,
                    r = this.$dtpElement.find(".dtp-picker-clock").innerWidth() / 2,
                    o = r / 1.7,
                    a = r / 1.5;
                this.$dtpElement.find(".dtp-hour-hand").css({
                    left: r + 1.5 * parseInt(n) + "px",
                    height: o + "px",
                    marginTop: r - o - parseInt(t) + "px"
                }).addClass(!0 === e ? "on" : ""), this.$dtpElement.find(".dtp-minute-hand").css({
                    left: r + 1.5 * parseInt(n) + "px",
                    height: a + "px",
                    marginTop: r - a - parseInt(t) + "px"
                }).addClass(!1 === e ? "on" : ""), this.$dtpElement.find(".dtp-clock-center").css({
                    left: r + parseInt(t) + parseInt(n) - i + "px",
                    marginTop: r - parseInt(n) / 2 - s + "px"
                }), this.animateHands(), this._centerBox()
            },
            animateHands: function() {
                var e = this.currentDate.hour();
                this.currentDate.minute(), this.rotateElement(this.$dtpElement.find(".dtp-hour-hand"), 30 * e), this.rotateElement(this.$dtpElement.find(".dtp-minute-hand"), 5 * Math.round(this.currentDate.minute() / 5) * 6)
            },
            isAfterMinDate: function(e, t, n) {
                var i = !0;
                if (void 0 !== this.minDate && null !== this.minDate) {
                    var s = l(this.minDate),
                        r = l(e);
                    t || n || (s.hour(0), s.minute(0), r.hour(0), r.minute(0)), s.second(0), r.second(0), s.millisecond(0), r.millisecond(0), i = (n || (r.minute(0), s.minute(0)), parseInt(r.format("X")) >= parseInt(s.format("X")))
                }
                return i
            },
            isBeforeMaxDate: function(e, t, n) {
                var i = !0;
                if (void 0 !== this.maxDate && null !== this.maxDate) {
                    var s = l(this.maxDate),
                        r = l(e);
                    t || n || (s.hour(0), s.minute(0), r.hour(0), r.minute(0)), s.second(0), r.second(0), s.millisecond(0), r.millisecond(0), i = (n || (r.minute(0), s.minute(0)), parseInt(r.format("X")) <= parseInt(s.format("X")))
                }
                return i
            },
            rotateElement: function(e, t) {
                b(e).css({
                    WebkitTransform: "rotate(" + t + "deg)",
                    "-moz-transform": "rotate(" + t + "deg)"
                })
            },
            showDate: function(e) {
                e && (this.$dtpElement.find(".dtp-actual-day").html(e.locale(this.params.lang).format("dddd")), this.$dtpElement.find(".dtp-actual-month").html(e.locale(this.params.lang).format("MMM").toUpperCase()), this.$dtpElement.find(".dtp-actual-num").html(e.locale(this.params.lang).format("DD")), this.$dtpElement.find(".dtp-actual-year").html(e.locale(this.params.lang).format("YYYY")))
            },
            showTime: function(e) {
                if (e) {
                    var t = 5 * Math.round(e.minute() / 5),
                        n = (this.params.shortTime ? e.format("hh") : e.format("HH")) + ":" + (2 == t.toString().length ? t : "0" + t);
                    this.params.date ? this.$dtpElement.find(".dtp-actual-time").html(n) : (this.params.shortTime ? this.$dtpElement.find(".dtp-actual-day").html(e.format("A")) : this.$dtpElement.find(".dtp-actual-day").html(" "), this.$dtpElement.find(".dtp-actual-maxtime").html(n))
                }
            },
            selectDate: function(e) {
                e && (this.currentDate.date(e), this.showDate(this.currentDate), this.$element.trigger("dateSelected", this.currentDate))
            },
            generateCalendar: function(e) {
                var t = {};
                if (null !== e) {
                    var n = l(e).locale(this.params.lang).startOf("month"),
                        i = l(e).locale(this.params.lang).endOf("month"),
                        s = n.format("d");
                    t.week = this.days, t.days = [];
                    for (var r = n.date(); r <= i.date(); r++) {
                        if (r === n.date()) {
                            var o = t.week.indexOf(s.toString());
                            if (0 < o)
                                for (var a = 0; a < o; a++) t.days.push(0)
                        }
                        t.days.push(l(n).locale(this.params.lang).date(r))
                    }
                }
                return t
            },
            constructHTMLCalendar: function(e, t) {
                var n = "";
                n += '<div class="dtp-picker-month">' + e.locale(this.params.lang).format("MMMM YYYY") + "</div>", n += '<table class="table dtp-picker-days"><thead>';
                for (var i = 0; i < t.week.length; i++) n += "<th>" + l(parseInt(t.week[i]), "d").locale(this.params.lang).format("dd").substring(0, 1) + "</th>";
                n += "</thead>", n += "<tbody><tr>";
                for (i = 0; i < t.days.length; i++) i % 7 == 0 && (n += "</tr><tr>"), n += '<td data-date="' + l(t.days[i]).locale(this.params.lang).format("D") + '">', 0 != t.days[i] && (n += !1 === this.isBeforeMaxDate(l(t.days[i]), !1, !1) || !1 === this.isAfterMinDate(l(t.days[i]), !1, !1) ? '<span class="dtp-select-day">' + l(t.days[i]).locale(this.params.lang).format("DD") + "</span>" : l(t.days[i]).locale(this.params.lang).format("DD") === l(this.currentDate).locale(this.params.lang).format("DD") ? '<a href="javascript:void(0);" class="dtp-select-day selected">' + l(t.days[i]).locale(this.params.lang).format("DD") + "</a>" : '<a href="javascript:void(0);" class="dtp-select-day">' + l(t.days[i]).locale(this.params.lang).format("DD") + "</a>", n += "</td>");
                return n + "</tr></tbody></table>"
            },
            setName: function() {
                for (var e = "", t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", n = 0; n < 5; n++) e += t.charAt(Math.floor(Math.random() * t.length));
                return e
            },
            isPM: function() {
                return this.$dtpElement.find("a.dtp-meridien-pm").hasClass("selected")
            },
            setElementValue: function() {
                this.$element.trigger("beforeChange", this.currentDate), void 0 !== b.material && this.$element.removeClass("empty"), this.$element.val(l(this.currentDate).locale(this.params.lang).format(this.params.format)), this.$element.parent().find(".rd-input-label").addClass("focus"), this.$element.trigger("change", this.currentDate)
            },
            toggleButtons: function(e) {
                if (e && e.isValid()) {
                    var t = l(e).locale(this.params.lang).startOf("month"),
                        n = l(e).locale(this.params.lang).endOf("month");
                    this.isAfterMinDate(t, !1, !1) ? this.$dtpElement.find("a.dtp-select-month-before").removeClass("invisible") : this.$dtpElement.find("a.dtp-select-month-before").addClass("invisible"), this.isBeforeMaxDate(n, !1, !1) ? this.$dtpElement.find("a.dtp-select-month-after").removeClass("invisible") : this.$dtpElement.find("a.dtp-select-month-after").addClass("invisible");
                    var i = l(e).locale(this.params.lang).startOf("year"),
                        s = l(e).locale(this.params.lang).endOf("year");
                    this.isAfterMinDate(i, !1, !1) ? this.$dtpElement.find("a.dtp-select-year-before").removeClass("invisible") : this.$dtpElement.find("a.dtp-select-year-before").addClass("invisible"), this.isBeforeMaxDate(s, !1, !1) ? this.$dtpElement.find("a.dtp-select-year-after").removeClass("invisible") : this.$dtpElement.find("a.dtp-select-year-after").addClass("invisible")
                }
            },
            toggleTime: function(e) {
                if (e) {
                    this.$dtpElement.find("a.dtp-select-hour").removeClass("disabled"), this.$dtpElement.find("a.dtp-select-hour").removeProp("disabled"), this.$dtpElement.find("a.dtp-select-hour").off("click");
                    var n = this;
                    this.$dtpElement.find("a.dtp-select-hour").each(function() {
                        var e = b(this).data("hour"),
                            t = l(n.currentDate);
                        t.hour(n.convertHours(e)).minute(0).second(0), !1 === n.isAfterMinDate(t, !0, !1) || !1 === n.isBeforeMaxDate(t, !0, !1) ? (b(this).prop("disabled"), b(this).addClass("disabled")) : b(this).on("click", n._onSelectHour.bind(n))
                    })
                } else {
                    this.$dtpElement.find("a.dtp-select-minute").removeClass("disabled"), this.$dtpElement.find("a.dtp-select-minute").removeProp("disabled"), this.$dtpElement.find("a.dtp-select-minute").off("click");
                    n = this;
                    this.$dtpElement.find("a.dtp-select-minute").each(function() {
                        var e = b(this).data("minute"),
                            t = l(n.currentDate);
                        t.minute(e).second(0), !1 === n.isAfterMinDate(t, !0, !0) || !1 === n.isBeforeMaxDate(t, !0, !0) ? (b(this).prop("disabled"), b(this).addClass("disabled")) : b(this).on("click", n._onSelectMinute.bind(n))
                    })
                }
            },
            _attachEvent: function(e, t, n) {
                e.on(t, n), this._attachedEvents.push([e, t, n])
            },
            _detachEvents: function() {
                for (var e = this._attachedEvents.length - 1; 0 <= e; e--) this._attachedEvents[e][0].off(this._attachedEvents[e][1], this._attachedEvents[e][2]), this._attachedEvents.splice(e, 1)
            },
            _onClick: function() {
                this.currentView = 0, this.$element.blur(), this.initDates(), this.show(), this.params.date ? (this.$dtpElement.find(".dtp-date").removeClass("hidden"), this.initDate()) : this.params.time && (this.$dtpElement.find(".dtp-time").removeClass("hidden"), this.initHours())
            },
            _onBackgroundClick: function(e) {
                e.stopPropagation(), this.hide()
            },
            _onElementClick: function(e) {
                e.stopPropagation()
            },
            _onCloseClick: function() {
                this.hide()
            },
            _onOKClick: function() {
                switch (this.currentView) {
                    case 0:
                        !0 === this.params.time ? this.initHours() : (this.setElementValue(), this.hide());
                        break;
                    case 1:
                        this.initMinutes();
                        break;
                    case 2:
                        this.setElementValue(), this.hide()
                }
            },
            _onCancelClick: function() {
                if (this.params.time) switch (this.currentView) {
                    case 0:
                        this.hide();
                        break;
                    case 1:
                        this.params.date ? this.initDate() : this.hide();
                        break;
                    case 2:
                        this.initHours()
                } else this.hide()
            },
            _onMonthBeforeClick: function() {
                this.currentDate.subtract(1, "months"), this.initDate(this.currentDate)
            },
            _onMonthAfterClick: function() {
                this.currentDate.add(1, "months"), this.initDate(this.currentDate)
            },
            _onYearBeforeClick: function() {
                this.currentDate.subtract(1, "years"), this.initDate(this.currentDate)
            },
            _onYearAfterClick: function() {
                this.currentDate.add(1, "years"), this.initDate(this.currentDate)
            },
            _onSelectDate: function(e) {
                this.$dtpElement.find("a.dtp-select-day").removeClass("selected"), b(e.currentTarget).addClass("selected"), this.selectDate(b(e.currentTarget).parent().data("date"))
            },
            _onSelectHour: function(e) {
                this.$dtpElement.find("a.dtp-select-hour").removeClass("selected"), b(e.currentTarget).addClass("selected");
                var t = parseInt(b(e.currentTarget).data("hour"));
                this.isPM() && (t += 12), this.currentDate.hour(t), this.showTime(this.currentDate), this.animateHands()
            },
            _onSelectMinute: function(e) {
                this.$dtpElement.find("a.dtp-select-minute").removeClass("selected"), b(e.currentTarget).addClass("selected"), this.currentDate.minute(parseInt(b(e.currentTarget).data("minute"))), this.showTime(this.currentDate), this.animateHands()
            },
            _onSelectAM: function(e) {
                b(".dtp-actual-meridien").find("a").removeClass("selected"), b(e.currentTarget).addClass("selected"), 12 <= this.currentDate.hour() && this.currentDate.subtract(12, "hours") && this.showTime(this.currentDate), this.toggleTime(1 === this.currentView)
            },
            _onSelectPM: function(e) {
                b(".dtp-actual-meridien").find("a").removeClass("selected"), b(e.currentTarget).addClass("selected"), this.currentDate.hour() < 12 && this.currentDate.add(12, "hours") && this.showTime(this.currentDate), this.toggleTime(1 === this.currentView)
            },
            convertHours: function(e) {
                var t = e;
                return e < 12 && this.isPM() && (t += 12), t
            },
            setDate: function(e) {
                this.params.currentDate = e, this.initDates()
            },
            setMinDate: function(e) {
                this.params.minDate = e, this.initDates()
            },
            setMaxDate: function(e) {
                this.params.maxDate = e, this.initDates()
            },
            destroy: function() {
                this._detachEvents(), this.$dtpElement.remove()
            },
            show: function() {
                var e = this;
                setTimeout(function() {
                    e.$dtpElement.removeClass("hidden"), e._centerBox()
                }, 300)
            },
            hide: function() {
                this.$dtpElement.addClass("hidden")
            },
            resetDate: function() {},
            _centerBox: function() {
                var e = (this.$dtpElement.height() - this.$dtpElement.find(".dtp-content").height()) / 2;
                this.$dtpElement.find(".dtp-content").css("marginLeft", -this.$dtpElement.find(".dtp-content").width() / 2 + "px"), this.$dtpElement.find(".dtp-content").css("top", e + "px")
            }
        }
    }(jQuery, moment), function(n) {
        "function" == typeof define && define.amd ? define(["jquery"], n) : "object" == typeof module && module.exports ? module.exports = function(e, t) {
            return void 0 === t && (t = "undefined" != typeof window ? require("jquery") : require("jquery")(e)), n(t), t
        } : n(jQuery)
    }, $("html").hasClass("smoothscroll")) {
    function ssc_init() {
        if (document.body) {
            var e = document.body,
                t = document.documentElement,
                n = window.innerHeight,
                i = e.scrollHeight;
            ssc_root = 0 <= document.compatMode.indexOf("CSS") ? t : e, ssc_activeElement = e, ssc_initdone = !0, top != self ? ssc_frame = !0 : n < i && (e.offsetHeight <= n || t.offsetHeight <= n) && (ssc_root.style.height = "auto", ssc_root.offsetHeight <= n) && ((n = document.createElement("div")).style.clear = "both", e.appendChild(n)), ssc_fixedback || (e.style.backgroundAttachment = "scroll", t.style.backgroundAttachment = "scroll"), ssc_keyboardsupport && ssc_addEvent("keydown", ssc_keydown)
        }
    }

    function ssc_scrollArray(l, c, d, u) {
        if (u = u || 1e3, ssc_directionCheck(c, d), ssc_que.push({
                x: c,
                y: d,
                lastX: c < 0 ? .99 : -.99,
                lastY: d < 0 ? .99 : -.99,
                start: +new Date
            }), !ssc_pending) {
            var h = function() {
                for (var e = +new Date, t = 0, n = 0, i = 0; i < ssc_que.length; i++) {
                    var s = ssc_que[i],
                        r = e - s.start,
                        o = ssc_animtime <= r,
                        a = o ? 1 : r / ssc_animtime;
                    ssc_pulseAlgorithm && (a = ssc_pulse(a)), t += r = s.x * a - s.lastX >> 0, n += a = s.y * a - s.lastY >> 0, s.lastX += r, s.lastY += a, o && (ssc_que.splice(i, 1), i--)
                }
                c && (e = l.scrollLeft, l.scrollLeft += t, t && l.scrollLeft === e && (c = 0)), d && (t = l.scrollTop, l.scrollTop += n, n && l.scrollTop === t && (d = 0)), c || d || (ssc_que = []), ssc_que.length ? setTimeout(h, u / ssc_framerate + 1) : ssc_pending = !1
            };
            setTimeout(h, 0), ssc_pending = !0
        }
    }

    function ssc_wheel(e) {
        ssc_initdone || ssc_init();
        var t = ssc_overflowingAncestor(n = e.target);
        if (!t || e.defaultPrevented || ssc_isNodeName(ssc_activeElement, "embed") || ssc_isNodeName(n, "embed") && /\.pdf/i.test(n.src)) return !0;
        var n = e.wheelDeltaX || 0,
            i = e.wheelDeltaY || 0;
        n || i || (i = e.wheelDelta || 0), 1.2 < Math.abs(n) && (n *= ssc_stepsize / 120), 1.2 < Math.abs(i) && (i *= ssc_stepsize / 120), ssc_scrollArray(t, -n, -i), e.preventDefault()
    }

    function ssc_keydown(e) {
        var t, n = e.target,
            i = e.ctrlKey || e.altKey || e.metaKey;
        if (/input|textarea|embed/i.test(n.nodeName) || n.isContentEditable || e.defaultPrevented || i || ssc_isNodeName(n, "button") && e.keyCode === ssc_key.spacebar) return !0;
        t = n = 0;
        var s = (i = ssc_overflowingAncestor(ssc_activeElement)).clientHeight;
        switch (i == document.body && (s = window.innerHeight), e.keyCode) {
            case ssc_key.up:
                t = -ssc_arrowscroll;
                break;
            case ssc_key.down:
                t = ssc_arrowscroll;
                break;
            case ssc_key.spacebar:
                t = -(t = e.shiftKey ? 1 : -1) * s * .9;
                break;
            case ssc_key.pageup:
                t = .9 * -s;
                break;
            case ssc_key.pagedown:
                t = .9 * s;
                break;
            case ssc_key.home:
                t = -i.scrollTop;
                break;
            case ssc_key.end:
                t = 0 < (s = i.scrollHeight - i.scrollTop - s) ? s + 10 : 0;
                break;
            case ssc_key.left:
                n = -ssc_arrowscroll;
                break;
            case ssc_key.right:
                n = ssc_arrowscroll;
                break;
            default:
                return !0
        }
        ssc_scrollArray(i, n, t), e.preventDefault()
    }

    function ssc_mousedown(e) {
        ssc_activeElement = e.target
    }

    function ssc_setCache(e, t) {
        for (var n = e.length; n--;) ssc_cache[ssc_uniqueID(e[n])] = t;
        return t
    }

    function ssc_overflowingAncestor(e) {
        var t = [],
            n = ssc_root.scrollHeight;
        do {
            var i = ssc_cache[ssc_uniqueID(e)];
            if (i) return ssc_setCache(t, i);
            if (t.push(e), n === e.scrollHeight) {
                if (!ssc_frame || ssc_root.clientHeight + 10 < n) return ssc_setCache(t, document.body)
            } else if (e.clientHeight + 10 < e.scrollHeight && (overflow = getComputedStyle(e, "").getPropertyValue("overflow"), "scroll" === overflow || "auto" === overflow)) return ssc_setCache(t, e)
        } while (e = e.parentNode)
    }

    function ssc_addEvent(e, t, n) {
        window.addEventListener(e, t, n || !1)
    }

    function ssc_removeEvent(e, t, n) {
        window.removeEventListener(e, t, n || !1)
    }

    function ssc_isNodeName(e, t) {
        return e.nodeName.toLowerCase() === t.toLowerCase()
    }

    function ssc_directionCheck(e, t) {
        e = 0 < e ? 1 : -1, t = 0 < t ? 1 : -1, ssc_direction.x === e && ssc_direction.y === t || (ssc_direction.x = e, ssc_direction.y = t, ssc_que = [])
    }

    function ssc_pulse_(e) {
        var t;
        return ((e *= ssc_pulseScale) < 1 ? e - (1 - Math.exp(-e)) : (--e, (t = Math.exp(-1)) + (1 - Math.exp(-e)) * (1 - t))) * ssc_pulseNormalize
    }

    function ssc_pulse(e) {
        return 1 <= e ? 1 : e <= 0 ? 0 : (1 == ssc_pulseNormalize && (ssc_pulseNormalize /= ssc_pulse_(1)), ssc_pulse_(e))
    }
    if (-1 === navigator.platform.toUpperCase().indexOf("MAC") && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini|BlackBerry)/)) {
        var ssc_activeElement, ssc_framerate = 150,
            ssc_animtime = 700,
            ssc_stepsize = 100,
            ssc_pulseAlgorithm = !0,
            ssc_pulseScale = 8,
            ssc_pulseNormalize = 1,
            ssc_keyboardsupport = !0,
            ssc_arrowscroll = 50,
            ssc_frame = !1,
            ssc_direction = {
                x: 0,
                y: 0
            },
            ssc_initdone = !1,
            ssc_fixedback = !0,
            ssc_root = document.documentElement,
            ssc_key = {
                left: 37,
                up: 38,
                right: 39,
                down: 40,
                spacebar: 32,
                pageup: 33,
                pagedown: 34,
                end: 35,
                home: 36
            },
            ssc_que = [],
            ssc_pending = !1,
            ssc_cache = {};
        setInterval(function() {
            ssc_cache = {}
        }, 1e4);
        var ssc_uniqueID = function() {
                var t = 0;
                return function(e) {
                    return e.ssc_uniqueID || (e.ssc_uniqueID = t++)
                }
            }(),
            ischrome = /chrome/.test(navigator.userAgent.toLowerCase());
        ischrome && (ssc_addEvent("mousedown", ssc_mousedown), ssc_addEvent("mousewheel", ssc_wheel), ssc_addEvent("load", ssc_init))
    }
}! function(n) {
    var e = n("[data-waypoint-to]");
    e.length && n(document).ready(function() {
        e.each(function() {
            var t = n(this);
            t.on("click", function(e) {
                e.preventDefault(), n("body, html").stop().animate({
                    scrollTop: n(t.attr("data-waypoint-to")).offset().top
                }, 800)
            })
        })
    })
}(jQuery);