<?php 
require 'config/config.php';

$page = array(

	'name' => 'prestashop-development-services-solutions-company',
	'title' => 'Prestashop Development Services',
	'keywords' => 'dsdsdsd',
	'description' => 'dsdsd',
	'allowIndex' => false,
);

require 'header/head.php';

require 'header/page-heading.php'; 

require 'templates/services/prestashop/content.php';

require 'templates/common/our-work.php';

$testimonialClass = "section section-md bg-default text-center";
require 'templates/common/testimonials.php';

//<!-- call-to-action section-->
require 'templates/common/call-to-action.php';

require 'footer/footer.php';

?>