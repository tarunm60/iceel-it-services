<!--RD Navbar Panel-->
<div class="rd-navbar-panel">
  <!--RD Navbar Toggle--> <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button><!--RD Navbar Brand-->
  <div class="rd-navbar-brand">
     <!--Brand--><a class="brand" href="index.html"><img class="brand-logo-dark" src="images/logo-default.png" srcset="images/logo-default@2x.png 2x" alt="iceel"><img class="brand-logo-light" src="images/logo-inverse.png" srcset="images/logo-inverse@2x.png 2x" alt="iceel"></a>
  </div>
</div>
<div class="rd-navbar-main-address rd-navbar-collapse">
  <div class="rd-navbar-main-address-item rd-navbar-main-address-item-last">
     <a class="header-phone" href="tel:#">
        <span class="unit align-items-center justify-content-lg-end">
           <span class="unit-left">
              <span class="svg-icon-wrapper svg-icon-wrapper-rounded svg-icon-wrapper-gray-100">
                 <svg class="icon icon-secondary" role="img">
                    <use xlink:href="images/svg/sprite.svg#phone-call"></use>
                 </svg>
              </span>
           </span>
           <span class="unit-body"><span class="small small-md">Phone Number</span><span class="heading-5 font-weight-sbold">+ 777 444 0000</span></span>
        </span>
     </a>
  </div>
  
  <div class="rd-navbar-main-address-item rd-navbar-main-address-item-average">
     <div class="rd-navbar-brand">
        <!--Brand--><a class="brand" href="index.html"><img class="brand-logo-dark" src="images/logo-default.png" srcset="images/logo-default@2x.png 2x" alt="iceel"><img class="brand-logo-light" src="images/logo-inverse.png" srcset="images/logo-inverse@2x.png 2x" alt="iceel"></a>
     </div>
  </div>

  <div class="rd-navbar-main-address-item rd-navbar-main-address-item-first">
     <a class="header-mail" href="mailto:#">
        <span class="unit align-items-center flex-lg-row-reverse justify-content-lg-end">
           <span class="unit-right">
              <span class="svg-icon-wrapper svg-icon-wrapper-rounded svg-icon-wrapper-gray-100">
                 <svg class="icon icon-secondary" role="img">
                    <use xlink:href="images/svg/sprite.svg#message"></use>
                 </svg>
              </span>
           </span>
           <span class="unit-body"><span class="small small-md">Email address</span><span class="heading-5 font-weight-sbold">info@iceel.com</span></span>
        </span>
     </a>
  </div>
  
</div>