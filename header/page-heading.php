<div class="section swiper-slider-wrapper">
	<div class="bg-decoration">
	   <div class="bg-decoration-item"></div>
	   <div class="bg-decoration-item"></div>
	   <div class="bg-decoration-item"></div>
	   <div class="bg-decoration-item"></div>
	   <div class="bg-decoration-item"></div>
  </div>
  <section class="page-title" style="background-image:url('<?php echo $siteUrl;?>/images/inner-pages-heading-bg.png')">
		<div class="auto-container">
			<!-- Section Icons -->
			<div class="section-icons">
				<!-- Icon One -->
				<div class="icon-one" style="background-image:url(<?php echo $siteUrl;?>/images/icons/icon-6.png)"></div>
				<!-- Icon Two -->
				<div class="icon-two" style="background-image:url(<?php echo $siteUrl;?>/images/icons/icon-7.png)"></div>
				<!-- Icon Three -->
				<div class="icon-three" style="background-image:url(<?php echo $siteUrl;?>/images/icons/icon-8.png)"></div>
				<!-- Icon Four -->
				<div class="icon-four" style="background-image:url(<?php echo $siteUrl;?>/images/icons/iceel-icon-cube.png)"></div>
				<!-- Icon Five -->
				<div class="icon-five" style="background-image:url(<?php echo $siteUrl;?>/images/icons/heading-icon-4.png)"></div>
				<!-- Icon Six -->
				<div class="icon-six" style="background-image:url(<?php echo $siteUrl;?>/images/icons/heading-icon-2.png)"></div>
			</div>
	    <div class="container inner-container clearfix">
	    	<div class="row">
	    		<div class="col-md-7 pull-left">
						<h1><?php echo $page['title']; ?></h1>
					</div>
	        <div class="col-md-5 pull-right">
						<ul class="bread-crumb clearfix">
							<li><a href="index.html">Home</a></li>
							<li><?php echo $page['title']; ?></li>
						</ul>
					</div>
	    	</div>
	    </div>
		</div>
	</section>
</div>