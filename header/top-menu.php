<div class="rd-navbar-main-element">
  <div class="rd-navbar-nav-wrap">
     <ul class="rd-navbar-nav">
        <li class="rd-nav-item active">
           <a class="rd-nav-link" href="<?php echo $siteUrl; ?>">Home</a>
        </li>
        <li class="rd-nav-item"><a class="rd-nav-link" href="about-iceel.php">About Iceel</a></li>
        <li class="rd-nav-item">
           <a class="rd-nav-link" href="it-services-and-support.php">IT Services</a>
           <ul class="rd-menu rd-navbar-megamenu">
              <li class="rd-megamenu-item">
                 <ul class="rd-megamenu-list">
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="typography.html">Digital Marketing Services</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="buttons.html">SEO Services</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="forms.html">SMM - SMO Services</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tabs-and-accordions.html">SEM - PPC Services</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tabs-and-accordions.html">App Store Optimization</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tabs-and-accordions.html">Local Advertising Services</a></li>
                 </ul>
              </li>
              <li class="rd-megamenu-item">
                 <ul class="rd-megamenu-list">
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="typography.html">Typography</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="buttons.html">Buttons</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="forms.html">Forms</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tabs-and-accordions.html">Tabs and accordions</a></li>
                 </ul>
              </li>
              <li class="rd-megamenu-item">
                 <ul class="rd-megamenu-list">
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="grid-system.html">Grid system</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="privacy-policy.html">Privacy policy</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="progress-bars.html">Progress bars</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tables.html">Tables</a></li>
                 </ul>
              </li>
              <li class="rd-megamenu-item">
                 <ul class="rd-megamenu-list">
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="under-construction.html">Under Construction</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="404.html">404</a></li>
                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="503.html">503</a></li>
                 </ul>
              </li>
           </ul>
        </li>
        <li class="rd-nav-item"><a class="rd-nav-link" href="pricing.html">Testimonials</a></li>
        <li class="rd-nav-item"><a class="rd-nav-link" href="our-portfolio.php">Our Portfolio</a></li>
        <li class="rd-nav-item">
           <a class="rd-nav-link" href="#">Blog</a>
        </li>
        <li class="rd-nav-item"><a class="rd-nav-link" href="contacts.php">Contacts</a></li>
     </ul>
  </div>
</div