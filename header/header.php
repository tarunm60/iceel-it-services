<body>
  <div class="preloader">
    <div class="preloader-body">
       <div class="ct-dual-ring"></div>
    </div>
  </div>
  <div class="page">
    <!-- Section Header Classic-->
    <header class="section page-header">
       <!--RD Navbar-->
       <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="211px" data-xl-stick-up-offset="211px" data-xxl-stick-up-offset="211px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
             <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
             <div class="rd-navbar-info-toggle rd-navbar-fixed-element-2" data-rd-navbar-toggle=".rd-navbar-aside-outer"><span></span></div>
             <div class="rd-navbar-main-outer">
                <div class="rd-navbar-main">
                   <?php require 'top-bar.php'; ?>
                   <?php require 'top-menu.php'; ?>
                </div>
             </div>
          </nav>
       </div>
    </header>