<!DOCTYPE html>
<html lang="en" class="wow-animation">
 <head>
  <title><?php echo $page['title']; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keyword" content="<?php echo $page['keywords']; ?>"/>
  <meta name="description" content="<?php echo $page['description']; ?>"/>
  <meta name="robots" content="noodp"/>
  <?php if ($page['allowIndex']) {
    echo '<meta name="robots" content="INDEX,FOLLOW">';
  } else {
    echo '<meta name="robots" content="NOINDEX,NOFOLLOW">';
  } ?>
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:500,600,700,800%7CRoboto:400,500">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/fontawesome.css">
  <link rel="stylesheet" href="css/material.css">
  <link rel="stylesheet" href="css/theme.css">
  <link rel="stylesheet" href="css/styles.css">
  <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
 </head>

 <?php require 'header.php'; ?>